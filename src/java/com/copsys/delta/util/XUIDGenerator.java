
package com.copsys.delta.util;

/**
 *
 * @author COping Systems Inc.
 */
import java.util.Random;
public class XUIDGenerator {
    private static final int NUM_CHARS = 25;
    private static String chars = "abcdefghijklmonpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static Random r = new Random();



public String getUniqueID() {
    char[] buf = new char[NUM_CHARS];
    for (int i = 0; i < buf.length; i++) {
        buf[i] = chars.charAt(r.nextInt(chars.length()));
    }
    return new String(buf);
    }

public String getUniqueID(int size) {
    char[] buf = new char[size];
    for (int i = 0; i < buf.length; i++) {
        buf[i] = chars.charAt(r.nextInt(chars.length()));
    }
    return new String(buf);
    }

}
