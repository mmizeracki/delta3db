/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.util;

/**
 *
 * @author Coping Systems Inc.
 */

import java.util.Timer;
import java.util.TimerTask;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HealthCheckTimerTask extends TimerTask {

  /**
  * Construct and use a TimerTask and Timer.
  */
  private final static long fONCE_PER_DAY = 1000*60*60*24;
  private final static long fONCE_PER_HOUR = 1000*60*60;
  private final static long fONCE_PER_MINUTE = 1000*60;
  private static final Logger logger = Logger.getLogger(HealthCheckTimerTask.class.getName());
  Timer timer = null;

  public void init() {
    timer = new Timer();
    //Calendar calendar = new GregorianCalendar();
    //Date trialTime = new Date();
    //calendar.setTime(trialTime);
    timer.scheduleAtFixedRate(this, 1000L, fONCE_PER_MINUTE);
  }


  /**
  * Implements TimerTask's abstract run method.
  */
  public void run(){

    int kb = 1024;
    Runtime runtime = Runtime.getRuntime();
    logger.log(Level.INFO,"Memory used: " + (runtime.totalMemory() - runtime.freeMemory()) / kb
    + ", Free: " + runtime.freeMemory() / kb
    + ", Total: " + runtime.totalMemory() / kb
    + ", Max: " + runtime.maxMemory() / kb
    + ", CPUs: " + runtime.availableProcessors());
    runtime = null;
  }
}


