/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.servlet;

import com.copsys.delta.server.processor.ProcessAtomic;
import com.copsys.delta.security.Auth;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Boston Advanced Analytics
 */

@WebServlet(urlPatterns="/ProcessAtomicServlet", asyncSupported=true)

public class ProcessAtomicServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String payload = req.getParameter("payload");
            Integer userId = Auth.getUserIdFromSession(req.getSession());
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            AsyncContext async = req.startAsync(req, resp);
            async.setTimeout(1 * 60 * 1000);            
            async.addListener(new AsyncListener() {
                public void onComplete(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessAtomicServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onComplete");                  
                }

                @Override
                public void onTimeout(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessAtomicServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onTimeout");
                }

                @Override
                public void onError(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessAtomicServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onError");
                }

                @Override
                public void onStartAsync(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessAtomicServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onStartAsync");
                }
            });
            ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
            executor.execute(new ProcessAtomic(async, decodedContent, userId));    
            response(resp, "Processing atomic fields.");
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessAtomicServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void response(HttpServletResponse resp, String msg) throws IOException {
            PrintWriter out = resp.getWriter();
            out.println(msg);
    }    
}
