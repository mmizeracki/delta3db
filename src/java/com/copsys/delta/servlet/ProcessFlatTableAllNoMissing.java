/*
 * Boston Advanced Analytics, Inc.
 */

package com.copsys.delta.servlet;

import com.copsys.delta.security.Auth;
import com.copsys.delta.server.processor.ProcessFlatTableNoMissing;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Boston Advanced Analytics
 */

@WebServlet(urlPatterns="/ProcessFlatTableAllNoMissing", asyncSupported=true)

public class ProcessFlatTableAllNoMissing extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String payload = req.getParameter("payload");
            Integer userId = Auth.getUserIdFromSession(req.getSession());
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            AsyncContext async = req.startAsync(req, resp);
            async.setTimeout(1 * 60 * 1000);            
            async.addListener(new AsyncListener() {
                public void onComplete(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessFlatTableAllNoMissing.class.getName()).log(Level.SEVERE, "ProcessFlatTableAllNoMissing onComplete");                  
                }

                @Override
                public void onTimeout(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessFlatTableAllNoMissing.class.getName()).log(Level.SEVERE, "ProcessFlatTableAllNoMissing onTimeout");
                }

                @Override
                public void onError(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessFlatTableAllNoMissing.class.getName()).log(Level.SEVERE, "ProcessFlatTableAllNoMissing onError");
                }

                @Override
                public void onStartAsync(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessFlatTableAllNoMissing.class.getName()).log(Level.SEVERE, "ProcessFlatTableAllNoMissing onStartAsync");
                }
            });
            ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
            executor.execute(new ProcessFlatTableNoMissing(async, decodedContent, userId));    
            response(resp, "Processing atomic fields.");
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessFlatTableAllNoMissing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void response(HttpServletResponse resp, String msg) throws IOException {
            PrintWriter out = resp.getWriter();
            out.println(msg);
    }    
}
