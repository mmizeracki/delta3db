/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.study;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.study.Filter;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Coping Systems Inc.
 */
public class FilterJpaController implements PersistenceHelper, Serializable {

    @SuppressWarnings("empty-statement")
    public FilterJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 
    
    public void create(Filter filter) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(filter);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }   

    public Filter edit(Filter filter) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            filter = em.merge(filter);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = filter.getIdFilter();
                if (findFilter(id) == null) {
                    throw new NonexistentEntityException("The study with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return filter;
    }    

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Filter filter;
            try {
                filter = em.getReference(Filter.class, id);
                filter.getIdFilter();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The study with id " + id + " no longer exists.", enfe);
            }
            em.remove(filter);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }    

    public List<Object> findOrgFilterEntities(Integer orgId) {
        return findOrgFilterEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgFilterEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgFilterEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgFilterEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Filter as o where Organization_idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findModelFilterEntities(Integer modelId) {
        return findModelFilterEntities(true, -1, -1, modelId);
    }

    public List<Object> findModelFilterEntities(int maxResults, int firstResult, Integer modelId) {
        return findModelFilterEntities(false, maxResults, firstResult, modelId);
    }

    private List<Object> findModelFilterEntities(boolean all, int maxResults, int firstResult, Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Filter as o where Model_idModel = " + modelId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Filter> findFilterEntities() {
        return findFilterEntities(true, -1, -1);
    }

    public List<Filter> findFilterEntities(int maxResults, int firstResult) {
        return findFilterEntities(false, maxResults, firstResult);
    }

    private List<Filter> findFilterEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Filter as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Filter findFilter(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Filter.class, id);
        } finally {
            em.close();
        }
    }

    public int getFilterCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Filter as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    private int getModelFilterCount(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Filter as o where Model_idModel = " + modelId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }  
    
    public int getOrgFilterCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Filter as o where Organization_idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }     
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.study.Filter)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Filter obj = edit(((com.copsys.delta.entity.study.Filter)object));
        AuditLogger.log(currentUserId, obj.getIdFilter(), "create", obj);
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Filter obj = edit(((com.copsys.delta.entity.study.Filter)object));
        AuditLogger.log(currentUserId, obj.getIdFilter(), "update", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        //((com.copsys.delta.entity.study.Filter)object).setModelidModel(currentModelId);  
        ((com.copsys.delta.entity.study.Filter)object).setOrganizationidOrganization(currentOrgId);  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.study.Filter)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Filter obj = edit(((com.copsys.delta.entity.study.Filter)object));
        AuditLogger.log(currentUserId, obj.getIdFilter(), "create", obj);
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        //((com.copsys.delta.entity.study.Filter)object).setModelidModel(currentModelId);  
        ((com.copsys.delta.entity.study.Filter)object).setOrganizationidOrganization(currentOrgId);          
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Filter)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Filter obj = edit(((com.copsys.delta.entity.study.Filter)object));
        AuditLogger.log(currentUserId, obj.getIdFilter(), "update", obj);
        return obj;
    }

    @Override
    public String getObjectType() {
        return "filters";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findModelFilterEntities(maxResults, firstResult, currentOrgId);  // this an exceptional behavior!
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getFilterCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getModelFilterCount(currentOrgId);  // unusual!
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.study.Filter)subject).getIdFilter(), "delete", subject);
        destroy(((com.copsys.delta.entity.study.Filter)subject).getIdFilter());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.study.Filter)subject).getIdFilter(), "delete", subject);
        destroy(((com.copsys.delta.entity.study.Filter)subject).getIdFilter());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
}
