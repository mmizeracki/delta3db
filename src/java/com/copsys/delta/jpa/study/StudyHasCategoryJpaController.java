/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.study;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.study.StudyHasCategory;
import com.copsys.delta.entity.study.StudyHasCategoryPK;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class StudyHasCategoryJpaController implements Serializable {

    @SuppressWarnings("empty-statement")
    public StudyHasCategoryJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(StudyHasCategory studyHasCategory) throws PreexistingEntityException, Exception {
        if (studyHasCategory.getStudyHasCategoryPK() == null) {
            studyHasCategory.setStudyHasCategoryPK(new StudyHasCategoryPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(studyHasCategory);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStudyHasCategory(studyHasCategory.getStudyHasCategoryPK()) != null) {
                throw new PreexistingEntityException("StudyHasCategory " + studyHasCategory + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public StudyHasCategory edit(StudyHasCategory studyHasCategory) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            studyHasCategory = em.merge(studyHasCategory);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                StudyHasCategoryPK id = studyHasCategory.getStudyHasCategoryPK();
                if (findStudyHasCategory(id) == null) {
                    throw new NonexistentEntityException("The studyHasCategory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return studyHasCategory;
    }

    public void destroy(StudyHasCategoryPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StudyHasCategory studyHasCategory;
            try {
                studyHasCategory = em.getReference(StudyHasCategory.class, id);
                studyHasCategory.getStudyHasCategoryPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The studyHasCategory with id " + id + " no longer exists.", enfe);
            }
            em.remove(studyHasCategory);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StudyHasCategory> findStudyHasCategoryEntities() {
        return findStudyHasCategoryEntities(true, -1, -1);
    }

    public List<StudyHasCategory> findStudyHasCategoryEntities(int maxResults, int firstResult) {
        return findStudyHasCategoryEntities(false, maxResults, firstResult);
    }

    private List<StudyHasCategory> findStudyHasCategoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from StudyHasCategory as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StudyHasCategory findStudyHasCategory(StudyHasCategoryPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StudyHasCategory.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudyHasCategoryCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from StudyHasCategory as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

   public List<StudyHasCategory> findStudyHasCategoryByStudyIdEntities(int studyidStudy) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("StudyHasCategory.findByStudyidStudy");
            q.setParameter("studyidStudy", studyidStudy);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
}
