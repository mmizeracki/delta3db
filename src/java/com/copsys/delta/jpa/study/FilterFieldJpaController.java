/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.study;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.study.FilterField;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class FilterFieldJpaController implements Serializable {

    @SuppressWarnings("empty-statement")
    public FilterFieldJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 
    public void create(FilterField filterField) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(filterField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public FilterField edit(FilterField filterField) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            filterField = em.merge(filterField);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = filterField.getIdFilterField();
                if (findFilterField(id) == null) {
                    throw new NonexistentEntityException("The filterField with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return filterField;
    }

    public FilterField destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        FilterField filterField;        
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            try {
                filterField = em.getReference(FilterField.class, id);
                filterField.getIdFilterField();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The filterField with id " + id + " no longer exists.", enfe);
            }
            em.remove(filterField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return filterField;
    }

    public List<FilterField> findFilterFieldEntities() {
        return findFilterFieldEntities(true, -1, -1);
    }

    public List<FilterField> findFilterFieldEntities(int maxResults, int firstResult) {
        return findFilterFieldEntities(false, maxResults, firstResult);
    }

    private List<FilterField> findFilterFieldEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from FilterField as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<FilterField> findByModelColumnidModelColumn(Integer id)
    {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("FilterField.findByModelColumnidModelColumn");
            q.setParameter("modelColumnidModelColumn", id);
            return q.getResultList();
        } finally {
            em.close();
        }        
    }
    
    public FilterField findFilterField(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FilterField.class, id);
        } finally {
            em.close();
        }
    }

    public List<FilterField> findFilterFieldByFilterId(Integer filterId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from FilterField as o where filteridFilter = " + filterId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getFilterFieldCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from FilterField as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
