/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.study;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.study.Study;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class StudyJpaController implements PersistenceHelper, Serializable {

    @SuppressWarnings("empty-statement")
    public StudyJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Study study) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Study edit(Study study) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            study = em.merge(study);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = study.getIdStudy();
                if (findStudy(id) == null) {
                    throw new NonexistentEntityException("The study with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return study;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Study study;
            try {
                study = em.getReference(Study.class, id);
                study.getIdStudy();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The study with id " + id + " no longer exists.", enfe);
            }
            em.remove(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findOrgStudyEntities(Integer orgId) {
        return findOrgStudyEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgStudyEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgStudyEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgStudyEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Object> findStudyEntities() {
        return findStudyEntities(true, -1, -1);
    }

    public List<Object> findStudyEntities(int maxResults, int firstResult) {
        return findStudyEntities(false, maxResults, firstResult);
    }

    private List<Object> findStudyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Object findStudy(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Study.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudyCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getStudyCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    public List<Study> findStudyEntitiesByFilterId(Integer filterId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where Filter_idFilter = " + filterId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
    public List<Study> findStudyEntitiesByModelId(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where idModel = " + modelId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
   @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.study.Study)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Study obj = edit(((com.copsys.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((com.copsys.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Study obj = edit(((com.copsys.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.copsys.delta.entity.study.Study)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.study.Study)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Study obj = edit(((com.copsys.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;          
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.copsys.delta.entity.study.Study)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((com.copsys.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.study.Study obj = edit(((com.copsys.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;  
    }

    @Override
    public String getObjectType() {
        return "studies";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findStudyEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findStudyEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgStudyEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getStudyCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getStudyCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.study.Study)subject).getIdStudy(), "delete", subject);
        destroy(((com.copsys.delta.entity.study.Study)subject).getIdStudy());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.study.Study)subject).getIdStudy(), "delete", subject);
        destroy(((com.copsys.delta.entity.study.Study)subject).getIdStudy());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
