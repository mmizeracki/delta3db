/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.study;

import com.copsys.delta.db.PersistenceManager;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.copsys.delta.entity.study.FilterHasField;
import com.copsys.delta.entity.study.FilterHasFieldPK;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Coping Systems Inc.
 */
public class FilterHasFieldJpaController implements Serializable {

    @SuppressWarnings("empty-statement")
    public FilterHasFieldJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(FilterHasField filterHasField) throws PreexistingEntityException, Exception {
        if (filterHasField.getFilterHasFieldPK() == null) {
            filterHasField.setFilterHasFieldPK(new FilterHasFieldPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(filterHasField);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFilterHasField(filterHasField.getFilterHasFieldPK()) != null) {
                throw new PreexistingEntityException("FilterHasField " + filterHasField + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public FilterHasField edit(FilterHasField filterHasField) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FilterHasField persistentFilterHasField = em.find(FilterHasField.class, filterHasField.getFilterHasFieldPK());
            filterHasField = em.merge(filterHasField);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                FilterHasFieldPK id = filterHasField.getFilterHasFieldPK();
                if (findFilterHasField(id) == null) {
                    throw new NonexistentEntityException("The filterHasField with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return filterHasField;
    }

    public void destroy(FilterHasFieldPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FilterHasField filterHasField;
            try {
                filterHasField = em.getReference(FilterHasField.class, id);
                filterHasField.getFilterHasFieldPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The filterHasField with id " + id + " no longer exists.", enfe);
            }
            em.remove(filterHasField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<FilterHasField> findFilterHasFieldEntities() {
        return findFilterHasFieldEntities(true, -1, -1);
    }

    public List<FilterHasField> findFilterHasFieldEntities(int maxResults, int firstResult) {
        return findFilterHasFieldEntities(false, maxResults, firstResult);
    }

    private List<FilterHasField> findFilterHasFieldEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from FilterHasField as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FilterHasField findFilterHasField(FilterHasFieldPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FilterHasField.class, id);
        } finally {
            em.close();
        }
    }

    public int getFilterHasFieldCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from FilterHasField as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

   public List<FilterHasField> findFilterHasFieldByFilterIdEntities(int filteridFilter) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("FilterHasField.findByFilteridFilter");
            q.setParameter("filteridFilter", filteridFilter);
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
}
