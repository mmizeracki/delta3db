/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.Method;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class MethodJpaController implements PersistenceHelper, Serializable {

    
    private EntityManagerFactory emf = null;
        
    public MethodJpaController() {
        emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Method method) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(method);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Method edit(Method method) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            method = em.merge(method);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = method.getIdMethod();
                if (findMethod(id) == null) {
                    throw new NonexistentEntityException("The method with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return method;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Method method;
            try {
                method = em.getReference(Method.class, id);
                method.getIdMethod();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The method with id " + id + " no longer exists.", enfe);
            }
            em.remove(method);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findMethodEntities() {
        return findMethodEntities(true, -1, -1);
    }

    public List<Object> findMethodEntities(int maxResults, int firstResult) {
        return findMethodEntities(false, maxResults, firstResult);
    }

    private List<Object> findMethodEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Method as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Object findMethod(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Method.class, id);
        } finally {
            em.close();
        }
    }

    public List<Object> findOrgMethodEntities(Integer orgId) {
        return findOrgMethodEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgMethodEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgMethodEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgMethodEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Method as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getMethodCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Method as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getMethodCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Method as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    
  @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.Method)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.Method)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.Method obj = edit(((com.copsys.delta.entity.Method)object));
        AuditLogger.log(currentUserId, obj.getIdMethod(), "create", obj);
        return obj;   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((com.copsys.delta.entity.Method)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.Method obj = edit(((com.copsys.delta.entity.Method)object));
        AuditLogger.log(currentUserId, obj.getIdMethod(), "update", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.copsys.delta.entity.Method)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.copsys.delta.entity.Method)object).setCreatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setCreatedBy(currentUserId);        
        ((com.copsys.delta.entity.Method)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.Method obj = edit(((com.copsys.delta.entity.Method)object));
        AuditLogger.log(currentUserId, obj.getIdMethod(), "create", obj);
        return obj;          
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.copsys.delta.entity.Method)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((com.copsys.delta.entity.Method)object).setUpdatedTS(currentTimestamp);
        ((com.copsys.delta.entity.Method)object).setUpdatedBy(currentUserId);      
        com.copsys.delta.entity.Method obj = edit(((com.copsys.delta.entity.Method)object));
        AuditLogger.log(currentUserId, obj.getIdMethod(), "update", obj);
        return obj;  
    }

    @Override
    public String getObjectType() {
        return "methods";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findMethodEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findMethodEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgMethodEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getMethodCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getMethodCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.Method)subject).getIdMethod(), "delete", subject);
        destroy(((com.copsys.delta.entity.Method)subject).getIdMethod());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.Method)subject).getIdMethod(), "delete", subject);
        destroy(((com.copsys.delta.entity.Method)subject).getIdMethod());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
}
