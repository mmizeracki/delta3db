/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.Audit;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class AuditJpaController implements Serializable {

    public AuditJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Audit audit) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(audit);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAudit(audit.getIdAudit()) != null) {
                throw new PreexistingEntityException("Audit " + audit + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Audit audit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            audit = em.merge(audit);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = audit.getIdAudit();
                if (findAudit(id) == null) {
                    throw new NonexistentEntityException("The audit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Audit audit;
            try {
                audit = em.getReference(Audit.class, id);
                audit.getIdAudit();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The audit with id " + id + " no longer exists.", enfe);
            }
            em.remove(audit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Audit> findAuditEntities() {
        return findAuditEntities(true, -1, -1);
    }

    public List<Audit> findAuditEntities(int maxResults, int firstResult) {
        return findAuditEntities(false, maxResults, firstResult);
    }

    private List<Audit> findAuditEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Audit as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Audit findAudit(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Audit.class, id);
        } finally {
            em.close();
        }
    }

    public int getAuditCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Audit as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
