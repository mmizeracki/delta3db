/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.model;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.model.Model;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.security.exceptions.DeltaOperationNotAllowedException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class ModelJpaController implements PersistenceHelper, Serializable  {
    public ModelJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Model model) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(model);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModel(model.getIdModel()) != null) {
                throw new PreexistingEntityException("Model " + model + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Model edit(Model model) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            model = em.merge(model);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = model.getIdModel();
                if (findModel(id) == null) {
                    throw new NonexistentEntityException("The model with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return model;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Model model;
            try {
                model = em.getReference(Model.class, id);
                model.getIdModel();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The model with id " + id + " no longer exists.", enfe);
            }
            em.remove(model);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findModelEntities() {
        return findModelEntities(true, -1, -1);
    }

    public List<Object> findModelEntities(int maxResults, int firstResult) {
        return findModelEntities(false, maxResults, firstResult);
    }

    private List<Object> findModelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Model as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findOrgModelEntities(Integer orgId) {
        return findOrgModelEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgModelEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgModelEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgModelEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Model as o where Organization_idOrganization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Model findModel(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Model.class, id);
        } finally {
            em.close();
        }
    }

    public List<Model> findModelByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Model.findByName");
            q.setParameter("name", name);
            return q.getResultList();            
        } finally {
            em.close();
        }
    }
    
    public int getModelCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Model as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getModelCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Model as o where Organization_idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Model)object).setCreatedTS(currentTimestamp);
        ((Model)object).setCreatedBy(currentUserId);        
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);        
        Model obj = edit(((Model)object));
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "create", obj);       
        return obj;  
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);   
        Model obj = edit(((Model)object));        
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "update", obj);
        return obj;     
    }

    @Override
    public String getObjectType() {
        return "models";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findModelEntities(maxResults, firstResult);
    }

    @Override
    public List<Object> findObjectEntities() {
         return findModelEntities();
    }

    @Override
    public int getObjectCount() {
        return getModelCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Model)object).setOrganization(currentOrgId);
        ((Model)object).setCreatedTS(currentTimestamp);
        ((Model)object).setCreatedBy(currentUserId);        
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);        
        Model obj = edit(((Model)object));
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "create", obj);       
        return obj;  
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ((Model)object).setOrganization(currentOrgId);    
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);   
        Model obj = edit(((Model)object));        
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgModelEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
         return findOrgModelEntities(currentOrgId);
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates. 
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getModelCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.security.exceptions.DeltaOperationNotAllowedException, NonexistentEntityException {
        if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
            throw new DeltaOperationNotAllowedException("Operation not authorized.");             
        }
        AuditLogger.log(currentUserId, ((Model)subject).getIdModel(), "delete", subject);                   
        destroy(((Model)subject).getIdModel());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
