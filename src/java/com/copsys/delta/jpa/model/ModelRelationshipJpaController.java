/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.model;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.model.ModelRelationship;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class ModelRelationshipJpaController implements PersistenceHelper, Serializable {

    public ModelRelationshipJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModelRelationship modelRelationship) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(modelRelationship);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModelRelationship(modelRelationship.getIdModelRelationship()) != null) {
                throw new PreexistingEntityException("ModelRelationship " + modelRelationship + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public ModelRelationship edit(ModelRelationship modelRelationship) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            modelRelationship = em.merge(modelRelationship);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = modelRelationship.getIdModelRelationship();
                if (findModelRelationship(id) == null) {
                    throw new NonexistentEntityException("The modelRelationship with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return modelRelationship;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelRelationship modelRelationship;
            try {
                modelRelationship = em.getReference(ModelRelationship.class, id);
                modelRelationship.getIdModelRelationship();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelRelationship with id " + id + " no longer exists.", enfe);
            }
            em.remove(modelRelationship);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findModelRelationshipEntities() {
        return findModelRelationshipEntities(true, -1, -1);
    }

    public List<Object> findModelRelationshipEntities(int maxResults, int firstResult) {
        return findModelRelationshipEntities(false, maxResults, firstResult);
    }

    private List<Object> findModelRelationshipEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ModelRelationship as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelRelationship findModelRelationship(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModelRelationship.class, id);
        } finally {
            em.close();
        }
    }

    public List<Object> findModelRelationshipByModel(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelRelationship.findByModelidModel");
            q.setParameter("modelidModel", modelId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<ModelRelationship> findModelRelationshipByModelId(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelRelationship.findByModelidModel");
            q.setParameter("modelidModel", modelId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<ModelRelationship> findModelRelationshipByTable(Integer tableId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelRelationship.findByModelTableid");
            q.setParameter("modelTableid", tableId);
            return q.getResultList();
        } finally {
            em.close();
        }
    } 
    
    public int getModelRelationshipCount(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from ModelRelationship as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getModelRelationshipCountByModel(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelRelationship.countRelationshipsByModelidModel");
            q.setParameter("modelidModel", modelId);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((ModelRelationship)object).setCreatedTS(currentTimestamp);
        ((ModelRelationship)object).setCreatedBy(currentUserId);        
        ((ModelRelationship)object).setUpdatedTS(currentTimestamp);
        ((ModelRelationship)object).setUpdatedBy(currentUserId); 
        ModelRelationship obj = edit(((ModelRelationship)object));
        AuditLogger.log(currentUserId, obj.getIdModelRelationship(), "create", obj);              
        return obj;      
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
        ((ModelRelationship)object).setUpdatedTS(currentTimestamp);
        ((ModelRelationship)object).setUpdatedBy(currentUserId);   
        ModelRelationship obj = edit(((ModelRelationship)object));
        AuditLogger.log(currentUserId, obj.getIdModelRelationship(), "update", obj);        
        return obj; 
    }

    @Override
    public String getObjectType() {
        return "relationships";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
         return findModelRelationshipEntities(maxResults, firstResult);
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from ModelRelationship as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public void deleteObject(Object object, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((ModelRelationship)object).getIdModelRelationship(), "delete", object);        
        destroy(((ModelRelationship)object).getIdModelRelationship());   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        // ignore pagination params
        return findModelRelationshipByModel(id);
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int modelId) {
        return getModelRelationshipCountByModel(modelId);
    }
    
}
