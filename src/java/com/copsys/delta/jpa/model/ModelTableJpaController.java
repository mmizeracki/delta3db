/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.model;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.model.ModelTable;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class ModelTableJpaController implements PersistenceHelper, Serializable {

    public ModelTableJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModelTable modelTable) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(modelTable);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModelTable(modelTable.getIdModelTable()) != null) {
                throw new PreexistingEntityException("ModelTable " + modelTable + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public ModelTable edit(ModelTable modelTable) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            modelTable = em.merge(modelTable);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = modelTable.getIdModelTable();
                if (findModelTable(id) == null) {
                    throw new NonexistentEntityException("The modelTable with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return modelTable;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelTable modelTable;
            try {
                modelTable = em.getReference(ModelTable.class, id);
                modelTable.getIdModelTable();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelTable with id " + id + " no longer exists.", enfe);
            }
            em.remove(modelTable);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findModelTableEntities() {
        return findModelTableEntities(true, -1, -1);
    }

    public List<Object> findModelTableEntities(int maxResults, int firstResult) {
        return findModelTableEntities(false, maxResults, firstResult);
    }

    private List<Object> findModelTableEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ModelTable as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelTable findModelTable(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModelTable.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<ModelTable> findModelTableByModel(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelTable.findByModelidModel");
            q.setParameter("modelidModel", modelId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }    

    public List<ModelTable> findModelTableByPrimary(Integer modelId, boolean primary) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelTable.findByPrimary");
            q.setParameter("modelidModel", modelId);
            q.setParameter("primaryTable", primary);
            return q.getResultList();
        } finally {
            em.close();
        }
    } 
    
    public int getModelTableCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from ModelTable as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((ModelTable)object).setCreatedTS(currentTimestamp);
        ((ModelTable)object).setCreatedBy(currentUserId);        
        ((ModelTable)object).setUpdatedTS(currentTimestamp);
        ((ModelTable)object).setUpdatedBy(currentUserId);        
        if ( ((ModelTable)object).getActive() == null ) {
            ((ModelTable)object).setActive(true);
        }    
        if ( ((ModelTable)object).getPrimaryTable() == null ) {
            ((ModelTable)object).setPrimaryTable(false);
        }   
        ModelTable obj = edit(((ModelTable)object));
        AuditLogger.log(currentUserId, obj.getIdModelTable(), "create", obj);        
        return obj; 
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
        ((ModelTable)object).setUpdatedTS(currentTimestamp);
        ((ModelTable)object).setUpdatedBy(currentUserId);    
        ModelTable obj = edit(((ModelTable)object));
        AuditLogger.log(currentUserId, obj.getIdModelTable(), "update", obj);
        return obj;   
    }

    @Override
    public String getObjectType() {
        return "modelTables";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findModelTableEntities(maxResults, firstResult);    
    }

    @Override
    public List<Object> findObjectEntities() {
        return findModelTableEntities();   
    }

    @Override
    public int getObjectCount() {
        return getModelTableCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((ModelTable)subject).getIdModelTable(), "delete", subject);
        destroy(((ModelTable)subject).getIdModelTable());
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
