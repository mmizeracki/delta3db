/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.model;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.model.ModelColumn;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class ModelColumnJpaController implements PersistenceHelper, Serializable {

    public ModelColumnJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModelColumn modelColumn) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(modelColumn);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModelColumn(modelColumn.getIdModelColumn()) != null) {
                throw new PreexistingEntityException("ModelColumn " + modelColumn + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public ModelColumn edit(ModelColumn modelColumn) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            modelColumn = em.merge(modelColumn);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = modelColumn.getIdModelColumn();
                if (findModelColumn(id) == null) {
                    throw new NonexistentEntityException("The modelColumn with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return modelColumn;        
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelColumn modelColumn;
            try {
                modelColumn = em.getReference(ModelColumn.class, id);
                modelColumn.getIdModelColumn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelColumn with id " + id + " no longer exists.", enfe);
            }
            em.remove(modelColumn);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findModelColumnEntities() {
        return findModelColumnEntities(true, -1, -1);
    }

    public List<Object> findModelColumnEntities(int maxResults, int firstResult) {
        return findModelColumnEntities(false, maxResults, firstResult);
    }

    private List<Object> findModelColumnEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ModelColumn as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelColumn findModelColumn(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModelColumn.class, id);
        } finally {
            em.close();
        }
    }

    public List<ModelColumn> findModelColumnByTable(Integer tableId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelColumn.findByModelTableidModelTable");
            q.setParameter("modelTableidModelTable", tableId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }  

    public List<ModelColumn> findModelColumnByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelColumn.findByName");
            q.setParameter("name", name);
            return q.getResultList();
        } finally {
            em.close();
        }
    }  
 

    public List<ModelColumn> findModelColumnByModelAndName(Integer idModel, String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("ModelColumn.findByModelAndName");
            q.setParameter("idModel", idModel);
            q.setParameter("name", name);
            return q.getResultList();
        } finally {
            em.close();
        }
    }  
    
    public int getModelColumnCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from ModelColumn as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((ModelColumn)object).setCreatedTS(currentTimestamp);
        ((ModelColumn)object).setCreatedBy(currentUserId);        
        ((ModelColumn)object).setUpdatedTS(currentTimestamp);
        ((ModelColumn)object).setUpdatedBy(currentUserId);        
        if ( ((ModelColumn)object).getKeyField() == null ) {
            ((ModelColumn)object).setKeyField(false);
        }
        if ( ((ModelColumn)object).getActive() == null ) {
            ((ModelColumn)object).setActive(false);
        }
        if ( ((ModelColumn)object).getAtomic() == null ) {
            ((ModelColumn)object).setAtomic(false);
        }
        if ( ((ModelColumn)object).getInsertable() == null ) {
            ((ModelColumn)object).setInsertable(false);
        }   
        ModelColumn obj = edit(((ModelColumn)object));        
        AuditLogger.log(currentUserId, ((ModelColumn)obj).getIdModelColumn(), "create", obj);
        return obj; 
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
        ((ModelColumn)object).setUpdatedTS(currentTimestamp);
        ((ModelColumn)object).setUpdatedBy(currentUserId);    
        ModelColumn obj = edit(((ModelColumn)object));
        AuditLogger.log(currentUserId, ((ModelColumn)object).getIdModelColumn(), "update", obj);        
        return obj;     
    }

    @Override
    public String getObjectType() {
        return "modelColumns";        
   }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findModelColumnEntities(maxResults, firstResult);        
   }

    @Override
    public List<Object> findObjectEntities() {
        return findModelColumnEntities();          
   }

    @Override
    public int getObjectCount() {
        return getModelColumnCount();
    }

    @Override
    public void deleteObject(Object object, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((ModelColumn)object).getIdModelColumn(), "delete", object);
        destroy(((ModelColumn)object).getIdModelColumn());
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
