/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.Configuration;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class ConfigurationJpaController implements PersistenceHelper  {
    
    private EntityManagerFactory emf = null;
        
    public ConfigurationJpaController() {
        emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Configuration configuration) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(configuration);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConfiguration(configuration.getIdConfiguration()) != null) {
                throw new PreexistingEntityException("Configuration " + configuration + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Configuration edit(Configuration configuration) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            configuration = em.merge(configuration);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = configuration.getIdConfiguration();
                if (findConfiguration(id) == null) {
                    throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return configuration;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Configuration configuration;
            try {
                configuration = em.getReference(Configuration.class, id);
                configuration.getIdConfiguration();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.", enfe);
            }
            em.remove(configuration);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findConfigurationEntities() {
        return findConfigurationEntities(true, -1, -1);
    }

    public List<Object> findConfigurationEntities(int maxResults, int firstResult) {
        return findConfigurationEntities(false, maxResults, firstResult);
    }

    private List<Object> findConfigurationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Configuration as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Configuration findConfiguration(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Configuration.class, id);
        } finally {
            em.close();
        }
    }

    public List<Object> findOrgConfigurationEntities(Integer orgId) {
        return findOrgConfigurationEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgConfigurationEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgConfigurationEntities(false, maxResults, firstResult, orgId);
    }
    
    private List<Object> findOrgConfigurationEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Configuration as o where Organization_idOrganization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getConfigurationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Configuration as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getOrgConfigurationCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Configuration as o where Organization_idOrganization = " + orgId.toString()).getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
 
        ((Configuration)object).setActive(Boolean.TRUE);                   // activate immediately        
        ((Configuration)object).setOrganization(currentOrgId);             // overwrite to current        
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Configuration)object).setCreatedTS(currentTimestamp);
        ((Configuration)object).setCreatedBy(currentUserId);        
        ((Configuration)object).setUpdatedTS(currentTimestamp);
        ((Configuration)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((Configuration)object).getIdConfiguration(), "create", object);               
        return edit(((Configuration)object));  
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Configuration)object).setOrganization(currentOrgId);           
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Configuration)object).setUpdatedTS(currentTimestamp);
        ((Configuration)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((Configuration)object).getIdConfiguration(), "update", object);               
        return edit(((Configuration)object));  
    }

    @Override
    public String getObjectType() {
        return "configurations";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities() {
        return findConfigurationEntities();
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgConfigurationEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        return findOrgConfigurationEntities(currentOrgId);
    }


    @Override
    public int getObjectCount() {
        return getConfigurationCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getOrgConfigurationCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
