/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.User;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.mail.GoogleMailer;
import com.copsys.delta.mail.Mailer;
import com.copsys.delta.property.Property;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.security.HashEncrypter;
import com.copsys.delta.util.XUIDGenerator;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class UserJpaController implements PersistenceHelper {
    public static String tempPassword = "Delta3pass";
    private EntityManagerFactory emf = null;
    
    public UserJpaController() {
        emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
        tempPassword = new Property("tempPassword").getParamValue(); //overwrite hardcoded default
    }
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public User create(User user) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return user;
    }

    public User edit(User user) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            user = em.merge(user);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = user.getIdUser();
                if (findUser(id) == null) {
                    throw new NonexistentEntityException("The user with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return user;
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            User user;
            try {
                user = em.getReference(User.class, id);
                user.getIdUser();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The user with id " + id + " no longer exists.", enfe);
            }
            em.remove(user);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }

    public List<Object> findUserEntities(int maxResults, int firstResult) {
        return findUserEntities(false, maxResults, firstResult);
    }

    private List<Object> findUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findOrgUserEntities(Integer orgId) {
        return findOrgUserEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgUserEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgUserEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgUserEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o where Organization_idOrganization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<User> findUserByAlias(String userAlias) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("User.findByAlias");
            q.setParameter("alias", userAlias);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public User findUser(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(User.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from User as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getOrgUserCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from User as o where Organization_idOrganization = " + orgId.toString()).getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((User)object).setPassFailureMax(3);
        ((User)object).setPassFailureCount(0);
        ((User)object).setLocale("eng_US");
        ((User)object).setSecurityHandler("1");  // means SH1
        ((User)object).setGuid(UUID.randomUUID().toString());    
        ((User)object).setActive(Boolean.TRUE);                   // activate immediately        
        //XUIDGenerator xuid = new XUIDGenerator();
        // SH-1 - generate random password and save with SH-1, then force change
        //String tmpPass = xuid.getUniqueID(8);
        //((User)object).setPassword(HashEncrypter.SHA1(tempPassword));
        // set user provided password
        if ( (((User)object).getPassword() != null) && (!"".equals(((User)object).getPassword())) ) {
            ((User)object).setPassword(HashEncrypter.SHA1(((User)object).getPassword()));
        } else {
            ((User)object).setPassword(HashEncrypter.SHA1(tempPassword));
        }
        if ( ((User)object).getOrganization() == 0 ) {
            ((User)object).setOrganization(1); // default to 1
        }
        ((User)object).setGroupTable(0);                          // defualt since no GroupTable initially
        ((User)object).setPerson(0);                              // defualt since no Person initially      
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((User)object).setPassChangedTS(currentTimestamp);
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTimeInMillis(currentTimestamp.getTime());
        cal.add (Calendar.MONTH, +12) ;
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts.setTime(cal.getTimeInMillis()) ;
        ((User)object).setPassExpirationTS(ts); 
        ((User)object).setCreatedTS(currentTimestamp);
        ((User)object).setCreatedBy(currentUserId);        
        ((User)object).setUpdatedTS(currentTimestamp);
        ((User)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((User)object).getIdUser(), "create", object);  
        if ( ((User)object).getEmailAddress1() != null && !"".equals(((User)object).getEmailAddress1()) ) {
            //GoogleMailer emm = new GoogleMailer();
            Mailer emm = new Mailer();
            emm.sendMail(((User)object).getEmailAddress1(),"Welcome to DELTA3","Hello user \"" + ((User)object).getAlias()
                    + "\",\n\nPlease log into DELTA application as soon as possible and change your password. Your temporary password is: " 
                    + tempPassword + "\n\n\nDELTA Team");
            emm = null;   
        }
        return edit(((User)object));  
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((User)object).setUpdatedTS(currentTimestamp);
        ((User)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((User)object).getIdUser(), "update", object);           
        return edit(((User)object));    
    }

    @Override
    public String getObjectType() {
        return "users";
     }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findUserEntities(maxResults, firstResult);
    }

    @Override
    public List<Object> findObjectEntities() {
        return findUserEntities();
    }

    @Override
    public int getObjectCount() {
        return getUserCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((User)object).setPassFailureMax(3);
        ((User)object).setPassFailureCount(0);
        ((User)object).setLocale("eng_US");
        ((User)object).setSecurityHandler("1");  // means SH1
        ((User)object).setGuid(UUID.randomUUID().toString());    
        ((User)object).setActive(Boolean.TRUE);                   // activate immediately        
        if ( (((User)object).getPassword() != null) && (!"".equals(((User)object).getPassword())) ) {
            ((User)object).setPassword(HashEncrypter.SHA1(((User)object).getPassword()));
        } else {
            ((User)object).setPassword(HashEncrypter.SHA1(tempPassword));
        } 
        ((User)object).setOrganization(currentOrgId);             // default and overwrite to current
        ((User)object).setGroupTable(0);                          // defualt since no GroupTable initially
        ((User)object).setPerson(0);                              // defualt since no Person initially
        //GoogleMailer emm = new GoogleMailer();
        //emm.sendMail(user.getEmailAddress1(),"Successful Registration with Delta","Please click the following link to log into Delta application: http://www.Delta.com/edpMain.html\n\nYour password is: " + tmpPass);
        //emm = null;         
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((User)object).setPassChangedTS(currentTimestamp);
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTimeInMillis(currentTimestamp.getTime());
        cal.add (Calendar.MONTH, +12) ;
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts.setTime(cal.getTimeInMillis()) ;
        ((User)object).setPassExpirationTS(ts); 
        ((User)object).setCreatedTS(currentTimestamp);
        ((User)object).setCreatedBy(currentUserId);        
        ((User)object).setUpdatedTS(currentTimestamp);
        ((User)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((User)object).getIdUser(), "create", object);               
        return edit(((User)object));  
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((User)object).setUpdatedTS(currentTimestamp);
        ((User)object).setUpdatedBy(currentUserId);
        ((User)object).setOrganization(currentOrgId);             // default and overwrite to current        
        AuditLogger.log(currentUserId, ((User)object).getIdUser(), "update", object);           
        return edit(((User)object));    
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgUserEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        return findOrgUserEntities(currentOrgId);
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getOrgUserCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
