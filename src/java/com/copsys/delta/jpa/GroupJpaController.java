/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.GroupTable;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class GroupJpaController implements PersistenceHelper, Serializable {

    private EntityManagerFactory emf = null;
    
    public GroupJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(GroupTable groupTable) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(groupTable);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public GroupTable edit(GroupTable groupTable) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            groupTable = em.merge(groupTable);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = groupTable.getIdGroup();
                if (findGroupTable(id) == null) {
                    throw new NonexistentEntityException("The groupTable with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return groupTable;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupTable groupTable;
            try {
                groupTable = em.getReference(GroupTable.class, id);
                groupTable.getIdGroup();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The groupTable with id " + id + " no longer exists.", enfe);
            }
            em.remove(groupTable);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findGroupTableEntities() {
        return findGroupTableEntities(true, -1, -1);
    }

    public List<Object> findGroupTableEntities(int maxResults, int firstResult) {
        return findGroupTableEntities(false, maxResults, firstResult);
    }

    private List<Object> findGroupTableEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from GroupTable as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public GroupTable findGroupTable(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(GroupTable.class, id);
        } finally {
            em.close();
        }
    }

    public int getGroupTableCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from GroupTable as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public GroupTable createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setCreatedTS(currentTimestamp);
        ((GroupTable)object).setCreatedBy(currentUserId);        
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "create", object);
        return edit(((GroupTable)object));            
    }

    @Override
    public GroupTable updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "update", object);        
        return edit(((GroupTable)object));          
      }

    @Override
    public String getObjectType() {
        return "groups";
     }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findGroupTableEntities(maxResults, firstResult);
    }
    
    @Override
    public List<Object> findObjectEntities() {
        return findGroupTableEntities();
    }
    
    @Override
    public int getObjectCount() {
        return getGroupTableCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
