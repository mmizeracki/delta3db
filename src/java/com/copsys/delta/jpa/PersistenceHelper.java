/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author Coping Systems Inc.
 */
public interface PersistenceHelper {
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception;
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception;
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception;
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception;    
    public String getObjectType();
    public List<Object> findObjectEntities(int maxResults, int firstResult);    
    public List<Object> findObjectEntities();
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id);  // this is generic method to query by id other than orgId    
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId);    
    public List<Object> findObjectEntities( Integer currentOrgId);
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId);        
    public int getObjectCount();
    public int getObjectCount(int currentUserId);
    public int getObjectCount(Integer currentOrgId);    
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException;
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception;    
}
