/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.logregr;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.logregr.LogregrField;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrFieldJpaController implements Serializable {

    @SuppressWarnings("empty-statement")
    public LogregrFieldJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public LogregrField create(LogregrField logregrField) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(logregrField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregrField;
    }

    public LogregrField edit(LogregrField logregrField) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            logregrField = em.merge(logregrField);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logregrField.getIdLogregrField();
                if (findLogregrField(id) == null) {
                    throw new NonexistentEntityException("The logregrField with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregrField;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogregrField logregrField;
            try {
                logregrField = em.getReference(LogregrField.class, id);
                logregrField.getIdLogregrField();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logregrField with id " + id + " no longer exists.", enfe);
            }
            em.remove(logregrField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LogregrField> findLogregrFieldEntities() {
        return findLogregrFieldEntities(true, -1, -1);
    }

    public List<LogregrField> findLogregrFieldEntities(int maxResults, int firstResult) {
        return findLogregrFieldEntities(false, maxResults, firstResult);
    }

    private List<LogregrField> findLogregrFieldEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LogregrField as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LogregrField findLogregrField(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LogregrField.class, id);
        } finally {
            em.close();
        }
    }

    public int getLogregrFieldCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from LogregrField as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<LogregrField> findByModelColumnidModelColumn(Integer id)
    {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("LogregrField.findByModelColumnidRisk");
            q.setParameter("modelColumnidRisk", id);
            return q.getResultList();
        } finally {
            em.close();
        }        
    }   
    
    public List<LogregrField> findLogregrFieldByLogregrId(Integer logregrId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LogregrField as o where idLogregr = " + logregrId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
}
