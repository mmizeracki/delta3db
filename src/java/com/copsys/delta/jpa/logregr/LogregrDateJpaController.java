/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.logregr;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.logregr.LogregrDate;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrDateJpaController implements Serializable {

    @SuppressWarnings("empty-statement")
    public LogregrDateJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public LogregrDate create(LogregrDate logregrDate) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(logregrDate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregrDate;
    }

    public LogregrDate edit(LogregrDate logregrDate) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            logregrDate = em.merge(logregrDate);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logregrDate.getIdLogregrDate();
                if (findLogregrDate(id) == null) {
                    throw new NonexistentEntityException("The logregrDate with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregrDate;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogregrDate logregrDate;
            try {
                logregrDate = em.getReference(LogregrDate.class, id);
                logregrDate.getIdLogregrDate();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logregrDate with id " + id + " no longer exists.", enfe);
            }
            em.remove(logregrDate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LogregrDate> findLogregrDateEntities() {
        return findLogregrDateEntities(true, -1, -1);
    }

    public List<LogregrDate> findLogregrDateEntities(int maxResults, int firstResult) {
        return findLogregrDateEntities(false, maxResults, firstResult);
    }

    private List<LogregrDate> findLogregrDateEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LogregrDate as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LogregrDate findLogregrDate(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LogregrDate.class, id);
        } finally {
            em.close();
        }
    }

    public int getLogregrDateCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from LogregrDate as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
 
    public List<LogregrDate> findLogregrDateByLogregrId(Integer logregrId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LogregrDate as o where idLogregr = " + logregrId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<LogregrDate> findLogregrDateByLogregrFieldId(Integer logregrFieldId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LogregrDate as o where idLogregrField = " + logregrFieldId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }     
}
