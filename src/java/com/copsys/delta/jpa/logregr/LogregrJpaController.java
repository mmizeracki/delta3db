/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.logregr;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.logregr.Logregr;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrJpaController implements PersistenceHelper, Serializable {
    
    @SuppressWarnings("empty-statement")
    public LogregrJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Logregr logregr) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(logregr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Logregr edit(Logregr logregr) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            logregr = em.merge(logregr);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logregr.getIdLogregr();
                if (findLogregr(id) == null) {
                    throw new NonexistentEntityException("The logregr with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregr;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Logregr logregr;
            try {
                logregr = em.getReference(Logregr.class, id);
                logregr.getIdLogregr();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logregr with id " + id + " no longer exists.", enfe);
            }
            em.remove(logregr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findLogregrEntities() {
        return findLogregrEntities(true, -1, -1);
    }

    public List<Object> findLogregrEntities(int maxResults, int firstResult) {
        return findLogregrEntities(false, maxResults, firstResult);
    }

    private List<Object> findLogregrEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }


    public List<Object> findOrgLogregrEntities(Integer orgId) {
        return findOrgLogregrEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgLogregrEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgLogregrEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgLogregrEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idOrganization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Logregr findLogregr(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Logregr.class, id);
        } finally {
            em.close();
        }
    }

    public List<Logregr> findLogregrByProcessId(Integer processId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idProcess = " + processId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    } 
    
    public List<Logregr> findLogregrByStudyId(Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    public int getLogregrCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Logregr as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getLogregrCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Logregr as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setCreatedTS(currentTimestamp);
        ((Logregr)object).setCreatedBy(currentUserId);        
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);        
        Logregr obj = edit(((Logregr)object));
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "create", obj);   
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);   
        Logregr obj = edit(((Logregr)object));        
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setIdOrganization(currentOrgId);
        ((Logregr)object).setCreatedTS(currentTimestamp);
        ((Logregr)object).setCreatedBy(currentUserId);        
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);        
        Logregr obj = edit(((Logregr)object));
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "create", obj);   
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ((Logregr)object).setIdOrganization(currentOrgId);    
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);   
        Logregr obj = edit(((Logregr)object));        
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public String getObjectType() {
        return "logregrs";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgLogregrEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getLogregrCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getLogregrCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Logregr)subject).getIdLogregr(), "delete", subject);
        destroy(((Logregr)subject).getIdLogregr());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
