/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa.process;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.process.Process;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class ProcessJpaController implements PersistenceHelper, Serializable {

    @SuppressWarnings("empty-statement")
    public ProcessJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }    

    public Process create(Process process) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(process);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProcess(process.getIdProcess()) != null) {
                throw new PreexistingEntityException("Process " + process + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return process;
    }

    public Process edit(Process process) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            process = em.merge(process);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = process.getIdProcess();
                if (findProcess(id) == null) {
                    throw new NonexistentEntityException("The process with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return process;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Process process;
            try {
                process = em.getReference(Process.class, id);
                process.getIdProcess();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The process with id " + id + " no longer exists.", enfe);
            }
            em.remove(process);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
            
    public List<Object> findProcessEntities() {
        return findProcessEntities(true, -1, -1);
    }

    public List<Object> findProcessEntities(int maxResults, int firstResult) {
        return findProcessEntities(false, maxResults, firstResult);
    }

    private List<Object> findProcessEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

            
    public List<Object> findOrgProcessEntities(Integer orgId) {
        return findOrgProcessEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgProcessEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgProcessEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgProcessEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Process findProcess(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Process.class, id);
        } finally {
            em.close();
        }
    }

    public List<Process> findProcessEntitiesByStudyId(Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findActiveProcessEntities() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where progress > 0 and progress < 100 and status = 'InProgress'");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getProcessCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getProcessCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setGuid(UUID.randomUUID().toString());      
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Process)object).setCreatedTS(currentTimestamp);
        ((Process)object).setCreatedBy(currentUserId);        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setGuid(UUID.randomUUID().toString());     
        ((Process)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Process)object).setCreatedTS(currentTimestamp);
        ((Process)object).setCreatedBy(currentUserId);        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;          
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;  
    }

    @Override
    public String getObjectType() {
        return "processes";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findProcessEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findProcessEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgProcessEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getProcessCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getProcessCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Process)subject).getIdProcess(), "delete", subject);
        destroy(((Process)subject).getIdProcess());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Process)subject).getIdProcess(), "delete", subject);
        destroy(((Process)subject).getIdProcess());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
