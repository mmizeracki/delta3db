/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.jpa.event;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.events.Alert;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Boston Advanced Analytics
 */
public class AlertJpaController implements PersistenceHelper, Serializable {

    public AlertJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Alert alert) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(alert);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAlert(alert.getIdAlert()) != null) {
                throw new PreexistingEntityException("Alert " + alert + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Alert edit(Alert alert) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            alert = em.merge(alert);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = alert.getIdAlert();
                if (findAlert(id) == null) {
                    throw new NonexistentEntityException("The alert with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return alert;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Alert alert;
            try {
                alert = em.getReference(Alert.class, id);
                alert.getIdAlert();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The alert with id " + id + " no longer exists.", enfe);
            }
            em.remove(alert);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findAlertEntities() {
        return findAlertEntities(true, -1, -1);
    }

    public List<Object> findAlertEntities(int maxResults, int firstResult) {
        return findAlertEntities(false, maxResults, firstResult);
    }

    private List<Object> findAlertEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Alert as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findUserAlertEntities(Integer userId) {
        return findUserAlertEntities(true, -1, -1, userId);
    }

    public List<Object> findUserAlertEntities(int maxResults, int firstResult, Integer userId) {
        return findUserAlertEntities(false, maxResults, firstResult, userId);
    }

    private List<Object> findUserAlertEntities(boolean all, int maxResults, int firstResult, Integer userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Alert as o where User_idUser = " + userId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Alert findAlert(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Alert.class, id);
        } finally {
            em.close();
        }
    }

    public int getAlertCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Alert as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getAlertCount(int userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Alert as o where User_idUser = " + Integer.toString(userId));
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Alert)object).setIdUser(currentUserId);
        ((Alert)object).setCreatedTS(currentTimestamp);
        ((Alert)object).setCreatedBy(currentUserId);        
        ((Alert)object).setUpdatedTS(currentTimestamp);
        ((Alert)object).setUpdatedBy(currentUserId);       
        Alert obj = edit(((Alert)object));     
        AuditLogger.log(currentUserId, ((Alert)object).getIdAlert(), "create", object);               
        return obj;         
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Alert)object).setUpdatedTS(currentTimestamp);
        ((Alert)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((Alert)object).getIdAlert(), "update", object);               
        return edit(((Alert)object)); 
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getObjectType() {
        return "alerts";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findAlertEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findAlertEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        return findUserAlertEntities(maxResults, firstResult, id);
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getAlertCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.events.Alert)subject).getIdAlert(), "delete", subject);
        destroy(((com.copsys.delta.entity.events.Alert)subject).getIdAlert());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        return getAlertCount(currentUserId);
    }
    
}
