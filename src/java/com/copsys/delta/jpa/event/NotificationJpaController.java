/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.jpa.event;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.events.Notification;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Boston Advanced Analytics
 */
public class NotificationJpaController implements PersistenceHelper, Serializable {

    public NotificationJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 


    public void create(Notification notification) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(notification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNotification(notification.getIdNotification()) != null) {
                throw new PreexistingEntityException("Notification " + notification + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Notification edit(Notification notification) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            notification = em.merge(notification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = notification.getIdNotification();
                if (findNotification(id) == null) {
                    throw new NonexistentEntityException("The notification with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return notification;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Notification notification;
            try {
                notification = em.getReference(Notification.class, id);
                notification.getIdNotification();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The notification with id " + id + " no longer exists.", enfe);
            }
            em.remove(notification);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findNotificationEntities() {
        return findNotificationEntities(true, -1, -1);
    }

    public List<Object> findNotificationEntities(int maxResults, int firstResult) {
        return findNotificationEntities(false, maxResults, firstResult);
    }

    private List<Object> findNotificationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Notification as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findUserNotificationEntities(Integer userId) {
        return findUserNotificationEntities(true, -1, -1, userId);
    }

    public List<Object> findUserNotificationEntities(int maxResults, int firstResult, Integer userId) {
        return findUserNotificationEntities(false, maxResults, firstResult, userId);
    }

    private List<Object> findUserNotificationEntities(boolean all, int maxResults, int firstResult, Integer userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Notification as o where User_idUser = " + userId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Notification findNotification(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Notification.class, id);
        } finally {
            em.close();
        }
    }

    public int getNotificationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Notification as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    
    public int getNotificationCount(int userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Notification as o where User_idUser = " + Integer.toString(userId));
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }  

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Notification)object).setIdUser(currentUserId);
        ((Notification)object).setCreatedTS(currentTimestamp);
        ((Notification)object).setCreatedBy(currentUserId);        
        Notification obj = edit(((Notification)object));     
        AuditLogger.log(currentUserId, ((Notification)object).getIdNotification(), "create", object);               
        return obj;         
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Notification)object).setUpdatedTS(currentTimestamp);
        ((Notification)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((Notification)object).getIdNotification(), "update", object);               
        return edit(((Notification)object)); 
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getObjectType() {
        return "notifications";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findNotificationEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findNotificationEntities();
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        return findUserNotificationEntities(maxResults, firstResult, id);
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getNotificationCount();
    }
    
    @Override
    public int getObjectCount(int userId) {
        return getNotificationCount(userId);
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.copsys.delta.entity.events.Notification)subject).getIdNotification(), "delete", subject);
        destroy(((com.copsys.delta.entity.events.Notification)subject).getIdNotification());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
