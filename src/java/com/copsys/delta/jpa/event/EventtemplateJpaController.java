/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.jpa.event;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.events.Eventtemplate;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Boston Advanced Analytics
 */
public class EventtemplateJpaController implements PersistenceHelper, Serializable {

    public EventtemplateJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Eventtemplate eventtemplate) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(eventtemplate);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEventtemplate(eventtemplate.getIdEventTemplate()) != null) {
                throw new PreexistingEntityException("Eventtemplate " + eventtemplate + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Eventtemplate edit(Eventtemplate eventtemplate) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            eventtemplate = em.merge(eventtemplate);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = eventtemplate.getIdEventTemplate();
                if (findEventtemplate(id) == null) {
                    throw new NonexistentEntityException("The eventtemplate with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return eventtemplate;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Eventtemplate eventtemplate;
            try {
                eventtemplate = em.getReference(Eventtemplate.class, id);
                eventtemplate.getIdEventTemplate();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The eventtemplate with id " + id + " no longer exists.", enfe);
            }
            em.remove(eventtemplate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findEventtemplateEntities() {
        return findEventtemplateEntities(true, -1, -1);
    }

    public List<Object> findEventtemplateEntities(int maxResults, int firstResult) {
        return findEventtemplateEntities(false, maxResults, firstResult);
    }

    private List<Object> findEventtemplateEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Eventtemplate as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Eventtemplate findEventtemplate(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Eventtemplate.class, id);
        } finally {
            em.close();
        }
    }

    public int getEventtemplateCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Eventtemplate as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Eventtemplate)object).setCreatedTS(currentTimestamp);
        ((Eventtemplate)object).setCreatedBy(currentUserId);        
        ((Eventtemplate)object).setUpdatedTS(currentTimestamp);
        ((Eventtemplate)object).setUpdatedBy(currentUserId);      
        Eventtemplate obj = edit(((Eventtemplate)object));     
        AuditLogger.log(currentUserId, ((Eventtemplate)object).getIdEventTemplate(), "create", object);               
        return obj;         
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Eventtemplate)object).setUpdatedTS(currentTimestamp);
        ((Eventtemplate)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((Eventtemplate)object).getIdEventTemplate(), "update", object);               
        return edit(((Eventtemplate)object));  
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getObjectType() {
        return "eventtemplates";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findEventtemplateEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findEventtemplateEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getEventtemplateCount();
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
