/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.copsys.delta.entity.Organization;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.util.XUIDGenerator;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Coping Systems Inc.
 */
public class OrganizationJpaController  implements PersistenceHelper, Serializable {

    @SuppressWarnings("empty-statement")
    public OrganizationJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public Organization create(Organization organization) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(organization);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return organization;
    }

    public Organization edit(Organization organization) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            organization = em.merge(organization);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = organization.getIdOrganization();
                if (findOrganization(id) == null) {
                    throw new NonexistentEntityException("The user with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return organization;
    }
 
     public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Organization organization;
            try {
                organization = em.getReference(Organization.class, id);
                organization.getIdOrganization();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The organiazation with id " + id + " no longer exists.", enfe);
            }
            em.remove(organization);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findOrganizationEntities() {
        return findOrganizationEntities(true, -1, -1);
    }

    public List<Object> findOrganizationEntities(int maxResults, int firstResult) {
        return findOrganizationEntities(false, maxResults, firstResult);
    }

    public List<Organization> findOrganizationByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Organization.findByName");
            q.setParameter("name", name);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Organization> findOrganizationByGUID(String guid) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Organization.findByGUID");
            q.setParameter("guid", guid);
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    private List<Object> findOrganizationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Organization as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Organization findOrganization(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Organization.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrganizationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Organization as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Object> findOrgOrganizationEntities(Integer orgId) {
        return findOrgOrganizationEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgOrganizationEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgOrganizationEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgOrganizationEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Organization as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }    

    public int getOrgOrganizationCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Organization as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }     
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Organization)object).setGuid(UUID.randomUUID().toString());
        XUIDGenerator xuid = new XUIDGenerator();       
        ((Organization)object).setKey1(xuid.getUniqueID(40));
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Organization)object).setCreatedTS(currentTimestamp);
        ((Organization)object).setCreatedBy(currentUserId);        
        ((Organization)object).setUpdatedTS(currentTimestamp);
        ((Organization)object).setUpdatedBy(currentUserId);      
        AuditLogger.log(currentUserId, ((Organization)object).getIdOrganization(), "create", object);        
        return edit(((Organization)object));   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((Organization)object).setUpdatedTS(currentTimestamp);
        ((Organization)object).setUpdatedBy(currentUserId);   
        AuditLogger.log(currentUserId, ((Organization)object).getIdOrganization(), "update", object);               
        return edit(((Organization)object));  
    }

    @Override
    public String getObjectType() {
        return "organizations";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findOrganizationEntities(maxResults, firstResult);
    }

    @Override
    public List<Object> findObjectEntities() {
        return findOrganizationEntities();
    }

    @Override
    public int getObjectCount() {
        return getOrganizationCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgOrganizationEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        return findOrgOrganizationEntities(currentOrgId);
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        return findOrgOrganizationEntities(true, maxResults, firstResult, currentOrgId);
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        return getOrgOrganizationCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
