/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.jpa;

import com.copsys.delta.db.PersistenceManager;
import com.copsys.delta.entity.Permissiontemplate;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class PermissiontemplateJpaController implements PersistenceHelper, Serializable {
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public PermissiontemplateJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }

    public void create(Permissiontemplate permissiontemplate) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(permissiontemplate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Permissiontemplate edit(Permissiontemplate permissiontemplate) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            permissiontemplate = em.merge(permissiontemplate);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = permissiontemplate.getIdPermissiontemplate();
                if (findPermissiontemplate(id) == null) {
                    throw new NonexistentEntityException("The permissiontemplate with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return permissiontemplate;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Permissiontemplate permissiontemplate;
            try {
                permissiontemplate = em.getReference(Permissiontemplate.class, id);
                permissiontemplate.getIdPermissiontemplate();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The permissiontemplate with id " + id + " no longer exists.", enfe);
            }
            em.remove(permissiontemplate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findPermissiontemplateEntities() {
        return findPermissiontemplateEntities(true, -1, -1);
    }

    public List<Object> findPermissiontemplateEntities(int maxResults, int firstResult) {
        return findPermissiontemplateEntities(false, maxResults, firstResult);
    }

    private List<Object> findPermissiontemplateEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Permissiontemplate as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Permissiontemplate findPermissiontemplate(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Permissiontemplate.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<Permissiontemplate> findPermissiontemplateByTag(String tag) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Permissiontemplate.findByTag");
            q.setParameter("tag", tag);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Permissiontemplate> findPermissiontemplateByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Permissiontemplate.findByName");
            q.setParameter("name", name);
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    public int getPermissiontemplateCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Permissiontemplate as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Permissiontemplate)object).setModule(1);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Permissiontemplate)object).setCreatedTS(currentTimestamp);
        ((Permissiontemplate)object).setCreatedBy(currentUserId);        
        ((Permissiontemplate)object).setUpdatedTS(currentTimestamp);
        ((Permissiontemplate)object).setUpdatedBy(currentUserId);    
        AuditLogger.log(currentUserId, ((Permissiontemplate)object).getIdPermissiontemplate(), "create", object);               
        return edit(((Permissiontemplate)object)); 
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());       
        ((Permissiontemplate)object).setUpdatedTS(currentTimestamp);
        ((Permissiontemplate)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((Permissiontemplate)object).getIdPermissiontemplate(), "update", object);               
        return edit(((Permissiontemplate)object)); 
    }

    @Override
    public String getObjectType() {
        return "permissiontemplates";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findPermissiontemplateEntities(maxResults, firstResult);
    }
    
    @Override
    public List<Object> findObjectEntities() {
        return findPermissiontemplateEntities();
    }
    
    @Override
    public int getObjectCount() {
        return getPermissiontemplateCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
