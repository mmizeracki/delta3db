package com.copsys.delta.property;

import java.util.Properties;


public class Property {
    private String paramName;
    private String paramValue;
    private Properties properties;

    public Property(String paramName){
     properties = new Properties();
        try {
           
        
        properties.load(getClass().getResourceAsStream("/config.properties"));

        this.paramValue=properties.getProperty(paramName);
        this.paramName=paramName;

        } catch (Exception e){
        
        }
    }
    public String getParamName()
    {
        return paramName;
    }
    public String getParamValue()
    {
        return paramValue;
    }
     public boolean containsOnlyNumbers(String str) {

        //It can't contain only numbers if it's null or empty...
        if (str == null || str.length() == 0)
            return false;

        for (int i = 0; i < str.length(); i++) {

            //If we find a non-digit character we return false.
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }

        return true;
    }
    private void getParamValue(String paramName)
    {
        this.paramValue=properties.getProperty(paramName);
        this.paramName=paramName;
    }
}
