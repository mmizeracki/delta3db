/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.security.exceptions;

/**
 *
 * @author Boston Advanced Analytics
 */
    
public class DeltaOperationNotAllowedException extends Exception {
    public DeltaOperationNotAllowedException(String message, Throwable cause) {
        super(message, cause);
    }
    public DeltaOperationNotAllowedException(String message) {
        super(message);
    }
}
