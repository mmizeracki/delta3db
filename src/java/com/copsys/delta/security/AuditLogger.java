/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.security;

import com.copsys.delta.entity.Audit;
import com.copsys.delta.jpa.AuditJpaController;
import com.copsys.delta.jpa.exceptions.PreexistingEntityException;
import com.google.gson.Gson;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Coping Systems Inc.
 */
public class AuditLogger {
    private static AuditJpaController auditController; 
    
    public static void log(int currentUserId, int objectId, String action, Object objectContent) {
        if ( auditController == null ) {
            auditController = new AuditJpaController();
        }        
        Audit audit = new Audit();
        Gson gson = new Gson();
        String json = gson.toJson(objectContent);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        audit.setCreatedTS(currentTimestamp);
        audit.setUserId(currentUserId);       
        audit.setSource("user");
        audit.setObjectId(objectId);
        audit.setAction(action);
        audit.setObjectType(objectContent.getClass().getSimpleName());
        audit.setObjectContent(json);
        try {
            auditController.create(audit);
        } catch (PreexistingEntityException ex) {
            Logger.getLogger(AuditLogger.class.getName()).log(Level.SEVERE, "Exception in audit: ", ex);
        } catch (Exception ex) {
            Logger.getLogger(AuditLogger.class.getName()).log(Level.SEVERE, "Exception in audit: ", ex);
        }
    }
}
