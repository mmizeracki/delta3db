package com.copsys.delta.security;

import java.sql.*;
import com.copsys.delta.db.ConnectionPool;
import com.copsys.delta.entity.Organization;
import com.copsys.delta.entity.User;
import com.copsys.delta.jpa.OrganizationJpaController;
import java.security.SignatureException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.*;

/**
 * User: Coping Systems Inc.
 * Date: Sept 1, 2013
 * Time: 12:41:56 PM
 */
public class Auth {
    private static OrganizationJpaController organizationController = null;  
    
    public static boolean verifyPassword(String alias, String pwd) {
     Logger logger = Logger.getLogger(Auth.class.getName());
     Connection connection = null;
     PreparedStatement selectStat = null;
     ResultSet UserRes = null;
     try
     {
        int usercount = -1;
        connection = ConnectionPool.getInstance().getConnection();
        pwd = HashEncrypter.SHA1(pwd);
        //selectStat = connection.prepareStatement("SELECT COUNT(*) AS usercount FROM d3_user where active='Y' and alias=? and password=?");
        // need to figure out how to code for MySQL (active=1) and MS-SQL (active='Y'), until then:
        selectStat = connection.prepareStatement("SELECT COUNT(*) AS usercount FROM d3_user where alias=? and password=?");
        logger.log(Level.INFO,"Attempting to authenticate user: " + alias);
        selectStat.setString(1, alias);
        selectStat.setString(2, pwd);

        UserRes = selectStat.executeQuery();
        UserRes.next();
        usercount = UserRes.getInt("usercount");
        UserRes.close();

        if ( usercount == 0 ) {
            logger.log(Level.INFO,"Authentication for user with name " + alias + " failed");
            return false;
        } else {
            return true;
        }
      } catch (Exception e1) {
            logger.log(Level.SEVERE,"Exception while authenticating user: " + e1);
        } finally {
            try {
                UserRes.close();
                selectStat.close();
                ConnectionPool.getInstance().free(connection);
            } catch (SQLException e) {
                ConnectionPool.getInstance().free(connection);
                logger.log(Level.SEVERE,"DB exception during login while releasing resources. " + e.toString());
            }
       }
       return false;
    }

    public static boolean createUserSession(User user, HttpSession session) {
     
        Logger logger = Logger.getLogger(Auth.class.getName());
        try
        {
            session.setAttribute("userAlias", user.getAlias());
            session.setAttribute("userId", user.getIdUser());
            session.setAttribute("organizationId", user.getOrganization());
            session.setAttribute("locale", user.getLocale());            
            session.setAttribute("email1", user.getEmailAddress1());
            session.setAttribute("email2", user.getEmailAddress2());
            session.setAttribute("userType", user.getType());
            session.setAttribute("org", user.getOrganization());

            logger.log(Level.INFO, "Session created for user: [{0}] email: [{1}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1")});
        } catch (Exception e1) {
            logger.log(Level.SEVERE, "Exception while establishing HTTP session for user: {0} {1}", new Object[]{user.getAlias(), e1.toString()});
            return false;
        } 
      return true;
    }

    public static boolean createSuperUserSession(Integer suId, User user, HttpSession session) {
     
        Logger logger = Logger.getLogger(Auth.class.getName());
        try
        {
            session.setAttribute("userAlias", user.getAlias());
            session.setAttribute("userId", user.getIdUser());
            session.setAttribute("organizationId", user.getOrganization());
            session.setAttribute("locale", user.getLocale());            
            session.setAttribute("email1", user.getEmailAddress1());
            session.setAttribute("email2", user.getEmailAddress2());
            session.setAttribute("userType", user.getType());
            session.setAttribute("org", user.getOrganization());
            session.setAttribute("superUserId", suId);

            logger.log(Level.INFO, "Session created for user: [{0}] email: [{1}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1")});
        } catch (Exception e1) {
            logger.log(Level.SEVERE, "Exception while establishing HTTP session for user: {0} {1}", new Object[]{user.getAlias(), e1.toString()});
            return false;
        } 
      return true;
    }
    
    public static boolean killSession(HttpSession session) {
     
        Logger logger = Logger.getLogger(Auth.class.getName());
        try
        {
            session.invalidate();
        } catch (Exception e1) {
            logger.log(Level.SEVERE, "Exception while invalidating HTTP session");
            return false;
        } 
      return true;
    }    
    
    public static boolean verifyUserSession(String userAlias, HttpSession session) {
        Logger logger = Logger.getLogger(Auth.class.getName());
        if ( (session.getAttribute("userAlias") != null) && session.getAttribute("userAlias").equals(userAlias) ) {
            logger.log(Level.INFO, "Session already exists for user: [{0}] email: [{1}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1")});
            return true;
        }
        return false;
    }

    public static Integer getUserIdFromSession(HttpSession session) {
        Logger logger = Logger.getLogger(Auth.class.getName());
        logger.log(Level.INFO, "Retieving userId for user: [{0}] email: [{1}] id: [{2}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1"), session.getAttribute("userId")});
        return (Integer)session.getAttribute("userId");
    }
 
    public static String getUserAliasFromSession(HttpSession session) {
        Logger logger = Logger.getLogger(Auth.class.getName());
        logger.log(Level.INFO, "Retieving userAlias for user: [{0}] email: [{1}] id: [{2}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1"), session.getAttribute("userId")});
        return (String)session.getAttribute("userAlias");
    }

    public static Integer getOrganizationIdFromSession(HttpSession session) {
        Logger logger = Logger.getLogger(Auth.class.getName());
        logger.log(Level.INFO, "Retieving organizationId for user: [{0}] email: [{1}] id: [{2}]", new Object[]{session.getAttribute("userAlias"), session.getAttribute("email1"), session.getAttribute("userId")});
        return (Integer)session.getAttribute("organizationId");
    }

    public static String getUserTypeFromSession(HttpSession session) {
        Logger logger = Logger.getLogger(Auth.class.getName());
        logger.log(Level.INFO, "Retieving userType for user: [{0}] email: [{1}] id: [{2}]", new Object[]{session.getAttribute("userType"), session.getAttribute("email1"), session.getAttribute("userId")});
        return (String)session.getAttribute("userType");
    }
    
    public static boolean verifyWSSignature(String method, String params, String signature, String publicKey) {
       
        Logger logger = Logger.getLogger(Auth.class.getName());
        logger.log(Level.INFO, "Verifying WS call for method: [{0}] key: [{1}]", new Object[]{method, publicKey});
        if ( organizationController == null ) {
             organizationController = new OrganizationJpaController();
        }
        //Organization org = organizationController.findOrganization(user.getOrganization_idOrganization());
        List<Organization> orgs = organizationController.findOrganizationByGUID(publicKey); 
        Organization org = (Organization) orgs.get(0); 
        try {
             String result = HMACSHA1Enctryptor.calculateRFC2104HMAC("method" + method + "params" + params, org.getKey1());
             if ( result.equals(signature)) {
                 return true;
             }
             // may need logic to retrieve first ADMIN user for this org as input for updatedBy
             //try { not needed?
             //    result = URLEncoder.encode(result, "UTF-8");
             //} catch (UnsupportedEncodingException ex) {
             //    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
             //}
        } catch (SignatureException ex) {
             logger.log(Level.SEVERE, "Exception while checking HMAC signature: ", ex);
        } 
        return false;
    }    
}
