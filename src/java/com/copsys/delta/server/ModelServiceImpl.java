/*
 * Model Service Implementation
 */
package com.copsys.delta.server;

import com.copsys.delta.db.ConnectionPool;
import com.copsys.delta.db.DBHelper;
import static com.copsys.delta.db.DBHelper.getObjectCount;
import com.copsys.delta.db.DatabaseNotSupportedException;
import com.copsys.delta.entity.Configuration;
import com.copsys.delta.entity.model.Model;
import com.copsys.delta.entity.model.ModelColumn;
import com.copsys.delta.entity.model.ModelRelationship;
import com.copsys.delta.entity.model.ModelTable;
import com.copsys.delta.entity.process.JobRequest;
import com.copsys.delta.entity.process.Process;
import com.copsys.delta.jpa.ConfigurationJpaController;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.model.ModelColumnJpaController;
import com.copsys.delta.jpa.model.ModelJpaController;
import com.copsys.delta.jpa.model.ModelRelationshipJpaController;
import com.copsys.delta.jpa.model.ModelTableJpaController;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.copsys.delta.util.Util;
import com.copsys.statproxy.Proxy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;


/**
 *
 * @author Coping Systems Inc.
 */
public class ModelServiceImpl {
    private static final int max_tables = 10;
    private static final Logger logger = Logger.getLogger(ModelServiceImpl.class.getName());    
    private static ModelColumnJpaController modelColumnController;
    private static ModelTableJpaController modelTableController;  
    private static ModelRelationshipJpaController modelRelationshipController;   
    private static ModelJpaController modelController;       
    private static ConfigurationJpaController configurationController;
    private static StudyServiceImpl studyService;    
    private static AdminServiceImpl adminService;      
    private Integer tempFieldGen = 1;
    private Integer tempTableGen = 1;       

    public String executeCreateStatement(String modelJSON, Integer currentUserId) {      
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            String sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);    
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaPU","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            AuditLogger.log(currentUserId, 0, "create table", sqlCreate);             
            DBHelper.executeSqlStatement("DeltaPU",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Created", false, false, false);
            return "Table created.";
        } catch (PersistenceException ex) {
                String errorString = "MB0001 SQL error while generating Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0001 Error while creating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;            
        }
    }    
    
    public String generateFlatTable(String modelJSON, Integer currentUserId) {     
        String resultString;
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            setModelStatus(m, currentUserId, "Processing of atomic fields started", false, false, false);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            String sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);    
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaPU","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            AuditLogger.log(currentUserId, 0, "create table", sqlCreate);             
            DBHelper.executeSqlStatement("DeltaPU",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Populating Flat Table with data");
            if ( "Ok".equals(resultString = populateFlatTableJDBC(m, mr, mt, mc, currentUserId)) ) {
                setModelStatus(m, currentUserId, "Processing of atomic fields finished", true, false, false);
                return "Processing of atomic fields finished";
            } else {
                setModelStatus(m, currentUserId, resultString);
                return resultString;
            }
        } catch (PersistenceException ex) {
                String errorString = "MB0002 SQL error while generating Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0003 Error while generating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                
        }
    } 

    public String deleteFlatTable(String modelJSON, Integer currentUserId) {        
        Model m = null;      
        try {
            m = parseModelFromJSON(modelJSON);
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());
            DBHelper.executeSqlStatement("DeltaPU","DROP TABLE " + m.getOutputName(),logger);
            setModelStatus(m, currentUserId, "Deleted", false, false, false);
            return "Table " + m.getOutputName() + " deleted.";
        }  catch (PersistenceException ex) {
                String errorString = "MB0004 SQL error while deleting Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;            
            }
        catch (Exception ex) {
            String errorString = "MB0005 Error while deleting Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                               
        }
    } 

    public String processMissingFields(String modelJSON, Integer currentUserId) {           
        String resultMessage;
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }      
            // followig if plays a role of a semaphore
            m = modelController.findModel(m.getIdModel());
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0065 Processing of atomic fields not finished. Cannot proceed");
                return "MB0065 Processing of atomic fields not finished. Cannot proceed";
            }            
            if ( checkIfTableIsEmpty(m.getOutputName()) == true ) {
                setModelStatus(m, currentUserId, "MB0009 Target table is empty. Cannot process");
                return "MB0009 Target table is empty. Cannot process";
            }         
            setModelStatus(m, currentUserId, "Processing of missing fields started", true, false, false);
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            ModelColumn primaryKey = getPrimayKeyField(mt, mc);            
            for (ModelColumn modelColumn : mc) {       
                if ( modelColumn.getSub() == true ) {
                    resultMessage = missingDataProcessor(currentUserId, modelColumn, m.getOutputName(), primaryKey);
                    if ( !"Ok".equals(resultMessage) ) {
                        setModelStatus(m, currentUserId, resultMessage);
                        return resultMessage;
                    }
                }
            }  
            setModelStatus(m, currentUserId, "Processing of missing fields finished", true, true, false);
            return "Processing of missing fields finished";
        } catch (PersistenceException ex) {
            String errorString = "MB0010 SQL Error while processing missing fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;            
            }
        catch (Exception ex) {
            String errorString = "MB0011 Error while processing missing fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                           
        }
    } 
    
    public String processVirtualFields(String modelJSON, Integer currentUserId) {          
        String resultMessage;
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }                
            m = modelController.findModel(m.getIdModel());
            // followig if plays a role of a semaphore
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0066 Atomic fields not processed. Cannot proceed");
                return "MB0066 Atomic fields not processed. Cannot proceed";
            }
            if ( checkIfTableIsEmpty(m.getOutputName()) == true ) {
                setModelStatus(m, currentUserId, "MB0006 Target table is empty. Cannot process");
                return "MB0006 Target table is empty. Cannot process";
            }
            setModelStatus(m, currentUserId, "Processing of virtual fields started", true, true, false);
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);  
            ModelColumn primaryKey = getPrimayKeyField(mt, mc);            
            mc = filterVirtualModelColumns(mc);
            if ( !mc.isEmpty() ) {
                String sqlStmt = "ALTER TABLE " + m.getOutputName();            
                for (ModelColumn modelColumn : mc) {           
                    String type = getFieldType(modelColumn,m.getOutputName());
                    sqlStmt += " ADD " + modelColumn.getName() + " " + type + " NULL,";
                }
                sqlStmt = Util.removeLastChar(sqlStmt);
                AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
                DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);    

                for (ModelColumn modelColumn : mc) {       
                    resultMessage = virtualFieldProcessor(currentUserId, modelColumn, m.getOutputName(), primaryKey);
                    if ( !"Ok".equals(resultMessage) ) {
                        setModelStatus(m, currentUserId, resultMessage);
                        return resultMessage;
                    }            
                }
            }
            setModelStatus(m, currentUserId, "Processing of virtual fields finished", true, true, true);
            return "Processing of Virtual Fields finished";
        } catch (PersistenceException ex) {
            String errorString = "MB0006 Error while processing virtual fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;                 
        } catch (Exception ex) {
            String errorString = "MB0008 Error while processing virtual fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                          
        }
    } 

    public String processFlatTableAll(String modelJSON, Integer currentUserId, boolean processMissing) {    
        Model m = parseModelFromJSON(modelJSON);
        return processFlatTableAll(m, currentUserId, processMissing);
    }
    
    public String processFlatTableAll(Model m, Integer currentUserId, boolean processMissing) {     
        String resultString = "Flat Table processing failed";
        try {
            //m = parseModelFromJSON(modelJSON);
            setModelStatus(m, currentUserId, "Processing of atomic fields started", false, false, false);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            String sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);    
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaPU","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            AuditLogger.log(currentUserId, 0, "create table", sqlCreate);             
            DBHelper.executeSqlStatement("DeltaPU",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Populating Flat Table with data");
            if ( "Ok".equals(resultString = populateFlatTableJDBC(m, mr, mt, mc, currentUserId)) ) {
                setModelStatus(m, currentUserId, "Processing of atomic fields finished", true, false, false);    
                ModelColumn primaryKey = getPrimayKeyField(mt, mc);                   
                if ( processMissing == true ) {
                    setModelStatus(m, currentUserId, "Processing of missing fields started");   
                    for (ModelColumn modelColumn : mc) {       
                        if ( modelColumn.getSub() == true ) {
                            resultString = missingDataProcessor(currentUserId, modelColumn, m.getOutputName(), primaryKey);
                            if ( !"Ok".equals(resultString) ) {
                                setModelStatus(m, currentUserId, resultString);
                                return resultString;
                            }
                        }
                    }  
                }
                setModelStatus(m, currentUserId, "Processing of virtual fields started", true, true, false);       
                mc = filterVirtualModelColumns(mc);
                if ( !mc.isEmpty() ) {
                    String sqlStmt = "ALTER TABLE " + m.getOutputName();            
                    for (ModelColumn modelColumn : mc) {           
                        String type = getFieldType(modelColumn,m.getOutputName());
                        sqlStmt += " ADD " + modelColumn.getName() + " " + type + " NULL,";
                    }
                    sqlStmt = Util.removeLastChar(sqlStmt);
                    AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);    

                    for (ModelColumn modelColumn : mc) {       
                        resultString = virtualFieldProcessor(currentUserId, modelColumn, m.getOutputName(), primaryKey);
                        if ( !"Ok".equals(resultString) ) {
                            setModelStatus(m, currentUserId, resultString);
                            return resultString;
                        }            
                    }
                }
                setModelStatus(m, currentUserId, "Processing of virtual fields finished", true, true, true);                
            } else {
                setModelStatus(m, currentUserId, resultString);
                return resultString;
            }
            
        } catch (PersistenceException ex) {
                String errorString = "MB0002 SQL error while generating Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString, false, false, false);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0003 Error while generating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString, false, false, false);
            return errorString;                
        }
        return resultString;
    } 
    
    public String verifyStatement(String tableName, String formula, Integer currentUserId) {      
        String sqlTestStmt = null;
        try {
            sqlTestStmt = "SELECT " + formula + " FROM " + tableName + " LIMIT 1";
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
            AuditLogger.log(currentUserId, 0, "verify statement", sqlTestStmt);            
            Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);
            List<Object[]> results = queryResult.getResultList();
            //return getDBObjectType(results.get(0));
            if ( results.size() > 0 ) {
                return "ok";
            } else {
                return "MB0012 Inalid formula statement.";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
            return "MB0013 Invalid formula.";            
        }    
    } 
    
    public String getDBObject(Integer currentUserId, int start, int limit, String orderBy, String direction, int filterId, int modelId) {
        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            Model model = modelController.findModel(modelId);          
            String filterString = "";
            if ( filterId != 0 ) {
                if ( studyService == null ) {
                    studyService = new StudyServiceImpl();
                }                   
               filterString = studyService.getSqlFromFilterFormulas(filterId);
            }
            String dataString = getDBObjectData(currentUserId, start, limit, orderBy, direction, model.getOutputName(), filterString); 
            return dataString;
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getModelMetadata(String load) {     
        String result = "MB0014 Metadata not retrieved.";
        int retryCount = 0;
        int retryMax = 2;
        String dataString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();   
        JSONArray metadata;
        Model model = null;
        
        while ( retryCount < retryMax ) { // JDBC connection used here can stale afer a while, retry once
            try {
                JSONObject JSONContent =  new JSONObject(load);
                JSONArray modelArray = JSONContent.getJSONArray("models");
                for (int i=0; i<modelArray.length(); i++) {
                    logger.log(Level.INFO, "Editing Model {0}", modelArray.getJSONObject(i).getString("name"));
                    Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                    String jString = modelArray.getJSONObject(i).toString();
                    model = gson.fromJson(jString, Model.class);  
                    if ( configurationController == null ) {
                        configurationController = new ConfigurationJpaController();
                    }
                    Configuration config = configurationController.findConfiguration(model.getIdConfiguration());
                    dataString = DBHelper.getMetadata(config.getType(), config.getConnectionInfo(), config.getDbUser(), config.getDbPassword());
                    break; // process just first record, for now
                    }
                retryCount = retryMax;
            } catch (JSONException ex) {
                retryCount = retryMax;
                logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
                result = "MB0015 Exception while parsing JSON model description.";
            } catch (EntityNotFoundException ex) {
                retryCount = retryMax;
                logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
                result = "MB0016 Exception while retrieving table metadata. Model not found.";
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while retrieving Model: {0} {1}", new Object[]{model.getName(), ex});
                retryCount++;
                if ( retryCount < retryMax ) {
                   logger.log(Level.SEVERE, "Retrying to retrieve Model {0} retryCount {1}", new Object[]{model.getName(), Integer.toString(retryCount)});
                } else {
                    result = "MB0017 Database connection failed while retrieving table metadata. " + ex.getMessage();
                }
            }  
        }
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Metadata retrieved");   
                //success.put("totalCount", Integer.valueOf(recordCounter));
                metadata = new JSONArray(dataString);
                success.put("totalCount", Integer.valueOf(metadata.length()));
                success.put("children", metadata);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0018 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    } 

    public String getModelDetails(String load) {     
        String result = "MB0019 Model has no details.";
        String fieldString = "";
        String tableString = "";
        String relationshipString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONArray modelArray = null;        
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        List<ModelRelationship> mr = null;
        JSONArray fieldMetadataJSON;
        JSONArray tableJSON;        
        JSONArray relationshipJSON;
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving details for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                mr = getModelRelationships(model.getIdModel());
                for (int j=0; j<mr.size(); j++) {
                    if ( j != 0 ) {
                        relationshipString += ",";
                    }                    
                    relationshipString = relationshipString + new JSONObject(mr.get(j)).toString();  
                }                    
                mt = getModelTables(model.getIdModel());
                for (int j=0; j<mt.size(); j++) {
                    if ( j != 0 ) {
                        tableString += ",";
                    }                    
                    tableString = tableString + new JSONObject(mt.get(j)).toString();  
                }                
                mc = getModelColumns(mt);
                for (int j=0; j<mc.size(); j++) {
                    if ( j != 0 ) {
                        fieldString += ",";
                    }                    
                    fieldString = fieldString + new JSONObject(mc.get(j)).toString();  
                }
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
            result = "MB0020 Exception while parsing JSON model description.";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0021 Exception while retrieving model metadata. Model not found.\n" + ex.getMessage();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB022 Exception while retrieving model metadata.\n" + ex.getMessage();
        }  
        try {
            if ( "".equals(fieldString) && "".equals(tableString) && "".equals(relationshipString) ) {     
                if ( result.contains("Model has no details.") ) {
                    success.put("success", true);   
                } else {
                    success.put("success", false);                       
                }
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model details retrieved");   
                success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(mc.size()));
                success.put("modelColumns", fieldMetadataJSON);                
                tableJSON = new JSONArray("["+tableString+"]");
                success.put("modelTables", tableJSON);    
                relationshipJSON = new JSONArray("["+relationshipString+"]");
                success.put("modelRelationships", relationshipJSON);                   
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0023 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }

    /* evoked by Web Service */
    public String getModelColumns(String load) {     
        String result = "MB0024 Model has no columns.";
        String fieldString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONArray modelArray = null;        
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        List<ModelRelationship> mr = null;
        JSONArray fieldMetadataJSON;
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving details for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class);                
                mt = getModelTables(model.getIdModel());               
                mc = getModelColumns(mt);
                for (int j=0; j<mc.size(); j++) {
                    if ( j != 0 ) {
                        fieldString += ",";
                    }        
                    fieldString = fieldString + new JSONObject(mc.get(j)).toString();  
                }
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
            result = "MB0025 Exception while parsing JSON model description.";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0026 Exception while retrieving model metadata. Model not found.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0027 Exception while retrieving model metadata.";
        }  
        try {
            if ( "".equals(fieldString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model columns retrieved");   
                //success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(mc.size()));
                success.put("modelColumns", fieldMetadataJSON);                                
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0028 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }
    
    /* evoked by Web Service */
    public String getOrgModelColumns(String load, Integer currentOrgId) {     
        String result = "MB0029 Organization has no models with columns.";
        String fieldString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();       
        List<Object> modelArray = null;
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        JSONArray fieldMetadataJSON;
        int fieldCounter = 0;
        Model model = null;
        
        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }              
            modelArray = modelController.findOrgModelEntities(currentOrgId);
            for (int i=0; i<modelArray.size(); i++) {
                model = (Model)modelArray.get(i);                
                mt = getModelTables(model.getIdModel());               
                mc = getModelColumns(mt);
                if ( (i != 0) && ( mc.size() > 0) ) {
                    fieldString += ",";
                }                
                fieldCounter += mc.size();
                for (int j=0; j<mc.size(); j++) {
                    // inject model id as it is helpful in UI
                    ModelColumn mcol = mc.get(j);
                    mcol.setIdModel(model.getIdModel());
                    String chunk = new JSONObject(mcol).toString();
                    if ( !chunk.equals("") ) {
                        if ( j != 0 ) {
                            fieldString += ",";
                        }        
                        fieldString = fieldString + chunk;  
                        }
                    else {
                        logger.log(Level.SEVERE, "ModelColumn skipped: JSON object is null");
                    }
                    }
                }
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0030 Exception while retrieving model metadata. Model not found.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0031 Exception while retrieving model metadata.";
        }  
        try {
            if ( "".equals(fieldString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model columns retrieved");   
                //success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(fieldCounter));
                success.put("modelColumns", fieldMetadataJSON);                                
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0032 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }    

    public boolean cleanupAfterModelTableDelete(String JSONString, Integer userId) {
        Integer [] tableIdArray = null;
        
        try {       
            tableIdArray = getTableIdFromTableJSON(JSONString);
            if ( modelColumnController == null ) {
               modelColumnController = new ModelColumnJpaController();
            }     
            for (int i=0; i<tableIdArray.length; i++) {            
                List<ModelColumn> modelColumns = new ArrayList();  
                modelColumns = modelColumnController.findModelColumnByTable(tableIdArray[i]);
                for (ModelColumn mc : modelColumns) {
                    modelColumnController.deleteObject(mc, userId);
                }   
            }
            if ( modelRelationshipController == null ) {
               modelRelationshipController = new ModelRelationshipJpaController();
            }        
            for (int i=0; i<tableIdArray.length; i++) {            
                List<ModelRelationship> modelRelationships = new ArrayList();  
                modelRelationships = modelRelationshipController.findModelRelationshipByTable(tableIdArray[i]);
                for (ModelRelationship mr : modelRelationships) {
                    modelRelationshipController.deleteObject(mr, userId);
                }              
            }
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while cleaning after ModelTable delete {0}", ex);         
            return false;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while cleaning after ModelTable delete {0}", ex);
            return false;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while cleaning after ModelTable delete {0}", ex);
            return false;
        }   
        return true;
    }

    public String deleteModel(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        
        String resultString = "Model successfully deleted";       
        Integer oldModelId;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            oldModelId = ((Model)subject).getIdModel();
            if ( studyService == null ) {
                studyService = new StudyServiceImpl();
            } 
            resultString = studyService.findStudiesByModel((Model)subject);
            if (  !"".equals(resultString) ) {
                // there are Studies this Model supports - return immediately
                return wrapAPI(controller.getObjectType(), "", "Object deleted", resultString);                 
            } else {
                resultString = "MB0320 No Studies found. Model deleted.";
            }
            List<ModelRelationship> modelRelationships = getModelRelationships(oldModelId);                         
            List<ModelTable> modelTables = getModelTables(oldModelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);                         
            for (ModelRelationship mr : modelRelationships) {             
                modelRelationshipController.deleteObject(mr, currentUserId);  
            }   
            for (ModelColumn mc : modelColumns) {                          
                modelColumnController.deleteObject(mc, currentUserId);                           
            }            
            for (ModelTable mt : modelTables) {
                modelTableController.deleteObject(mt, currentUserId);
            }             
            controller.deleteObject(subject, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            resultString = "MB0033 Exception while deleting model: "  + ex.getMessage();
            JSONString = "";
        }  
        return wrapAPI(controller.getObjectType(), JSONString, "Object deleted", resultString);        
    }
    
    public String cloneModel(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        
        String resultString = "Model successfully cloned";       
        Integer oldModelId;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            oldModelId = ((Model)subject).getIdModel();
            ((Model)subject).setIdModel(0);
            ((Model)subject).setDescription("");
            ((Model)subject).setStatus("New");
            subject = controller.createObjects(subject, currentOrgId, currentUserId);
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});    
            return "MB0034 JSON Parsing exception while creating new " + controller.getObjectType();            
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            return "MB0035 Database exception while creating new " + controller.getObjectType();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            return "MB0036 Exception while creating new "  + controller.getObjectType();
        }
        
        try {
            Model m = (Model)subject;
            List<ModelRelationship> modelRelationships = getModelRelationships(oldModelId);                         
            List<ModelTable> modelTables = getModelTables(oldModelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);                      
            for (ModelTable mt : modelTables) {
                Integer oldTableId = mt.getIdModelTable();   
                if ( oldModelId.compareTo(mt.getModelidModel()) == 0 ) {
                    mt.setIdModelTable(0);
                    mt.setModelidModel(m.getIdModel());
                    mt = (ModelTable)modelTableController.createObjects(mt, currentUserId);
                }
                for (ModelColumn mc : modelColumns) {  
                    Integer oldColumnId = mc.getIdModelColumn();                            
                    if ( oldTableId.compareTo(mc.getModelTableidModelTable()) == 0 ) {
                        //ModelColumn newMc = mc;    
                        mc.setIdModelColumn(0);           
                        mc.setModelTableidModelTable(mt.getIdModelTable()); 
                        mc = (ModelColumn)modelColumnController.createObjects(mc, currentUserId);  
                    }
                    replaceTableKeyInRelationships(modelRelationships,oldColumnId,mc.getIdModelColumn(), mt.getIdModelTable());                             
                }
            }     
            for (ModelRelationship mr : modelRelationships) {             
                mr.setModelidModel(m.getIdModel());     
                mr.setIdModelRelationship(0);
                mr = (ModelRelationship)modelRelationshipController.updateObjects(mr, currentUserId);  
            }                                
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while cloning Model {0}", ex);         
            return "MB0037 JSON syntax exception while cloning Model";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while cloning Model {0}", ex);
            return "MB0038 Entity not found exception while cloning Model";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while cloning Model {0}", ex);
            return "MB0039 Database exception while cloning Model";
        }   
        return resultString;
    }

    public String exportModel(String JSONString, Integer currentOrgId, Integer currentUserId, Object subject) {
        
        String resultString = "Model not exported";   
        Integer modelId;
        Model model = null;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            modelId = ((Model)subject).getIdModel();
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            model = modelController.findModel(modelId);     
            JSONObject theModel = new JSONObject(model);
            String serializedModel = theModel.toString();
            //resultString = "{\"exportModel\":{\"version\":\"300\"},{\"model\":" + serializedModel;
            resultString = "{\"objectType\":\"Model\",\"version\":\"300\",\"model\":" + serializedModel;            
            //subject = controller.createObjects(subject, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while exporting Model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0040 Exception while exporting Model "  + model.getName();
        }
        
        try {
            List<ModelRelationship> modelRelationships = getModelRelationships(modelId);                         
            List<ModelTable> modelTables = getModelTables(modelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);    
            resultString += ",\"modelTables\":[";
            int i = 0; int j = 0;
            for (ModelTable mt : modelTables) {
                JSONObject theTable = new JSONObject(mt);
                String serializedTable = theTable.toString();
                if ( i++ != 0 ) {
                    resultString += ",";
                }                
                resultString += serializedTable;
            }   
            resultString += "],\"modelColumns\":[";            
            for (ModelColumn mc : modelColumns) {  
                JSONObject theColumn = new JSONObject(mc);
                String serializedColumn = theColumn.toString();  
                if ( j++ != 0 ) {
                    resultString += ",";
                }                    
                resultString += serializedColumn;
            }                  
            resultString += "],\"modelTableRelationships\":[";
            i = 0;
            for (ModelRelationship mr : modelRelationships) {             
                JSONObject theRelationship = new JSONObject(mr);
                String serializedRelationship = theRelationship.toString();  
                if ( i++ != 0 ) {
                    resultString += ",";
                }
                resultString += serializedRelationship;
            }  
            resultString += "]}";                
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while exporting Model {0}", ex);         
            return "MB0041 JSON syntax exception while exporting Model";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while exporting Model {0}", ex);
            return "MB0042 Entity not found exception while exporting Model";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while exporting Model {0}", ex);
            return "MB0043 Database exception while exporting Model";
        }   
        return resultString;
    }

    public String importModel(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        
        String resultString = "Model not imported.";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Integer modelId;
        Model model = new Model();
        JSONObject newModel;        
        JSONArray tableArray;
        JSONArray columnArray;
        JSONArray relationshipArray;       
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();    
        List<ModelTable> origMt = new ArrayList();
        List<ModelColumn> origMc = new ArrayList();
        List<ModelTable> newMt = new ArrayList();
        List<ModelColumn> newMc = new ArrayList();        
        
        try {
            JSONObject modelObject = impObject.getJSONObject("model");
            tableArray =  impObject.getJSONArray("modelTables");
            columnArray =  impObject.getJSONArray("modelColumns");
            relationshipArray =  impObject.getJSONArray("modelTableRelationships");            
            String jString = modelObject.toString();
            model = gson.fromJson(jString, model.getClass());    
            // re-use existing object to persist into new db record
            model.setIdModel(0);
            //model.setIdConfiguration(0); // force new user to pick db config
            model.setOrganization(currentOrgId);
            model.setName(model.getName());
            model.setStatus("Data Model imported");
            model.setIsAtomicFinished(Boolean.FALSE);
            model.setIsMissingFinished(Boolean.FALSE);
            model.setIsVirtualFinished(Boolean.FALSE);            
            model.setCreatedTS(currentTimestamp);
            model.setCreatedBy(currentUserId);        
            model.setUpdatedTS(currentTimestamp);
            model.setUpdatedBy(currentUserId);                  
            model = modelController.edit(model);     
            modelId = model.getIdModel();
            newModel = new JSONObject(model);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing Model: {0} {1}", new Object[]{model.getName(), ex});
            throw ex;
            //return "MB0044 Exception while importing Model "  + model.getName() + " " + ex.getMessage();
        }
       
        try {
   
            for (int i=0; i<tableArray.length(); i++) {
                ModelTable mt = new ModelTable();
                mt = gson.fromJson(tableArray.getString(i), mt.getClass());  
                origMt.add(i, mt);
                // create new entity
                ModelTable nMt = new ModelTable();
                nMt.setIdModelTable(0);
                nMt.setModelidModel(modelId);
                // copy old data
                nMt.setActive(mt.getActive());
                nMt.setName(mt.getName());
                nMt.setPhysicalName(mt.getPhysicalName());
                nMt.setPrimaryTable(mt.getPrimaryTable());
                nMt.setDescription(mt.getDescription());
                nMt.setCreatedTS(currentTimestamp);
                nMt.setCreatedBy(currentUserId);        
                nMt.setUpdatedTS(currentTimestamp);
                nMt.setUpdatedBy(currentUserId);                       
                nMt = modelTableController.edit(nMt);     
                newMt.add(i, nMt);       
            }   
            for (int j=0; j<columnArray.length(); j++) {  
                ModelColumn mc = new ModelColumn();
                mc = gson.fromJson(columnArray.getString(j), mc.getClass());   
                origMc.add(j, mc);
                // create new entity
                ModelColumn nMc = new ModelColumn();
                nMc.setIdModelColumn(0);
                nMc.setModelTableidModelTable(translateTableId(origMt, newMt, mc.getModelTableidModelTable()));
                nMc.setIdModel(modelId);
                // copy old data
                nMc.setActive(mc.getActive());
                nMc.setAtomic(mc.getAtomic());
                nMc.setDefaultValue(mc.getDefaultValue());
                nMc.setDefaultValueType(mc.getDefaultValueType());
                nMc.setDescription(mc.getDescription());
                nMc.setFieldClass(mc.getFieldClass());
                nMc.setFieldKind(mc.getFieldKind());
                nMc.setFormula(mc.getFormula());
                nMc.setInsertable(mc.getInsertable());
                nMc.setKeyField(mc.getKeyField());
                nMc.setName(mc.getName());
                nMc.setPhysicalName(mc.getPhysicalName());
                nMc.setSub(mc.getSub());
                nMc.setType(mc.getType());
                nMc.setVerified(mc.getVerified());
                nMc.setVirtual(mc.getVirtual());
                
                nMc.setCreatedTS(currentTimestamp);
                nMc.setCreatedBy(currentUserId);        
                nMc.setUpdatedTS(currentTimestamp);
                nMc.setUpdatedBy(currentUserId);                     
                nMc = modelColumnController.edit(nMc);     
                newMc.add(j, nMc);
            }            
            for (int i=0; i<relationshipArray.length(); i++) {             
                ModelRelationship mr = new ModelRelationship();
                mr = gson.fromJson(relationshipArray.getString(i), mr.getClass());   
                mr.setIdModelRelationship(0);
                mr.setTable1(translateTableId(origMt, newMt, mr.getTable1()));
                mr.setTable2(translateTableId(origMt, newMt, mr.getTable2()));                
                mr.setKey1(translateKeyId(origMc, newMc, mr.getKey1()));
                mr.setKey2(translateKeyId(origMc, newMc, mr.getKey2()));                 
                mr.setModelidModel(modelId);
                mr.setCreatedTS(currentTimestamp);
                mr.setCreatedBy(currentUserId);        
                mr.setUpdatedTS(currentTimestamp);
                mr.setUpdatedBy(currentUserId);                         
                mr = modelRelationshipController.edit(mr); 
            }  
            resultString = newModel.toString();     
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while importing Model {0}", ex);         
            throw ex;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while importing Model {0}", ex);
            throw ex;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while importing Model {0}", ex);
            throw ex;
        }  
        model = null;
        tableArray = null;
        columnArray = null;
        relationshipArray = null;       
        gson = null;    
        origMt = null;
        origMc = null;
        newMt = null;
        newMc = null;         
        return resultString;
    }    

    public String importModelWithOverwrite(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        
        String resultString = "MB0012 Model import failed";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Integer modelId;
        Model model = new Model();
        JSONObject newModel;        
        JSONArray tableArray;
        JSONArray columnArray;
        JSONArray relationshipArray;     
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();    
        List<Model> origModel;
        List<ModelTable> dbMt;
        List<ModelColumn> dbMc;
        List<ModelRelationship> dbMr;                 
        List<ModelTable> impMt = new ArrayList();
        List<ModelColumn> impMc = new ArrayList();     
        List<ModelRelationship> impMr = new ArrayList();                  
        
        try {
            JSONObject modelObject = impObject.getJSONObject("model");
            tableArray =  impObject.getJSONArray("modelTables");
            columnArray =  impObject.getJSONArray("modelColumns");
            relationshipArray =  impObject.getJSONArray("modelTableRelationships");            
            String jString = modelObject.toString();
            model = gson.fromJson(jString, model.getClass());    
            if ( modelTableController == null ) {
               modelTableController = new ModelTableJpaController();
            }               
            if ( modelColumnController == null ) {
               modelColumnController = new ModelColumnJpaController();
            }              
            if ( modelRelationshipController == null ) {
               modelRelationshipController = new ModelRelationshipJpaController();
            }              
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            origModel = modelController.findModelByName(model.getName());                        
            if ( origModel.isEmpty() ) {
                // create brand new model
                return importModel(impObject, currentOrgId, currentUserId);
            }
            model.setIdModel(origModel.get(0).getIdModel());
            model.setStatus("Data Model Imported");    
            model.setIsAtomicFinished(Boolean.FALSE);
            model.setIsMissingFinished(Boolean.FALSE);
            model.setIsVirtualFinished(Boolean.FALSE);
            model.setUpdatedTS(currentTimestamp);
            model.setUpdatedBy(currentUserId);               
            model = modelController.edit(model);   
            AuditLogger.log(currentUserId, model.getIdModel(), "import", model);
            modelId = model.getIdModel();
            newModel = new JSONObject(model);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing Model: {0} {1}", new Object[]{model.getName(), ex});
            throw ex;
        }
       
        try {
            // 1. parse imported table information
            for (int i=0; i<tableArray.length(); i++) {
                ModelTable mt = new ModelTable();
                mt = gson.fromJson(tableArray.getString(i), mt.getClass());  
                impMt.add(i, mt);
            }
            // 2. retreive from db original table information
            dbMt = getModelTables(modelId);
            // 3. save all refreshed tables and check if there are new tables and add them to dtMt list
            for (int i=0; i<impMt.size(); i++) {
                ModelTable nMt = new ModelTable();      
                nMt.setModelidModel(modelId);
                // copy imported data
                nMt.setActive(impMt.get(i).getActive());
                nMt.setName(impMt.get(i).getName());
                nMt.setPhysicalName(impMt.get(i).getPhysicalName());
                nMt.setPrimaryTable(impMt.get(i).getPrimaryTable());
                nMt.setDescription(impMt.get(i).getDescription());      
                nMt.setUpdatedTS(currentTimestamp);
                nMt.setUpdatedBy(currentUserId);                  
                if ( isTableNew(impMt.get(i), dbMt) == true ) {
                    // for all new imported tables create entity
                    nMt.setIdModelTable(0);
                    nMt.setCreatedTS(currentTimestamp);
                    nMt.setCreatedBy(currentUserId);     
                    nMt = modelTableController.edit(nMt);     
                    AuditLogger.log(currentUserId, nMt.getIdModelTable(), "import insert", nMt);
                    dbMt.add(nMt);                    
                } else {
                    nMt.setIdModelTable(translateOverwrittenTableId(dbMt,impMt,impMt.get(i).getIdModelTable()));
                    nMt.setCreatedTS(impMt.get(i).getCreatedTS());
                    nMt.setCreatedBy(impMt.get(i).getCreatedBy());  
                    nMt = modelTableController.edit(nMt);     
                    AuditLogger.log(currentUserId, nMt.getIdModelTable(), "import update", nMt);                    
                }                       
            }

            // 1. parse imported column information
            for (int j=0; j<columnArray.length(); j++) {
                ModelColumn mc = new ModelColumn();
                mc = gson.fromJson(columnArray.getString(j), mc.getClass());   
                impMc.add(j, mc);
            }     
            // 2. retreive from db original column/field information
            dbMc = getModelColumns(dbMt);          
            // 3. save all refreshed coumns/fields and check if there are new columns and add them to dbMc list
            for (int j=0; j<impMc.size(); j++) {  
                ModelColumn nMc = new ModelColumn();
                nMc.setIdModel(modelId);
                // copy imported data
                nMc.setActive(impMc.get(j).getActive());
                nMc.setAtomic(impMc.get(j).getAtomic());
                nMc.setDefaultValue(impMc.get(j).getDefaultValue());
                nMc.setDefaultValueType(impMc.get(j).getDefaultValueType());
                nMc.setDescription(impMc.get(j).getDescription());
                nMc.setFieldClass(impMc.get(j).getFieldClass());
                nMc.setFieldKind(impMc.get(j).getFieldKind());
                nMc.setFormula(impMc.get(j).getFormula());
                nMc.setInsertable(impMc.get(j).getInsertable());
                nMc.setKeyField(impMc.get(j).getKeyField());
                nMc.setName(impMc.get(j).getName());
                nMc.setPhysicalName(impMc.get(j).getPhysicalName());
                nMc.setSub(impMc.get(j).getSub());
                nMc.setType(impMc.get(j).getType());
                nMc.setVerified(impMc.get(j).getVerified());
                nMc.setVirtual(impMc.get(j).getVirtual());   
                nMc.setModelTableidModelTable(translateOverwrittenTableId(dbMt,impMt,impMc.get(j).getModelTableidModelTable()));                                    
                nMc.setUpdatedTS(currentTimestamp);
                nMc.setUpdatedBy(currentUserId);         
                if ( isColumnNew(impMc.get(j), dbMc) == true ) {    
                    nMc.setIdModelColumn(0);                    
                    nMc.setCreatedTS(currentTimestamp);
                    nMc.setCreatedBy(currentUserId);                           
                    nMc = modelColumnController.edit(nMc);    
                    AuditLogger.log(currentUserId, nMc.getIdModelColumn(), "import insert", nMc);
                    //newMc.add(nMc);
                    dbMc.add(nMc);
                } else {
                    nMc.setIdModelColumn((translateOverwrittenKeyId(dbMc, impMc, impMc.get(j).getIdModelColumn())));
                    nMc.setCreatedTS(impMc.get(j).getCreatedTS());
                    nMc.setCreatedBy(impMc.get(j).getCreatedBy());                          
                    nMc = modelColumnController.edit(nMc);    
                    AuditLogger.log(currentUserId, nMc.getIdModelColumn(), "import update", nMc);                        
                }
            }            

            // 1. parse imported relationship information
            for (int j=0; j<relationshipArray.length(); j++) {
                ModelRelationship mr = new ModelRelationship();
                mr = gson.fromJson(relationshipArray.getString(j), mr.getClass());  
                impMr.add(j, mr);
            } 
            // 2. retreive from db original relationship information
            dbMr = getModelRelationships(modelId);          
            // 3. check if there are new relations and save them in db            
            for (int i=0; i<relationshipArray.length(); i++) {        
                if ( isRelationshipNew(dbMt, impMt, impMr.get(i), dbMr) == true ) {                
                    ModelRelationship mr = new ModelRelationship();
                    mr = gson.fromJson(relationshipArray.getString(i), mr.getClass());   
                    mr.setIdModelRelationship(0);
                    mr.setTable1(translateOverwrittenTableId(dbMt, impMt, mr.getTable1()));
                    mr.setTable2(translateOverwrittenTableId(dbMt, impMt, mr.getTable2()));                
                    mr.setKey1(translateOverwrittenKeyId(dbMc, impMc, mr.getKey1()));
                    mr.setKey2(translateOverwrittenKeyId(dbMc, impMc, mr.getKey2()));                 
                    mr.setModelidModel(modelId);
                    mr.setCreatedTS(currentTimestamp);
                    mr.setCreatedBy(currentUserId);        
                    mr.setUpdatedTS(currentTimestamp);
                    mr.setUpdatedBy(currentUserId);                      
                    mr = modelRelationshipController.edit(mr); 
                    AuditLogger.log(currentUserId, mr.getIdModelRelationship(), "import insert", mr);
                }
            }  
            resultString = newModel.toString();           
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while importing Model {0}", ex);         
            throw ex;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while importing Model {0}", ex);
            throw ex;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while importing Model {0}", ex);
            throw ex;
        }  
        model = null;
        tableArray = null;
        columnArray = null;
        relationshipArray = null;       
        gson = null;    
        dbMt = null;
        dbMc = null;
        dbMr = null;
        impMt = null;
        impMc = null;
        impMr = null;        
        return resultString;
    }  
  
    private boolean isTableNew(ModelTable mt, List<ModelTable> dbMtList) {
        boolean isNew = true;
        for (ModelTable dbMtList1 : dbMtList) {
            if (dbMtList1.getName() == null ? mt.getName() == null : dbMtList1.getName().equals(mt.getName())) {
                isNew = false;
                break;
            }
        }
        return isNew;
    } 
  
    private boolean isColumnNew(ModelColumn mc, List<ModelColumn> dbMcList) {
        boolean isNew = true;
        for (ModelColumn dbMcList1 : dbMcList) {
            if (dbMcList1.getName() == null ? mc.getName() == null : dbMcList1.getName().equals(mc.getName())) {
                isNew = false;
                break;
            }
        }
        return isNew;
    } 
  
    private boolean isRelationshipNew(List<ModelTable> dbMt, List<ModelTable> impMt, ModelRelationship impMr, List<ModelRelationship> dbMrList) {
        boolean isNew = true;
        int table1id = translateOverwrittenTableId(dbMt, impMt, impMr.getTable1());
        int table2id = translateOverwrittenTableId(dbMt, impMt, impMr.getTable2()); 
        
        for (ModelRelationship dbMr : dbMrList) {
            if ( (dbMr.getTable1() == table1id) && (dbMr.getTable2() == table2id) ) {
                isNew = false;
                break;
            }
        }
        return isNew;
    }
    
    // this method is invoked from WS
    public String getModelStatus(String load, Integer currentUserId) {
        JSONArray modelArray = null;        
        Model model = null;
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                model = modelController.findModel(model.getIdModel());
                break; // process just first record, for now
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0063 Exception while reading model status " + ex.getMessage();
        }       
       return model.getStatus();
    }        
    
    // this method is invoked from WS
    public String setModelStatus(String load, Integer currentUserId, String status) {
        JSONArray modelArray = null;        
        Model model = null;
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");
            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                if ( !"Temp status".equals(status) ) {
                    model.setStatus(status);
                }
                modelController.updateObjects(model, currentUserId);
                break; // process just first record, for now
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0064 Exception while updating model status " + ex.getMessage();
        }       
       return status; // mirror back to browser
    }        

    public int setModelStatus(Model model, Integer currentUserId, String status) {           
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            model.setStatus(status);
            modelController.updateObjects(model, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    }  

    public int setModelStatus(Model model, Integer currentUserId, String status, boolean a, boolean m, boolean v) {           
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            model.setIsAtomicFinished(a);
            model.setIsMissingFinished(m);
            model.setIsVirtualFinished(v);
            model.setStatus(status);
            modelController.updateObjects(model, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    } 
    
    public int setModelStatusByColumn(String JSONColumn, Integer currentUserId, String status) {     
        ModelColumn modelColumn = new ModelColumn();
        ModelTable modelTable = null;
        Model model = null;
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }        
        try {     
            JSONArray objectArray =  new JSONArray(JSONColumn);
            for (int i=0; i<objectArray.length(); i++) {            
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                modelColumn = gson.fromJson(jString, modelColumn.getClass());
                modelTable = modelTableController.findModelTable(modelColumn.getModelTableidModelTable());
                model = modelController.findModel(modelTable.getModelidModel());
                try {
                    model.setStatus(status);
                    modelController.updateObjects(model, currentUserId);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
                    return -1;
                }
            }
            return model.getIdModel();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing coluns/tables to update status of model: {0} {1}", new Object[]{model.getName(), ex});
            return -2;
        }  
    }  
 
    public int setModelStatusByTable(String JSONTable, Integer currentUserId, String status) {     
        ModelTable modelTable = new ModelTable();
        Model model = null;
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }        
        try {     
            JSONArray objectArray =  new JSONArray(JSONTable);
            for (int i=0; i<objectArray.length(); i++) {            
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                modelTable = gson.fromJson(jString, modelTable.getClass());
                model = modelController.findModel(modelTable.getModelidModel());
                try {
                    model.setStatus(status);
                    modelController.updateObjects(model, currentUserId);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
                    return -1;
                }
            }
            return model.getIdModel();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing table JSON to update status of model: {0} {1}", new Object[]{model.getName(), ex});
            return -2;
        }  
    } 
    
    private Integer translateTableId(List<ModelTable> origMt, List<ModelTable> newMt, int current) {
        for (int i=0; i<origMt.size(); i++) {
            if ( origMt.get(i).getIdModelTable() == current ) {
                return newMt.get(i).getIdModelTable();
            }
        }        
        return 0;
    }

    
    private Integer translateOverwrittenTableId(List<ModelTable> dbMtList, List<ModelTable> impMtList, int current) {
        String nameToSearch = null;
        for (ModelTable impMt : impMtList) {
            if (impMt.getIdModelTable() == current) {
                nameToSearch = impMt.getName();
            }
        }        
        for (ModelTable dbMt : dbMtList) {
            if (dbMt.getName() == null ? nameToSearch == null : dbMt.getName().equals(nameToSearch)) {
                return dbMt.getIdModelTable();
            }
        }        
        return 0;
    }
    
    private Integer translateKeyId(List<ModelColumn> origMc, List<ModelColumn> newMc, int current) {
        for (int i=0; i<origMc.size(); i++) {
            if ( origMc.get(i).getIdModelColumn() == current ) {
                return newMc.get(i).getIdModelColumn();
            }
        }        
        return 0;
    }    
    
    private Integer translateOverwrittenKeyId(List<ModelColumn> dbMcList, List<ModelColumn> impMcList, int current) {
        String nameToSearch = null;
        for (ModelColumn impMc : impMcList) {
            if (impMc.getIdModelColumn() == current) {
                nameToSearch = impMc.getName();
            }
        }          
        for (ModelColumn dbMc : dbMcList) {
            if ( dbMc.getName() == null ? nameToSearch == null : dbMc.getName().equals(nameToSearch) ) {
                return dbMc.getIdModelColumn();
            }
        }        
        return 0;
    }  
    
    private String getColumnsOmitSubs(Model m) {
        String result = "";
        List<ModelTable> mt = getModelTables(m.getIdModel());
        List<ModelColumn> mc = getModelColumns(mt);       
        for (ModelColumn modelColumn : mc) {       
            if ( modelColumn.getSub() == false ) {
                result += modelColumn.getName() + ",";
            }
        }          
        return Util.removeLastChar(result);
    } 
    
    private String getAllColumns(Model m) {
        String result = "";
        List<ModelTable> mt = getModelTables(m.getIdModel());
        List<ModelColumn> mc = getModelColumns(mt);       
        for (ModelColumn modelColumn : mc) {       
                result += modelColumn.getName() + ",";
        }          
        return Util.removeLastChar(result);
    }     
    
    public JSONObject submitFlatTabletoProxy(Process process, String filterWhereClause, String methodName, String orderField, boolean useSubs, Proxy proxy) throws Exception {
        JobRequest job = null;
        JSONObject jobObject = null;

        try {
            Query queryResult;
            if ( modelController == null ) {
                modelController = new ModelJpaController();
            }
            Model model = modelController.findModel(process.getIdModel());
            if ( model == null ) return null;        
            String sqlStmt;       
            if ( useSubs == false ) {
                sqlStmt = "select " + getAllColumns(model) + " from ";
            } else {
                sqlStmt = "select * from ";
            }
            int colCount = getTableWidth(model.getOutputName());
            
            if ( filterWhereClause == null ) {
                sqlStmt += model.getOutputName();
            } else {
                sqlStmt += model.getOutputName() + " WHERE " + filterWhereClause; 
            }
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);

            List<Object[]> resultValues = queryResult.getResultList();
            int rowCount = resultValues.size();
            logger.log(Level.SEVERE, "Reserving memory for array[rows-{0},columns-{1}]", new Object[]{Integer.toString(rowCount), Integer.toString(colCount)});
            double[] data = new double[rowCount * colCount];
            // changes to double array will be visible after return
            String returnString = getObjectDataIntoDoubleArray(model.getOutputName(), filterWhereClause, orderField, "ASC", data);
            if ( !"OK".equals(returnString) ) throw new IOException(returnString);

            String memKey = proxy.storeDoubleArray(data);   
            if ( memKey == null ) return null;
            //model.getOutputName();
            String columnString = getDBObjectColumns(model.getOutputName(), filterWhereClause); 
            job = new JobRequest(process);     
            job.setMemoryKey(memKey);  
            job.setMethod(methodName);
            job.setColumnCount(colCount);
            job.setColumns(columnString);
            JSONArray columns = new JSONArray(columnString);
            jobObject = new JSONObject(job);
            jobObject.put("columns", columns);            
            return jobObject;
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Error while populating double array before submission: ", ex.getMessage());
            throw ex;
        }
    }
    
    private List<ModelRelationship> replaceTableKeyInRelationships(List<ModelRelationship> modelRelations, Integer oldColumnId, Integer newColumnId, Integer newTableId) {
        for (ModelRelationship mr: modelRelations) {
            if ( oldColumnId.compareTo(mr.getKey1()) == 0 ) {
                mr.setTable1(newTableId);
                mr.setKey1(newColumnId);                                                            
            }      
            if ( oldColumnId.compareTo(mr.getKey2()) == 0 ) {                           
                mr.setTable2(newTableId);
                mr.setKey2(newColumnId);                                                                        
            }             
        }
        return modelRelations;
    }
    
    /* get ModelId from Model JSON, not used */
    public int getModelId(String load) {     
        JSONArray modelArray = null;        
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving ModelId: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    }
 
    /* get TableId from Column JSON */
    public int getTableId(String load) {     
        JSONArray modelArray = null;        
        ModelColumn column = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("columns");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving TableId for Column {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                column = gson.fromJson(jString, ModelColumn.class); 
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving ModelId: {0} {1}", new Object[]{column.getName(), ex});
            return 0;
        }  
        
       return column.getModelTableidModelTable();
    }
  
     /* get TableIds from Table JSON */
    public Integer[] getTableIdFromTableJSON(String load) {     
        Integer [] tableIdArray = null;
        try {
            JSONArray objectArray =  new JSONArray(load);
            tableIdArray = new Integer[objectArray.length()];
            for (int i=0; i<objectArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                ModelTable subject = gson.fromJson(jString, ModelTable.class); 
                logger.log(Level.INFO, "Retrieving TableIds from JSON: found id {0}", Integer.toString(subject.getIdModelTable()));                
                tableIdArray[i] = subject.getIdModelTable();
                if ( i == (max_tables-1) ) break;
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving TableIds and processing JSON: {0}", ex);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving TableIds from JSON: {0}", ex);
            return null;
        }  
        
       return tableIdArray;
    }
    
    static public Model parseModelFromJSON(String modelJSON) {
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(modelJSON);
            JSONArray modelArray = JSONContent.getJSONArray("models");
            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Processing Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class);  
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
        } 
        return model;
    }
    
    public List<ModelRelationship> getModelRelationships(int modelId)
    {
        if ( modelRelationshipController == null ) {
           modelRelationshipController = new ModelRelationshipJpaController();
        }    
        List<ModelRelationship> modelRelationships = modelRelationshipController.findModelRelationshipByModelId(modelId);
        return modelRelationships;
    }
    
    public List<ModelTable> getModelTables(int modelId)
    {
        if ( modelTableController == null ) {
           modelTableController = new ModelTableJpaController();
        }    
        List<ModelTable> modelTables = modelTableController.findModelTableByModel(modelId);
        return modelTables;
    }
    
    public List<ModelColumn> getModelColumns(List<ModelTable> modelTables)
    {
        if ( modelColumnController == null ) {
           modelColumnController = new ModelColumnJpaController();
        }           
        List<ModelColumn> modelColumns = new ArrayList();     
        
        for (ModelTable mt : modelTables) {
            modelColumns.addAll(modelColumnController.findModelColumnByTable(mt.getIdModelTable()));
        }
        return modelColumns;
    }    
 
    public int getTableWidth(Process process) {
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            Model model = modelController.findModel(process.getIdModel());
            if ( model == null ) {
                logger.log(Level.SEVERE, "Model not found while getting table width: {0}", process.getIdModel().toString());
                return -1;
            }
            return getTableWidth(model.getOutputName());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting table width: {0}", ex);
            return -1;
        }
    }
    
    //--------------------------------------------------------------------------
    public int getTableWidth(String tableName) {
        try {          
            //String sqlTestStmt = "SELECT * FROM " + tableName + " LIMIT 1";
            // following is not optimal, but necessare for quick Microsoft fix!
            String sqlTestStmt = "SELECT * FROM " + tableName;
            Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);

            List<Object[]> results = queryResult.getResultList();
            int count = results.get(0).length;         
            return count;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting number of table columns: {0}", ex);
            return -1;
        }
    }    
    
    private ModelTable lookupModelTables(List<ModelTable> modelTables, int id) {
        for (ModelTable mt : modelTables) {
            if ( mt.getIdModelTable() == id ) {
                return mt;
            }
        }
        return null;
    }
    
    private ModelColumn lookupModelColumns(List<ModelColumn> modelColumns, int id) {
        for (ModelColumn mc : modelColumns) {
            if ( mc.getIdModelColumn() == id ) {
                return mc;
            }
        }
        return null;
    }
  
    private List<ModelColumn> filterInsertableAtomicModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getInsertable() == true) && (mc.getAtomic() == true)) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }

    private List<ModelColumn> filterInsertableAtomicNoScalarsModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getInsertable() == true) && (mc.getAtomic() == true) 
                    && (("".equals(mc.getFormula())) || (mc.getSub() == true))) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }    
    
    private List<ModelColumn> filterVirtualModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getVirtual()== true) ) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }
    
    private int lookupModelByTableId(int id) {
        ModelTable mt = modelTableController.findModelTable(id);
        return mt.getModelidModel();
    }
    
    private int lookupTableByColumnId(int id) {
        ModelColumn mc = modelColumnController.findModelColumn(id);
        return mc.getModelTableidModelTable();
    }
    
    private boolean checkIfTableIsEmpty(String tableName) {
        try {
            String sqlTestStmt = "SELECT * FROM " + tableName + " LIMIT 1";
            Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);
            queryResult.getSingleResult();
            List<Object[]> results = queryResult.getResultList();
            if ( results.get(0) == null) {
                return true;
            } else {
                return false;        
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while checking if table is empty: {0}", ex);
            return true;
        }
    }
    
    /* get Primary Key for target OST table from Column JSON */
    /* Primary Key is by default the first key of Primary Table, this may need to change */
    private ModelColumn getPrimayKeyField(List<ModelTable> mt, List<ModelColumn> mc) {  
        int primaryTableId = 0; 
        for (ModelTable modelTable : mt) {
            if ( modelTable.getPrimaryTable() == true ) {
                primaryTableId = modelTable.getIdModelTable();
                break;
            }
        }   
        for (ModelColumn modelColumn : mc) {
            if ( (modelColumn.getKeyField() == true) 
                    && (modelColumn.getModelTableidModelTable() == primaryTableId) ) {
                return modelColumn;
            }
        }       
        return null;
    }
    
    // this is MySQL specific, JPQL does not support create table
    private String constructCreateStatement(String tableName, List<ModelColumn> modelColumns, List<ModelTable> modelTables, List<ModelRelationship> modelRelationships) {
        String sqlStmt = "CREATE TABLE `" + tableName + "` (`id` INT(11) NOT NULL AUTO_INCREMENT,";
        // process all the fields but exclude non-insertable and non-atomic ones
        List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicModelColumns(modelColumns);
        for (ModelColumn mc : modelColumnsInsertable) { // needs quotes around strings and time stamps
            sqlStmt = sqlStmt + "`" + mc.getName() + "` " + mc.getType() + " DEFAULT " + mc.getDefaultValue() + ",";
        }  
        // process all one-to-many Relationships looking for key2 (many)
        for (ModelColumn mc : modelColumns) { 
            if ( (mc.getAtomic() == true) && (mc.getKeyField() == true) ) {
                for (ModelRelationship mr : modelRelationships) {
                    if ("2".equals(mr.getType()) && (mc.getIdModelColumn().compareTo(mr.getKey2()) == 0) && (!"".equals(mc.getFormula()))) {                
                        try {
                            String tn = lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName();
                            String keyFieldType = getFieldType(mc, tn);
                            if ( keyFieldType != null ) {
                                sqlStmt = sqlStmt + "`" + mc.getName() + "` " + keyFieldType + ",";
                            }
                        } catch (PersistenceException ex) {
                            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0001 Error while constructing CREATE statement. ", ex);            
                            return "MB0001 SQL Error while constructing CREATE statement.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
                        }  catch (Exception ex) {
                            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0001 SQL Error while constructing CREATE statement.", ex.getMessage());
                        }
                    }   
                }
            }
        }
        sqlStmt += " PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        return sqlStmt;
    }

    private String getOriginalFieldType(ModelColumn mc, String tableName) throws Exception {
         String sqlTestStmt = null;
         try {
             sqlTestStmt = "SELECT " + mc.getName() + " FROM " + tableName + " LIMIT 1";
             logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
             Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);
             queryResult.getSingleResult();
             List<Object[]> results = queryResult.getResultList();
             return getDBObjectType(results.get(0));
         } catch (Exception ex) {
             logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
             throw ex;
         }
    }
 
    private String getFieldType(ModelColumn mc, String tableName) throws Exception {
         String sqlTestStmt = null;
         try {
             sqlTestStmt = "SELECT " + mc.getFormula() + " FROM " + tableName + " LIMIT 1";
             logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
             Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);
             queryResult.getSingleResult();
             List<Object[]> results = queryResult.getResultList();
             return getDBObjectType(results.get(0));
         } catch (Exception ex) {
             logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
             throw ex;
         }
    }
    
    private String getDBObjectType(Object obj) {
        if (obj instanceof String) {
            int len = ((String)obj).length();
            return "VARCHAR(256)"; // how to find true size?
        }
        if (obj instanceof Integer) {
            return "INTEGER(11)";
        }   
        if (obj instanceof BigInteger) {
            return "INTEGER(11)";
        }        
        if (obj instanceof BigDecimal) {
            return "DECIMAL(10,2)";
        }         
        if (obj instanceof Double) {
            return "DOUBLE";                 
        }  
        if (obj instanceof Character) {
            return "CHAR";
        }    
        if (obj instanceof Date) {
            return "DATE";
        }                   
        if (obj instanceof Timestamp) {
            return "TIMESTAMP";
        }      
        return "VARCHAR(256)"; // default 
    }

    private String populateFlatTableJDBC(Model model, List<ModelRelationship> modelRelationships, List<ModelTable> modelTables, List<ModelColumn> modelColumns, Integer currentUserId) {
        //Integer tempFieldGen = 1;
        //Integer tempTableGen = 1;   
        ResultSet queryResult = null;
    
        try {
            List<ModelColumn> primaryKeys = new ArrayList();        
            if ( modelTableController == null ) {        
                modelTableController = new ModelTableJpaController();
            }    
            List<ModelTable> modelTablePrime = modelTableController.findModelTableByPrimary(model.getIdModel(), true);
            if ( modelTablePrime.size() == 0 ) {
               return "MB0048 Error. Primary Table not found."; 
            }
            String primaryTablePhName = modelTablePrime.get(0).getPhysicalName();
            
            logger.log(Level.INFO, "Found primary table {0} for Model {1}. Ignoring other tables if present.", new Object[]{primaryTablePhName, modelTablePrime.get(0).getModelidModel()});
            
            String sqlStmtSelectJoin = "SELECT "; 
            String sqlStmtJoin = " FROM ";
            String sqlStmtInsert = "INSERT INTO " + model.getOutputName() + " ("; 
            
            // extract all primary table keys
            for (ModelColumn mc : modelColumns) {
                if ( (mc.getModelTableidModelTable().intValue() == modelTablePrime.get(0).getIdModelTable().intValue()) && (mc.getKeyField() == true) ) {
                    primaryKeys.add(mc);
                }
            }
            
            // include in final insert all the insertable and atomic fields
            //List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicModelColumns(modelColumns);
            // except those that have formula and are not sub - used for scalar one-to-many operations
            List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);
            for (ModelColumn mc : modelColumnsInsertable) {
                    sqlStmtInsert = sqlStmtInsert + mc.getName() + ",";
            }       

            // include in SELECT for JOIN all the insertable and atomic fields but exclude scalar fields
            // except those that have formula and are not sub - used for scalar one-to-many operations
            modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);
            for (ModelColumn mc : modelColumnsInsertable) {
                    sqlStmtSelectJoin = sqlStmtSelectJoin + " " + lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName() + ".";
                    sqlStmtSelectJoin = sqlStmtSelectJoin + mc.getPhysicalName() + ",";
            }    
            
            sqlStmtJoin += primaryTablePhName;
            
            // join syntax is MySQL specific
            // construct as many JOINs as releationship records   
            // still needs branch for scalar joins
            for (ModelRelationship mr : modelRelationships) {
                if ( "1".equals(mr.getType()) ) { // regular join
                    sqlStmtJoin = sqlStmtJoin + " LEFT JOIN " 
                        + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName() + " ON ";
                    // loop for compound keys
                    for (int i=0; i<primaryKeys.size(); i++) {
                        if ( i > 0 ) {
                            sqlStmtJoin = sqlStmtSelectJoin + " AND ";
                        }
                        sqlStmtJoin = sqlStmtJoin + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName()
                            + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                            + " = " + lookupModelTables(modelTables, mr.getTable1()).getPhysicalName() 
                            + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();
                    }
                } else {
                    Integer tempFieldGenStart = tempFieldGen;
                    sqlStmtJoin = sqlStmtJoin + " LEFT JOIN ( SELECT " 
                        + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + formatAtomicNonKeyTableColumns(modelColumns, mr.getTable2())    
                        + ", " + formatFormulaTableColumnsForScalar(modelColumns, mr.getTable2())
                        + " FROM " + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName()
                        + " GROUP BY " + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + ") AS D3TEMP_TABLE" + tempTableGen.toString() + " ON ";

                    sqlStmtJoin = sqlStmtJoin + "D3TEMP_TABLE" + tempTableGen.toString()
                        + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                        + " = " + lookupModelTables(modelTables, mr.getTable1()).getPhysicalName() 
                        + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();   

                    // add fields from scalar functions using their new logical/temp names
                    sqlStmtInsert = sqlStmtInsert + formatFormulaTableColumns(modelColumns, mr.getTable2()) + ",";
                    for ( ;tempFieldGenStart<tempFieldGen; tempFieldGenStart++) {
                        sqlStmtSelectJoin += " D3TEMP_TABLE" + tempTableGen.toString() 
                                + ".D3TEMP_FIELD" + tempFieldGenStart.toString() + ",";
                    }                                   
                    sqlStmtSelectJoin = replaceNameWithAlias(sqlStmtSelectJoin,
                                                lookupModelTables(modelTables, mr.getTable2()).getPhysicalName(),
                                                "D3TEMP_TABLE" + tempTableGen.toString());                   
                    tempFieldGen++;
                    tempTableGen++;                    
                }
            }                
            sqlStmtSelectJoin = Util.removeLastChar(sqlStmtSelectJoin);
            logger.log(Level.INFO, "Executing SQL statement: {0} {1}", new Object[]{sqlStmtSelectJoin, sqlStmtJoin}); 
            setModelStatus(model, currentUserId, "Fetching atomic fields");
            if ( configurationController == null ) {
                configurationController = new ConfigurationJpaController();
            }            
            Configuration conConf = configurationController.findConfiguration(model.getIdConfiguration());
            
            ResultSet queryCountResult;  
            queryCountResult = DBHelper.executeJdbcQuery(conConf,"select count(*) as recordcount " + sqlStmtJoin,logger);
            queryCountResult.next();
            String totalCount = queryCountResult.getString("recordcount");             
            
            queryResult = DBHelper.executeJdbcQuery(conConf,sqlStmtSelectJoin + sqlStmtJoin,logger);
            sqlStmtInsert = Util.removeLastChar(sqlStmtInsert) + ") VALUES (";      
            int columnCount = queryResult.getMetaData().getColumnCount();  
            Object[] obj = new Object[columnCount];
            // for each row of data retieved insert new row of data into DST table
            long counter = 0;
            while (queryResult.next()) {
                String sqlStmtInsertData;     
                for (int i = 0; i < columnCount; i++) {        
                    obj[i] = queryResult.getObject(i+1);
                }
                sqlStmtInsertData = getInsertValues(obj);   
                sqlStmtInsertData = Util.removeLastChar(sqlStmtInsertData);
                sqlStmtInsertData += ")";
                // later add ability to populate memory instead of saving to DB
                // which could be followed immediately with call to StatProxy for statistical processing
                DBHelper.executeSqlStatement("DeltaPU",sqlStmtInsert + sqlStmtInsertData,logger);  
                //DBHelper.executeJdbcStatement(conConf,sqlStmtInsert + sqlStmtInsertData,logger);  
                counter++;
                if ( counter%1000 == 0 ) {
                    setModelStatus(model, currentUserId, "Atomic fields processed: " + Long.toString(counter) + " records of " + totalCount);
                }
            }

            return "Ok";
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0003 Error while constructing join statement", ex);
            return "MB0049 Error while processing atomic fields.\n" + ex.getMessage();                
        } finally {
            if ( queryResult != null ) {
                try {
                    Statement st = queryResult.getStatement();
                    st.close();
                    queryResult.close();
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, "Exception while closing db connection of sql query: {0}", new Object[]{ex});
                }
            }
        }
    }
     
    // this is MySQL specific
    private String populateFlatTable(Model model, List<ModelRelationship> modelRelationships, List<ModelTable> modelTables, List<ModelColumn> modelColumns) {
        //Integer tempFieldGen = 1;
        //Integer tempTableGen = 1;   
    
        try {
            List<ModelColumn> primaryKeys = new ArrayList();        
            if ( modelTableController == null ) {        
                modelTableController = new ModelTableJpaController();
            }    
            List<ModelTable> modelTablePrime = modelTableController.findModelTableByPrimary(model.getIdModel(), true);
            if ( modelTablePrime.isEmpty() ) {
               return "MB0050 Error. Primary Table not found."; 
            }
            String primaryTablePhName = modelTablePrime.get(0).getPhysicalName();
            
            logger.log(Level.INFO, "Found primary table {0} for Model {1}. Ignoring other tables if present.", new Object[]{primaryTablePhName, modelTablePrime.get(0).getModelidModel()});
            
            String sqlStmtSelectJoin = "SELECT "; 
            String sqlStmtJoin = " FROM ";
            String sqlStmtInsert = "INSERT INTO " + model.getOutputName() + " ("; 
            
            // extract all primary table keys
            for (ModelColumn mc : modelColumns) {
                if ( (mc.getModelTableidModelTable() == modelTablePrime.get(0).getIdModelTable().intValue()) && (mc.getKeyField() == true) ) {
                    primaryKeys.add(mc);
                }
            }
            
            // include in final insert all the insertable and atomic fields
            //List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicModelColumns(modelColumns);
            // except those that have formula and are not sub - used for scalar one-to-many operations
            List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);
            for (ModelColumn mc : modelColumnsInsertable) {
                    sqlStmtInsert = sqlStmtInsert + mc.getName() + ",";
            }       

            // include in SELECT for JOIN all the insertable and atomic fields but exclude scalar fields
            // except those that have formula and are not sub - used for scalar one-to-many operations
            modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);
            for (ModelColumn mc : modelColumnsInsertable) {
                    sqlStmtSelectJoin = sqlStmtSelectJoin + " " + lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName() + ".";
                    sqlStmtSelectJoin = sqlStmtSelectJoin + mc.getPhysicalName() + ",";
            }    
            
            sqlStmtJoin += primaryTablePhName;
            
            // construct as many JOINs as releationship records   
            // still needs branch for scalar joins
            for (ModelRelationship mr : modelRelationships) {
                if ( "1".equals(mr.getType()) ) { // regular join
                    sqlStmtJoin = sqlStmtJoin + " LEFT JOIN " 
                        + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName() + " ON ";
                    // loop for compound keys
                    for (int i=0; i<primaryKeys.size(); i++) {
                        if ( i > 0 ) {
                            sqlStmtJoin = sqlStmtSelectJoin + " AND ";
                        }
                        sqlStmtJoin = sqlStmtJoin + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName()
                            + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                            + " = " + lookupModelTables(modelTables, mr.getTable1()).getPhysicalName() 
                            + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();
                    }
                } else {
                    Integer tempFieldGenStart = tempFieldGen;
                    sqlStmtJoin = sqlStmtJoin + " LEFT JOIN ( SELECT " 
                        + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + formatAtomicNonKeyTableColumns(modelColumns, mr.getTable2())    
                        + formatFormulaTableColumnsForScalar(modelColumns, mr.getTable2())
                        //+ lookupModelColumns(modelColumns, mr.getKey2()).getFormula() 
                        //+ " AS D3TEMP_FIELD" + tempFieldGen.toString() 
                        + " FROM " + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName()
                        + " GROUP BY " + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + ") AS D3TEMP_TABLE" + tempTableGen.toString() + " ON ";

                    sqlStmtJoin = sqlStmtJoin + "D3TEMP_TABLE" + tempTableGen.toString()
                        + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                        + " = " + lookupModelTables(modelTables, mr.getTable1()).getPhysicalName() 
                        + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();   

                    // add fields from scalar functions using their new logical/temp names
                    sqlStmtInsert = sqlStmtInsert + formatFormulaTableColumns(modelColumns, mr.getTable2()) + ",";
                    //sqlStmtInsert += lookupModelColumns(modelColumns, mr.getKey2()).getName() + ",";
                    //modelColumnsInsertable.add(lookupModelColumns(modelColumns, mr.getKey2()));
                    for ( ;tempFieldGenStart<tempFieldGen; tempFieldGenStart++) {
                        sqlStmtSelectJoin += " D3TEMP_TABLE" + tempTableGen.toString() 
                                + ".D3TEMP_FIELD" + tempFieldGenStart.toString() + ",";
                    }
                    
                    //sqlStmtSelectJoin = sqlStmtSelectJoin + " D3TEMP_TABLE" + tempTableGen.toString() 
                    //        + ".D3TEMP_FIELD" + tempFieldGen.toString() + ",";                    
                    sqlStmtSelectJoin = replaceNameWithAlias(sqlStmtSelectJoin,
                                                lookupModelTables(modelTables, mr.getTable2()).getPhysicalName(),
                                                "D3TEMP_TABLE" + tempTableGen.toString());                   
                    tempFieldGen++;
                    tempTableGen++;                    
                }
            }     
            
            sqlStmtSelectJoin = Util.removeLastChar(sqlStmtSelectJoin);
            logger.log(Level.INFO, "Executing SQL statement: {0} {1}", new Object[]{sqlStmtSelectJoin, sqlStmtJoin});
                
            Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmtSelectJoin + sqlStmtJoin,logger);
            List<Object[]> results = queryResult.getResultList();
            sqlStmtInsert = Util.removeLastChar(sqlStmtInsert) + ") VALUES (";           
            // for each row of data retieved insert new row of data into DST table
            for (Object[] obj : results) {
                String sqlStmtInsertData = getInsertValues(obj);                
                sqlStmtInsertData = Util.removeLastChar(sqlStmtInsertData);
                sqlStmtInsertData += ")";
                // later add ability to populate memory instead of saving to DB
                // which could be followed immediately with call to StatProxy for statistical processing
                DBHelper.executeSqlStatement("DeltaPU",sqlStmtInsert + sqlStmtInsertData,logger);  
            }
            return "Ok";
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0051 Error while constructing join statement", ex);            
            return "MB0051 SQL Error while processing atomic fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0052 Error while constructing join statement", ex);
            return "MB0052 Error while processing atomic fields.\n" + ex.getMessage();                
        }
    }
  
    private String missingDataProcessor(int currentUserId, ModelColumn mc, String tableName, ModelColumn primaryKey) {
        String resultMessage = "MB0005 Error. Missing data processing failed. ";
        Query queryResult;
        String sqlStmtData;
        String sqlStmt;
        String type;
        String originalType;
        List<ModelColumn> modelColumns = new ArrayList();            
        
        // add temp column to target OST table
        try {
            originalType = getOriginalFieldType(mc, tableName);
            type = getFieldType(mc,tableName);            
            sqlStmt = "ALTER TABLE " 
                    + tableName 
                    + " ADD D3temp_" + mc.getName() + " " + type;
            AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0053 Error while processing missing fields. ", ex);            
            return "MB0053 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while adding column {0} to table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }

        sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getName() + ", " + mc.getFormula() + " FROM " + tableName;
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        
        // add 3 columns to parse in the for loop below: primary Key, original and temporary        
        modelColumns.add(primaryKey);
        modelColumns.add(mc);
        ModelColumn D3temp_mc = mc;
        D3temp_mc.setPhysicalName("D3temp_" + mc.getPhysicalName());
        modelColumns.add(D3temp_mc);         
        try {
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0054 Error while processing missing fields. ", ex);            
            return "MB0054 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        }catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return resultMessage;
        }    
        List<Object[]> results = queryResult.getResultList();        
        if ( results.size() == 1 ) {
            // this is function which resulted in one value for all records
            // substitue function with value in the select and re-run query
            // to obtain full set of records
            sqlStmtData = getMissingDataFunctionValue(results.get(0));
            sqlStmtData = Util.removeLastChar(sqlStmtData);             
            sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getName() + ", " + sqlStmtData + " FROM " + tableName;
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            //Query queryResult;
            try {
                queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);
            } catch (PersistenceException ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0055 Error while processing missing fields. ", ex);            
                return "MB0055 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
            } catch (Exception ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                return resultMessage;
            }       
            results = queryResult.getResultList();
        }
       
        String sqlStmtUpdate = "UPDATE " + tableName + " SET D3temp_" + mc.getName() + "=";        

        for (Object[] obj : results) {        
            sqlStmtData = getMissingDataFinalValue(obj);
            sqlStmtData = Util.removeLastChar(sqlStmtData);         
            try {
                if ( results.size() == 1 ) {
                    // for aggregate functions update the whole column
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate + sqlStmtData,logger);                    
                } else {
                    String sqlStmtWhere = " WHERE " + primaryKey.getName() + "=" + getUpdateKey(obj);
                    sqlStmtWhere = Util.removeLastChar(sqlStmtWhere);                      
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate + sqlStmtData + sqlStmtWhere,logger);
                }
            } catch (PersistenceException ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0056 Error while processing missing fields. ", ex);            
                return "MB0056 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
            } catch (Exception ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                return resultMessage;
            }
        }  
        // rename original column to raw_column
        try {
            sqlStmt = "ALTER TABLE " + tableName 
                    + " CHANGE " + mc.getName() 
                    + " raw_" + mc.getName() + " " + originalType;
            AuditLogger.log(currentUserId, 0, "rename column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0057 Error while processing missing fields. ", ex);            
            return "MB0057 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while renaming column to raw_{0} in table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }          
        // rename temp column to orginal column
        try {
            sqlStmt = "ALTER TABLE " + tableName 
                    + " CHANGE D3temp_" + mc.getName() 
                    + " " + mc.getName() + " " + type;
            AuditLogger.log(currentUserId, 0, "rename column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0058 Error while processing missing fields. ", ex);            
            return "MB0058 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while renaming column to raw_{0} in table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }        
        return "Ok";
    }

    private String virtualFieldProcessor(int currentUserId, ModelColumn mc, String tableName, ModelColumn primaryKey) {

        String resultMessage = "MB0059 Error while processing virtual fields.";
        
        String sqlStmtUpdate = "UPDATE " + tableName + " SET " + mc.getName() + " = " + mc.getFormula();             
        try {
            DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate,logger);                    
        } catch (PersistenceException ex) {
            if ( ex.getCause().getCause().toString().contains("Invalid use of group function") ) {
                String sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getFormula() + " FROM " + tableName;
                logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
                Query queryResult;
                try {
                    queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);
                    List<Object[]> results = queryResult.getResultList();
                    Object[] obj = results.get(0); 
                    String sqlStmtData = getUpdateValues(obj);      
                    sqlStmtData = Util.removeLastChar(sqlStmtData);    
                    sqlStmtUpdate = "UPDATE " + tableName + " SET " + mc.getName() + " = " + sqlStmtData;
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate,logger);   
                    return "Ok";
                } catch (Exception iex) {
                    Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0061 Error while processing virtual fields. ", iex);            
                    return "MB0061 SQL Error while processing virtual fields.\n" + iex.getCause().toString() + "\n" + iex.getCause().getCause().toString();
                }         
            }
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB00062 Error while processing virtual fields formula: {0}, {1}", new Object[]{mc.getFormula(), ex.getMessage()});            
            return "MB0062 SQL Error while processing virtual fields. Formula: " + mc.getFormula() + " Exception: "+ ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "MB0060 SQL Error while processing virtual fields. Formula: " + mc.getFormula() + " Exception: "+ ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } 
        return "Ok";
    }
       
    private String virtualFieldProcessorOld(int currentUserId, ModelColumn mc, String tableName, ModelColumn primaryKey) {

        String sqlStmt;
        String resultMessage = "MB0059 Error while processing virtual fields.";
        
        // add column to target OST table first
        try {
            String type = getFieldType(mc,tableName);
            sqlStmt = "ALTER TABLE " 
                    + tableName 
                    + " ADD " + mc.getName() + " " + type;
            AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaPU",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0060 Error while processing virtual fields. ", ex);            
            return "MB0060 SQL Error while processing virtual fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while adding column {0} to table {1}" + new Object[]{mc.getName(),tableName}, ex);            
            return resultMessage;
        }
        
        sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getFormula() + " FROM " + tableName;
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        Query queryResult;
        try {
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0061 Error while processing virtual fields. ", ex);            
            return "MB0061 SQL Error while processing virtual fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return resultMessage;
        }       
        List<Object[]> results = queryResult.getResultList();
        List<ModelColumn> modelColumns = new ArrayList();
        modelColumns.add(primaryKey);
        modelColumns.add(mc);
        
        String sqlStmtUpdate = "UPDATE " + tableName + " SET " + mc.getName() + "=";        
        String sqlStmtData;

        for (Object[] obj : results) {        
            sqlStmtData = getUpdateValues(obj);
            sqlStmtData = Util.removeLastChar(sqlStmtData);           
            try {
                if ( results.size() == 1 ) {
                    // for aggregate functions update the whole column
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate + sqlStmtData,logger);                    
                } else {
                    String sqlStmtWhere = " WHERE " + primaryKey.getName() + "=" + getUpdateKey(obj);
                    sqlStmtWhere = Util.removeLastChar(sqlStmtWhere);                     
                    DBHelper.executeSqlStatement("DeltaPU",sqlStmtUpdate + sqlStmtData + sqlStmtWhere,logger);
                }
            } catch (PersistenceException ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB00062 Error while processing virtual fields. ", ex);            
                return "MB0062 SQL Error while processing virtual fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
            } catch (Exception ex) {
                    Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    return resultMessage;
            }
        }      
        return "Ok";
    }
    
    private String replaceNameWithAlias(String sqlStatement, String name, String alias) {
        String result = sqlStatement.replace(name, alias);
        return result;
    }  
    
    private String formatAtomicNonKeyTableColumns(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getInsertable() == true) 
                    && (mc.getKeyField() == false && (("".equals(mc.getFormula()))) || (mc.getFormula() == null)) ) {
                sqlStmtColumns += "," + mc.getPhysicalName();
            }
        }  
        return sqlStmtColumns;
    }
    
    private String formatFormulaTableColumnsForScalar(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getInsertable() == true) && (mc.getKeyField() == false) 
                    && (!"".equals(mc.getFormula())) && (mc.getFormula() != null)) {
                sqlStmtColumns += mc.getFormula() + " AS D3TEMP_FIELD" + tempFieldGen.toString() + ",";
                tempFieldGen++;
            }
        }  
        return Util.removeLastChar(sqlStmtColumns);        
    }
   
    private String formatFormulaTableColumns(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getKeyField() == false) && (!"".equals(mc.getFormula())) && (mc.getFormula() != null)) {
                sqlStmtColumns += mc.getName() + ",";
            }
        }  
        return Util.removeLastChar(sqlStmtColumns);        
    }
    
    private String getInsertValues(Object[] obj) {
        String sqlStmtInsertData = "";
        for (int ii=0; ii< obj.length; ii++) {
            sqlStmtInsertData += getInsertValuesSingleRow(obj[ii]);
        }   
        return sqlStmtInsertData;
    }
    
    /* The difference between this method and getInsertValues is that first column (primary key) is skipped */
    private String getUpdateValues(Object[] obj) {
        String sqlStmtInsertData = "";
        for (int ii=1; ii< obj.length; ii++) {
            sqlStmtInsertData += getInsertValuesSingleRow(obj[ii]);
        }   
        return sqlStmtInsertData;
    }

    
    /* The logic for missing data substitution */
    private String getMissingDataFinalValue(Object[] obj) {
        // check if column had data orgiinally and use, otherwise substitute
        String oldData = getInsertValuesSingleRow(obj[1]);  
        String newData = getInsertValuesSingleRow(obj[2]);  
        if ( "NULL,".equals(oldData) ) {
            return newData;
        }
        return oldData;
    }

    /* Retrieve value retured by function formula */
    private String getMissingDataFunctionValue(Object[] obj) {
        return getInsertValuesSingleRow(obj[2]);  
    }
    
    /* The difference between this method and getInsertValues is that only first column (primary key) is fetched */
    private String getUpdateKey(Object[] obj) {
        return getInsertValuesSingleRow(obj[0]); 
    }    
    
    private String getInsertValuesSingleRow(Object obj) {
        String sqlStmtInsertData = "";
        if ( obj == null ) {
            //if ( mc.getDefaultValue() == null ) {
                sqlStmtInsertData += "NULL,";
            //} else {
            //    sqlStmtInsertData += mc.getDefaultValue() + ",";
            //}
        } else {
            if (obj instanceof String) {
              //logger.log(Level.INFO, "Processing String field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += "'" + obj + "',";
            }
            if (obj instanceof Boolean) {
              //logger.log(Level.INFO, "Processing Boolean field {0}", new Object[]{obj[ii]});
                if ( obj.equals(true) ) {
                    sqlStmtInsertData += "1,";
                } else {
                    sqlStmtInsertData += "0,";
                }
            }        
             if (obj instanceof Short) {
              //logger.log(Level.INFO, "Processing Short field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }           
            if (obj instanceof Integer) {
              //logger.log(Level.INFO, "Processing Integer field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }    
            if (obj instanceof BigInteger) {
              //logger.log(Level.INFO, "Processing BigInteger field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            } 
            if (obj instanceof BigDecimal) {
              //logger.log(Level.INFO, "Processing BigDecimal field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }              
            if (obj instanceof Double) {
              //logger.log(Level.INFO, "Processing Double field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }  
            if (obj instanceof Character) {
              //logger.log(Level.INFO, "Processing Character field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += "'" + obj + "',";
            }    
            if (obj instanceof Date) {
              //logger.log(Level.INFO, "Processing Date field {0}", new Object[]{obj[ii]});
              String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
              //logger.log(Level.INFO, "Processing Timestamp field after re-formatting {0}", new Object[]{date});
              sqlStmtInsertData += "'" + date + "',";
            }                   
            if (obj instanceof Timestamp) {
              //logger.log(Level.INFO, "Processing Timestamp field {0}", new Object[]{obj[ii]});
              String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
              //logger.log(Level.INFO, "Processing Timestamp field after re-formatting {0}", new Object[]{date});
              sqlStmtInsertData += "'" + date + "',";
            }       
        }
        return sqlStmtInsertData;
    }    
    
    public static String getDBObjectData(Integer currentUserId, int start, int limit, String orderField, String direction, String objectName, String filter) {
        
        DatabaseMetaData dbMetaData = null;
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String errorMessage = "";         
        String sqlStmt = "";
        String schema = null;
        String driverName = null;
            
        try {
            conn = ConnectionPool.getInstance().getConnection();
            dbMetaData = conn.getMetaData(); 
            driverName = dbMetaData.getDriverName();
            
            String metaDataString = "[";                     
            String orderBy= "";
            
            if ( !"".equals(orderField) ) {
                orderBy = " order by " + orderField + " " + direction;
            }
            
            switch ( driverName ) {
                case "Microsoft JDBC Driver 4.0 for SQL Server":
                    sqlStmt =  "select * from  (select  ROW_NUMBER() OVER(order by id) as row, * from " + objectName 
                        + ") as tbl WHERE  row >= "  + Integer.toString(start) + " AND row <= " + Integer.toString(limit);
                    if ( !"".equals(filter) ) {
                        sqlStmt += " AND " + filter;
                    }
                    schema = "dbo";
                    break;
                case "MySQL Connector Java":
                    if ( !"".equals(filter) ) {
                        sqlStmt = "select * from " + objectName + " where " + filter + " " + orderBy + " LIMIT " + Integer.toString(start) + ", " + Integer.toString(limit);                                          
                    } else {
                        sqlStmt = "select * from " + objectName + orderBy + " LIMIT " + Integer.toString(start) + ", " + Integer.toString(limit);                        
                    }                   
                    break;
                default:
                    throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
            }   

            logger.log(Level.SEVERE, "Executing for TableViewer: "+ sqlStmt);
            AuditLogger.log(currentUserId, 0, "TableViewer", sqlStmt);   
            st = conn.createStatement();     
            rs = st.executeQuery(sqlStmt);
            
            ResultSetMetaData metadata = rs.getMetaData();            
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                if ( i != 0 ) {
                    metaDataString += ",";
                }
                logger.log(Level.INFO, "Name: "+ metadata.getColumnLabel(i + 1)+ ", Type: "+ metadata.getColumnTypeName(i + 1).toUpperCase());
                metaDataString = metaDataString + "{\"label\":\"" + metadata.getColumnLabel(i + 1) 
                        + "\",\"type\":\"" + DBHelper.getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) 
                        + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) 
                        + "\",\"precision\":\"" + metadata.getPrecision(i + 1) 
                        + "\",\"scale\":\"" + metadata.getScale(i + 1)                   
                        + "\"}"; 
            }
            metaDataString += "]";      
            
            String dataString = "[";
            int j = 0;
            while (rs.next()) {
                if ( j++ != 0 ) {
                    dataString += ",";
                }    
                dataString += "{";
                for (int i = 0; i < metadata.getColumnCount(); i++) {   
                    if ( i != 0 ) {
                        dataString += ",";
                    }
                    dataString = dataString + "\"" + metadata.getColumnLabel(i + 1) + "\":";                     
                    switch ( metadata.getColumnTypeName(i+1).toUpperCase() ) {
                        case "VARCHAR":
                        case "CHAR":
                            //String str = rs.getString(metadata.getColumnLabel(i+1));
                            dataString = dataString + "\"" + rs.getString(metadata.getColumnLabel(i+1)) + "\"";
                            break;
                        case "BIT":
                            int booleanBit = rs.getInt(metadata.getColumnLabel(i+1));
                            dataString = dataString + Integer.toString(booleanBit);
                            break;          
                        case "SMALLINT":
                            int integerS = rs.getInt(metadata.getColumnLabel(i+1));
                            dataString = dataString + Integer.toString(integerS);
                            break;                            
                        case "INT":
                            int integerI = rs.getInt(metadata.getColumnLabel(i+1));
                            dataString = dataString + Integer.toString(integerI);
                            break;
                        case "BIGINT":
                            int integerBI = rs.getInt(metadata.getColumnLabel(i+1));
                            dataString = dataString + Integer.toString(integerBI);
                            break;                            
                        case "LONG":
                            long l = rs.getLong(metadata.getColumnLabel(i+1));
                            dataString = dataString + Long.toString(l);                            
                            break;                            
                        case "DECIMAL":
                            BigDecimal bd = rs.getBigDecimal(metadata.getColumnLabel(i+1));
                            if ( bd == null ) {
                               dataString = dataString + "\"null\"";    
                            } else {
                                dataString = dataString + "\"" + bd.toString() + "\"";   
                            }
                            break;                           
                        case "DOUBLE":
                            double d = rs.getDouble(metadata.getColumnLabel(i+1));
                            dataString = dataString + Double.toString(d);                              
                            break;
                        case "FLOAT":
                            float f = rs.getFloat(metadata.getColumnLabel(i+1));
                            dataString = dataString + Float.toString(f);                                    
                            break;                            
                        case "DATE":
                            Date date = rs.getDate(metadata.getColumnLabel(i+1));
                            if ( date == null ) {
                                dataString = dataString + "\"null\"";    
                            } else {                            
                                dataString = dataString + "\"" + date.toString() + "\"";        
                            }
                            break;
                        case "DATETIME":
                            //Timestamp dt = rs.getTimestamp(metadata.getColumnLabel(i+1));
                            //dataString = dataString + "\"" + dt.toString() + "\"";       
                            dataString = dataString + "\"" + rs.getString(metadata.getColumnLabel(i+1)) + "\"";                            
                            break;                               
                        case "TIMESTAMP":
                            Timestamp ts = rs.getTimestamp(metadata.getColumnLabel(i+1));
                             if ( ts == null ) {
                                dataString = dataString + "\"null\"";    
                            } else {                           
                                dataString = dataString + "\"" + ts.toString() + "\"";     
                            }
                            break;            
                        }
                }
                dataString += "}";
            }
            dataString += "]";      
            String returnString = "{\"success\":true,\"totalCount\":" + getObjectCount(conn, objectName, filter)
                                    + ",\"metaData\":" + metaDataString + ",\"content\":" + dataString + "}";
            return returnString;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving db info and data of DBObject: {0} {1}", new Object[]{objectName, ex});
            errorMessage = ex.getMessage();
        } catch (DatabaseNotSupportedException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving db info and data of DBObject: {0} {1}", new Object[]{objectName, ex});
            errorMessage = ex.getMessage();
        } finally {
            try {
                if ( rs != null ) rs.close();
                if ( st != null ) st.close();
                //conn.close(); do not close connection -  it is cached
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Exception while closing db connection of DBObject: {0} {1}", new Object[]{objectName, ex});
            }
        }
        return "{\"success\":false,\"status\":{\"message\":\"" + errorMessage + "\"},\"totalCount\":0,\"metaData\":[],\"data\":[]}";
    }

    public static String getDBObjectColumns(String objectName, String filter) {
        
        DatabaseMetaData dbMetaData = null;
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String errorMessage = "";         
        String sqlStmt = "";
        String schema = null;
        String driverName = null;
            
        try {
            conn = ConnectionPool.getInstance().getConnection();
            dbMetaData = conn.getMetaData(); 
            driverName = dbMetaData.getDriverName();
            
            String metaDataString = "[";                     
            
            switch ( driverName ) {
                case "Microsoft JDBC Driver 4.0 for SQL Server":
                    // this query does not work!
                    sqlStmt =  "select * from " + objectName;
                    if ( !"".equals(filter) && (filter != null)  ) {
                        sqlStmt += " WHERE " + filter;
                    }
                    schema = "dbo";
                    break;
                case "MySQL Connector Java":
                    if ( !"".equals(filter) && (filter != null)  ) {
                        sqlStmt = "select * from " + objectName + " where " + filter;                                          
                    } else {
                        sqlStmt = "select * from " + objectName;                        
                    }                   
                    break;
                default:
                    throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
            }   

            st = conn.createStatement();     
            rs = st.executeQuery(sqlStmt);
            
            ResultSetMetaData metadata = rs.getMetaData();            
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                if ( i != 0 ) {
                    metaDataString += ",";
                }
                logger.log(Level.INFO, "Name: "+ metadata.getColumnLabel(i + 1) + ", Type: "+ metadata.getColumnTypeName(i + 1).toUpperCase());
                metaDataString = metaDataString + "{\"name\":\"" + metadata.getColumnLabel(i + 1) + "\"}"; 
            }
            metaDataString += "]";                      
            return metaDataString;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[]{objectName, ex});
            errorMessage = ex.getMessage();
        } catch (DatabaseNotSupportedException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[]{objectName, ex});
            errorMessage = ex.getMessage();
        } finally {
            try {
                if ( rs != null ) rs.close();
                if ( st != null ) st.close();
                //conn.close(); do not close connection -  it is cached
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Exception while closing db connection of DBObject: {0} {1}", new Object[]{objectName, ex});
            }
        }
        return errorMessage;
    }
 
    public static String getObjectDataIntoDoubleArray(String objectName, String filter, String orderField, String direction, double[] data) {
        
        DatabaseMetaData dbMetaData = null;
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String errorMessage = "OK";         
        String sqlStmt = "";
        String schema = null;
        String driverName = null;
        String dataString = "X"; 
        String colTypeName = "Y";
            
        try {
            conn = ConnectionPool.getInstance().getConnection();
            dbMetaData = conn.getMetaData(); 
            driverName = dbMetaData.getDriverName();
            
            String metaDataString = "[";                     
            String orderBy= "";
            
            if ( !"".equals(orderField) ) {
                orderBy = " order by " + orderField + " " + direction;
            }
            
            switch ( driverName ) {
                case "Microsoft JDBC Driver 4.0 for SQL Server":
                    // this query does not work!
                    sqlStmt =  "select * from " + objectName;
                    if ( !"".equals(filter) && (filter != null) ) {
                        sqlStmt += " WHERE " + filter;
                    }
                    schema = "dbo";
                    break;
                case "MySQL Connector Java":
                    if ( !"".equals(filter)  && (filter != null)  ) {
                        sqlStmt = "select * from " + objectName + " where " + filter;                                          
                    } else {
                        sqlStmt = "select * from " + objectName;                        
                    }                   
                    break;
                default:
                    throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
            }   

            logger.log(Level.INFO, "Executing to populate double array for proxy: "+ sqlStmt);
            st = conn.createStatement();     
            rs = st.executeQuery(sqlStmt);
            
            ResultSetMetaData metadata = rs.getMetaData();                        
            int j = 0;
            while (rs.next()) {
                for (int i = 0; i < metadata.getColumnCount(); i++) {                 
                    data[j] = (double) j;
                    colTypeName = metadata.getColumnTypeName(i+1);
                    //String colName = metadata.getColumnName(i+1);
                    switch ( colTypeName.toUpperCase() ) {
                        case "VARCHAR":                         
                        case "CHAR":
                            try {
                                dataString = rs.getString(metadata.getColumnLabel(i+1));
                                if ( (dataString.charAt(0) == 'Y') || (dataString.charAt(0) == 'y') 
                                        || (dataString.charAt(0) == '1') || (dataString.charAt(0) == 'T')
                                        || (dataString.charAt(0) == 't') ) {
                                    data[j] = 1.0; 
                                } else {
                                    data[j] = 0.0;
                                }
                            } catch (Exception e) {
                                data[j] = -1.0;
                                //throw e;
                            }
                            break;
                        case "SMALLINT":
                        case "INT":
                        case "BIT":
                            int integerI = rs.getInt(metadata.getColumnLabel(i+1));
                            data[j] = (double) integerI;
                            break;
                        case "LONG":
                            long l = rs.getLong(metadata.getColumnLabel(i+1));
                            data[j] = (double) l;                         
                            break;                            
                        case "DECIMAL":
                            try {
                                BigDecimal bd = rs.getBigDecimal(metadata.getColumnLabel(i+1)); 
                                data[j] = bd.doubleValue(); 
                            } catch (Exception e) {
                                data[j] = -2.0;
                                //throw e;
                            }
                            break;                           
                        case "DOUBLE":
                            data[j] = rs.getDouble(metadata.getColumnLabel(i+1));                             
                            break;
                        case "FLOAT":
                            data[j] = rs.getFloat(metadata.getColumnLabel(i+1));                                 
                            break;                            
                        case "DATE":
                            try {
                                //Date date = rs.getDate(metadata.getColumnLabel(i+1));                         
                                //dataString = new java.text.SimpleDateFormat("yyyyMMdd.HHmmss").format(date.toString());
                                dataString = rs.getString(metadata.getColumnLabel(i+1));
                                dataString = Util.dateToDigitString(dataString);                                
                                if ( dataString == null ) {
                                    data[j] = 0.0;
                                } else {
                                    data[j] = Double.parseDouble(dataString);
                                }
                            } catch (Exception e) {
                                data[j] = -3.0; 
                                //throw e;
                            }
                            break;
                        case "DATETIME":     
                            dataString = rs.getString(metadata.getColumnLabel(i+1));
                            if ( dataString == null ) {
                                data[j] = 0.0;
                            } else {
                                data[j] = Double.parseDouble(Util.dateToDigitString(dataString));
                            }
                            break;                               
                        case "TIMESTAMP":
                            try {
                                Timestamp ts = rs.getTimestamp(metadata.getColumnLabel(i+1));
                                dataString = ts.toString();
                                if ( dataString == null ) {
                                    data[j] = 0.0;
                                } else {
                                    data[j] = Double.parseDouble(dataString);
                                }
                            } catch (Exception e) {
                                data[j] = -4.0; 
                                //throw e;
                            }
                            break;            
                        }
                    //logger.log(Level.INFO, "Retireved value {0} from column {1} {2}", new Object[]{Double.toString(data[j]), colName, colTypeName});
                    j++;
                }
            }
            return errorMessage;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while formatting double array from DBObject: {0} {1}", new Object[]{objectName, ex});
            errorMessage = "Exception while formatting double array for " + dataString + " type " + colTypeName + " : " + ex.getMessage();
        } finally {
            try {
                if ( rs != null ) rs.close();
                if ( st != null ) st.close();
                //conn.close(); do not close connection -  it is cached
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Exception when closing record set for double array extraction from DBObject: {0} {1}", new Object[]{objectName, ex});
                errorMessage = "Exception when closing record set for double array extraction from DBObject " + objectName + " : " + ex.getMessage();
            }
        }
        return errorMessage;
    }    
    
    /* delegete this work to common method in admin service */
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }    
}
