package com.copsys.delta.server;

/**
 *
 * @author Coping Systems Inc.
 */

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NotificationTimerTask extends TimerTask {

  /**
  * Construct and use a TimerTask and Timer.
  */
  private final static long fONCE_PER_DAY = 1000*60*60*24;
  private final static long fONCE_PER_HOUR = 1000*60*60;
  private final static long fONCE_PER_MINUTE = 1000*60;
  private static final Logger logger = Logger.getLogger(NotificationTimerTask.class.getName());
  Timer timer = null;
  private static EventServiceImpl eventService;    

  public void init() {
    eventService = new EventServiceImpl();  
    timer = new Timer();
    timer.scheduleAtFixedRate(this, 1000L, fONCE_PER_MINUTE);
  }


  /**
  * Implements TimerTask's abstract run method.
  */
  public void run(){
      logger.log(Level.INFO, "NotificationTimer processing events!");
      eventService.processEvents();
  }
}


