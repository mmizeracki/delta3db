/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server;

import com.copsys.delta.entity.logregr.Logregr;
import com.copsys.delta.entity.logregr.LogregrDate;
import com.copsys.delta.entity.logregr.LogregrDateComp;
import com.copsys.delta.entity.logregr.LogregrDateFieldComp;
import com.copsys.delta.entity.logregr.LogregrField;
import com.copsys.delta.entity.model.ModelColumn;
import com.copsys.delta.entity.study.Study;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.logregr.LogregrDateJpaController;
import com.copsys.delta.jpa.logregr.LogregrFieldJpaController;
import com.copsys.delta.jpa.logregr.LogregrJpaController;
import com.copsys.delta.jpa.model.ModelColumnJpaController;
import com.copsys.delta.jpa.process.ProcessJpaController;
import com.copsys.delta.jpa.study.StudyJpaController;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.server.exceptions.EntityExistsException;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrServiceImpl {

    private static final Logger logger = Logger.getLogger(LogregrServiceImpl.class.getName());
    private static LogregrDateJpaController logregrdateController;
    private static LogregrFieldJpaController logregrfieldController;
    private static LogregrJpaController logregrController;
    private static StudyJpaController studyController;
    private static ModelColumnJpaController modelColumnController;
    private static ProcessJpaController processController;
    private static AdminServiceImpl adminService;

    public LogregrServiceImpl() {
        if (logregrController == null) {
            logregrController = new LogregrJpaController();
        }
        if (logregrfieldController == null) {
            logregrfieldController = new LogregrFieldJpaController();
        }
        if (logregrdateController == null) {
            logregrdateController = new LogregrDateJpaController();
        }
        if (studyController == null) {
            studyController = new StudyJpaController();
        }
        if (modelColumnController == null) {
            modelColumnController = new ModelColumnJpaController();
        }
        if (processController == null) {
            processController = new ProcessJpaController();
        }
    }

    public String createLogregr(String JSONString, Integer currentOrgId, Integer currentUserId) {
        String result = "Logregr Objects(s) not created";
        Object subject = null;
        String dataString = "";
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), logregrController.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, new Logregr().getClass());               
                subject = logregrController.createObjects(subject, currentOrgId, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                LogregrField entField = new LogregrField(); 
                entField.setIdLogregr(((Logregr)subject).getIdLogregr());
                entField.setActive(Boolean.TRUE);
                entField.setName("intercept");
                entField.setValue("C");
                entField.setModelColumnidRisk(0);
                entField = createLogregrField(entField, currentUserId);
                LogregrDate entDate = new LogregrDate(); 
                entDate.setIdLogregr(((Logregr)subject).getIdLogregr());  
                entDate.setActive(Boolean.TRUE);
                entDate.setValue("");
                entDate.setDescription("default");
                entDate.setName("");
                entDate.setIdLogregrField(entField.getIdLogregrField());
                entDate.setModelColumnidSequencer(((Logregr)subject).getModelColumnidSequencer());
                entDate = createLogregrDate(entDate, currentUserId);
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{logregrController.getObjectType(), ex});
            result = "LR0017 Exception while creating new "  + logregrController.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(logregrController.getObjectType(), dataString, "Object(s) created", result);
    }
         
    public String getLogregrFields(String payload) {

        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        JSONArray JSONArrayObjects = null;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrFieldJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression object into JSON", ex.getMessage());
            result = "LR0001 Exception parsing Logistic Regression object into JSON";
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", Integer.valueOf(JSONArrayObjects.length()) + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrfields", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0002 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String createLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Field for logregr formula not created";
        LogregrField entity = null;
        String dataString = "";

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int ii = 0; ii < anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Field {0} for Logistic Regression Formula", anArray.getJSONObject(ii).getString("name"));
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                entity = gson.fromJson(jString, LogregrField.class);
                if (ii > 0) {
                    dataString = dataString.concat(",");
                }
                //LogregrField ent = createLogregrFieldString(entity, currentUserId);
                dataString = dataString + createLogregrFieldString(entity, currentUserId);
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new field for logregr formula: {0} {1}", new Object[]{entity.getName(), ex});
            result = "LR0003 Exception while creating new Formula:" + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field created", result);
    }

    public String updateLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        LogregrField entity;
        int idLogregr = 0, idModelColumn = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrField.class);
                idModelColumn = entity.getModelColumnidRisk();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Field {0} for logregr Formula {1}", new Object[]{Integer.toString(idModelColumn), Integer.toString(idLogregr)});
                entity = updateLogregrField(entity, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(entity).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0004 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field updated", result);
    }

    public String deleteLogregrField(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not deleted";
        String dataString = "";
        int idLogregrField = 0;

        try {
            JSONObject JSONContentRoot = new JSONObject(JSONString);
            idLogregrField = JSONContentRoot.getInt("idLogregrField");
            logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(idLogregrField)});
            LogregrField ent = deleteLogregrField(idLogregrField, currentUserId);
            dataString = dataString + new JSONObject(ent).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrField), ex});
            result = "LR0005 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field deleted", result);
    }

    public String deleteLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula field not deleted";
        String dataString = "";
        int idLogregrField = 0;

        try {
            JSONArray objectArray = new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i = 0; i < objectArray.length(); i++) {
                JSONObject JSONContentRoot = objectArray.getJSONObject(i);
                idLogregrField = JSONContentRoot.getInt("idLogregrField");
                LogregrField ent = deleteLogregrField(idLogregrField, currentUserId);
                if (sepCounter++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
                List<LogregrDate> aList = logregrdateController.findLogregrDateByLogregrFieldId(idLogregrField);
                for (LogregrDate t : aList) {
                    logger.log(Level.INFO, "Deleting Formula Date {0}", new Object[]{Integer.toString(t.getIdLogregrDate())});
                    deleteLogregrDate(t.getIdLogregrDate(), currentUserId);
                }
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrField), ex});
            result = "LR0006 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Filter Formulas deleted", result);
    }
    
//------------------------------------------------------------------------------ Logregr Date/Value
    public String getLogregrDates(String payload) {

        String dataString = "";
        String result = "No dates found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        JSONArray JSONArrayObjects = null;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Dates for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrDateJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression object into JSON", ex.getMessage());
            result = "LR0007 Exception parsing Logistic Regression object into JSON";
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", JSONArrayObjects.length() + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrdates", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0008 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String createLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Date for logregr formula not created";
        LogregrDate entity = null;
        String dataString = "";

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int ii = 0; ii < anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Date/Value {0} for Logistic Regression Formula", anArray.getJSONObject(ii).getString("name"));
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                LogregrDate ent = createLogregrDate(entity, currentUserId);
                if (ii > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new date for logregr formula: {0} {1}", new Object[]{entity.getName(), ex});
            result = "LR0009 Exception while creating new Formula:" + ex.getMessage();
        }
        return wrapAPI("logregrdates", dataString, "Logistic Regression Formula field created", result);
    }

    private LogregrDate updateLogregrDate(LogregrDate ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} updating relationship between date {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString((ent.getIdLogregrDate())),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrdateController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "update", ent);
        return ent;
    }

    public String updateLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();      
        JSONArray JSONArrayObjects = null;        
        LogregrDate entity;
        int idLogregr = 0, idLogregrDate = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                idLogregrDate = entity.getIdLogregrDate();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Date {0} for logregr Formula {1}", new Object[]{Integer.toString(idLogregrDate), Integer.toString(idLogregr)});
                entity = updateLogregrDate(entity, currentUserId);
            }
            logger.log(Level.INFO, "Retrieving Dates for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrDateJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0010 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", JSONArrayObjects.length() + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrdates", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0008 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }
    
    public String updateLogregrDates_old(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        LogregrDate entity;
        int idLogregr = 0, idLogregrDate = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                idLogregrDate = entity.getIdLogregrDate();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Date {0} for logregr Formula {1}", new Object[]{Integer.toString(idLogregrDate), Integer.toString(idLogregr)});
                entity = updateLogregrDate(entity, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(entity).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0010 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrdates", dataString, "Logistic Regression Formula date updated", result);
    }

    public String deleteLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula date not deleted";
        String dataString = "";
        int idLogregrDate = 0;

        try {
            JSONArray objectArray = new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i = 0; i < objectArray.length(); i++) {
                JSONObject JSONContentRoot = objectArray.getJSONObject(i);
                idLogregrDate = JSONContentRoot.getInt("idLogregrDate");
                logger.log(Level.INFO, "Deleting Formula Date {0}", new Object[]{Integer.toString(idLogregrDate)});
                LogregrDate ent = deleteLogregrDate(idLogregrDate, currentUserId);
                if (sepCounter++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrDate), ex});
            result = "LR0011 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Filter Formulas deleted", result);
    }

    private LogregrDate deleteLogregrDate(int idLogregrField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and formula", new Object[]{Integer.toString((currentUserId)), Integer.toString((idLogregrField))});

        LogregrDate ent = new LogregrDate(idLogregrField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrdateController.destroy(idLogregrField);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "delete", ent);
        return ent;
    }

    public String getLRFString(String payload, Integer currUserId) {

        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        int colMax = 0;
        int rowMax = 0;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));

            List<LogregrField> rows = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);
            List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID

            if ((columns != null) && !columns.isEmpty()) {
                colMax = columns.size();
                logger.log(Level.INFO, "Logistic Regression Formula {0} has dates. Retrieving {1} formula column data.", new Object[]{Integer.toString(idLogregr), Integer.toString(colMax)});
                if ((rows != null) && !rows.isEmpty()) {
                    rowMax = rows.size();
                    logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula row data.", new Object[]{Integer.toString(idLogregr), Integer.toString(rowMax)});
                    for (int j = 0; j < colMax / rowMax; j++) {
                        Integer sequencer = columns.get(j * rowMax).getModelColumnidSequencer();
                        if (sequencer == 0) {
                            dataString = "Sequencer field not defined for the study.";
                            break;
                        }
                        ModelColumn mc = modelColumnController.findModelColumn(sequencer);
                        //dataString += "\"" + mc.getName() + "\" >= " + columns.get(j).getName() + "<br>";
                        dataString += columns.get(j).getDescription() + "<br>";                        
                        if ( j + 1 < colMax / rowMax ) {
                            // it is not the last column
                            dataString += "\"" + mc.getName() + "\" < " + columns.get(j+1).getName() + "<br>";
                        }
                        /*if (j == 0) {
                            dataString += "\"" + mc.getName() + "\" < " + columns.get(j).getName() + "<br>";
                        } else {
                            dataString += "\"" + mc.getName() + "\" >= " + columns.get(j - 1).getName()
                                    + " < " + columns.get(j).getName() + "<br>";
                        }*/
                        for (int i = 0; i < rowMax; i++) {
                            if (i > 0) {
                                dataString = dataString.concat(" + ");
                            }
                            LogregrField theRow = getLogregrFieldById(rows, columns.get(i * colMax / rowMax + j).getIdLogregrField());
                            if (theRow.getName().equals("intercept")) {
                                dataString = dataString + columns.get(i * colMax / rowMax + j).getValue();
                            } else {
                                dataString = dataString + columns.get(i * colMax / rowMax + j).getValue() + " * " + theRow.getName();
                            }
                        }
                        dataString += "<br><br>";
                    }
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression Formula", ex.getMessage());
            result = "LR0012 Exception parsing Logistic Regression Formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                success.put("success", true);
                status.put("message", dataString);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String cloneLRF(String payload, Integer currUserId) {
        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));
            Logregr lr = logregrController.findLogregr(idLogregr);
            Logregr lrNew = new Logregr();
            lrNew.setName(lr.getName() + " Clone");
            lrNew.setIdOrganization(lr.getIdOrganization());
            lrNew.setActive(Boolean.TRUE);
            lrNew.setIdModel(lr.getIdModel());
            lrNew.setIdProcess(0);
            lrNew.setIdStudy(lr.getIdStudy());
            lrNew.setIntervalData(lr.getIntervalData());
            lrNew.setModelColumnidSequencer(lr.getModelColumnidSequencer());
            lrNew.setDescription(lr.getDescription());
            lrNew.setOriginalLogregrId(lr.getIdLogregr());
            lrNew.setStatus("cloned");
            lrNew = (Logregr) logregrController.createObjects(lrNew, currUserId);
            List<LogregrField> rows = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);
            List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            HashMap<Integer, Integer> modelColumnLookup = new HashMap();
            for (LogregrField lrf : rows) {
                LogregrField lrfNew = new LogregrField();
                lrfNew.setName(lrf.getName());
                lrfNew.setActive(Boolean.TRUE);
                lrfNew.setDescription(lrf.getDescription());
                lrfNew.setModelColumnidRisk(lrf.getModelColumnidRisk());
                lrfNew.setValue(lrf.getValue());
                lrfNew.setIdLogregr(lrNew.getIdLogregr());
                lrfNew.setCreatedTS(currentTimestamp);
                lrfNew.setCreatedBy(currUserId);                 
                lrfNew = logregrfieldController.create(lrfNew);
                modelColumnLookup.put(lrf.getIdLogregrField(), lrfNew.getIdLogregrField());
            }
            for (LogregrDate lrd : columns) {
                LogregrDate lrdNew = new LogregrDate();
                lrdNew.setName(lrd.getName());
                lrdNew.setActive(Boolean.TRUE);
                lrdNew.setValue(lrd.getValue());
                lrdNew.setDescription(lrd.getDescription());
                lrdNew.setModelColumnidSequencer(lrd.getModelColumnidSequencer());
                lrdNew.setIdLogregrField(modelColumnLookup.get(lrd.getIdLogregrField()));
                lrdNew.setIdLogregr(lrNew.getIdLogregr());
                lrdNew.setCreatedTS(currentTimestamp);
                lrdNew.setCreatedBy(currUserId);                   
                logregrdateController.create(lrdNew);
            }
            //dataString = new JSONObject(lrNew).toString();
            dataString = "Logistic Regression Formula cloned";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression Formula", ex.getMessage());
            result = "LR0013 Exception parsing Logistic Regression Formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                success.put("success", true);
                status.put("message", dataString);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }
    
    public String getLRFIntervals(String studyId, String type, String chunking, Integer currentUserId) {
            
        String responseString = "";
        Study s = (Study) studyController.findStudy(Integer.parseInt(studyId));
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());
        responseString = "[";
        boolean isChunking = false;
        if ( chunking == "true" ) {
            isChunking = true;
        }
         
        List<String> chunks = getDateChunks(isChunking, type, s.getStartTS(), s.getEndTS());
        for (int i=0; i< chunks.size(); i++) {
            responseString += "{\"interval\":\"" + chunks.get(i) + "\",";
            responseString += "\"intervalQuery\":\"" + mc.getName() + " >= '" + chunks.get(i) + "'";
            if ( i + 1 < chunks.size() ) {
                // it is not the last column
                responseString += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'\"},";
            } else {
                responseString += "\"}";
            }
            /*if (i == 0) {
                responseString += "\"intervalQuery\":\"" + mc.getName() + " < '" + chunks.get(i) + "'";
            } else {
                responseString += "\"intervalQuery\":\"" + mc.getName() + " >= '" + chunks.get(i) + "'"
                        + " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'\"},";
            }*/
        }
        return responseString + "]";
    }    

    public String deleteLogregr(String JSONString, Integer currentUserId) {
        String result = "Objects(s) not deleted";
        String dataString = "";
        Logregr subject = new Logregr();
        try {
            JSONArray objectArray = new JSONArray(JSONString);
            for (int i = 0; i < objectArray.length(); i++) {
                logger.log(Level.INFO, "Deleting Object {0} " + Integer.toString(i), logregrController.getObjectType());
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());
                List<LogregrField> lrf = logregrfieldController.findLogregrFieldByLogregrId(((Logregr) subject).getIdLogregr());
                deleteLogregrFields(lrf);
                List<LogregrDate> lrd = logregrdateController.findLogregrDateByLogregrId(((Logregr) subject).getIdLogregr());
                deleteLogregrDates(lrd);
                logregrController.deleteObject(subject, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(subject).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Logistic Regression object: {0}", ex.getMessage());
            result = "LR0014 Exception while deleting Logistic Regression object: " + ex.getMessage();
        }
        return wrapAPI(logregrController.getObjectType(), dataString, "Object(s) deleted", result);
    }
    
    // this method prepares time periods for Logistic Regression formula 
    // based on Study start and end dates and chunking flag
    // chunking is done by year only in this implementation
    private List<String> getDateChunks(boolean chunking, String type, Timestamp tsStart, Timestamp tsEnd) {
        List<String> stringChunks = new ArrayList();
        String startYear = new SimpleDateFormat("yyyy").format(tsStart);        
        String monthDay = new SimpleDateFormat("MM/dd").format(tsStart);
        String startMonth = new SimpleDateFormat("MM").format(tsStart);
        String startDay = new SimpleDateFormat("dd").format(tsStart);
        int intervalYearStart = Integer.parseInt(startYear);    
        int intervalMonthStart = Integer.parseInt(startMonth);          
        
        if ( chunking == false ) {
            // start with study start date
            stringChunks.add(new SimpleDateFormat("yyyy/MM/dd").format(tsStart));       
            switch ( type ) {
                case "year":
                    // add one year
                    intervalYearStart++;
                    stringChunks.add(Integer.toString(intervalYearStart) + "/" + monthDay);   
                    break;
                case "quarter":
                    // add two months
                    intervalMonthStart += 2;    
                case "month":
                    // add one month
                    intervalMonthStart += 1;
                    if ( intervalMonthStart > 12 ) {
                        intervalMonthStart = intervalMonthStart % 12;
                        intervalYearStart++; 
                    }                    
                    stringChunks.add(Integer.toString(intervalYearStart) + "/" + Integer.toString(intervalMonthStart)
                        + "/" + startDay);   
                    break;                     
            }
        } else {
            switch ( type ) {
                case "year":            
                    int start = Integer.parseInt(startYear);
                    String endYear = new SimpleDateFormat("yyyy").format(tsEnd);
                    int end = Integer.parseInt(endYear);
                    for (int i=0; start <= end; start++, i++) {
                        stringChunks.add(Integer.toString(start) + "/" + monthDay);
                    }
                    break;
            }
        }
        return stringChunks;
    }
  
    //-------------------------------------------------------------------------- LRF parsing, etc
    // this method is called before submitting job to calculate LRF
    // it (should) create logergDate for constant/intercept row for selected Interval
    // right now it defaults to first year with no chunking
    public List<LogregrDate> initializeLRFResults(com.copsys.delta.entity.process.Process p, Integer currentUserId) {
            
        List<LogregrDate> lrdList = new ArrayList();
        Study s = (Study) studyController.findStudy(p.getIdStudy());
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());
        Logregr lr = new Logregr();
        lr.setIdModel(p.getIdModel());
        lr.setIdStudy(p.getIdStudy());
        lr.setIdProcess(p.getIdProcess());        
        lr.setModelColumnidSequencer(s.getIdDate());
        lr.setName("LRF for Study " + Integer.toString(p.getIdStudy()));
        lr.setDescription(p.getName());
        lr.setFormula(p.getData());
        lr.setActive(Boolean.TRUE);
        lr.setOriginalLogregrId(0);
        lr.setStatus("auto");
        try {
            List<String> chunks = getDateChunks(s.getIsChunking(), s.getChunkingBy(),  s.getStartTS(), s.getEndTS());
            lr.setIntervalData(chunks.get(0)); // for now use first interval
            lr.setIntervalQuery(getIntervalQueryStep2(s));
            lr = (Logregr) logregrController.createObjects(lr, p.getIdOrganization(), currentUserId);
            LogregrField lrf = new LogregrField();
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            lrf.setCreatedTS(currentTimestamp);
            lrf.setCreatedBy(currentUserId);        
            lrf.setUpdatedTS(currentTimestamp);
            lrf.setUpdatedBy(currentUserId);               
            lrf.setActive(Boolean.TRUE);
            lrf.setIdLogregr(lr.getIdLogregr());
            lrf.setModelColumnidRisk(0); // means constant/intercept record
            lrf.setValue("C");
            lrf.setName("intercept");
            //lrf = logregrfieldController.create(lrf);          

            for (int i=0; i< chunks.size(); i++) {
                LogregrDate lrd = new LogregrDate();
                lrd.setActive(Boolean.TRUE);
                lrd.setIdLogregr(lr.getIdLogregr());
                lrd.setIdLogregrField(lrf.getIdLogregrField());
                lrd.setModelColumnidSequencer(s.getIdDate());
                lrd.setValue("0.0");    
                lrd.setCreatedTS(currentTimestamp);
                lrd.setCreatedBy(currentUserId);        
                lrd.setUpdatedTS(currentTimestamp);
                lrd.setUpdatedBy(currentUserId);                    
                lrd.setName(chunks.get(i));
                String sqlWhereClause = mc.getName() + " >= '" + chunks.get(i) + "'";
                if ( i + 1 < chunks.size() ) {
                    // it is not the last column
                    sqlWhereClause += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
                }
                        /*if (i == 0) {
                            sqlWhereClause = mc.getName() + " < '" + chunks.get(i) + "'";
                        } else {
                            sqlWhereClause = mc.getName() + " >= '" + chunks.get(i) + "'"
                                    + " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
                        }    */            
                lrd.setDescription(sqlWhereClause);
                //logregrdateController.create(lrd);
                lrdList.add(lrd);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while persisting LRF for process: {0} {1}", new Object[]{p.getName(), ex.getMessage()});
        }
        return lrdList;
    }
    
    // method evoked when response is recieved from external stat package
    // //first it moves results from Process to existing Logregr
    // or creates a new Logregr
    public String saveLRFResults(com.copsys.delta.entity.process.Process p, Study s, String lrfResults) {

        try {
            List<Logregr> lrList = logregrController.findLogregrByProcessId(p.getIdProcess());
            Logregr lr;
            if (lrList.size() > 0) {
                lr = lrList.get(0); // assume one logreg one process relationship
                lr.setFormula(p.getData());
                lr = (Logregr) logregrController.updateObjects(lr, p.getIdOrganization(), 0);
            } else {
                lr = new Logregr();
                lr.setIdModel(p.getIdModel());
                lr.setIdStudy(p.getIdStudy());
                lr.setName("LRF for Study " + p.getName());
                lr.setDescription(p.getName());
                lr.setFormula(p.getData());
                lr.setActive(Boolean.TRUE);
                lr.setOriginalLogregrId(0);
                lr.setStatus("auto");
                lr = (Logregr) logregrController.createObjects(lr, p.getIdOrganization(), 0); 
            }
            parseLRFResults(p, lr, 0); // 0 means system
            String modelLRF = getLRFforStudy(s.getIdStudy().toString());
            JSONObject newModel = new JSONObject(modelLRF);
            JSONArray params = new JSONArray(s.getMethodParams());     
            JSONObject inputs = params.getJSONObject(1).getJSONObject("inputs");   // Step 2 of LR
            inputs.put("model", newModel.getJSONObject("model"));
            params.getJSONObject(1).put("inputs",inputs);
            s.setMethodParams(params.toString());
            s = (Study) studyController.edit(s);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving LR from process with ID: {0} {1}", new Object[]{p.getName(), ex});
            return "LR0015 Error receiving LRF: " + ex.getMessage();
        }
        return null;
    }

    public String formatLRFParamsStep2(Study s) {
        String responseStringModel = "{\"model\": {\"type\": \"ArrayList<String>\",\"class\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
        List<Logregr> lrList = logregrController.findLogregrByStudyId(s.getIdStudy());    
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());
        for (int i=0; i<lrfList.size(); i++) {
            if ( i > 0 ) {
                responseStringModel += ",";
            }
            String fieldName = lrfList.get(i).getName();
            responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
            List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
            responseStringModel += lrdList.get(0).getValue() + "\"}";
        }
        responseStringModel += "]}";
        // defaut to generic "id"
        String responseStringUniqueId = "\"studyUniqueId\":{\"type\":\"String\",\"class\":\"Case ID\",\"inputText\":\"Unique ID\",\"description\": \"Select the unique id for the study\",\"required\": false,\"values\": [\"id\"]}";
        String responseStringDepend = "\"dependantVariableSelection\":{\"values\":[\"";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdOutcome());
        responseStringDepend += mc.getName();
        responseStringDepend += "\"],\"inputText\": \"Dependent Variable\",\"description\": \"Select the dependent variable for the study from the list\",\"required\": true,\"type\": \"String\",\"class\": \"Outcome\"}}";
        return responseStringModel + "," + responseStringUniqueId + "," + responseStringDepend;
    }

    public String getLRFforStudy(String studyId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByStudyId(Integer.valueOf(studyId));
        if ( lrList.size() < 1 ) {
            result = "{\"model\":\"\"}";
        } else {
            Logregr lr = lrList.get(lrList.size()-1); // get the last one
            //String stringJSON = "{\"logregrs\":[{\"idLogregr\":" + lr.getIdLogregr().toString()
            //        + ",\"name\":\""  + lr.getName() + "\"}]}";  
            //result = getLRFString(stringJSON, userId);   
            result = formatLRFModelString(lrList);
        }
        return result;
    }
 

    public String getLRFforProcess(String processId, Integer userId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByProcessId(Integer.valueOf(processId));
        if ( lrList.size() < 1 ) {
            result = "{\"logregrs\":\"\"}";
        } else {        
            Logregr lr = lrList.get(lrList.size()-1); // get the last one (and the only one)
            String stringJSON = "{\"logregrs\":[{\"idLogregr\":" + lr.getIdLogregr().toString()
                    + ",\"name\":\""  + lr.getName() + "\"}]}";  
            result = getLRFString(stringJSON, userId);    
        }
        return result;
    }
    
    public String formatLRFModelString(List<Logregr> lrList) {
        String responseStringModel = "{\"model\": {\"type\": \"ArrayList<String>\",\"class\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());
        for (int i=0; i<lrfList.size(); i++) {
            if ( i > 0 ) {
                responseStringModel += ",";
            }
            String fieldName = lrfList.get(i).getName();
            responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
            List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
            responseStringModel += lrdList.get(0).getValue() + "\"}";
        }
        responseStringModel += "]}}";
        return responseStringModel;
    }
    
    public String getIntervalQueryStep2(Study s) {
        String responseString = "";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());

        List<String> chunks = getDateChunks(s.getIsChunking(), s.getChunkingBy(), s.getStartTS(), s.getEndTS());
        for (int i=0; i< chunks.size(); i++) {
            responseString = mc.getName() + " >= '" + chunks.get(i) + "'";
            if ( i + 1 < chunks.size() ) {
                // it is not the last column
                responseString += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
            } 
            if ( (s.getIsChunking() == false) && (i == 1) ) {
                // if no chunking select everything past first interval
                return responseString;
            }
        }        
        return responseString;
    }    

    public String prepareJSONResponse(String callId, String message, String result, int counter) {
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            if ("".equals(result)) {
                success.put("success", false);
                status.put("message", message);
            } else {
                success.put("success", true);
                status.put("message", message);
                success.put("totalCount", Integer.toString(counter));
                success.put(callId, new JSONArray(result));
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in DELTA3 WS method " + callId, ex);
            result = "{\"status\":{\"message\":\"JSON error in DELTA3 WS method " + callId + "\"},\"success\":false}";
        }
        return result;
    }

    private String parseLRFResults(com.copsys.delta.entity.process.Process p, Logregr lr, Integer currentUserId) {
        String result = "Parsing of Logistic Regression Formula results. ";
        JSONObject formulaJSON;
        JSONArray resultsArrayJSON;
        JSONObject resultsJSON;
        JSONArray dataArrayJSON;
        JSONArray theArray;
        //List<ModelColumn> mc = new ArrayList();  
        Study s = (Study) studyController.findStudy(p.getIdStudy());
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());

        try {
            logger.log(Level.INFO, "Retrieving logregr object {0}", Integer.toString(lr.getIdLogregr()));
            formulaJSON = new JSONObject(lr.getFormula());
            resultsArrayJSON = formulaJSON.getJSONArray("results");
            // process only first array member
            resultsJSON = (JSONObject) resultsArrayJSON.get(0);
            dataArrayJSON = resultsJSON.getJSONArray("data");

            // remove all regrfields and regrdates attached to this logregr object
            List<LogregrField> lrf = logregrfieldController.findLogregrFieldByLogregrId(lr.getIdLogregr());
            deleteLogregrFields(lrf);
            List<LogregrDate> lrd = logregrdateController.findLogregrDateByLogregrId(lr.getIdLogregr());
            deleteLogregrDates(lrd);

            for (int i = 0; i < dataArrayJSON.length(); i++) {
                theArray = dataArrayJSON.getJSONArray(i);
                LogregrField entF = new LogregrField();
                entF.setActive(Boolean.TRUE);
                String fieldName = (String) theArray.get(1);
                if ( "intercept".equals(fieldName) ) {
                    //fieldName = "Constant";
                    entF.setModelColumnidRisk(0);
                } else {
                    List<ModelColumn> mcList = modelColumnController.findModelColumnByModelAndName(lr.getIdModel(), fieldName);
                    if (mcList.isEmpty()) {
                        result += " Field " + fieldName + " not found. ";
                        continue;
                    }
                    entF.setModelColumnidRisk(mcList.get(0).getIdModelColumn());
                }
                entF.setName(fieldName);
                entF.setValue("");
                entF.setIdLogregr(lr.getIdLogregr());
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
                entF.setCreatedTS(currentTimestamp);
                entF.setUpdatedTS(currentTimestamp);
                entF.setCreatedBy(currentUserId);
                entF.setUpdatedBy(currentUserId);
                entF = logregrfieldController.edit(entF);
                AuditLogger.log(currentUserId, entF.getIdLogregrField(), "create", entF);

                LogregrDate entD = new LogregrDate();
                List<String> chunks = getDateChunks(s.getIsChunking(), s.getChunkingBy(),  s.getStartTS(), s.getEndTS());
                //entD.setName(chunks.get(0));     // temp hardcoded to first interval
                entD.setName("");     // temp hardcoded to default blank
                String sqlWhereClause = mc.getName() + " >= '" + chunks.get(0) + "'";
                if ( i + 1 < chunks.size() ) {
                    // it is not the last column
                    sqlWhereClause += " AND " + mc.getName() + " < '" + chunks.get(1) + "'";
                }
                /*String sqlWhereClause = null;
                        if (i == 0) {
                            sqlWhereClause = mc.getName() + " < '" + chunks.get(i) + "'";
                        } else {
                            sqlWhereClause = mc.getName() + " >= '" + chunks.get(i) + "'"
                                    + " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
                        }  */              
                entD.setDescription(sqlWhereClause);                
                entD.setActive(Boolean.TRUE);
                entD.setIdLogregr(lr.getIdLogregr());
                entD.setIdLogregrField(entF.getIdLogregrField());
                entD.setValue(Double.toString(theArray.getDouble(2)));
                entD.setModelColumnidSequencer(lr.getModelColumnidSequencer());
                entD.setCreatedTS(currentTimestamp);
                entD.setUpdatedTS(currentTimestamp);
                entD.setCreatedBy(currentUserId);
                entD.setUpdatedBy(currentUserId);
                entD.setName(chunks.get(1));
                entD = logregrdateController.edit(entD);
                AuditLogger.log(currentUserId, entD.getIdLogregrDate(), "create", entD);
            }
            result = "Logistic Regression Formula results succesfully parsed and stored.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression object into JSON", ex.getMessage());
            result = "LR0016 Exception parsing Logistic Regression object into JSON: " + ex.getMessage();
        }
        return result;
    }
 

    private String getLogregrDateJSON(int idLogregr) {
        logger.log(Level.INFO, "Retrieving Dates for Formula {0}", Integer.toString(idLogregr));
        String dataString = "";
        int counter = 0;

        List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);

        if ((columns != null) && !columns.isEmpty()) {
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID           
            counter = columns.size();
            logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula data.", new Object[]{Integer.toString(idLogregr), Integer.toString(counter)});

            for (int i = 0; i < counter; i++) {
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(columns.get(i)).toString();
            }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    }
    
    private LogregrField updateLogregrField(LogregrField ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} updating relationship between field {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString((ent.getModelColumnidRisk())),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "update", ent);
        return ent;
    }
 
    private LogregrField deleteLogregrField(int idLogregrField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and formula", new Object[]{Integer.toString((currentUserId)), Integer.toString((idLogregrField))});

        LogregrField ent = new LogregrField(idLogregrField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrfieldController.destroy(idLogregrField);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "delete", ent);
        return ent;
    }   
    
    private String getLogregrFieldJSON(int idLogregr) {
        logger.log(Level.INFO, "Retrieving Fileds for Formula {0}", Integer.toString(idLogregr));
        String dataString = "";
        int counter = 0;

        List<LogregrField> columns = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);

        if ((columns != null) && !columns.isEmpty()) {
            counter = columns.size();
            logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula data.", new Object[]{Integer.toString(idLogregr), Integer.toString(counter)});

            for (int i = 0; i < counter; i++) {
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(columns.get(i)).toString();
            }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    }

    private LogregrDate createLogregrDate(LogregrDate ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} creating relationship between date field {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString(ent.getModelColumnidSequencer()),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        if ( "".equals(ent.getValue()) ) {
            ent.setValue("0.0");
            }
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrdateController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "create", ent);
        return ent;
    }
    
    private LogregrField createLogregrField(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} creating relationship between risk field {1} and logregr formula {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getModelColumnidRisk()), ent.getName()});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "create", ent);
        return ent;
    }

    private String createLogregrFieldString(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        String dataString;
        String regrDateString;
        logger.log(Level.INFO, "User {0} creating relationship between risk field {1} and logregr formula {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getModelColumnidRisk()), ent.getName()});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "create", ent);
        dataString = new JSONObject(ent).toString();
        regrDateString = duplicateFieldDates(ent, currentUserId);
        return new StringBuilder(dataString).insert(dataString.length() - 1, "," + regrDateString).toString();
    }

    private String duplicateFieldDates(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        String dataString = "";
        logger.log(Level.INFO, "User {0} duplicating relationship between risk field {1} and date", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getIdLogregrField()), ent.getName()});

        List<LogregrField> fieldList = logregrfieldController.findLogregrFieldByLogregrId(ent.getIdLogregr());
        if (fieldList.size() > 1) { // check if there are more LegregrFields beyond the one being processed
            LogregrField topEnt = fieldList.get(0); // use first one as a source of Date records
            int ii = 0;
            List<LogregrDate> dateList = logregrdateController.findLogregrDateByLogregrFieldId(topEnt.getIdLogregrField());
            // to make sure JSON passed to UI is sorted by dates which become header names in UI:
            Collections.sort(dateList, new LogregrDateComp());
            // the following is necessary to revers previous sort and mimic similar action in getLogregrDates()
            Collections.sort(dateList, new LogregrDateFieldComp()); // sort by LogregrField ID
            for (LogregrDate t : dateList) {
                logger.log(Level.INFO, "Duplicating Formula Date {0}", new Object[]{Integer.toString(t.getIdLogregrDate())});
                t.setIdLogregrField(ent.getIdLogregrField());
                t.setIdLogregr(ent.getIdLogregr());
                t.setValue("");
                t.setIdLogregrDate(0);
                t = createLogregrDate(t, currentUserId);
                if (ii++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(t).toString();
            }
        }
        return "\"logregrdates\":[" + dataString + "]";
    }

    private LogregrField deleteLogregrFields(List<LogregrField> list) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        for (LogregrField row : list) {
            logregrfieldController.destroy(row.getIdLogregrField());
        }
        return null;
    }

    private LogregrField deleteLogregrDates(List<LogregrDate> list) throws com.copsys.delta.jpa.exceptions.NonexistentEntityException {
        for (LogregrDate row : list) {
            logregrdateController.destroy(row.getIdLogregrDate());
        }
        return null;
    }

    private LogregrField getLogregrFieldById(List<LogregrField> list, Integer idx) {
        for (LogregrField row : list) {
            if (Objects.equals(row.getIdLogregrField(), idx)) {
                return row;
            }
        }
        return null;
    }

    /* delegete this work to common method in admin service */
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }        
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }
    
      
}
