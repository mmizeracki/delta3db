/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.server;

import com.copsys.delta.db.PersistenceManager;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Boston Advanced Analytics
 */
public class StartStopContextListener implements ServletContextListener {
    private static final Logger logger = Logger.getLogger(StartStopContextListener.class.getName());  
    NotificationTimerTask notificationTimer;
    CommunicationTimerTask communicationTimer;
    StatServiceImpl statService;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.log(Level.INFO, "Starting Tomcat DELTA3 context!");
        // create the thread pool
        int cpus = Runtime.getRuntime().availableProcessors();
        int maxThreads = (int) (cpus * 0.5);
        maxThreads = (maxThreads > 0 ? maxThreads : 1);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(maxThreads, maxThreads, 120,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(2*maxThreads));
        sce.getServletContext().setAttribute("executor",executor);        
        notificationTimer = new NotificationTimerTask();
        notificationTimer.init();       
        statService = new StatServiceImpl();
        statService.initializeStatPackage("system");
        communicationTimer = new CommunicationTimerTask();
        communicationTimer.init();         
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.log(Level.INFO, "Stopping Tomcat DELTA3 context!");
        ThreadPoolExecutor executor = (ThreadPoolExecutor) sce.getServletContext().getAttribute("executor");
        executor.shutdown();       
        if ( statService.proxy != null ) {
            statService.proxy.shutdown();
        }
        PersistenceManager.closeEntityManagerFactory();
    }

}