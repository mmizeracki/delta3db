/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.server.exceptions;

import java.io.Serializable;

/**
 *
 * @author Coping Systems Inc.
 */
public class UserExistsException extends Exception implements Serializable {

  private String param;

  public UserExistsException() {
  }

  public UserExistsException(String user) {
    this.param = user;
  }

  public String getParam() {
    return this.param;
  }
}