/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server.exceptions;

/**
 *
 * @author Coping Systems Inc.
 */

public class EntityExistsException extends Exception {
    public EntityExistsException(String message, Throwable cause) {
        super(message, cause);
    }
    public EntityExistsException(String message) {
        super(message);
    }
}