/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.server.exceptions;

/**
 *
 * @author Coping Systems Inc.
 */
import java.io.Serializable;

/**
 *
 * @author Marek
 */
public class CopsysException extends Exception implements Serializable {

  private String param1;
  private String param2;
  private String param3;

  public CopsysException() {
  }

  public CopsysException(String param) {
    this.param1 = param;
  }

  public CopsysException(String param1, String param2) {
    this.param1 = param1;
    this.param2 = param2;
  }

  public CopsysException(String param1, String param2, String param3) {
    this.param1 = param1;
    this.param2 = param2;
    this.param3 = param3;
  }

  public String getParam1() {
    return this.param1;
  }

  public String getParam2() {
    return this.param2;
  }

  public String getParam3() {
    return this.param3;
  }
}