/*
 * Boston Advanced Analytics
 */
package com.copsys.delta.server.rest;

import com.copsys.delta.entity.events.Alert;
import com.copsys.delta.entity.events.Event;
import com.copsys.delta.entity.events.Eventtemplate;
import com.copsys.delta.entity.events.Notification;
import com.copsys.delta.jpa.event.AlertJpaController;
import com.copsys.delta.jpa.event.EventJpaController;
import com.copsys.delta.jpa.event.EventtemplateJpaController;
import com.copsys.delta.jpa.event.NotificationJpaController;
import com.copsys.delta.security.Auth;
import com.copsys.delta.server.AdminServiceImpl;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc.
 */
@Path("event")
@Consumes({"text/plain", "text/html", "application/html", "application/xhtml", "application/x-www-form-urlencoded", "application/json"})
public class Delta3EventResource {

    private static final Logger logger = Logger.getLogger(Delta3EventResource.class.getName());
    private static AdminServiceImpl adminService;
    private static EventtemplateJpaController eventtemplateController;
    private static EventJpaController eventController;
    private static NotificationJpaController notificationController;
    private static AlertJpaController alertController;

    public Delta3EventResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }         
        if (eventController == null) {
            eventController = new EventJpaController();
        }        
        if (eventtemplateController == null) {
            eventtemplateController = new EventtemplateJpaController();
        }
        if (alertController == null) {
            alertController = new AlertJpaController();
        } 
        if (notificationController == null) {
            notificationController = new NotificationJpaController();
        }        
    }
    //-------------------------------------------------------------------------- Eventtemplates

    /**
     * @param page
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getEventtemplates")
    @Produces("application/json")
    public Response getEventtemplates(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.getEventtemplates]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), eventtemplateController, new Eventtemplate());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);
        return Response.ok(result).build();
    }

    @Path("/createEventtemplates")
    @POST
    @Produces("application/json")
    public Response createEventtemplates(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.createEventtemplates]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, eventtemplateController, new Eventtemplate());
            } else {
                result = adminService.createObjects(decodedContent, userId, eventtemplateController, new Eventtemplate());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Eventtemplate not created").build();
    }

    @POST
    @Path("/updateEventtemplates")
    @Produces("application/json")
    public Response updateEventtemplates(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.updateEventtemplates]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, eventtemplateController, new Eventtemplate());
            } else {
                result = adminService.updateObjects(decodedContent, userId, eventtemplateController, new Eventtemplate());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Eventtemplate not updated").build();
    }

    //-------------------------------------------------------------------------- Alerts    
    @GET
    @Path("/getAlerts")
    @Produces("application/json")
    public Response getAlerts(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.getAlerts]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        result = adminService.getObjectsById(Integer.parseInt(start), Integer.parseInt(limit), userId, alertController, new Alert());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);
        return Response.ok(result).build();
    }

    @Path("/createAlerts")
    @POST
    @Produces("application/json")
    public Response createAlerts(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.createAlerts]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, alertController, new Alert());
            } else {
                result = adminService.createObjects(decodedContent, userId, alertController, new Alert());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Alert not created").build();
    }

    @POST
    @Path("/updateAlerts")
    @Produces("application/json")
    public Response updateAlerts(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.updateAlerts]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, alertController, new Alert());
            } else {
                result = adminService.updateObjects(decodedContent, userId, alertController, new Alert());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Alert not updated").build();
    }

    @Path("/removeAlerts")
    @POST
    @Produces("application/json")
    public Response removeAlerts(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.removeAlerts]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, alertController, new Alert());
            } else {
                result = adminService.deleteObjects(decodedContent, userId, eventController, new Alert());
            }
            return Response.ok(result.toString()).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.serverError().build();
    }

    //-------------------------------------------------------------------------- Events    
    @GET
    @Path("/getEvents")
    @Produces("application/json")
    public Response getEvents(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.getEvents]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, eventController, new Event());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);
        return Response.ok(result).build();
    }

    @Path("/createEvents")
    @POST
    @Produces("application/json")
    public Response createEvents(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.createEvents]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, eventController, new Event());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, eventController, new Event());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Event not created").build();
    }

    @POST
    @Path("/updateEvents")
    @Produces("application/json")
    public Response updateEvents(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.updateEvents]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, eventController, new Event());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, eventController, new Event());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Event not updated").build();
    }

    @Path("/removeEvents")
    @POST
    @Produces("application/json")
    public Response removeEvents(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.removeEvents]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, eventController, new Event());
            } else {
                result = adminService.deleteObjects(decodedContent, userId, eventController, new Event());
            }
            return Response.ok(result.toString()).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.serverError().build();
    }

    //-------------------------------------------------------------------------- Notifications
    @GET
    @Path("/getNotifications")
    @Produces("application/json")
    public Response getNotifications(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String result = null;
        String logId = "DELTA3 WS method [event.getNotifications]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        result = adminService.getObjectsById(Integer.parseInt(start), Integer.parseInt(limit), userId, notificationController, new Notification());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);
        return Response.ok(result).build();
    }

    @Path("/createNotifications")
    @POST
    @Produces("application/json")
    public Response createNotifications(String payload, @Context HttpServletRequest request) {
        String result = null;
        String logId = "DELTA3 WS method [event.createNotifications]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, notificationController, new Notification());
            } else {
                result = adminService.createObjects(decodedContent, userId, notificationController, new Notification());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Notification not created").build();
    }

    @POST
    @Path("/updateNotifications")
    @Produces("application/json")
    public Response updateNotifications(String payload, @Context HttpServletRequest request) {
        String result = null;
        String logId = "DELTA3 WS method [event.updateNotifications]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, notificationController, new Notification());
            } else {
                result = adminService.updateObjects(decodedContent, userId, notificationController, new Notification());
            }
            return Response.ok(result).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Notification not updated").build();
    }

    @Path("/removeNotifications")
    @POST
    @Produces("application/json")
    public Response removeNotifications(String payload, @Context HttpServletRequest request) {
        String result = "";
        String logId = "DELTA3 WS method [event.removeNotifications]";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            if (decodedContent.charAt(0) == '{') { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, notificationController, new Notification());
            } else {
                result = adminService.deleteObjects(decodedContent, userId, notificationController, new Notification());
            }
            return Response.ok(result.toString()).build();
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }
        return Response.serverError().build();
    }

}
