/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server.rest;

import com.copsys.delta.entity.Configuration;
import com.copsys.delta.entity.GroupTable;
import com.copsys.delta.entity.Method;
import com.copsys.delta.entity.Organization;
import com.copsys.delta.entity.Permissiontemplate;
import com.copsys.delta.entity.Role;
import com.copsys.delta.entity.User;
import com.copsys.delta.entity.events.Eventtemplate;
import com.copsys.delta.jpa.ConfigurationJpaController;
import com.copsys.delta.jpa.GroupJpaController;
import com.copsys.delta.jpa.MethodJpaController;
import com.copsys.delta.jpa.OrganizationJpaController;
import com.copsys.delta.jpa.PermissiontemplateJpaController;
import com.copsys.delta.jpa.RoleJpaController;
import com.copsys.delta.jpa.UserJpaController;
import com.copsys.delta.security.Auth;
import com.copsys.delta.server.AdminServiceImpl;
import com.copsys.delta.server.EventServiceImpl;
import com.copsys.delta.server.ModelServiceImpl;
import com.copsys.delta.server.processor.ProcessFlatTable;
import com.copsys.delta.server.processor.ProcessFlatTableNoMissing;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc
 */

@Path("admin")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3AdminResource {
    private static final Logger logger = Logger.getLogger(Delta3AdminResource.class.getName());;
    private static AdminServiceImpl adminService;
    private static UserJpaController userController;
    private static OrganizationJpaController organizationController;
    private static RoleJpaController roleController;  
    private static PermissiontemplateJpaController permissiontemplateController;   
    private static GroupJpaController groupController;
    private static ConfigurationJpaController configurationController;
    private static MethodJpaController methodController;  
    private static EventServiceImpl eventService;
    private static ModelServiceImpl modelService;    

    /**
     * Creates a new instance of Delta3AdminResource
     */
    public Delta3AdminResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( userController == null ) {
            userController = new UserJpaController();
        }      
        if ( organizationController == null ) {
            organizationController = new OrganizationJpaController();
        }    
        if ( roleController == null ) {
            roleController = new RoleJpaController();
        }         
        if ( permissiontemplateController == null ) {
            permissiontemplateController = new PermissiontemplateJpaController();
        }     
        if ( groupController == null ) {
            groupController = new GroupJpaController();
        }   
        if ( configurationController == null ) {
            configurationController = new ConfigurationJpaController();
        }  
        if ( methodController == null ) {
            methodController = new MethodJpaController();
        }      
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }
        if ( modelService == null ) {
            modelService = new ModelServiceImpl();
        }        
    }

    @Path("/login")
    @GET
    @Produces("application/json")
    public Response loginUser(@Context UriInfo info, @Context HttpServletRequest request) {
        
        logger.log(Level.SEVERE,"Received WS request to authenticate user");        
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();        
        String userAlias = info.getQueryParameters().getFirst("Username");
        String password = info.getQueryParameters().getFirst("Password");
        String result = adminService.login(userAlias, password, request.getSession());
        try {
            if ( !result.equals("") ) {
                success.put("success", "false");                  
                status.put("message", result);                
                success.put("status", status);
            } else {
                success.put("success", "true");                  
                status.put("message", "User authenticated.");                
                success.put("status", status);       
  
            }   
            return Response.ok(success.toString()).build();                
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS authUser: ", ex);
        }
        return Response.serverError().build();  
    }

     /**
     * GET method for updating or creating an instance of Delta3AdminResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @Path("/loginUserAndGetAuth")
    @GET
    @Produces("application/json")
    public Response loginUserAndGetAuth(@Context UriInfo info, @Context HttpServletRequest request) {
        
        logger.log(Level.SEVERE,"Received WS request to authenticate and authorize user");        
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();     
        String userAlias = info.getQueryParameters().getFirst("Username");
        String password = info.getQueryParameters().getFirst("Password");
        String result = adminService.login(userAlias, password, request.getSession());        
        try {
            if ( !result.equals("") ) {
                success.put("success", "false");                  
                status.put("message", result);                
                success.put("status", status);
                return Response.status(Response.Status.NOT_FOUND).entity("User " + userAlias + " not authenticated.").build();
            } else {   
                result = "Not authorized"; // to be overwritten by call to adminService
                try {    
                    result = adminService.getUserAuthInfo(userAlias);            
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception in WS loginUserAndGetAuth: ", ex.getMessage()); 
                }                  
                return Response.ok(result).build();      
            }   
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS authUser: ", ex);
        }
        return Response.serverError().build();  
    }
    
    @Path("/logout")
    @POST
    @Produces("application/json")
    public Response logoutUser(@Context HttpServletRequest request) {
        
        logger.log(Level.SEVERE,"Received WS request to logout user");        
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();        
        try {
            if ( !adminService.logout(request.getSession()) ) {
                success.put("success", "false");                  
                status.put("message", "User session not terminateted");                
                success.put("status", status);
                return Response.status(Response.Status.NOT_FOUND).entity("User session not terminated.").build();
            } else {
                success.put("success", "true");                  
                status.put("message", "User session terminated");                
                success.put("status", status);       
                return Response.ok(success.toString()).build();      
            }   
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS logoutUser: ", ex);
        }
        return Response.serverError().build();  
    }
        
    @Path("/getCurrentUser")
    @GET
    @Produces("application/json")
    public Response getCurrentUser(@Context HttpServletRequest request) {
        
        String alias = null;
        
        if ( (alias = Auth.getUserAliasFromSession(request.getSession())) != null ) {
           return Response.ok(alias).build();
        } else {
           return Response.status(Response.Status.NOT_FOUND).entity("User not logged in").build();            
        }
    }
    
    @Path("/auth")
    @POST
    @Produces("application/json")
    public Response authUser(String load) {
        
        logger.log(Level.SEVERE,"Received WS request to authenticate user: " + load); 
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();            
        JSONObject requestContent;        
        String userAlias = null;
        String password = null; 
        String organization = null;
        
        try {
            requestContent = new JSONObject(load);
            JSONArray userArray = requestContent.getJSONArray("users");
            JSONObject user = (JSONObject)userArray.get(0);
            userAlias = user.getString("userAlias");
            password = user.getString("password");   
            organization = user.getString("organization");
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        try {
            String result = adminService.authorize(userAlias, password, organization);
            if ( result.equals("OK") == true ) {
                status.put("message", "User authorized");                
                success.put("status", status);
                success.put("success", true);          
                return Response.ok(success.toString()).build();      
            } else {
                status.put("message", result);                
                success.put("status", status);
                success.put("success", false);  
                return Response.ok(success.toString()).build();                      
                //return Response.status(Response.Status.NOT_FOUND).entity("User " + userAlias + "and Organization " + organization + " not authenticated.").build();
            }   
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.authUser]: ", ex);
        }
        return Response.serverError().build();  
    }
  
    @Path("/getUserAuthInfo")
    @POST
    @Produces("application/json")
    public Response getUserAuthInfo(String load, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        String userAlias = null;
        String logId = "DELTA3 WS method [admin.getUserAuthInfo]";
        String result = "WA0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
         try {
            requestContent = new JSONObject(load);
            JSONArray userArray = requestContent.getJSONArray("userAuth");
            JSONObject user = (JSONObject)userArray.get(0);
            userAlias = user.getString("alias");    
            result = adminService.getUserAuthInfo(userAlias);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }

  
    @Path("/getCurrentUserAuthInfo")
    @POST
    @Produces("application/json")
    public Response getCurrentUserAuthInfo(@Context HttpServletRequest request) {  
        String userAlias = null;
        String logId = "DELTA3 WS method [admin.getCurrentUserAuthInfo]";
        String result = "WA0001 " + logId + " failed";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        userAlias = Auth.getUserAliasFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {    
            result = adminService.getUserAuthInfo(userAlias);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " Exception: ", ex.getMessage()); 
            result = logId + " Exception: " + ex.getMessage();
        }       
        return Response.ok(result).build(); 
    }
 
    @Path("/changePassword")
    @POST  
    @Produces("application/json")
    public Response changePassword(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.changePassword]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");                     
            result = adminService.changePassword(decodedContent, userId);       
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
 
    @Path("/resetPassword")
    @POST  
    @Produces("application/json")
    public Response resetPassword(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.resetPassword]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");                     
            result = adminService.resetPassword(decodedContent, userId);       
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/addUserRole")
    @POST
    @Produces("application/json")
    public Response addUserRole(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.addUserRole]";
        String result = "WA0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.createUserRoles(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
   
    @Path("/removeUserRole")
    @POST
    @Produces("application/json")
    public Response removeUserRole(String payload, @Context HttpServletRequest request) {
 
        String logId = "DELTA3 WS method [admin.removeUserRole]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.deleteUserRoles(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
     
  
    @Path("/getRolePermissionInfo")
    @POST
    @Produces("application/json")
    public Response getRolePermissionInfo(String load, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        int idRole;
        String logId = "DELTA3 WS method [admin.getRolePermission]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
         try {
            requestContent = new JSONObject(load);
            JSONArray userArray = requestContent.getJSONArray("rolePermissions");
            JSONObject role = (JSONObject)userArray.get(0);
            idRole = role.getInt("idRole");
            if ( adminService == null ) {
                adminService = new AdminServiceImpl();
            }     
            result = adminService.getRolePermissionInfo(idRole);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }
 
    @Path("/addRolePermission")
    @POST
    @Produces("application/json")
    public Response addRolePermission(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.addRolePermission]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.createRolePermissions(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
   
    @Path("/removeRolePermission")
    @POST
    @Produces("application/json")
    public Response removeRolePermission(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.removeRolePermission]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.deleteRolePermissions(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, "Delta 3 WS method [admin.removeRolePermission] exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
 
    //-------------------------------------------------------------------------- Users    
    @GET
    @Path("/getUsers")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getUsers(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {      
        String logId = "DELTA3 WS method [admin.getUsers]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());     
        String userType = Auth.getUserTypeFromSession(request.getSession());     
        //if (adminService == null) {
        //    adminService = new AdminServiceImpl();
        //}
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        if ( "SADMIN".equals(userType) && (organizationId == 1)  ) { 
            // assuming this is COPSYS sys admin pull all users for all orgs
            result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), userController, new User());            
        } else {
            result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, userController, new User());
        }
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateUsers")
    @Produces("application/json")
    public Response updateUsers(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateUsers]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                if ( organizationId.intValue() == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.updateObjects("[" + decodedContent + "]", userId, userController, new User());
                } else {
                    result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, userController, new User());                    
                }
            } else {
                if ( organizationId.intValue() == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.updateObjects(decodedContent, userId, userController, new User());      
                } else {
                    result = adminService.updateOrgObjects(decodedContent, organizationId, userId, userController, new User());                      
                }
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createUsers")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createUsers(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createUsers]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                if ( organizationId == 1 ) { // assume this is COPSYS sys admin
                    result = adminService.createObjects("[" + decodedContent + "]", userId, userController, new User());
                } else {
                    result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, userController, new User());                    
                }
            } else {
                if ( organizationId == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.createObjects(decodedContent, userId, userController, new User());  
                } else {
                    result = adminService.createOrgObjects(decodedContent, organizationId, userId, userController, new User());                      
                }
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    //-------------------------------------------------------------------------- Organizations
    @GET
    @Path("/getOrganizations")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getOrganizations(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getOrganizations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());     
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());   
        String userType = Auth.getUserTypeFromSession(request.getSession());   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());   
        if ( "SADMIN".equals(userType) && (organizationId == 1)  ) { 
            result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationController, new Organization());
        } else {
            result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, organizationController, new Organization());
        }
        return Response.ok(result).build();       
    }    
   
    @POST
    @Path("/updateOrganizations")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateOrganizations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateOrganizations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, organizationController, new Organization());
            } else {
                result = adminService.updateObjects(decodedContent, userId, organizationController, new Organization());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, "Delta 3 WS method [admin.updateOrganizations] exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
      
    @Path("/createOrganizations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createOrganizations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createOrganizations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, organizationController, new Organization());
            } else {
                result = adminService.createObjects(decodedContent, userId, organizationController, new Organization());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    //-------------------------------------------------------------------------- Roles
    @GET
    @Path("/getRoles")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getRoles(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getRoles]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), roleController, new Role());
        return Response.ok(result).build();  
    }    
  
    @Path("/createRoles")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createRoles(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createRoles]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, roleController, new Role());
            } else {
                result = adminService.createObjects(decodedContent, userId, roleController, new Role());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateRoles")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateRoles(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateRoles]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, roleController, new Role());
            } else {
                result = adminService.updateObjects(decodedContent, userId, roleController, new Role());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
   
    //-------------------------------------------------------------------------- Permission Templates
    @GET
    @Path("/getPermissiontemplates")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getPermissiontemplates(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getPermissiontemplates]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());         
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), permissiontemplateController, new Permissiontemplate());
        return Response.ok(result).build();  
    }    
  
    @Path("/createPermissiontemplates")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createPermissionTemplates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createPermissiontemplates]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, permissiontemplateController, new Permissiontemplate());
            } else {
                result = adminService.createObjects(decodedContent, userId, permissiontemplateController, new Permissiontemplate());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updatePermissiontemplates")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updatePermissiontemplates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updatePermissiontemplates]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, permissiontemplateController, new Permissiontemplate());
            } else {
                result = adminService.updateObjects(decodedContent, userId, permissiontemplateController, new Permissiontemplate());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    //-------------------------------------------------------------------------- Groups
    @GET
    @Path("/getGroups")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getGroups(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getGroups]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), groupController, new GroupTable());
        return Response.ok(result).build();  
    }    
  
    @Path("/createGroups")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createGroups(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createGroups]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, groupController, new GroupTable());
            } else {
                result = adminService.createObjects(decodedContent, userId, groupController, new GroupTable());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateGroups")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateGroups(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateGroups]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());    
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, groupController, new GroupTable());
            } else {
                result = adminService.updateObjects(decodedContent, userId, groupController, new GroupTable());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }          
   
    //-------------------------------------------------------------------------- Configurations    
    @GET
    @Path("/getConfigurations")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getConfigurations(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getConfigurations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, configurationController, new Configuration());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateConfigurations")
    @Produces("application/json")
    public Response updateConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateConfigurations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }       
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_EVENT, "Data Source configuration updated");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createConfigurations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createConfigurations]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.createOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_EVENT, "New Data Source created");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    //-------------------------------------------------------------------------- Methods    
    @GET
    @Path("/getMethods")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getMethods(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getMethods]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, methodController, new Method());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateMethods")
    @Produces("application/json")
    public Response updateMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateMethods]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }     
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_EVENT, "Statistical Method updated");            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createMethods")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createMethods]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {

               result = adminService.createOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }      
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_EVENT, "New Statistical Method added");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
 
    @Path("/importObjects")
    @POST
    @Produces("application/json")
    public Response importObjects(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.importObjects]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = adminService.importObjects(decodedContent, organizationId, userId);                
            }   
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing object: ", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    

    @Path("/importFile")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importFile(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.importFile]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = adminService.importObjects(decodedContent, organizationId, userId);                
            }    
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing object from file: ", ex);
            result = "File import error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 

    @Path("/importModelFileAndProcessAll")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importModelFileAndProcessAll(String payload, @Context HttpServletRequest request) {
        JSONObject responseJSON;        
        String logId = "DELTA3 WS method [admin.importFileAndProcessAll]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------WebKitFormBoundary");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = adminService.importObjects(decodedContent, organizationId, userId);                
            }  
            responseJSON = new JSONObject(result);
            JSONObject statusJSON = responseJSON.getJSONObject("status");
            if ( responseJSON.getBoolean("success") == true ) {
                ThreadPoolExecutor executor = (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");
                executor.execute(new ProcessFlatTable(null, result, userId));  
                result = "Data Model imported successfully and Flat Table processing started.\nYou can monitor the process from Model Builder.";                
            } else {
                result = statusJSON.getString("message");
            }
            //result = modelService.processFlatTableAll(result, userId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing and processing Data Model: ", ex);
            result = "File import and processing Flat Table error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 
    
    @Path("/importModelFileAndProcessNoMissing")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importModelFileAndProcessNoMissing(String payload, @Context HttpServletRequest request) {
        JSONObject responseJSON;        
        String logId = "DELTA3 WS method [admin.importFileAndProcessNoMissing]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------WebKitFormBoundary");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = adminService.importObjects(decodedContent, organizationId, userId);                
            }  
            responseJSON = new JSONObject(result);
            JSONObject statusJSON = responseJSON.getJSONObject("status");
            if ( responseJSON.getBoolean("success") == true ) {
                ThreadPoolExecutor executor = (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");
                executor.execute(new ProcessFlatTableNoMissing(null, result, userId));  
                result = "Data Model imported successfully and Flat Table processing started." 
                        + "\nMissing fields processing turned off."
                        + "\nYou can monitor the process from Model Builder.";                
            } else {
                result = statusJSON.getString("message");
            }
            //result = modelService.processFlatTableAll(result, userId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing and processing (no missing) Data Model: ", ex);
            result = "File import and processing (no missing) Flat Table error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 
        
    @Path("/goPseudo")
    @POST
    @Produces("application/json")
    public Response goPseudo(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.goPseudo]";
        String result = "WA0001 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        if ( organizationId != 1 ) {
                result = "WA0002 " + logId + " User not authorized.";
        } else {
            try {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");           
                User user = adminService.goPseudo(decodedContent, userId);       
                Auth.createSuperUserSession(userId, user, request.getSession());
                return Response.ok(result).build();  
            } catch (UnsupportedEncodingException ex) {
                logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            }    
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of Delta3AdminResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }
}
