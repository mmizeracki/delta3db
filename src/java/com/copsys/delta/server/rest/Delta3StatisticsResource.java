/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server.rest;

import com.copsys.delta.entity.process.Process;
import com.copsys.delta.jpa.process.ProcessJpaController;
import com.copsys.delta.security.Auth;
import com.copsys.delta.server.AdminServiceImpl;
import com.copsys.delta.server.StatServiceImpl;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc.
 */
@Path("process")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3StatisticsResource {
    private static final Logger logger = Logger.getLogger(Delta3StatisticsResource.class.getName());
    private static StatServiceImpl statService; 
    private static AdminServiceImpl adminService;  
    private static ProcessJpaController processController;      
    private String _corsHeaders;    


    /**
     * Creates a new instance of Delta3ModelResource
     */
    public Delta3StatisticsResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }      
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }         
    }

    @Path("/initializeStatPackage")
    @GET  
    @Produces("application/json")
    public Response initializeStatPackage(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.initializeStatPackage]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request.getSession());

        result = statService.initializeStatPackage(alias);                             
        return Response.ok(result).build();   
    }
    
    @Path("/getStatPackageConfig")
    @POST  
    @Produces("application/json")
    public Response getStatPackageConfig(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getStatPackageConfig]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {    
            result = statService.getStatConfig();             
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting stat configuration: ", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    @Path("/getStatResults")
    @POST  
    @Produces("application/json")
    public Response getStatResults(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getStatResults]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = statService.getStatResults(decodedContent);                             
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/getStatResult")
    @GET  
    @Produces("application/json")
    public Response getStatResult(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getStatResult]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request.getSession());

        result = statService.getStatResult(processId, guid, alias);                             
        return Response.ok(result).build();         
    }

    @Path("/getJobStatus")
    @GET  
    @Produces("application/json")
    public Response getJobStatus(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getJobStatus]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request.getSession());

        result = statService.getJobStatus(processId, guid, alias);                             
        return Response.ok(result).build();   
    }
    
    //-------------------------------------------------------------------------- ProcessesGrid CRUD
        /**
     * Retrieves representation of an instance of com.copsys.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getProcesses")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getProcesses(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getProcesses]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                           
      
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, processController, new Process());
        return Response.ok(result).build();  
    }    
  
    @Path("/createProcesses")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.createProcesses]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, processController, new Process());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateProcesses")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.updateProcesses]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, processController, new Process());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @POST
    @Path("/removeProcesses")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.removeProcesses]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        Integer organizationId = Auth.getOrganizationIdFromSession(request.getSession());         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteOrgObjects("[" + decodedContent + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.deleteOrgObjects(decodedContent, organizationId, userId, processController, new Process());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    //-------------------------------------------------------------------------- Get Descriptive Stats
    @POST
    @Path("/getDStats")
    @Produces("application/json")
    public Response getDStats(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getDStats]";
        String result = "DS0001 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request.getSession());
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());             
     
        result = statService.getDStatsPreprocessor(payload);
        return Response.ok(result).build();  
    }    


    
   @OPTIONS
   @Path("process")
   public Response corsMyResourceShare(@HeaderParam("Access-Control-Request-Headers") String requestH) {
      _corsHeaders = requestH;
      return makeCORS(Response.ok(), requestH);
   }
   
   private Response makeCORS(Response.ResponseBuilder req, String returnMethod) {
       Response.ResponseBuilder rb = req.header("Access-Control-Allow-Origin", "*")
          .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

       if (!"".equals(returnMethod)) {
          rb.header("Access-Control-Allow-Headers", returnMethod);
       }

       return rb.build();
    }

   private Response makeCORS(Response.ResponseBuilder req) {
       return makeCORS(req, _corsHeaders);
    }      
}
