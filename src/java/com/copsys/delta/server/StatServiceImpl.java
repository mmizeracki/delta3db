/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server;

import com.copsys.delta.db.Continuous;
import com.copsys.delta.db.DBHelper;
import com.copsys.delta.db.Dichotomous;
import com.copsys.delta.db.Enumerated;
import com.copsys.delta.entity.User;
import com.copsys.delta.entity.events.Eventtemplate;
import com.copsys.delta.entity.logregr.LogregrDate;
import com.copsys.delta.entity.model.Model;
import com.copsys.delta.entity.model.ModelColumn;
import com.copsys.delta.entity.model.ModelTable;
import com.copsys.delta.entity.process.Process;
import com.copsys.delta.entity.study.Study;
import com.copsys.delta.jpa.UserJpaController;
import com.copsys.delta.jpa.logregr.LogregrJpaController;
import com.copsys.delta.jpa.model.ModelColumnJpaController;
import com.copsys.delta.jpa.model.ModelJpaController;
import com.copsys.delta.jpa.process.ProcessJpaController;
import com.copsys.delta.jpa.study.StudyJpaController;
import com.copsys.delta.property.Property;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.copsys.delta.util.Util;
import com.copsys.proxy.service.ProxyBridge;
import com.copsys.proxy.service.StatPackageStatus;
import com.copsys.statproxy.Proxy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class StatServiceImpl {

    private static final Logger logger = Logger.getLogger(StatServiceImpl.class.getName());
    private static ProxyBridge pb;
    static Proxy proxy;
    private static ModelServiceImpl modelService;
    private static EventServiceImpl eventService;
    private static StudyServiceImpl studyService;    
    private static LogregrServiceImpl logregrService;    
    private static ModelJpaController modelController;    
    private static ModelColumnJpaController modelColumnController;
    private static ProcessJpaController processController;
    private static StudyJpaController studyController;
    private static UserJpaController userController;
    private static LogregrJpaController logregrController;
    
    public StatServiceImpl() {  
        if ( modelService == null ) {
            modelService = new ModelServiceImpl();
        }       
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }     
        if ( logregrService == null ) {
            logregrService = new LogregrServiceImpl();
        }          
        if ( logregrController == null ) {
            logregrController = new LogregrJpaController();
        }        
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }  
        if ( studyController == null ) {
            studyController = new StudyJpaController();
        }        
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }          
        if ( modelColumnController == null ) {
            modelColumnController = new ModelColumnJpaController();
        }        
        if ( userController == null ) {
            userController = new UserJpaController();
        }       
        initializeProxy();
    }

    //---------------------------------------------------------------------------------------------------------------
    // Following method is a main entry point for Descriptive Statistics
    // getXYZ() methods are first level preocessors, which call calculateXYZ() methods
    // calculate methods throw exceptions into get methods, which should prepare error messages and wrap them in JSON
    public String getDStatsPreprocessor(String load) {
        String result = "DS0010 Descriptive Statistics not supported.";
        try {
            JSONObject JSONContent = new JSONObject(load);
            JSONArray dStatsArray = JSONContent.getJSONArray("dStats");
            JSONObject dStatsObject = dStatsArray.getJSONObject(0);
            Model model = ModelServiceImpl.parseModelFromJSON(dStatsArray.getJSONObject(0).toString());
            JSONArray fieldArray = dStatsObject.getJSONArray("fields");
            JSONArray filterArray = dStatsObject.getJSONArray("filters");

            if (fieldArray.length() == 0) {
                // special case: no fields, no filters - get all Descriptive Stats
                return getAllDS(model, dStatsObject);
            }
            
            ModelColumn mc = modelColumnController.findModelColumn((int) fieldArray.getLong(0));
            switch (mc.getFieldClass()) {
                case "Treatment":
                case "Risk Adjustment":
                case "Outcome":
                    switch (mc.getFieldKind()) {
                        case "Dichotomous":
                            result = getDichotomousStats(model, dStatsObject, fieldArray, filterArray, mc);
                            break;
                        case "Continuous":
                            result = getContinuousStats(model, dStatsObject, fieldArray, filterArray, mc);
                            break;
                    }
                    break;
                case "Category":
                    result = getCategoryStats(model, dStatsObject, fieldArray, filterArray, mc);
                    break;
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex.toString()});
            return prepareJSONResponse("dStats", "DS0011 Exception while parsing JSON statistics description.", "", 0);
        }
        return result;
    }    
    
    public String getStatConfigRemote() {       
        proxy.getStatConfig();
        return null;
    }

    public String getStatConfig() {

        String resultString = null;
        
        // following reads from WEB-INF/classes directory
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("studydefinitionJSON.txt");
        try {
            resultString = Util.StringfromStream(inputStream);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Study configuration file not found.", ex.getMessage());
        }
        return resultString;
    }

    public String getStatResults(String load) {
        proxy.getResults(load);
        return null;
    }

    public String getStatResult(String processId, String guid, String alias) {
        String load = "{\"auth\":" + getAuthString(processId, alias) + "," + getProcessString(processId, guid) + "}";    
        proxy.getResults(load);
        return null;
    }
    
    public String submitStatJobLR1(Study study, String methodName, String filterWhereClause, String orderField,  Integer currentOrgId, Integer currentUserId) {
        String submitString = "empty";
        Process process = new Process();
        double[] data = null;   

        process.setIdModel(study.getIdModel());
        process.setIdStudy(study.getIdStudy());
        process.setProgress(0);
        process.setActive(Boolean.TRUE);
        process.setName(study.getName() + " step LR1");
        process.setStatus("submitted");
        List<LogregrDate> lrdList;
        try {
            process = (Process) processController.createObjects(process, currentOrgId, currentUserId);
            lrdList = logregrService.initializeLRFResults(process, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0001 Error submitting job: " + ex.getMessage();
        }
        
        if ( lrdList.isEmpty() ) return "DS0011 Configuration not supported.";
        if ( "".equals(study.getMethodParams()) ) return "DS0211 Study parameters are missing.";
        
        for (int i=0; i<lrdList.size(); i++) {
            if ( filterWhereClause == null ) {
                filterWhereClause = lrdList.get(i).getDescription();
            }  else {
                filterWhereClause += " AND " + lrdList.get(i).getDescription();
            }          
            try {
                JSONObject jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, methodName, orderField, study.getIsMissingDataUsed(), proxy);
                if (jobObject == null) {
                    return "DS0002 Creation of new Process failed. Possibly Model or Flat Table does not exist.";
                }                
                JSONArray params = new JSONArray(study.getMethodParams());     
                JSONObject step = params.getJSONObject(0);
                jobObject.put("inputs", step.getJSONObject("inputs"));
                User user = userController.findUser(currentUserId);
                jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), user.getAlias())));
                submitString = jobObject.toString();
                //logger.log(Level.INFO, "SubmitStatJob LR1 ==================> {0}", submitString);                
                proxy.sumbitStatJob(submitString);                
                eventService.logEvent(currentOrgId, currentUserId, Eventtemplate.SUBMIT_EVENT, "LR1");
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
                return "DS0003 Error submitting job: " + ex.getMessage();
            }
            break;
        }
        return "OK";
    }

    public String submitStatJobLR2(Study study, String methodName, String filterWhereClause, String orderField,  Integer currentOrgId, Integer currentUserId) {
        String submitString = "empty";
        Process process = new Process();
        double[] data = null;

        process.setIdModel(study.getIdModel());
        process.setIdStudy(study.getIdStudy());
        process.setProgress(0);
        process.setActive(Boolean.TRUE);
        process.setName(study.getName() + " step LR2");
        process.setStatus("submitted");
        try {
            process = (Process) processController.createObjects(process, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0004 Error submitting job: " + ex.getMessage();
        }        
        
        if ( "".equals(study.getMethodParams()) ) return "DS0212 Study parameters are missing.";
        
        String intervalQuery = logregrService.getIntervalQueryStep2(study);
        if ( filterWhereClause == null ) {
            filterWhereClause = intervalQuery;
        }  else {
            filterWhereClause += " AND " + intervalQuery;
        }         
        try {
            JSONObject jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, "logisticRegressionAnalysis", orderField,  study.getIsMissingDataUsed(), proxy);
            if (jobObject == null) {
                return "DS0005 Creation of new Process failed. Possibly Model or Flat Table does not exist.";
            }            
            JSONArray params = new JSONArray(study.getMethodParams());              
            JSONObject step = params.getJSONObject(1);
            jobObject.put("inputs", step.getJSONObject("inputs"));
            User user = userController.findUser(currentUserId);
            jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), user.getAlias())));
            submitString = jobObject.toString();
            //logger.log(Level.INFO, "SubmitStatJob LR2 ==================> {0}", submitString);
            proxy.sumbitStatJob(submitString);
            eventService.logEvent(currentOrgId, currentUserId, Eventtemplate.SUBMIT_EVENT, "LR2");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0006 Error submitting job: " + ex.getMessage();
        }
        return "OK";
    }    

    public String submitStatJobP1(Study study, String methodName, String filterWhereClause, String orderField,  Integer currentOrgId, Integer currentUserId) {
        String submitString = "empty";
        Process process = new Process();
        double[] data = null;

        process.setIdModel(study.getIdModel());
        process.setIdStudy(study.getIdStudy());
        process.setProgress(0);
        process.setActive(Boolean.TRUE);
        process.setName(study.getName() + " step P1");
        process.setStatus("submitted");
        try {
            process = (Process) processController.createObjects(process, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0007 Error submitting job: " + ex.getMessage();
        }

        if ( "".equals(study.getMethodParams()) ) return "DS0213 Study parameters are missing.";
        
        try {    
            JSONObject jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, methodName, orderField, study.getIsMissingDataUsed(), proxy);
            if (jobObject == null) {
                return "DS0008 Creation of new Process failed. Possibly Model or Flat Table does not exist.";
            }            
            JSONArray params = new JSONArray(study.getMethodParams());     
            JSONObject step = params.getJSONObject(0);
            jobObject.put("inputs", step.getJSONObject("inputs"));
            User user = userController.findUser(currentUserId);
            jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), user.getAlias())));
            //logger.log(Level.INFO, "SubmitStatJob P1 ==================> {0}", submitString);            
            submitString = jobObject.toString();
            proxy.sumbitStatJob(submitString);
            eventService.logEvent(currentOrgId, currentUserId, Eventtemplate.SUBMIT_EVENT, "P1");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0009 Error submitting job: " + ex.getMessage();
        }
        return "OK";
    }

    public String submitStatJobSPRT1(Study study, String methodName, String filterWhereClause, String orderField,  Integer currentOrgId, Integer currentUserId) {
        String submitString = "empty";
        Process process = new Process();
        double[] data = null;

        process.setIdModel(study.getIdModel());
        process.setIdStudy(study.getIdStudy());
        process.setProgress(0);
        process.setActive(Boolean.TRUE);
        process.setName(study.getName() + " step SPRT1");
        process.setStatus("submitted");
        try {
            process = (Process) processController.createObjects(process, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS00021 Error submitting job: " + ex.getMessage();
        }

        if ( "".equals(study.getMethodParams()) ) return "DS0214 Study parameters are missing.";
        
        try {     
            JSONObject jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, methodName, orderField,  study.getIsMissingDataUsed(), proxy);
            if (jobObject == null) {
                return "DS0022 Creation of new Process failed. Possibly Model or Flat Table does not exist.";
            }            
            JSONArray params = new JSONArray(study.getMethodParams());     
            JSONObject step = params.getJSONObject(0);
            jobObject.put("inputs", step.getJSONObject("inputs"));
            User user = userController.findUser(currentUserId);
            jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), user.getAlias())));
            submitString = jobObject.toString();
            //logger.log(Level.INFO, "SubmitStatJob SPRT1 ==================> {0}", submitString);            
            proxy.sumbitStatJob(submitString);
            eventService.logEvent(currentOrgId, currentUserId, Eventtemplate.SUBMIT_EVENT, "SPRT1");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0023 Error submitting job: " + ex.getMessage();
        }
        return "OK";
    }
    
    public String getJobStatus(String processId, String guid, String alias) {
        String load = "{\"auth\":" + getAuthString(processId, alias) + "," + getProcessString(processId, guid) + "}";      
        proxy.getProcessStatus(load);
        return null;
    }  
    
    public String periodicJobStatusCheck(String alias) {
        List<Object>  processList = new ArrayList();
        processList = processController.findActiveProcessEntities();
        for ( Object activeProcess : processList ) {
            String load = "{\"auth\":" + getAuthString(((Process)activeProcess).getIdProcess().toString(), alias) 
                    + "," + getProcessString(((Process)activeProcess).getIdProcess().toString(), ((Process)activeProcess).getGuid()) + "}";
            logger.log(Level.INFO, "Checking status of " + load);
            proxy.getProcessStatus(load);
        }   
        return null;
    }
    
    public String initializeStatPackage(String alias) {     
        URL servlet = null;
        URLConnection con = null;
        logger.log(Level.INFO, "User {0} checking status of statistical package.", alias);
        StatPackageStatus st = new StatPackageStatus();
        if ( !st.getStatus().equals("OK") ) {
            logger.log(Level.INFO, "User {0} initializing statistical package.", alias);
            try { 
                servlet = new URL( "http://" + new Property("externalStatPackageHostPort").getParamValue() + "/AnalyticsBrokerProxy/ProxyAccessor" ); 
                con = servlet.openConnection();
                con.setConnectTimeout(Integer.parseInt(new Property("externalStatPackageConnectionTimeout").getParamValue()));
                con.setReadTimeout(Integer.parseInt(new Property("externalStatPackageReadTimeout").getParamValue()));
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); 
                String line = Util.StringfromBufferedReader(in); 
                logger.log(Level.INFO, "OCEANS initialization response: {0}", line);	                
                if ( line.contains("Job Initiated") ) {
                    st.setStatus("OK");
                }            
            } catch ( FileNotFoundException e) {
                st.setStatus("Not Installed");
                logger.log(Level.SEVERE, "FileNotFoundException while initializing OCEANS: {0}", e.getMessage());                
            } catch (Exception e) {
                st.setStatus("Error");
                logger.log(Level.SEVERE, "Exception while initializing OCEANS: {0}", e.getMessage());
            }        
        }
        return st.getStatus();
    }

    public int receiveResults(String JSONString) throws Exception {
        logger.log(Level.SEVERE, "DELTA3 updating Process: " + JSONString);    
        Integer userId = new Integer(0); // means "system"
        Process originalProcess = null;
  
        try {        

            JSONObject responseObject =  new JSONObject(JSONString);
            JSONArray processArray = responseObject.getJSONArray("processes");
            for (int i=0; i<processArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                JSONObject processObject = new JSONObject(jString);
                Object subject = gson.fromJson(jString, Process.class);   
                originalProcess = ((ProcessJpaController)processController).findProcess(((Process)subject).getIdProcess());              
                if ( originalProcess != null ) {
                    originalProcess.setProgress(((Process)subject).getProgress());
                    originalProcess.setStatus(((Process)subject).getStatus());     
                    originalProcess.setMessage(((Process)subject).getMessage());
                    JSONArray results = processObject.getJSONArray("results"); // this key is ignored by gson, which allows processing
                    JSONObject processType = results.getJSONObject(0);    // this throws exception in Propensity!                  
                    originalProcess.setData("{\"results\":" + results.toString() + "}");
                    subject = processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                    // branch on result.name                   
                    if ( "LogisticRegressionFormulaOutput".equals(processType.getString("name")) ) {
                        Study s = (Study) studyController.findStudy(((Process)subject).getIdStudy());
                        if ( studyService == null ) {
                            studyService = new StudyServiceImpl();
                        }                         
                        logregrService.saveLRFResults((Process)subject, s, "{\"results\":" + results.toString() + "}");                        
                        studyService.advanceStatus("OK", s, 0);
                    }         
                    if ( "DescriptiveStatisticsOutput".equals(processType.getString("name")) ) {
                        //logregrService.saveLRFResults((Process)subject, "{\"results\":" + results.toString() + "}");
                        Study s = (Study) studyController.findStudy(((Process)subject).getIdStudy());
                        if ( studyService == null ) {
                            studyService = new StudyServiceImpl();
                        }                         
                        studyService.advanceStatus("OK", s, 0);                        
                    }
                } else {
                    logger.log(Level.SEVERE, "Received Process ID that does not match local database: {0}", new Object[]{((Process)subject).getIdProcess()});  
                    if ( originalProcess != null ) {
                        originalProcess.setProgress(0);
                        originalProcess.setStatus("Error");     
                        originalProcess.setMessage("Received Process ID that does not match DELTA database.");   
                        processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                        return -2;
                        }
                    }
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Process Object: {0}", new Object[]{ex});
            if ( originalProcess != null ) {
                originalProcess.setProgress(100);
                originalProcess.setStatus("Error");     
                originalProcess.setMessage("Invalid results: " + ex.getMessage());   
                processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                }            
            return -1;
        }                                
        return 0;
    }
 
    public int receiveSubmit(String JSONString) throws Exception {
        logger.log(Level.SEVERE, "DELTA3 updating Process: " + JSONString);    
        Integer userId = new Integer(0); // means "system"
        Process originalProcess = null;
  
        try {        

            JSONObject responseObject =  new JSONObject(JSONString);
            JSONArray processArray = responseObject.getJSONArray("processes");
            for (int i=0; i<processArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                Object subject = gson.fromJson(jString, Process.class);   
                originalProcess = ((ProcessJpaController)processController).findProcess(((Process)subject).getIdProcess());              
                if ( originalProcess != null ) {
                    originalProcess.setProgress(((Process)subject).getProgress());
                    originalProcess.setStatus(((Process)subject).getStatus());     
                    originalProcess.setMessage(((Process)subject).getMessage());           
                    subject = processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                } else {
                    logger.log(Level.SEVERE, "Received Process ID that does not match local database: {0}", new Object[]{((Process)subject).getIdProcess()});  
                    if ( originalProcess != null ) {
                        originalProcess.setProgress(0);
                        originalProcess.setStatus("Error");     
                        originalProcess.setMessage("Received Process ID that does not match DELTA database.");   
                        processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                        return -2;
                        }
                    }
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Process Object: {0}", new Object[]{ex});
            if ( originalProcess != null ) {
                originalProcess.setProgress(100);
                originalProcess.setStatus("Error");     
                originalProcess.setMessage("Invalid results: " + ex.getMessage());   
                processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                }            
            return -1;
        }                                
        return 0;
    }
    
    private String prepareJSONResponse(String callId, String message, String result, int counter) {
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            if ("".equals(result)) {
                success.put("success", false);
                status.put("message", message);
            } else {
                success.put("success", true);
                status.put("message", message);
                success.put("totalCount", Integer.toString(counter));
                success.put(callId, new JSONArray(result));
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in DELTA3 WS method " + callId, ex);
            result = "{\"status\":{\"message\":\"JSON error in DELTA3 WS method " + callId + "\"},\"success\":false}";
        }
        return result;
    }
    
    private String getAllDS(Model model, JSONObject dStatsObject) {
        String dataString;
        String dstats = "{\"type\":\"ALL\",\"statistics\": [";

        List<ModelTable> mt = modelService.getModelTables(model.getIdModel());
        List<ModelColumn> modelColumn = modelService.getModelColumns(mt);
        ModelColumn mc = null;
        int i = 0;
        for (; i < modelColumn.size(); i++) {
            try {
                mc = modelColumn.get(i);
                if ((i > 0) && (mc.getInsertable() == true)) {
                    dstats += ",";
                }
                if (mc.getInsertable() == true) {
                    if (mc.getType().startsWith("INT") 
                            || mc.getType().equals("DOUBLE") 
                            || mc.getType().equals("FLOAT") ) {
                        Continuous cField = new Continuous(model.getOutputName(), mc.getName(), "");
                        dstats += calculateStatsCandD(cField, mc);
                    } else {
                        if (mc.getType().equals("BIT") 
                                || mc.getType().startsWith("TINYINT")) {
                            Dichotomous dField = new Dichotomous(model.getOutputName(), mc.getName());
                            dstats += calculateStatsDandD(dField, mc);
                        } else {                        
                            dstats += calculateTotalAndPercentNullStats(model.getOutputName(), mc);
                        }
                    }
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while getting All Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
                //return prepareJSONResponse("dStats","DS001x Database connection failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
                dstats += "{\"filter\":\"\""
                        + ",\"field\":\"" + mc.getName() + "\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
                        + ",\"count\":\"\",\"nullCount\":\"\",\"notNullPercent\":\"\"}";
            }
        }
        dstats += "]}";
        dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, i);
    }

    private String getDichotomousStats(Model model, JSONObject dStatsObject, JSONArray fieldArray, JSONArray filterArray, ModelColumn mc) {
        String dataString = "";
        int filterCount = 0;

        try {
            String dstats = "{\"type\":\"DD\",\"statistics\": [";
            Dichotomous dMain = null;

            dMain = new Dichotomous(model.getOutputName(), mc.getName());
            logger.log(Level.INFO, "Dichotomous field {0} processed: {1}, {2}, {3}, {4}, {5}, {6}",
                    new Object[]{mc.getName(), dMain.getTrueValue(), dMain.getFalseValue(), dMain.getNullValue(),
                        Integer.toString(dMain.getTrueCount()), Integer.toString(dMain.getFalseCount()), Integer.toString(dMain.getNullCount())});
            dstats += "{\"TRUE\":\"" + dMain.getTrueValue() + "\""
                    + ",\"FALSE\":\"" + dMain.getFalseValue() + "\""
                    + ",\"NULL\":" + dMain.getNullValue() + "}";
            dstats += ",{\"TRUE\":" + Integer.toString(dMain.getTrueCount())
                    + ",\"FALSE\":" + Integer.toString(dMain.getFalseCount())
                    + ",\"NULL\":" + Integer.toString(dMain.getNullCount()) + "}";

            for (filterCount = 0; filterCount < filterArray.length(); filterCount++) {
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(filterCount));
                Dichotomous dFilter = new Dichotomous(model.getOutputName(), mcf.getName());
                logger.log(Level.INFO, "Dichotomous filter {0} processed: true={1}, false={2}, null={3}, trueCount={4}, falseCount={5}, nullCount={6}",
                        new Object[]{mcf.getName(), dFilter.getTrueValue(), dFilter.getFalseValue(), dFilter.getNullValue(),
                            Integer.toString(dFilter.getTrueCount()), Integer.toString(dFilter.getFalseCount()), Integer.toString(dFilter.getNullCount())});
                dstats += "," + calculateStatsDandD(dMain, dFilter, dFilter.getTrueValue());
                dstats += "," + calculateStatsDandD(dMain, dFilter, dFilter.getFalseValue());
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0012 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0013 Database operation failed while getting Descriptive Statistics from table: " + model.getOutputName() + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0014 Database connection failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, filterCount + 1);
    }

    private String getContinuousStats(Model model, JSONObject dStatsObject, JSONArray fieldArray, JSONArray filterArray, ModelColumn mc) {
        String dataString = "";
        int filterCount = 0;

        try {
            String dstats = "{\"type\":\"CD\",\"statistics\": [";
            Continuous field = null;

            field = new Continuous(model.getOutputName(), mc.getName(), "");
            logger.log(Level.INFO, "Continuous field {0} processed: min={1}, mean={2}, max={3}, total={4}, notNullPercent={5}",
                    new Object[]{mc.getName(), field.getMinValue(), field.getMeanValue(), field.getMaxValue(),
                        Integer.toString(field.getTotalCount()), field.getNotNullPercent()});
            dstats += calculateStatsCandD(field, mc);

            for (filterCount = 0; filterCount < filterArray.length(); filterCount++) {
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(filterCount));
                if ("Dichotomous".equals(mcf.getFieldKind())) {
                    Dichotomous dFilter = new Dichotomous(model.getOutputName(), mcf.getName());
                    logger.log(Level.INFO, "Dichotomous filter {0} processed: true={1}, false={2}, null={3}, trueCount={4}, falseCount={5}, nullCount={6}",
                            new Object[]{mcf.getName(), dFilter.getTrueValue(), dFilter.getFalseValue(), dFilter.getNullValue(),
                                Integer.toString(dFilter.getTrueCount()), Integer.toString(dFilter.getFalseCount()), Integer.toString(dFilter.getNullCount())});
                    dstats += ",";
                    Continuous fieldTrue = new Continuous(model.getOutputName(), mc.getName(), dFilter.getName() + " = '" + dFilter.getTrueValue() + "'");
                    dstats += calculateStatsCandD(fieldTrue, mcf) + ",";
                    Continuous fieldFalse = new Continuous(model.getOutputName(), mc.getName(), dFilter.getName() + " = '" + dFilter.getFalseValue() + "'");
                    dstats += calculateStatsCandD(fieldFalse, mcf);
                }
                if ("Enumerated".equals(mcf.getFieldKind())) {
                    Enumerated eFilter = new Enumerated(model.getOutputName(), mcf.getName());
                    logger.log(Level.INFO, "Enumerated filter {0} processed: values={1}, null={2}, nullCount={3}",
                            new Object[]{mcf.getName(), eFilter.getValue().toString(), eFilter.getNullValue(), Integer.toString(eFilter.getNullCount())});
                    for (int ii = 0; ii < eFilter.getSize(); ii++) {
                        dstats += ",";
                        Continuous filteredField = new Continuous(model.getOutputName(), mc.getName(), eFilter.getName() + " = '" + eFilter.getValue()[ii] + "'");
                        dstats += calculateStatsCandD(filteredField, mcf);
                    }
                }
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0015 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0016 Database operation failed while getting Descriptive Statistics from table: " + model.getOutputName() + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0017 Database operation failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, filterCount + 1);
    }

    private String getCategoryStats(Model model, JSONObject dStatsObject, JSONArray fieldArray, JSONArray filterArray, ModelColumn mc) {

        String dataString = "";
        int count = 0;

        try {
            String dstats = "{\"type\":\"CAT\",\"statistics\": [";
            dstats += calculateCategoryStatsD(model.getOutputName(), mc);

            for (int j = 0; j < filterArray.length(); j++) {
                Dichotomous dFilter = null;
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(j));
                if ("Dichotomous".equals(mcf.getFieldKind())) {
                    dFilter = new Dichotomous(model.getOutputName(), mcf.getName());
                    dstats += "," + calculateCategoryStatsDWithFilter(model.getOutputName(), mc, dFilter.getName(), dFilter.getTrueValue());
                    dstats += "," + calculateCategoryStatsDWithFilter(model.getOutputName(), mc, dFilter.getName(), dFilter.getFalseValue());
                }
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0018 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0019 Database operation failed while getting Descriptive Statistics from table: " + model.getOutputName() + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0020 Database connection failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, count);
    }

    private String calculateTotalAndPercentNullStats(String tableName, ModelColumn mc) throws Exception {
        Query queryResult;
        DecimalFormat df = new DecimalFormat("#.##");
        String notNullPercent;

        String sqlStmt = "select count(" + mc.getName() + ") from "
                + tableName;
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        List<Object[]> resultCounts = queryResult.getResultList();
        String tmp = String.valueOf(resultCounts.get(0));
        int totalCount = Integer.parseInt(tmp);

        sqlStmt = "select count(" + mc.getName() + ") from "
                + tableName + " where "
                + mc.getName() + " = null";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        resultCounts = queryResult.getResultList();
        tmp = String.valueOf(resultCounts.get(0));
        int nullCount = Integer.parseInt(tmp);

        if (totalCount != 0) {
            notNullPercent = df.format(100 * (totalCount - nullCount) / totalCount);
        } else {
            notNullPercent = df.format(0);
        }
        String dstats = "{\"filter\":\"\""
                + ",\"field\":\"" + mc.getName() + "\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
                + ",\"count\":" + Integer.toString(totalCount)
                + ",\"nullCount\":" + Integer.toString(nullCount)
                + ",\"notNullPercent\":" + notNullPercent
                + ",\"class\":" + mc.getFieldClass()
                + ",\"kind\":" + mc.getFieldKind()
                + "}";
        return dstats;
    }

    /* this method returns descriptive statistcs for Dichotomous field based on Dichotomous filters */
    private String calculateStatsDandD(Dichotomous field, Dichotomous filter, String trueOrFalse) throws Exception {

        Query queryResult;
        String sqlStmt = "select count(" + field.getName() + ") from "
                + field.getTableName() + " where "
                + filter.getName() + " = '" + trueOrFalse + "' AND "
                + field.getName() + " = '" + field.getTrueValue() + "'";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        List<Object[]> resultCounts = queryResult.getResultList();
        String tmp = String.valueOf(resultCounts.get(0));
        int trueCount = Integer.parseInt(tmp);

        sqlStmt = "select count(" + field.getName() + ") from "
                + field.getTableName() + " where "
                + filter.getName() + " = '" + trueOrFalse + "' AND "
                + field.getName() + " = '" + field.getFalseValue() + "'";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        resultCounts = queryResult.getResultList();
        tmp = String.valueOf(resultCounts.get(0));
        int falseCount = Integer.parseInt(tmp);

        String dstats = "{\"TRUE\":" + Integer.toString(trueCount)
                + ",\"FALSE\":" + Integer.toString(falseCount)
                + ",\"NULL\":\"\"}";
        return dstats;
    }

    /* this method is used for calculating dichotomous stats when "all" stats are requested */
    private String calculateStatsDandD(Dichotomous field, ModelColumn mc) throws Exception {

        //Query queryResult;
        DecimalFormat df = new DecimalFormat("#.##");
        String notNullPercent;

        if (field.getTotalCount() != 0) {
            notNullPercent = df.format(100 * (field.getTotalCount() - field.getNullCount()) / field.getTotalCount());
        } else {
            notNullPercent = df.format(0);
        }

        String dstats = "{\"filter\":\"\""
                + ",\"field\":\"" + field.getName() + "\""
                + ",\"min\":\"" + field.getFalseValue() + "\""
                + ",\"mean\":\"" + field.getMeanValue() + "\""
                + ",\"max\":\"" + field.getTrueValue() + "\""
                + ",\"value25\":\"\""
                + ",\"value75\":\"\""
                + ",\"median\":\"\""
                + ",\"std\":\"" + field.getStdValue() + "\""
                + ",\"count\":" + Integer.toString(field.getTotalCount())
                + ",\"nullCount\":" + Integer.toString(field.getNullCount())
                + ",\"notNullPercent\":" + notNullPercent
                + ",\"class\":" + mc.getFieldClass()
                + ",\"kind\":" + mc.getFieldKind()
                + "}";        
        return dstats;
    }
    
    /* this method returns descriptive statistcs for Continuous field based on Dichotomous filters */
    private String calculateStatsCandD(Continuous field, ModelColumn mc) {
        String dstats = "{\"filter\":\"" + field.getFilterString() + "\""
                + ",\"field\":\"" + field.getName() + "\""
                + ",\"min\":\"" + field.getMinValue() + "\""
                + ",\"mean\":\"" + field.getMeanValue() + "\""
                + ",\"max\":\"" + field.getMaxValue() + "\""
                + ",\"value25\":\"" + field.getValue25() + "\""
                + ",\"value75\":\"" + field.getValue75() + "\""
                + ",\"median\":\"" + field.getMedianValue() + "\""
                + ",\"std\":\"" + field.getStdValue() + "\""
                + ",\"count\":" + Integer.toString(field.getTotalCount())
                + ",\"nullCount\":" + Integer.toString(field.getNullCount())
                + ",\"notNullPercent\":" + field.getNotNullPercent()
                + ",\"class\":" + mc.getFieldClass()
                + ",\"kind\":" + mc.getFieldKind()
                + "}";
        return dstats;
    }

    /* this method returns descriptive statistcs for Category field */
    private String calculateCategoryStatsD(String modelTable, ModelColumn mc) throws Exception {

        int limit = 100;
        int count;
        String sqlStmt;
        Query queryResult;
        List<Object[]> results;
        String dstats = "";

        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);

        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        results = queryResult.getResultList();
        count = results.size();

        boolean nullOccurenceFlag = false;
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                dstats += ",";
            }
            String tmpValue, tmpCount;
            if (results.get(i)[0] == null) {
                tmpValue = "NULL";
                tmpCount = String.valueOf(results.get(i)[1].toString());
                nullOccurenceFlag = true;
            } else {
                if ((i == 0) && (nullOccurenceFlag == false)) {
                    // compensate for db not reporting count of records with nulls
                    dstats += formatCategoryStatsDNullJSON("", "") + ",";
                }
                tmpValue = results.get(i)[0].toString();
                tmpCount = String.valueOf(results.get(i)[1].toString());
            }
            dstats += "{\"filter\":\"\"";
            dstats += ",\"value\":\"" + tmpValue + "\"";
            dstats += ",\"count\":" + tmpCount + "}";
        }
        return dstats;
    }

    /* this method returns descriptive statistcs for Category field based on Dichotomous filters */
    private String calculateCategoryStatsDWithFilter(String modelTable, ModelColumn mc, String filterName, String filterValue) throws Exception {

        int limit = 100;
        int count, countWithFilter;
        String sqlStmt;
        Query queryResult;
        List<Object[]> results, resultsWithFilter;
        String dstats = "";

        // establish number of values without filter
        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        results = queryResult.getResultList();
        count = results.size();

        // apply filter and get count
        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " where " + filterName + " = '" + filterValue + "'"
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU", sqlStmt, logger);
        resultsWithFilter = queryResult.getResultList();
        countWithFilter = resultsWithFilter.size();

        boolean nullOccurenceFlag = false;
        int j;
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                dstats += ",";
            }
            String tmpValue, tmpCount;
            j = getMatchingFilteredRecord(countWithFilter, resultsWithFilter, results.get(i)[0]);
            if (j == countWithFilter) { // no match
                // compensate for db not reporting count of records with nulls
                dstats += formatCategoryStatsDNullJSON(filterName, filterValue);
                continue;
            }
            if (resultsWithFilter.get(j)[0] == null) {
                tmpValue = "NULL";
                tmpCount = String.valueOf(resultsWithFilter.get(j)[1].toString());
                nullOccurenceFlag = true;
            } else {
                if ((i == 0) && (nullOccurenceFlag == false)) {
                    // compensate for db not reporting count of records with nulls
                    dstats += formatCategoryStatsDNullJSON(filterName, filterValue) + ",";
                }
                tmpValue = resultsWithFilter.get(j)[0].toString();
                tmpCount = String.valueOf(resultsWithFilter.get(j)[1].toString());
            }
            dstats += "{\"filter\":\"" + filterName + " = '" + filterValue + "'\"";
            dstats += ",\"value\":\"" + tmpValue + "\"";
            dstats += ",\"count\":" + tmpCount + "}";
        }
        return dstats;
    }

    private String formatCategoryStatsDNullJSON(String filterName, String filterValue) {
        String dstats = "";

        if (!"".equals(filterName)) {
            dstats += "{\"filter\":\"" + filterName + " = '" + filterValue + "'\"";
        } else {
            dstats += "{\"filter\":\"\"";
        }
        dstats += ",\"value\":\"NULL\"";
        dstats += ",\"count\":0}";
        return dstats;
    }

    private int getMatchingFilteredRecord(int max, List<Object[]> results, Object o) {
        int i = 0;
        for (; i < max; i++) {
            if (results.get(i)[0] == null) {
                if (o == null) {
                    break; // match on null
                } else {
                    continue;
                }
            }
            if (results.get(i)[0].equals(o)) {
                break;
            }
        }
        return i;
    }
    
    private String getAuthString(String process, String alias)
    {    
        String authString = "{\"idApp\": \"DELTA3.0\",\"idProject\": \"" 
            + process + "\",\"idUser\": \"" + alias + "\"}";
        return authString;
    }     
    
    private String getProcessString(String processId, String guid) {
        StringTokenizer stP = new StringTokenizer(processId, ",");
        StringTokenizer stG = new StringTokenizer(guid, ",");
        String processString = "\"processes\":[";        
        int i = 0;
        while (stP.hasMoreElements()) {
            if ( i++ > 0 ) {
                processString += ',';
            }
            processString += "{\"idProcess\": " + stP.nextElement() + ",\"guid\": \"" + stG.nextElement() + "\"}";         
        }      
        processString += "]";
        return processString;
    }
    
    private void initializeProxy() {
        if (pb == null) {
            logger.log(Level.SEVERE, "Creating new ProxyBridge");
            pb = new ProxyBridge();
        }
        if (proxy == null) {
            logger.log(Level.SEVERE, "Creating new Proxy");
            proxy = pb.getProxy(Proxy.NON_MULTICAST, new Property("hazelcastPartnerIp").getParamValue(), new Property("hazelcastInterface").getParamValue());
        }        
    }
}
