/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.server;

import com.copsys.delta.entity.Organization;
import com.copsys.delta.entity.Permission;
import com.copsys.delta.entity.PermissionPK;
import com.copsys.delta.entity.Permissiontemplate;
import com.copsys.delta.entity.PermissiontemplateComp;
import com.copsys.delta.entity.Role;
import com.copsys.delta.entity.User;
import com.copsys.delta.entity.UserHasRole;
import com.copsys.delta.entity.UserHasRolePK;
import com.copsys.delta.jpa.OrganizationJpaController;
import com.copsys.delta.jpa.PermissionJpaController;
import com.copsys.delta.jpa.PermissiontemplateJpaController;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.RoleJpaController;
import com.copsys.delta.jpa.UserHasRoleJpaController;
import com.copsys.delta.jpa.UserJpaController;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.security.Auth;
import com.copsys.delta.security.HashEncrypter;
import com.copsys.delta.server.exceptions.EntityExistsException;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.copsys.delta.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Boston Advanced Analytics Inc.
 */
public class AdminServiceImpl {
    
    private static final Logger logger = Logger.getLogger(AdminServiceImpl.class.getName());    
    private static UserJpaController userController;
    private static OrganizationJpaController organizationController;
    private static RoleJpaController roleController;
    private static UserHasRoleJpaController userhasroleController;
    private static PermissionJpaController permissionController;
    private static PermissiontemplateJpaController permissiontemplateController;
    private static ModelServiceImpl modelService;

    //------------------------------------------------------------------ Generic
    public String createObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not created";
        String dataString = "";
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                subject = controller.createObjects(subject, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0003 Exception while creating new "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) created", result);
    }
        
    public String updateObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not updated";
        String dataString = "";  
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Updating Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                subject = controller.updateObjects(subject, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0008 Exception while updating "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) updated", result);
    }
  
     public String deleteObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not deleted";
        String dataString = "";  
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Deleting Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                controller.deleteObject(subject, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0008 Exception while deleting "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) deleted", result);
    }
     
    public String getObjects(int start, int limit, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all users
            subjects = controller.findObjectEntities();
        } else {
            subjects = controller.findObjectEntities(limit, start);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(controller.getObjectCount()));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  

    public String getObjectsById(int start, int limit, int id, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all users
            subjects = controller.findObjectEntitiesById(limit, start, id);
        } else {
            subjects = controller.findObjectEntitiesById(limit, start, id);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(controller.getObjectCount(id)));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjectsById]: ", ex);  
            result = "JSON error in WS method [admin.getObjectsById]: " + ex.getMessage();
        }        
        return result;
    }  

    public String importObjects(String JSONString, Integer currentOrgId, Integer currentUserId) {
        String result = "Objects(s) not imported";
        String dataString = "";  
        String objectType = "object";
        JSONObject impObject;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                impObject =  (JSONObject) objectArray.get(0);
                objectType = impObject.getString("objectType");     
                String objectVersion = impObject.getString("version");                    
                logger.log(Level.INFO, "Importing object type " + objectType);
                switch ( objectType ) {
                    case "Model":
                        if ( modelService == null ) {
                            modelService = new ModelServiceImpl();
                        } 
                        dataString = modelService.importModelWithOverwrite(impObject, currentOrgId, currentUserId);
                        break;
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing new Object: {0} {1}", new Object[]{"Object", ex});
            result = "DA0108 Exception while importing "  + objectType + ": " + ex.getMessage();
        }   
        return wrapAPI(objectType.toLowerCase()+"s", dataString, objectType + " imported", result);
    }
    
    //------------------------------------------------------------------ Generic by Org ID
    public String createOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not created";
        String dataString = "";
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                subject = controller.createObjects(subject, currentOrgId, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0003 Exception while creating new "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) created", result);
    }
        
    public String updateOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not updated";
        String dataString = "";  
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Updating Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                subject = controller.updateObjects(subject, currentOrgId, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0008 Exception while updating "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) updated", result);
    }
  
     public String deleteOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        String result = "Objects(s) not deleted";
        String dataString = "";  
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Deleting Object {0} " + Integer.toString(i), controller.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());               
                controller.deleteObject(subject, currentOrgId, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            result = "DA0008 Exception while deleting "  + controller.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(controller.getObjectType(), dataString, "Object(s) deleted", result);
    }
     
    public String getOrgObjects(int start, int limit, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all objects
            subjects = controller.findObjectEntities(currentOrgId);
        } else {
            subjects = controller.findObjectEntities(limit, start, currentOrgId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(controller.getObjectCount(currentOrgId)));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  

    public String getOrgObjectsById(int start, int limit, int id, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all users
            //subjects = controller.findObjectEntitiesById(id); **for now controller ignores pagination anyway
            subjects = controller.findObjectEntitiesById(limit, start, id, currentOrgId);
        } else {
            subjects = controller.findObjectEntitiesById(limit, start, id, currentOrgId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(max));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  
 
    public String changePassword(String JSONString, Integer currentUserId) {
        String result = "Password not changed.";
        User user = null;
        String dataString = "";
        if ( userController == null ) {
           userController = new UserJpaController();
        }
        user = userController.findUser(currentUserId);     
        
        try {
            JSONObject JSONContent =  new JSONObject(JSONString);
            logger.log(Level.INFO, "Changing password for User {0}", user.getAlias());
            if ( Auth.verifyPassword(user.getAlias(), JSONContent.optString("op")) == false ) {
                result = "DA0100 Authentication error.";
            } else {
                user.setPassword(HashEncrypter.SHA1(JSONContent.optString("np")));
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
                user.setPassChangedTS(currentTimestamp);                
                user = userController.edit(user);      
                dataString = "[]";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while saving new password for User: {0} {1}", new Object[]{user.getAlias(), ex});
            result = "DA0102 Exception while saving new password for User " + user.getAlias() + ": " + ex.getMessage();
            
        }   
        return wrapAPI("password", dataString, "Password changed.", result);      
    }

    public String resetPassword(String alias, Integer currentUserId) {
        String result = "Password not changed.";
        List<User> users = null;
        String dataString = "";
        if ( userController == null ) {
           userController = new UserJpaController();
        }
        users = userController.findUserByAlias(alias);     
        
        try {
            logger.log(Level.SEVERE, "Resetting password for user {0} by user with ID {1}", new Object[]{alias, currentUserId.toString()});
            users.get(0).setPassword(HashEncrypter.SHA1(userController.tempPassword));
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            users.get(0).setPassChangedTS(currentTimestamp);                
            userController.edit(users.get(0));      
            dataString = "[]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while restting password for User: {0} {1}", new Object[]{alias, ex});
            result = "DA0102 Exception while resetting password for User " + alias + ": " + ex.getMessage();
            
        }   
        return wrapAPI("password", dataString, "Password reset successful.", result);      
    }
    
    //-------------------------------------------------------------------------- User Roles
    public List<Role> getUserRoles(int userId) {
        List<Role> roles = new ArrayList();
        if ( userhasroleController == null ) {
           userhasroleController = new UserHasRoleJpaController();
        }
        if ( roleController == null ) {
           roleController = new RoleJpaController();
        }
        UserHasRolePK uhrPK = new UserHasRolePK();       
        uhrPK.setUseridUser(userId);
        List<UserHasRole> uhr = userhasroleController.findUserHasRoleByUserIdEntities(userId);
        if ( (uhr.isEmpty()) ) {
            logger.log(Level.INFO, "UserRole does not exist");
            return null;
        }
        for (UserHasRole t : uhr) {
            int i = t.getUserHasRolePK().getRoleidRole();
            Role r = roleController.findRole(i);
            if ( r != null ) {
                roles.add(r);
            }
        }
        return roles;
    }
    
    public boolean checkUserHasRoleExists(int userId, int roleId) {
        if ( userhasroleController == null ) {
           userhasroleController = new UserHasRoleJpaController();
        }
        UserHasRolePK uhrPK = new UserHasRolePK(userId,roleId);          
        UserHasRole uhr = userhasroleController.findUserHasRole(uhrPK);
        if ( (uhr == null) ) {
            logger.log(Level.INFO, "UserRole does not exist");
            return false;
        } else {
            logger.log(Level.INFO, "UserRole does not exist");            
            return true;
        }
    }
    
    private UserHasRole createUserRoles(User user, int id, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} creating relationship between role {1} and user {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), user.getAlias()});        

        if ( userhasroleController == null ) {
            userhasroleController = new UserHasRoleJpaController();
        }         
        if ( checkUserHasRoleExists(user.getIdUser(),id) == true ) {
            throw new EntityExistsException("The role for user " + user.getAlias() + " already exists.");
        }        
        UserHasRolePK uhrPK = new UserHasRolePK(user.getIdUser(),id); 
        UserHasRole uhr = new UserHasRole(uhrPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        uhr.setCreatedTS(currentTimestamp);
        uhr.setUpdatedTS(currentTimestamp);
        uhr.setCreatedBy(currentUserId);
        uhr.setUpdatedBy(currentUserId);    
        uhr = userhasroleController.edit(uhr);
        AuditLogger.log(currentUserId, uhr.getUserHasRolePK().getUseridUser(), "create", uhr);           
        return uhr;
    }
     
    public String createUserRoles(String JSONString, Integer currentUserId) {
        String result = "UserRole(s) not created";
        User user = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray userArray = JSONContentRoot.getJSONArray("users");
            for (int ii=0; ii<userArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = userArray.getJSONObject(ii).toString();
                user = gson.fromJson(jString, User.class);   
                JSONArray roleArray = JSONContentRoot.getJSONArray("roles");                
                for (int i=0; i<roleArray.length(); i++) {
                    UserHasRole uhr = createUserRoles(user, roleArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(uhr).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new User: {0} {1}", new Object[]{user.getAlias(), ex});
            result = "DA0010 Exception while creating new User: " + ex.getMessage();
        }   
        return wrapAPI("userRoles", dataString, "User Role(s) created", result);
    }

    private UserHasRole deleteUserRoles(User user, int id, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship between role {1} and user {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), user.getAlias()});
        if ( userhasroleController == null ) {
            userhasroleController = new UserHasRoleJpaController();
        }               
        UserHasRolePK uhrPK = new UserHasRolePK(user.getIdUser(),id); 
        UserHasRole uhr = new UserHasRole(uhrPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        uhr.setUpdatedTS(currentTimestamp);
        uhr.setUpdatedBy(currentUserId);    
        userhasroleController.destroy(uhrPK);
        AuditLogger.log(currentUserId, uhr.getUserHasRolePK().getUseridUser(), "delete", uhr);           
        return uhr;
    }
     
    public String deleteUserRoles(String JSONString, Integer currentUserId) {
        String result = "UserRole(s) not deleted";
        User user = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray userArray = JSONContentRoot.getJSONArray("users");
            for (int ii=0; ii<userArray.length(); ii++) {
                logger.log(Level.INFO, "Deleting Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = userArray.getJSONObject(ii).toString();
                user = gson.fromJson(jString, User.class);   
                JSONArray roleArray = JSONContentRoot.getJSONArray("roles");                
                for (int i=0; i<roleArray.length(); i++) {
                    UserHasRole uhr = deleteUserRoles(user, roleArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(uhr).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting  role releationship for User: {0} {1}", new Object[]{user.getAlias(), ex});
            result = "DA0010 Exception while deleting role releationship for User: " + ex.getMessage();
        }   
        return wrapAPI("userRoles", dataString, "User Role(s) deleted", result);
    }
 
    //-------------------------------------------------------------------------- Role Permissions
    public boolean checkRoleHasPermissionExists(int permId, int roleId) {
        if ( permissionController == null ) {
           permissionController = new PermissionJpaController();
        }
        PermissionPK pPK = new PermissionPK(permId,roleId);          
        Permission permission = permissionController.findPermission(pPK);
        if ( (permission == null) ) {
            logger.log(Level.INFO, "Role Permisison does not exist");
            return false;
        } else {
            logger.log(Level.INFO, "Role Permission does exist");            
            return true;
        }
    }
    
    private Permission createRolePermissions(int roleId, int permId, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} creating relationship between role {1} and permission template {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((roleId)), Integer.toString((permId))});        

        if ( permissionController == null ) {
            permissionController = new PermissionJpaController();
        }         
        if ( checkRoleHasPermissionExists(permId, roleId) == true ) {
            throw new EntityExistsException("The permission for role " + Integer.toString(roleId) + " already exists.");
        }        
        PermissionPK pPK = new PermissionPK(permId, roleId); 
        Permission permission = new Permission(pPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        permission.setCreatedTS(currentTimestamp);
        permission.setUpdatedTS(currentTimestamp);
        permission.setCreatedBy(currentUserId);
        permission.setUpdatedBy(currentUserId);    
        permission = permissionController.edit(permission);
        AuditLogger.log(currentUserId, permission.getPermissionPK().getPermTempidPermTemp(), "create", permission);   
        return permission;
    }
     
    public String createRolePermissions(String JSONString, Integer currentUserId) {
        String result = "RolePermission(s) not created";
        Role role = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
            for (int ii=0; ii<roleArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Permissions for Role {0}", roleArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = roleArray.getJSONObject(ii).toString();
                role = gson.fromJson(jString, Role.class);   
                JSONArray permissionArray = JSONContentRoot.getJSONArray("permissions");                
                for (int i=0; i<permissionArray.length(); i++) {
                    Permission permission = createRolePermissions(role.getIdRole(), permissionArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(permission).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new permission for Role: {0} {1}", new Object[]{role.getName(), ex});
            result = "DA0010 Exception while creating new Permission: " + ex.getMessage();
        }   
        return wrapAPI("rolePermissions", dataString, "Role Permissions(s) created", result);
    }

    private Permission deleteRolePermissions(int roleId, int permId, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship between role {1} and permission {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((roleId)), Integer.toString(permId)});
        if ( permissionController == null ) {
            permissionController = new PermissionJpaController();
        }               
        PermissionPK pPK = new PermissionPK(permId,roleId); 
        Permission permission = new Permission(pPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        permission.setUpdatedTS(currentTimestamp);
        permission.setUpdatedBy(currentUserId);    
        permissionController.destroy(pPK);
        AuditLogger.log(currentUserId, permission.getPermissionPK().getPermTempidPermTemp(), "delete", permission);  
        return permission;
    }
     
    public String deleteRolePermissions(String JSONString, Integer currentUserId) {
        String result = "Role Permission(s) not deleted";
        Role role = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
            for (int ii=0; ii<roleArray.length(); ii++) {
                logger.log(Level.INFO, "Deleting Permissions for Role {0}", roleArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = roleArray.getJSONObject(ii).toString();
                role = gson.fromJson(jString, Role.class);   
                JSONArray permissionArray = JSONContentRoot.getJSONArray("permissions");                
                for (int i=0; i<permissionArray.length(); i++) {
                    Permission permission = deleteRolePermissions(role.getIdRole(), permissionArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(permission).toString();
                    }
                }
            dataString = "[" + dataString + "]";
            result = "Role Permission(s) deleted";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting permission releationship for Role: {0} {1}", new Object[]{role.getName(), ex});
            result = "DA0010 Exception while deleting permission releationship for Role: " + ex.getMessage();
        }   
        return wrapAPI("rolePermissions", dataString, "Role Permission(s) deleted", result);
    }
    
    public boolean checkUserExists(String userAlias) {
        if ( userController == null ) {
           userController = new UserJpaController();
        }
        List<User> user = userController.findUserByAlias(userAlias);
        if ( (user == null) ) {
            logger.log(Level.INFO, "User " + userAlias + " does not exist");
            return false;
        } else {
            if ( user.size() == 0 ) {
                logger.log(Level.INFO, "User " + userAlias + " does not exist");
                return false;
            }
            logger.log(Level.INFO, "User " + userAlias + " exists");
            return true;
        }
    }
    
    public String getUserAuthInfo(String userAlias) {
        String dataStringRoles = "";
        String dataStringMenu = "";
        int roleRecordCounter = 0;
        String result = "No users found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONRoles = new JSONObject(); 
        
        if ( userController == null ) {
           userController = new UserJpaController();
        }
        if ( permissionController == null ) {
           permissionController = new PermissionJpaController();
        }        
        List<User> users = userController.findUserByAlias(userAlias);
        if ( (users != null) && !users.isEmpty() ) {
            logger.log(Level.INFO, "User {0} exists. Retrieving roles", userAlias);
            List<Role> roles = getUserRoles(users.get(0).getIdUser());
            result = "No roles found";
            if ( (roles != null) && !roles.isEmpty() ) {
                roleRecordCounter = roles.size();
                try {
                    for (int i=0; i<roleRecordCounter; i++) {
                        logger.log(Level.INFO, "Processing role {0}", roles.get(i).getName());
                        if ( i > 0 ) {
                            dataStringRoles = dataStringRoles.concat(",");
                        } 
                        dataStringRoles = dataStringRoles + new JSONObject(roles.get(i)).toString();      
                        List<Permission> permissions = permissionController.findPermissionByRoleId(roles.get(i).getIdRole());
                        List<Permissiontemplate> permTemplate = getRolePermissions(permissions);
                        dataStringRoles = Util.removeLastChar(dataStringRoles) + ",\"permissions\":" + getRolePermissionsJSON(permTemplate) + "}";
                        dataStringMenu += buildMenuFromPermissions(permTemplate);
                    }
                    dataStringRoles = "[" + dataStringRoles + "]";
                    dataStringMenu = "[" + Util.removeLastChar(dataStringMenu) + "]";
                } catch (Exception ex) {
                    Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    result = "DA0027 Exception parsing Role into JSON";
                }   
            }
        }
        try {
            if ( "".equals(dataStringRoles) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(roleRecordCounter) + " userAuth entries retrieved");   
                success.put("totalCount", Integer.valueOf(roleRecordCounter));
                success.put("mainMenu", dataStringMenu);
                JSONRoles.put("roles", new JSONArray(dataStringRoles));
                success.put("userAuth", JSONRoles);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }         
        return result;
    }

    private List<Permissiontemplate> getRolePermissions(List<Permission> permissions) {
        List<Permissiontemplate> permissiontemplates = new ArrayList();
        if ( permissiontemplateController == null ) {
           permissiontemplateController = new PermissiontemplateJpaController();
        } 
        for (Permission t : permissions) {
            int i = t.getPermissionPK().getPermTempidPermTemp();
            Permissiontemplate p = permissiontemplateController.findPermissiontemplate(i);
            if ( p != null ) {
                permissiontemplates.add(p);
            }
        }
        Collections.sort(permissiontemplates, new PermissiontemplateComp());
        return permissiontemplates;
    }
    
    private String getRolePermissionsJSON(int idRole) {
        String dataStringPermissions = "";
        int permissionCounter = 0;
 
        logger.log(Level.INFO, "Retrieving Permissions for Role {0}", Integer.toString(idRole));
        if ( permissionController == null ) {
           permissionController = new PermissionJpaController();
        }
        
        List<Permission> permissions = permissionController.findPermissionByRoleId(idRole);
        if ( (permissions != null) && !permissions.isEmpty() ) {  
            logger.log(Level.INFO, "Role {0} has permissions. Retrieving permission data.", Integer.toString(idRole));
            List<Permissiontemplate> permtemp = getRolePermissions(permissions);            
            permissionCounter = permtemp.size();
            for (int i=0; i<permissionCounter; i++) {
                //logger.log(Level.INFO, "Processing role {0}", permissions.get(i).getTag());
                if ( i > 0 ) {
                    dataStringPermissions = dataStringPermissions.concat(",");
                } 
                dataStringPermissions = dataStringPermissions + new JSONObject(permtemp.get(i)).toString();
                }
            dataStringPermissions = "[" + dataStringPermissions + "]";
            return dataStringPermissions;
        }
        return dataStringPermissions;
    }

    private String getRolePermissionsJSON(List<Permissiontemplate> permtemp) {
        String dataStringPermissions = "";
        int permissionCounter = 0;
        logger.log(Level.INFO, "Retrieving permission data.");       
        permissionCounter = permtemp.size();
        for (int i=0; i<permissionCounter; i++) {
            //logger.log(Level.INFO, "Processing role {0}", permissions.get(i).getTag());
            if ( i > 0 ) {
                dataStringPermissions = dataStringPermissions.concat(",");
            } 
            dataStringPermissions = dataStringPermissions + new JSONObject(permtemp.get(i)).toString();
            }
        dataStringPermissions = "[" + dataStringPermissions + "]";
        return dataStringPermissions;
    }
    
    public String getRolePermissionInfo(int idRole) {
            
        String dataStringPermissions = "";
        String result = "No permissions found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONPermissions = new JSONObject();  
        JSONArray JSONArrayPermissions = null;

        logger.log(Level.INFO, "Retrieving Permission for Role {0}.", Integer.toString(idRole));         
        try {
            dataStringPermissions = getRolePermissionsJSON(idRole);
        } catch (Exception ex) {
            Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            result = "DA0027 Exception parsing Permission into JSON";
        }   
        try {
            if ( "".equals(dataStringPermissions) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                JSONArrayPermissions = new JSONArray(dataStringPermissions);
                success.put("success", true);   
                status.put("message", Integer.valueOf(JSONArrayPermissions.length()) + " role permission entries retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayPermissions.length()));
                JSONPermissions.put("permissions", JSONArrayPermissions);
                success.put("rolePermissions", JSONPermissions);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getRolePermissions]: ", ex);  
            result = "JSON error in WS method [admin.getRolePermissions]";
        }         
        return result;
    }
        
    //--------------------------------------------------------------------- Misc
    public String login(String userAlias, String password, HttpSession session) {

        logger.log(Level.INFO, "Logging in user {0}", userAlias);
        User user = null;
        
        if ( Auth.verifyUserSession(userAlias, session)) {
            return "";
        }
        
        if ( Auth.verifyPassword(userAlias, password) ) {
            logger.log(Level.INFO, "User {0} authenticated", userAlias);
            if ( userController == null ) {
                userController = new UserJpaController();
           }
           ArrayList users = new ArrayList();
           users = (ArrayList) userController.findUserByAlias(userAlias);
           user = (User) users.get(0); 
           if ( user.getPassExpirationTS().before(new Date()) ) {
               logger.log(Level.INFO, "Password expired on {0}", user.getPassExpirationTS().toString());
               return "Password expired";               
           }   
           if ( user.getActive() == false ) {
               logger.log(Level.INFO, "User {0} is not active", user.getAlias());
               return "User profile not active";               
           }            
           Auth.createUserSession(user, session);
           return "";
        }
        return "User not authenticated.";
    }
 
    public boolean logout(HttpSession session) {

        logger.log(Level.INFO, "Terminating session {0}", session.toString());
        
        if ( Auth.killSession(session)) {
            return true;
        }
        return false;
    }
    
    public String authorize(String userAlias, String password, String organization) {

        logger.log(Level.INFO, "Authorizing User {0} for Organization {1}", new Object[]{userAlias, organization});
        User user = null;
        ArrayList users = new ArrayList();
        if ( userController == null ) {
            userController = new UserJpaController();
        }        
        users = (ArrayList) userController.findUserByAlias(userAlias);
        user = (User) users.get(0);         
        
        if ( Auth.verifyPassword(userAlias, password) && (user.getActive() == true) ) {
           logger.log(Level.INFO, "User {0} authenticated.", userAlias);
           if ( organizationController == null ) {
                organizationController = new OrganizationJpaController();
           }
           if ( user.getPassExpirationTS().before(new Date()) ) {
               logger.log(Level.INFO, "Password expired.", user.getPassExpirationTS().toString());
               return "DA0001 Password expired.";               
           }
           Organization org = organizationController.findOrganization(user.getOrganization());           
           if ( org == null ) {
               logger.log(Level.INFO, "Organization with ID {0} not authorized. Not in database.", org.getIdOrganization().toString());
               return "DA0002 User not authorized.";
           }      
           if ( !org.getIdOrganization().toString().equals(organization) ) {
               logger.log(Level.INFO, "Organization with ID {0} not authorized. Not matching request id {1}", new Object[]{org.getIdOrganization().toString(),organization});
               return "DA0003 User not authorized.";               
           }
           logger.log(Level.INFO, "User from Organization with ID {0} authorized", org.getIdOrganization().toString());           
           return "OK";
        }
        return ""; 
    }
    
     public User goPseudo(String JSONString, Integer currentUserId) {
        User user = new User();
        if ( userController == null ) {
            userController = new UserJpaController();
        }        
        try {
            JSONObject userAuth =  new JSONObject(JSONString);
            JSONArray users = userAuth.getJSONArray("userAuth");
            JSONObject goUser = users.optJSONObject(0);
            user = userController.findUser(goUser.getInt("idUser"));
            AuditLogger.log(currentUserId, user.getIdUser(), "pseudo", user);                      
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while switching users from superuser {0} to {1}: {2}", new Object[]{currentUserId.toString(), user.getIdUser(), ex});
        }   
        return user;
    }   

    private String buildMenuFromPermissions(List<Permissiontemplate> permTemplate) {
        String menuString = "";
        for (Permissiontemplate pt : permTemplate) {
            if ( (pt.getActive() == true) && ("menu".equals(pt.getType().toLowerCase())) ) {
                menuString += "{text:'" + pt.getName()
                        + "',iconCls:'" + pt.getTag().toLowerCase()
                        + "-icon16',controller:'delta3.controller." + pt.getTag() + "'},";             
            }
        }
        return menuString;
    }
    
  public String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();         
        try {
            if ( "".equals(result) ) {        
                success.put("success", false);       
                status.put("message", msgFailure);   
            } else {
                success.put("success", true);   
                status.put("message", msgSuccess);   
                success.put(objectId, new JSONArray(result));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin." + objectId + "] while processing " + msgFailure, ex);  
        }
        return msgFailure;    
    }
}
