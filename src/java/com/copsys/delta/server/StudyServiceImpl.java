/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server;

import com.copsys.delta.db.DBHelper;
import com.copsys.delta.entity.Method;
import com.copsys.delta.entity.model.Model;
import com.copsys.delta.entity.model.ModelColumn;
import com.copsys.delta.entity.study.Filter;
import com.copsys.delta.entity.study.FilterField;
import com.copsys.delta.entity.study.FilterHasField;
import com.copsys.delta.entity.study.FilterHasFieldPK;
import com.copsys.delta.entity.study.Study;
import com.copsys.delta.entity.study.StudyHasCategory;
import com.copsys.delta.entity.study.StudyHasCategoryPK;
import com.copsys.delta.entity.process.Process;
import com.copsys.delta.jpa.MethodJpaController;
import com.copsys.delta.jpa.PersistenceHelper;
import com.copsys.delta.jpa.exceptions.NonexistentEntityException;
import com.copsys.delta.jpa.model.ModelColumnJpaController;
import com.copsys.delta.jpa.model.ModelJpaController;
import com.copsys.delta.jpa.process.ProcessJpaController;
import com.copsys.delta.jpa.study.FilterFieldJpaController;
import com.copsys.delta.jpa.study.FilterHasFieldJpaController;
import com.copsys.delta.jpa.study.FilterJpaController;
import com.copsys.delta.jpa.study.StudyHasCategoryJpaController;
import com.copsys.delta.jpa.study.StudyJpaController;
import com.copsys.delta.security.AuditLogger;
import com.copsys.delta.server.exceptions.EntityExistsException;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class StudyServiceImpl {
    private static final Logger logger = Logger.getLogger(StudyServiceImpl.class.getName());    
    private static FilterJpaController filterController;  
    private static StudyHasCategoryJpaController studyhascategoryController;
    private static FilterHasFieldJpaController filterhasfieldController;    
    private static FilterFieldJpaController filterfieldController;        
    private static ModelColumnJpaController modelcolumnController;    
    private static StudyJpaController studyController;
    private static MethodJpaController methodController;    
    private static ModelJpaController modelController; 
    private static ProcessJpaController processController;
    private static AdminServiceImpl adminService;    
    private static StatServiceImpl statService; 
    private static String[][] methodStates = {
            {"logisticRegression", "propensityScoreAnalysis", "riskAdjustedSPRT"},
            {"Error", "New", "LR 1 of 2 In Progress", "LR Stage 2 of 2 Ready", "LR 2 of 2 In Progress", "LR Completed"},
            {"Error", "New", "PA 1 of 1 In Progress", "PA Completed"},
            {"Error", "New", "SPRT 1 of 1 In Progress", "SPRT Completed"}
        };

    public StudyServiceImpl() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }        
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }         
        if ( methodController == null ) {
           methodController = new MethodJpaController();
        }    
        if ( studyController == null ) {
           studyController = new StudyJpaController();
        }       
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }         
        if ( modelcolumnController == null ) {
           modelcolumnController = new ModelColumnJpaController();
        }               
        if ( studyhascategoryController == null ) {
           studyhascategoryController = new StudyHasCategoryJpaController();
        }   
        if ( filterController == null ) {
            filterController = new FilterJpaController();
        }    
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }         
        if ( filterfieldController == null ) {
           filterfieldController = new FilterFieldJpaController();
        }     
        if ( filterhasfieldController == null ) {
           filterhasfieldController = new FilterHasFieldJpaController();
        }        
    }
    //-------------------------------------------------------------------------- Study
    public String getOrgStudies(int start, int limit, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all users
            subjects = controller.findObjectEntities(currentOrgId);
        } else {
            subjects = controller.findObjectEntities(limit, start, currentOrgId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            // insert categories object
            String category = getStudyCategoryJSON(((Study)subjects.get(recordCounter)).getIdStudy());     
            if ( "".equals(category) ) {
                category = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"categories\":" + category).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"categories\":" + category).toString();
            }
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(controller.getObjectCount(currentOrgId)));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  
    
    public String deleteStudies(String JSONString, Integer currentUserId) {
        String result = "Study deleted";
        Study study = null;
        String dataString = "";
      
        try {
            JSONArray studyArray = new JSONArray(JSONString);
            for (int i=0; i<studyArray.length(); i++) {
                JSONObject JSONContentRoot =  studyArray.getJSONObject(i);
                logger.log(Level.INFO, "Deleting Study {0}", JSONContentRoot.getInt("idStudy"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = JSONContentRoot.toString();
                if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                dataString = dataString + jString;
                study = gson.fromJson(jString, Study.class);   
                Integer studyId = JSONContentRoot.getInt("idStudy");       
                List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(studyId);
                for (StudyHasCategory t : shcOld) {
                    int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                    StudyHasCategoryPK shcPK = new StudyHasCategoryPK(studyId,id); 
                    studyhascategoryController.destroy(shcPK);
                }  
                studyController.deleteObject(study, currentUserId);
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Study: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0001 Exception while deleting category releationship for study: " + ex.getMessage();
        }   
        return wrapAPI("studies", dataString, "Studies deleted", result);
    }
  
    public String cloneStudy(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Study subject) {
        
        String resultString = "Study successfully cloned";       
        Integer oldStudyId;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            //Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateSerializer()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            oldStudyId = ((Study)subject).getIdStudy();
            ((Study)subject).setIdStudy(0);
            ((Study)subject).setName(((Study)subject).getName() + " Clone");
            ((Study)subject).setOriginalStudyId(oldStudyId);
            ((Study)subject).setStatus("New");
            // cloning assumes coping ids for Outcomes, Treatments, Method and Model and the rest of params
            subject = (Study) controller.createObjects(subject, currentOrgId, currentUserId);
            // now clone study_has_category relationships


            List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(oldStudyId);
            for (StudyHasCategory t : shcOld) {
                int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                StudyHasCategoryPK shcPK = new StudyHasCategoryPK(subject.getIdStudy(),id); 
                StudyHasCategory shc = new StudyHasCategory(shcPK);
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
                shc.setCreatedTS(currentTimestamp);
                shc.setUpdatedTS(currentTimestamp);
                shc.setCreatedBy(currentUserId);
                shc.setUpdatedBy(currentUserId);    
                shc = studyhascategoryController.edit(shc);
            }            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while cloning study {0}: {1}", new Object[]{subject.getName(), ex});
            return "ST0002 Exception while creating new study: "  + subject.getName();
        }
   
        return resultString;
    }
   
    public String splitStudy(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Study subject) {
        
        String resultString = "Study successfully split";       
        Integer oldStudyId;

        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            //Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            oldStudyId = ((Study)subject).getIdStudy();
            ((Study)subject).setName(((Study)subject).getName() + " Clone");            
            ((Study)subject).setOriginalStudyId(oldStudyId);            
            // cycle through study_has_category relationships
            List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(oldStudyId);
            for (StudyHasCategory t : shcOld) {
                // create new study
                ((Study)subject).setIdStudy(0);              
                ((Study)subject).setDescription("Cloned by Catagory Field ID: " + Integer.toString(t.getStudyHasCategoryPK().getModelColumnidModelColumn()));
                subject = (Study) controller.createObjects(subject, currentOrgId, currentUserId);         
                // create study_has_category
                int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                StudyHasCategoryPK shcPK = new StudyHasCategoryPK(subject.getIdStudy(),id); 
                StudyHasCategory shc = new StudyHasCategory(shcPK);
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
                shc.setCreatedTS(currentTimestamp);
                shc.setUpdatedTS(currentTimestamp);
                shc.setCreatedBy(currentUserId);
                shc.setUpdatedBy(currentUserId);    
                shc = studyhascategoryController.edit(shc);
            }            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while cloning study {0}: {1}", new Object[]{subject.getName(), ex});
            return "ST0003 Exception while cloning new study: "  + subject.getName();
        }
   
        return resultString;
    }

    public String initStudyMethods(String decodedContent, Integer orgId, Integer userId) {
        
        String statConfig = null;
        if ( "remote".equals(decodedContent) ) {
            statConfig = statService.getStatConfigRemote();
        } else {
            statConfig = statService.getStatConfig();            
        }
        if ( statConfig != null ) {
            saveStudyMethods(statConfig, orgId, userId);
        } else {
            logger.log(Level.SEVERE, "Not initialized stat methods for org {0}", orgId.toString());            
        }
        return "Study Methods configuration updated.";
    }    
 
    // this method is used by initStudyMethods() triggered by WS
    // and from call back used to communicate with exteral stat package
    public String saveStudyMethods(String statConfig, Integer orgId, Integer userId) {
        try {
            JSONObject content = new JSONObject(statConfig);
            JSONObject conf = content.getJSONObject("statConfig");
            JSONArray methods = conf.getJSONArray("methods");
            
            List<Object> methodRecords = methodController.findOrgMethodEntities(orgId);           
            // record that match by name needs to be kept, only params changed
            // deletes are not allowed
            for (int i=0; i<methods.length(); i++) {
                Object orig;
                JSONObject m = methods.getJSONObject(i);
                Method method = new Method();    
                method.setActive(Boolean.TRUE);
                method.setIdOrganization(orgId);
                method.setName(m.getString("name"));
                method.setMethodParams(m.getJSONArray("steps").toString());  
                if ( (orig = isMethodNew(methodRecords, m.getString("name"))) == null ) {
                    method.setIdMethod(0);
                    logger.log(Level.SEVERE, "Inserting statistical method: {0}", m.getString("name"));
                    methodController.createObjects(method, orgId, userId);
                } else {
                    method.setIdMethod(((Method)orig).getIdMethod());
                    method.setCreatedBy(((Method)orig).getCreatedBy());
                    method.setCreatedTS(((Method)orig).getCreatedTS());
                    method.setDescription(((Method)orig).getDescription());
                    logger.log(Level.SEVERE, "Updating statistical method: {0}", m.getString("name"));
                    methodController.updateObjects(method, orgId, userId);
                }
            }
            return null;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while initializing stat methods for org {0}: {1}", new Object[]{orgId.toString(), ex.getMessage()});
        } 
        return null;
    }

    private Object isMethodNew (List<Object> mr, String name) {
        for (Object m : mr) {
            if ( ((Method)m).getName() == null ? name == null : ((Method)m).getName().equals(name)  ) {
               return m; 
            }
        }        
        return null;
    } 
    
    public String executeStudy(String JSONString, Integer currentOrgId, Integer currentUserId, Study subject) {
        
        String resultString = "ST0112 General study execution error";      
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            // find method name to be a key to config
            Method method = (Method) methodController.findMethod(subject.getIdMethod());
            //ModelColumn orderField = modelcolumnController.findModelColumn(subject.getIdKey());
            ModelColumn orderField = modelcolumnController.findModelColumn(subject.getIdDate());        
            int methodIndex = getMethodStatusOffset(method.getName());
            // main study execution switch
            switch ( method.getName() ) {
                case "logisticRegression":
                    // following code is specialized for Logistic Regression 2 step process
                    switch ( subject.getStatus() ) {
                        case "New": // needs to be matched with methodStates array
                            resultString = statService.submitStatJobLR1(subject, 
                                    method.getName(), 
                                    getSqlFromFilterFormulas(subject.getIdFilter()), 
                                    orderField.getName(), 
                                    currentOrgId, 
                                    currentUserId);
                            break;
                        case "LR Stage 2 of 2 Ready": // needs to be matched with methodStates array
                            resultString = statService.submitStatJobLR2(subject, 
                                    method.getName(), 
                                    getSqlFromFilterFormulas(subject.getIdFilter()), 
                                    orderField.getName(), 
                                    currentOrgId, 
                                    currentUserId);       
                            break;
                    }                   
                    break;
                case "propensityScoreAnalysis":
                    // following code is specialized for Propensity 1 step process
                    switch ( subject.getStatus() ) {
                        case "New": // needs to be matched with methodStates array
                            resultString = statService.submitStatJobP1(subject, 
                                    method.getName(), 
                                    getSqlFromFilterFormulas(subject.getIdFilter()), 
                                    orderField.getName(), 
                                    currentOrgId, 
                                    currentUserId);
                            break;
                    }                        
                    break;
                case "riskAdjustedSPRT":
                    // following code is specialized for Propensity 1 step process
                    switch ( subject.getStatus() ) {
                        case "New": // needs to be matched with methodStates array
                            resultString = statService.submitStatJobSPRT1(subject, 
                                    method.getName(), 
                                    getSqlFromFilterFormulas(subject.getIdFilter()), 
                                    orderField.getName(), 
                                    currentOrgId, 
                                    currentUserId);
                            break;
                    }                        
                    break;                    
            }  
            resultString = advanceStatus(resultString, subject, currentUserId);      
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON exception while executing study {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0004 Error while executing study " + subject.getName() + ": " + ex.getMessage();            
        } 
        return resultString;
    }

    /*public String getStudyNextStepParams(String JSONString, Integer currentOrgId, Integer currentUserId, Study subject) throws JSONException {
        String resultString = null;
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            // find method name to be a key to config
            Method method = (Method) methodController.findMethod(subject.getIdMethod());            
            String statConfig = statService.getStatConfig();
            JSONObject content = new JSONObject(statConfig);
            JSONObject conf = content.getJSONObject("statConfig");
            JSONArray methods = conf.getJSONArray("methods");
            JSONObject m = null;
            for (int i=0; i< methods.length(); i++) {
                m = methods.getJSONObject(i);
                if (m.getString("name").equals(method.getName()) ) {
                    break;
                }
            }
            if ( m != null ) {
                JSONArray steps = m.getJSONArray("steps");              
                switch ( subject.getStatus() ) {
                    case "new":
                        resultString = steps.get(0).toString();
                        break;
                    case "LR1 completed":
                    case "P1 completed":
                        resultString = steps.get(1).toString();       
                        break;
                }             
            } else {
                resultString = "No configuration found for " + method.getName();
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving step parameters for study " + subject.getIdStudy().toString(), ex.getMessage());
            throw ex;
        }
        return resultString;
    }*/
    
    public String rollbackStudyStatus(String JSONString, Integer currentUserId) {
        
        String resultString = "Study status successfully changed";      
        Study subject = new Study();
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            Method m = (Method) methodController.findMethod(subject.getIdMethod());            
            rollbackStatusBypassInProgress(subject, currentUserId);   
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON exception while executing study {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0005 Error while rolling back study status " + subject.getName() + ". Problem with study configuration: " + ex.getMessage();            
        } 
        return resultString;
    }

    public String advanceStudyStatus(String JSONString, Integer currentUserId) {    
        String resultString = "Study status successfully changed.";      
        Study subject = new Study();
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            Method m = (Method) methodController.findMethod(subject.getIdMethod());
            advanceStatusBypassInProgress("OK", subject, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON exception while advancing study status {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0006 Error while advancing study status " + subject.getName() + ": " + ex.getMessage();            
        } 
        return resultString;        
    }
    
    public String advanceStatus(String resultString, Study s, Integer currentUserId) {       
        Method method = (Method) methodController.findMethod(s.getIdMethod());        
        String status;
        if ( "OK".equals(resultString) ) {
            status = getNextMethodStatus(getMethodStatusOffset(method.getName()),s.getStatus());             
        } else {
            status = resultString;
        }
        resultString = "Study status successfully changed.";
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while advancing status for study " + s.getName(), ex);
            resultString = "ST0110 Error while advancing status for study " + s.getName() + ": " + ex.getMessage();    
        }
        return resultString;
    }

    public String advanceStatusBypassInProgress(String resultString, Study s, Integer currentUserId) {       
        Method method = (Method) methodController.findMethod(s.getIdMethod());        
        String status;
        if ( "OK".equals(resultString) ) {
            status = getNextMethodStatus(getMethodStatusOffset(method.getName()),s.getStatus());         
            if ( status.endsWith("In Progress") ) {
                status = getNextMethodStatus(getMethodStatusOffset(method.getName()),status);          
            }
        } else {
            status = resultString;
        }
        resultString = "Study status successfully changed.";
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while advancing status for study " + s.getName(), ex);
            resultString = "ST0111 Error while advancing status for study " + s.getName() + ": " + ex.getMessage();    
        }
        return resultString;
    }
    
    public String rollbackStatus(Study s, Integer currentUserId) {
        Method method = (Method) methodController.findMethod(s.getIdMethod());           
        String status = getPreviousMethodStatus(getMethodStatusOffset(method.getName()),s.getStatus());                               
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status for study " + s.getName(), ex);
            status = "error";
        }
        return status;
    }

    public String rollbackStatusBypassInProgress(Study s, Integer currentUserId) {
        Method method = (Method) methodController.findMethod(s.getIdMethod());           
        String status = getPreviousMethodStatus(getMethodStatusOffset(method.getName()),s.getStatus());     
        if ( status.endsWith("In Progress") ) {
            status = getPreviousMethodStatus(getMethodStatusOffset(method.getName()),status);          
        }        
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status for study " + s.getName(), ex);
            status = "error";
        }
        return status;
    }
    
    public String setStudyStatus(Integer studyId, String status, Integer currentUserId) {
        Study s = (Study) studyController.findStudy(studyId);
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status on study " + s.getName(), ex);
            status = "error";
        }
        return status;
    }

    //-------------------------------------------------------------------------- Study Details
    public String getStudyDetails(String studyId, Integer organizationId, Integer userId) {
        String result = "Study details not retrieved";
        String dataString = "";
        Study study = null;
        try {
            Integer stdId = Integer.parseInt(studyId);
            study = (Study)studyController.findStudy(stdId);
            if ( study.getIdOrganization() != organizationId ) {
                result = "ST0113 User not authorized";
            } else {
                JSONObject JSONStudy = new JSONObject(study);
                ModelColumn outcome;
                outcome = modelcolumnController.findModelColumn(JSONStudy.getInt("idOutcome"));
                JSONObject JSONOutcome = new JSONObject(outcome);
                JSONStudy.put("outcome", JSONOutcome);
                dataString = "[" + JSONStudy.toString() + "]";
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving Study details: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0114 Exception while retrieving study details: " + ex.getMessage(); 
        }
        return wrapAPI("studyDetails", dataString, "Study details retrieved", result);
    }
    
    //-------------------------------------------------------------------------- Study Categories
    public List<ModelColumn> getStudyCategories(int studyId) {
        List<ModelColumn> columns = new ArrayList();
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK();       
        shcPK.setStudyidStudy(studyId);
        List<StudyHasCategory> shc = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(studyId);
        if ( (shc.isEmpty()) ) {
            logger.log(Level.INFO, ""
                    + "StudyCategory does not exist");
            return null;
        }
        for (StudyHasCategory t : shc) {
            int i = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
            ModelColumn r = modelcolumnController.findModelColumn(i);
            if ( r != null ) {
                columns.add(r);
            }
        }
        return columns;
    }
    
    public boolean checkStudyHasCategoryExists(int userId, int roleId) {
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(userId,roleId);          
        StudyHasCategory shc = studyhascategoryController.findStudyHasCategory(shcPK);
        if ( (shc == null) ) {
            logger.log(Level.INFO, "StudyCategory does not exist");
            return false;
        } else {
            logger.log(Level.INFO, "StudyCategory does not exist");            
            return true;
        }
    }
    
    private StudyHasCategory createStudyCategories(Study study, int id, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} creating relationship between category {1} and study {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), study.getName()});        
 
        if ( checkStudyHasCategoryExists(study.getIdStudy(),id) == true ) {
            throw new EntityExistsException("The category for study " + study.getName() + " already exists.");
        }        
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(study.getIdStudy(),id); 
        StudyHasCategory shc = new StudyHasCategory(shcPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        shc.setCreatedTS(currentTimestamp);
        shc.setUpdatedTS(currentTimestamp);
        shc.setCreatedBy(currentUserId);
        shc.setUpdatedBy(currentUserId);    
        shc = studyhascategoryController.edit(shc);
        return shc;
    }
     
    public String createStudyCategories(String JSONString, Integer currentUserId) {
        String result = "Study Categories not created";
        Study study = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray anArray = JSONContentRoot.getJSONArray("studies");
            for (int ii=0; ii<anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Categories for Study {0}", anArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                study = gson.fromJson(jString, Study.class);   
                JSONArray categoryArray = JSONContentRoot.getJSONArray("categories");                
                for (int i=0; i<categoryArray.length(); i++) {
                    StudyHasCategory shc = createStudyCategories(study, categoryArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(shc).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Study Category: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0008 Exception while creating new Category: " + ex.getMessage();  
        }   
        return wrapAPI("studyCategories", dataString, "Study Categories created", result);
    }

    private StudyHasCategory deleteStudyCategories(Study study, int id, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship between category {1} and study {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), study.getName()});
             
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(study.getIdStudy(),id); 
        StudyHasCategory shc = new StudyHasCategory(shcPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        shc.setUpdatedTS(currentTimestamp);
        shc.setUpdatedBy(currentUserId);    
        studyhascategoryController.destroy(shcPK);
        return shc;
    }
     
    public String deleteStudyCategories(String JSONString, Integer currentUserId) {
        String result = "StudyCategories not deleted";
        Study study = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray anArray = JSONContentRoot.getJSONArray("studies");
            for (int ii=0; ii<anArray.length(); ii++) {
                logger.log(Level.INFO, "Deleting Categories for Study {0}", anArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                study = gson.fromJson(jString, Study.class);   
                JSONArray categoryArray = JSONContentRoot.getJSONArray("categories");                
                for (int i=0; i<categoryArray.length(); i++) {
                    StudyHasCategory uhr = deleteStudyCategories(study, categoryArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(uhr).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Study: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0009 Exception while deleting category releationship for study: " + ex.getMessage();
        }   
        return wrapAPI("studyCategories", dataString, "Study Categories deleted", result);
    }
    
    public String getStudyCategoryInfo(int idStudy) {
            
        String dataString = "";
        String result = "No categories found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONCategories = new JSONObject();  
        JSONArray JSONArrayCategories = null;

        logger.log(Level.INFO, "Retrieving Categories for Study {0}.", Integer.toString(idStudy));         
        try {
            dataString = getStudyCategoryJSON(idStudy);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            result = "DA0010 Exception parsing Study into JSON";
        }   
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                JSONArrayCategories = new JSONArray(dataString);
                success.put("success", true);   
                status.put("message", Integer.valueOf(JSONArrayCategories.length()) + " study categories retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayCategories.length()));
                JSONCategories.put("categories", JSONArrayCategories);
                success.put("studyCategories", JSONCategories);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [study.getStudyCategory]: ", ex);  
            result = "JSON error in WS method [study.getStudyCategory]";
        }         
        return result;
    }
  
    //-------------------------------------------------------------------------- Filter Formulas
    public List<FilterField> getFilterFormulas(int filterId) {

        List<FilterField> ent = filterfieldController.findFilterFieldByFilterId(filterId);
        if ( (ent.isEmpty()) ) {
            logger.log(Level.INFO, ""
                    + "FilterField does not exist");
            return null;
        }
        return ent;
    }
        
    public boolean checkFilterHasFormulaExists(int filterId, int formulaId) {

        FilterHasFieldPK entPK = new FilterHasFieldPK(filterId,formulaId);          
        FilterHasField ent = filterhasfieldController.findFilterHasField(entPK);
        if ( (ent == null) ) {
            logger.log(Level.INFO, "Filter Field does not exist");
            return false;
        } else {
            logger.log(Level.INFO, "Filter Field does not exist");            
            return true;
        }
    }

   public String getSqlFromFilterFormulas(int filterId) {

        if ( filterId == 0 ) return null;
        
        List<ModelColumn> columns = new ArrayList();
        List<FilterField> ffList = filterfieldController.findFilterFieldByFilterId(filterId);
        if ( (ffList.isEmpty()) ) {
            logger.log(Level.INFO, ""
                    + "FilterField does not exist");
            return null;
        }
        for (FilterField t : ffList) {
            int i = t.getModelColumnidModelColumn();
            ModelColumn r = modelcolumnController.findModelColumn(i);
            if ( r != null ) {
                columns.add(r);
            }
        }
        //=================== this should stay in synch with JavaScript in FilterFormulaGrid.js
        int orFlag = 0;
        String sqlOR = "";
        String sql = "";
        // build ORs first
        for (int i=0; i<ffList.size(); i++) {
            FilterField r = ffList.get(i);   
            if ( "Include".equals(r.getType()) ) {
                String name = "";
                for (int j=0; j<columns.size(); j++) {
                    if ( columns.get(j).getIdModelColumn() == r.getModelColumnidModelColumn() ) {
                        name = columns.get(j).getName();
                        break;
                    }
                }
                if ( orFlag > 0 ) {
                    sql += " OR ";
                }
                orFlag++;
                sql += "(" + name + " " + r.getFormula() + ")";        
            }
        }
        if ( orFlag > 1 ) {
            sqlOR = "(" + sql + ")";
        } else {
            sqlOR = sql;
        }
        // now built negative ANDs
        int andFlag = 0;
        sql = "";
        for (int i=0; i<ffList.size(); i++) {
            FilterField r = ffList.get(i);        
            if ( "Exclude".equals(r.getType()) ) {
                String name = "";
                for (int j=0; j<columns.size(); j++) {
                    if ( columns.get(j).getIdModelColumn() == r.getModelColumnidModelColumn() ) {
                        name = columns.get(j).getName();
                        break;
                    }
                }
                if ( (orFlag > 0) || (andFlag > 0) ) {
                    sql += " AND ";
                }
                andFlag++;
                sql += "NOT (" + name + " " + r.getFormula() + ")";      
            }
        }                    
        //===================
        return sqlOR+sql;
    }          
    
    private FilterField createFilterFormulas(Filter filter, int id, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} creating relationship between formula {1} and filter {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), filter.getName()});        
 
        FilterField ent = new FilterField(); 
        ent.setFilteridFilter(filter.getIdFilter());
        ent.setModelColumnidModelColumn(id);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);    
        ent = filterfieldController.edit(ent);
        return ent;
    }
     
    public String createFilterFormulas(String JSONString, Integer currentUserId) {
        String result = "Filter formula not created";
        Filter filter = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray anArray = JSONContentRoot.getJSONArray("filters");
            for (int ii=0; ii<anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Formulas for Filter {0}", anArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                filter = gson.fromJson(jString, Filter.class);   
                JSONArray formulaArray = JSONContentRoot.getJSONArray("formulas");                
                for (int i=0; i<formulaArray.length(); i++) {
                    FilterField ent = createFilterFormulas(filter, formulaArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(ent).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Filter Formula: {0} {1}", new Object[]{filter.getName(), ex.getMessage()});
            result = "ST0011 Exception while creating new Formula: " + ex.getMessage();  
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formulas created", result);
    }
 
    private FilterField updateFilterFormula(FilterField ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} updating relationship between field {1} and filter {2}", 
                new Object[]{Integer.toString((currentUserId)), 
                    Integer.toString((ent.getModelColumnidModelColumn())), 
                    Integer.toString(ent.getFilteridFilter())});            
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);    
        filterfieldController.edit(ent);
        return ent;
    }
    
    public String updateFilterFormula(String JSONString, Integer currentUserId) {
        String result = "Filter Formula not updated";
        String dataString = "";
        FilterField entity;
        int idFilter = 0, idModelColumn = 0;
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
            String jString = JSONContentRoot.toString();
            entity = gson.fromJson(jString, FilterField.class);             
            idModelColumn = entity.getModelColumnidModelColumn();
            idFilter = entity.getFilteridFilter();
            logger.log(Level.INFO, "Updating Formula Field {0} for Filter {1}", new Object[]{Integer.toString(idModelColumn),Integer.toString(idFilter)});
            entity = updateFilterFormula(entity, currentUserId);
            dataString = dataString + new JSONObject(entity).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating category releationship for Filter: {0} {1}", new Object[]{Integer.toString(idFilter), ex});
            result = "ST0012 Exception while updating formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formula updated", result);
    }
    
    private FilterField deleteFilterFormulas(int idFilterField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and filter", new Object[]{Integer.toString((currentUserId)), Integer.toString((idFilterField))});             
        FilterField ent = new FilterField(idFilterField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);    
        filterfieldController.destroy(idFilterField);
        return ent;
    }

    public String deleteFilterFormula(String JSONString, Integer currentUserId) {
        String result = "Filter Formula not deleted";
        String dataString = "";
        int idFilterField = 0;
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            idFilterField = JSONContentRoot.getInt("idFilterField");
            logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(idFilterField)});
            FilterField ent = deleteFilterFormulas(idFilterField, currentUserId);
            dataString = dataString + new JSONObject(ent).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for FilterField: {0} {1}", new Object[]{Integer.toString(idFilterField), ex});
            result = "ST0013 Exception while deleting formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formula deleted", result);
    }

    public String deleteFilterFormulas(String JSONString, Integer currentUserId) {
        String result = "Filter Formulas not deleted";
        String dataString = "";
        
        int idFilter = 0;
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i=0; i<objectArray.length(); i++) {                        
                JSONObject JSONContentRoot =  objectArray.getJSONObject(i);
                idFilter = JSONContentRoot.getInt("idFilter");
                List<FilterField> ffList = filterfieldController.findFilterFieldByFilterId(idFilter);
                for (FilterField t : ffList) {
                    logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(t.getIdFilterField())});
                    FilterField ent = deleteFilterFormulas(t.getIdFilterField(), currentUserId);
                    if ( sepCounter++ > 0 ) {
                        dataString = dataString.concat(",");
                    }                     
                    dataString = dataString + new JSONObject(ent).toString();
                }                
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Filter: {0} {1}", new Object[]{Integer.toString(idFilter), ex});
            result = "ST0014 Exception while deleting formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formulas deleted", result);
    }
    
    public String getFilterFormulaInfo(int idFilter) {
            
        String dataString = "";
        String result = "No formulas found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONCategories = new JSONObject();  
        JSONArray JSONArrayCategories = null;

        logger.log(Level.INFO, "Retrieving Formulas for Filter {0}.", Integer.toString(idFilter));         
        try {
            dataString = getFilterFormulaJSON(idFilter);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Filter into JSON", ex.getMessage());
            result = "ST0015 Exception parsing Filter into JSON";
        }   
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", true);       // change from false not to alarm user, possible todo
                status.put("message", result);   
            } else {
                JSONArrayCategories = new JSONArray(dataString);
                success.put("success", true);   
                status.put("message", JSONArrayCategories.length() + " filter formulas retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayCategories.length()));
                JSONCategories.put("formulas", JSONArrayCategories);
                success.put("filterFormulas", JSONCategories);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [study.getFilterFormulaInfo]: ", ex);  
            result = "JSON error in WS method [study.getFilterFormulaInfo]";
        }         
        return result;
    }
      
    public String getFilters(int start, int limit, Integer userId, Integer organizationId) {
   
        // this method gets filter by OrganizationId
        // the one implemnted through adminService.getOrgObjects() gets them by ModelId
        String logId = "DELTA3 WS method [study.getFilters]";
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                      
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{filterController.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all
            subjects = filterController.findOrgFilterEntities(organizationId);
        } else {
            subjects = filterController.findOrgFilterEntities(limit, start, organizationId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {         
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString(); 
            String fields = getFilterFormulaJSON(((Filter)subjects.get(recordCounter)).getIdFilter());      
            if ( "".equals(fields) ) {
                fields = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"fields\":" + fields).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"fields\":" + fields).toString();
            }            
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + filterController.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(filterController.getOrgFilterCount(organizationId)));
                success.put(filterController.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in " + logId + ": ", ex);  
            result = "JSON error in " + logId;
        }        
        return result;        
    }    

    public String getFilterUsage(Integer filterId, Integer userId, Integer organizationId) {

        String logId = "DELTA3 WS method [study.getFilterUsage]";
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());               
        
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Study> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of studies dependent on filter {0}", new Object[]{Integer.toString(filterId)});
        subjects = studyController.findStudyEntitiesByFilterId(filterId);
        int max = subjects.size();
        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", recordCounter + " " + studyController.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(recordCounter));
                success.put(studyController.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in " + logId + ": ", ex);  
            result = "JSON error in " + logId;
        }        
        return result;        
    }    
 
    public String validateFilter(String JSONString, Integer userId) {
        String result = "[]";
        Filter filter = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray filterFormulaArray = JSONContentRoot.getJSONArray("filterFormulas");
            dataString = "Filter(s) validated: ";
            for (int ii=0; ii<filterFormulaArray.length(); ii++) {
                // for now response formatting is only for a single filter 
                JSONObject filterFormula = filterFormulaArray.getJSONObject(ii);  
                String frontSql = filterFormula.getString("condition");
                JSONObject filterObject = filterFormula.getJSONObject("filter");
                String filterName = filterObject.getString("name");   
                int filterId = filterObject.getInt("idFilter");
                String backSql = getSqlFromFilterFormulas(filterId);
                logger.log(Level.INFO, "Verifying Formulas for Filter {0}", filterName);
                if ( !frontSql.equals(backSql) ) {
                    dataString = "SQL generation mismatch for filter " + filterName + 
                            " Id " + Integer.toString(filterId) + ". Contact your System Administrator";
                    result = "";
                    break;
                    }
                filter = filterController.findFilter(filterId);
                int modelId = filter.getModelidModel();
          
                Model model = modelController.findModel(modelId);     
                String validationResult = verifyFilter(model.getOutputName(), backSql, userId);
                if ( !"Ok".equals(validationResult) ) {
                    dataString = "Filter " + filterName + " was not validated: " + validationResult;
                    result = "";
                    break;
                    }
                dataString += filterName + " ";
                }
            logger.log(Level.SEVERE, dataString);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Filter Formula: {0} {1}", new Object[]{JSONString, ex});
            dataString = "ST0016 Exception while verifying Formula: " + ex.getMessage();  
        }   
        return wrapAPI("filterFormulas", result, dataString, dataString);
    }    

    public String findStudiesByModel(Model m) {    
        String returnString = "";
        List<Study> sList;
        
        try {
            sList = studyController.findStudyEntitiesByModelId(m.getIdModel());
            if ( (sList != null) && !sList.isEmpty() ) {
                returnString = "Model is used by Study: ";
                int i = 0;
                for (Study s: sList) {
                   if ( i++ > 0 ) {
                       returnString += ",";
                   }
                   returnString += s.getName();
               }
            }
            return returnString;            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception when searching for Study that uses: " + m.getIdModel().toString(), ex.getMessage());
            returnString =  "ST0017 Exception when searching for study: " + ex.getMessage();            
        }    
        return returnString;
    } 
 
    public String getResultsforStudy(String studyId, Integer userId) {      
        String dataString = "";
        List<Process> processList = processController.findProcessEntitiesByStudyId(Integer.valueOf(studyId));
        if ( processList.size() > 0 ) {
            Process process = processList.get(processList.size()-1); // get the last one (and the only one)
            dataString = "[" + new JSONObject(process).toString() + "]";   
        }             
        return wrapAPI("process", dataString, "Results retrieved", "ST0018 Study not processed by statistic engine");
    }    
    
    private String verifyFilter(String tableName, String condition, Integer currentUserId) {      
        String sqlTestStmt = null;
        try {
            sqlTestStmt = "SELECT * FROM " + tableName + " WHERE " + condition + " AND (1 = 0)";
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
            AuditLogger.log(currentUserId, 0, "verify filter", sqlTestStmt);            
            Query queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlTestStmt,logger);
            List<Object[]> results = queryResult.getResultList();
            return "Ok";            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception in evaluating filter: " + sqlTestStmt, ex.getMessage());
            return ex.getMessage();            
        }    
    } 
    
    private String getStudyCategoryJSON(int idStudy) {
        logger.log(Level.INFO, "Retrieving Categories for Study {0}", Integer.toString(idStudy));       
        String dataString = "";
        int categoryCounter = 0;
        List<ModelColumn> columns = getStudyCategories(idStudy);

        if ( (columns != null) && !columns.isEmpty() ) { 
            categoryCounter = columns.size();
            logger.log(Level.INFO, "Study {0} has categories. Retrieving {1} category data.", new Object[]{Integer.toString(idStudy), Integer.toString(categoryCounter)});        

            for (int i=0; i<categoryCounter; i++) {
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(columns.get(i)).toString();
                }
            dataString = "[" + dataString + "]";
            return dataString;
        }       
        return dataString;
    } 

    private String getFilterFormulaJSON(int idFilter) {
        logger.log(Level.INFO, "Retrieving Formulas for Filter {0}", Integer.toString(idFilter));       
        String dataString = "";
        int counter = 0;
        //List<ModelColumn> columns = getFilterFormulas(idFilter);
        List<FilterField> columns = getFilterFormulas(idFilter);

        if ( (columns != null) && !columns.isEmpty() ) { 
            counter = columns.size();
            logger.log(Level.INFO, "Filter {0} has formulas. Retrieving {1} formula data.", new Object[]{Integer.toString(idFilter), Integer.toString(counter)});        

            for (int i=0; i<counter; i++) {
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(columns.get(i)).toString();
                }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    } 

  
    private int getMethodStatusOffset(String methodName) {
        int i=0;
        for ( ; i< methodStates[0].length; i++) {
            if ( methodName.equals(methodStates[0][i])) {
                return i+1;
            }
        }
        return -1;    
    }
 
    private String getNextMethodStatus(int methodOffset, String statusName) {
        int i=0;
        for ( ; i< methodStates[methodOffset].length; i++) {
            if ( statusName.equals(methodStates[methodOffset][i])) {
                if ( i < methodStates[methodOffset].length - 1 ) {
                    return methodStates[methodOffset][++i];
                } else {
                    break; // this was is last allowed state
                }
            }
        }
        return methodStates[methodOffset][0];    
    }
    
    private String getPreviousMethodStatus(int methodOffset, String statusName) {
        int i=0;
        if ( statusName.equals(methodStates[methodOffset][0]) ) {
            return methodStates[methodOffset][1]; // special case: error -> first State
        }
        for ( ; i< methodStates[methodOffset].length; i++) {
            if ( statusName.equals(methodStates[methodOffset][i])) {
                if ( i > 2 ) {
                    return methodStates[methodOffset][--i];
                } else {
                    break; // this is the first allowed state
                }
            }
        }
        return methodStates[methodOffset][1];    
    }    
        
    /* delegete this work to common method in admin service */  
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
      return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure); 
    }    
  
    //-------------------------------------------------------------------------- following is custom handling of gson dates
    private static final String[] DATE_FORMATS = new String[] {
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss.S"
    };

    private class DateDeSerializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement jsonElement, Type typeOF, JsonDeserializationContext context) throws JsonParseException {
            for (String format : DATE_FORMATS) {
                try {
                    return new SimpleDateFormat(format, Locale.US).parse(jsonElement.getAsString());
                } catch (ParseException e) {        
                    int i = 0;
                }
            }
            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                    + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));                
        }
    }  
}
