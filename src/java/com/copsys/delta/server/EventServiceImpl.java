/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.server;

import com.copsys.delta.entity.events.Alert;
import com.copsys.delta.entity.events.Event;
import com.copsys.delta.entity.events.Eventtemplate;
import com.copsys.delta.entity.events.Notification;
import com.copsys.delta.jpa.event.AlertJpaController;
import com.copsys.delta.jpa.event.EventJpaController;
import com.copsys.delta.jpa.event.EventtemplateJpaController;
import com.copsys.delta.jpa.event.NotificationJpaController;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Coping Systems Inc.
 */
public class EventServiceImpl {

    private static final Logger logger = Logger.getLogger(EventServiceImpl.class.getName());
    private static EventJpaController eventController;
    private static EventtemplateJpaController eventtemplateController;
    private static AlertJpaController alertController;
    private static NotificationJpaController notificationController;
    private static AdminServiceImpl adminService;

    public Integer logEvent(Integer currentOrgId, Integer currentUserId, Integer idEventtemplate, String data) {
        Integer returnInt = 0;
        if (eventtemplateController == null) {
            eventtemplateController = new EventtemplateJpaController();
        }
        if (eventController == null) {
            eventController = new EventJpaController();
        }             
        Eventtemplate et = eventtemplateController.findEventtemplate(idEventtemplate);
        if ( et.getActive() == true ) {
            if ( "FAST".equals(et.getType()) && (et.getActive() == true) ) {
                // check immediately if this user has Alert set up
                // then log event and return userId, otherwise return 0 after logging event
                if (alertController == null) {
                    alertController = new AlertJpaController();
                } 
                List<Object> alerts = alertController.findUserAlertEntities(currentUserId);
                for (Object al : alerts) {
                    if ( (((Alert)al).getActive() == true) 
                            && (Objects.equals(((Alert)al).getIdEventTemplate(), et.getIdEventTemplate())) ) {
                        returnInt = currentUserId;
                    }
                }
            }
            Event event = new Event();
            event.setIdEvent(0);
            event.setIdEventTemplate(idEventtemplate);
            event.setDescription(data);
            try {
                eventController.createObjects(event, currentOrgId, currentUserId);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "EA0001 Exception while storing event: ", ex);
            }
        }
        return returnInt;
    }

    public void processEvents() {
        if (eventController == null) {
            eventController = new EventJpaController();
        }
        if (eventtemplateController == null) {
            eventtemplateController = new EventtemplateJpaController();
        }
        if (alertController == null) {
            alertController = new AlertJpaController();
        }
        if (notificationController == null) {
            notificationController = new NotificationJpaController();
        }
        List<Object> eventtemplates = eventtemplateController.findObjectEntities();
        List<Object> alerts = alertController.findObjectEntities();
        List<Object> events = eventController.findNewEventEntities(Boolean.TRUE, 0, 0); // get all of them
        for (Object e : events) {
            Event updatedEvent = (Event) e;
            if (checkEventActive((Event) e, eventtemplates) == true) {
                for (Object al : alerts) {
                    if (Objects.equals(((Alert) al).getIdEventTemplate(), ((Event) e).getIdEventTemplate())) {
                        if (((Alert) al).getActive() == true) {
                            Notification n = new Notification();
                            n.setAckRequired(((Alert) al).getAckRequired());
                            n.setIdUser(((Alert) al).getIdUser());
                            n.setTimeOut(((Alert) al).getTimeOut());
                            n.setName(((Alert) al).getName());
                            n.setDescription(((Alert) al).getDescription());
                            n.setAcknowledged(false);
                            n.setRetryCount(0);
                            n.setEventData(((Event) e).getDescription());
                            n.setIdEvent(((Event) e).getIdEvent());
                            n.setActive(Boolean.TRUE);
                            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
                            n.setCreatedTS(currentTimestamp);
                            n.setCreatedBy(0); // system user
                            try {
                                notificationController.create(n); // bypass audit log
                            } catch (Exception ex) {
                                Logger.getLogger(EventServiceImpl.class.getName()).log(
                                        Level.SEVERE, "Exception while creating notification for event "
                                        + Integer.toString(updatedEvent.getIdEvent()), ex);
                            }
                        }
                    }
                }
            }
            try {
                eventController.updateObjects(updatedEvent, 0);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while updating event " + Integer.toString(updatedEvent.getIdEvent()), ex);
            }
        }
        logger.log(Level.SEVERE, "Processed {0} events.", Integer.toString(events.size()));
    }

    private boolean checkEventActive(Event e, List<Object> eventtemplates) {
        for (Object et : eventtemplates) {
            if (Objects.equals(((Eventtemplate) et).getIdEventTemplate(), e.getIdEventTemplate())) {
                if (((Eventtemplate) et).getActive() == true) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkAlertActive(Event e, List<Object> alerts) {
        for (Object al : alerts) {
            if (Objects.equals(((Alert) al).getIdEventTemplate(), e.getIdEventTemplate())) {
                if (((Alert) al).getActive() == true) {
                    return true;
                }
            }
        }
        return false;
    }

    /* delegete this work to common method in admin service */
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }
}
