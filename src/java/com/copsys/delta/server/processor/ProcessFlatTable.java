/*
 * This class is used to start asynch processing of Atomic Fields
 */

package com.copsys.delta.server.processor;

import com.copsys.delta.server.ModelServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;

/**
 *
 * @author Boston Advanced Analytics
 */
public class ProcessFlatTable implements Runnable {
    private AsyncContext asyncContext;
    private String data;
    private Integer userId;
    private ModelServiceImpl modelService = null;
    private static final Logger logger = Logger.getLogger(ProcessFlatTable.class.getName());        

    public ProcessFlatTable(AsyncContext asyncContext, String load, Integer id) {
        this.asyncContext = asyncContext;
        data = load;
        userId = id;
        modelService = new ModelServiceImpl();
        logger.log(Level.SEVERE,"ProcessFlatTable initialized. Data to be processed: " + data);
    }

    public void run() {
        if ( asyncContext != null ) {
            asyncContext.complete();    
        }
        modelService.processFlatTableAll(data, userId, true);   
        modelService = null; userId = null; data = null; asyncContext = null;
        logger.log(Level.SEVERE,"ProcessFlatTable finished.");
    }     
}
