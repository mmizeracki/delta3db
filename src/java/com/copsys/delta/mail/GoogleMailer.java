/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.mail;

/**
 *
 * @author Coping Systems Inc.
 */

import java.security.Security;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.logging.Logger;
import java.util.logging.Level;

public class GoogleMailer {
    private static final Logger logger = Logger.getLogger(GoogleMailer.class.getName());
    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_PORT = "465";
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    private static final String[] sendTo = {""};


    public boolean sendMail(String toAddress,String subject,String body)
    {
        try
        {
            Properties props = new Properties() ;
            final String gmailUser = "yourname@gmail.com" ;
            boolean debug = true;
            final String passwd = "xxxxxx" ;
         
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            props.put("mail.smtp.host", SMTP_HOST_NAME);
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "true");
            props.put("mail.smtp.port", SMTP_PORT);
            props.put("mail.smtp.socketFactory.port", SMTP_PORT);
            props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.put("mail.smtp.socketFactory.fallback", "false");
            

            Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(gmailUser, passwd);
                    }
                });            

            try {
                session.setDebug(debug);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(gmailUser));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(toAddress));
                message.setSubject(subject);
                message.setText(body); 
                logger.log(Level.INFO, "About to send message using Gmail to {0}", toAddress);
                Transport.send(message);                
            } catch (MessagingException e) {
                    throw new RuntimeException(e);
            }

        }
        catch (RuntimeException e)
        {
            logger.log(Level.SEVERE, "An error occured sending email through Gmail: {0}", e.toString());
            return false;
        }
        return true;
    }
}
