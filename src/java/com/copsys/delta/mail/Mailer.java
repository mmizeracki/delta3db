/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.mail;

import com.copsys.delta.property.Property;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Session;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Transport;

/**
 *
 * @author Coping Systems Inc.
 */
public class Mailer {
 
private Logger logger = Logger.getLogger(Mailer.class.getName());
String smtpServer = "";
String smtpPort = "";
String authAddress = "";
String authPassword = "";
String siteName = "";
String send = "";   
static Session sessionMail = null;

public Mailer() {
    if ( sessionMail == null ) {
        Properties props = new Properties();
        smtpServer = new Property("smtpServer").getParamValue();
        smtpPort = new Property("smtpPort").getParamValue();
        authAddress = new Property("authAddress").getParamValue();
        authPassword = new Property("authPassword").getParamValue();
        send = new Property("send").getParamValue(); // not used
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");

        // create some properties and get the default Session
        sessionMail = Session.getInstance(props, new Authenticator() {
             @Override
             public PasswordAuthentication getPasswordAuthentication() {
                     return new PasswordAuthentication(authAddress, authPassword);
             }
            });
        sessionMail.setDebug(true);
    }
}

public boolean sendMail(String emailAddressTo, String subject, String message)
{                              
    try{
        // create a message
        Message msg = new MimeMessage(sessionMail);

        // set the from and to address
        InternetAddress addressFrom = new InternetAddress(authAddress);
        msg.setFrom(addressFrom);

        InternetAddress[] addressTo = new InternetAddress[1];
        addressTo[0] = new InternetAddress(emailAddressTo);
        msg.setRecipients(Message.RecipientType.TO, addressTo);


        // Optional : You can also set your custom headers in the Email if you Want
        msg.addHeader("site", new Property("siteName").getParamValue());

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message, "text/plain");
        Transport.send(msg);
    } catch(MessagingException e) {
        logger.log(Level.SEVERE, "Exception while sending email {0}", e.getMessage());
        return false;
    }
    return true;
}
}
