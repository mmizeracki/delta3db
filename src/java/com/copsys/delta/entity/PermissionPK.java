/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Coping Systems Inc.
 */
@Embeddable
public class PermissionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "PermTemp_idPermTemp")
    private int permTempidPermTemp;
    @Basic(optional = false)
    @Column(name = "Role_idRole")
    private int roleidRole;

    public PermissionPK() {
    }

    public PermissionPK(int permTempidPermTemp, int roleidRole) {
        this.permTempidPermTemp = permTempidPermTemp;
        this.roleidRole = roleidRole;
    }

    public int getPermTempidPermTemp() {
        return permTempidPermTemp;
    }

    public void setPermTempidPermTemp(int permTempidPermTemp) {
        this.permTempidPermTemp = permTempidPermTemp;
    }

    public int getRoleidRole() {
        return roleidRole;
    }

    public void setRoleidRole(int roleidRole) {
        this.roleidRole = roleidRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) permTempidPermTemp;
        hash += (int) roleidRole;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionPK)) {
            return false;
        }
        PermissionPK other = (PermissionPK) object;
        if (this.permTempidPermTemp != other.permTempidPermTemp) {
            return false;
        }
        if (this.roleidRole != other.roleidRole) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.PermissionPK[ permTempidPermTemp=" + permTempidPermTemp + ", roleidRole=" + roleidRole + " ]";
    }
    
}
