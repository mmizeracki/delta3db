/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Coping Systems Inc.
 */
@Embeddable
public class UserHasRolePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "User_idUser")
    private int useridUser;
    @Basic(optional = false)
    @Column(name = "Role_idRole")
    private int roleidRole;

    public UserHasRolePK() {
    }

    public UserHasRolePK(int useridUser, int roleidRole) {
        this.useridUser = useridUser;
        this.roleidRole = roleidRole;
    }

    public int getUseridUser() {
        return useridUser;
    }

    public void setUseridUser(int useridUser) {
        this.useridUser = useridUser;
    }

    public int getRoleidRole() {
        return roleidRole;
    }

    public void setRoleidRole(int roleidRole) {
        this.roleidRole = roleidRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) useridUser;
        hash += (int) roleidRole;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasRolePK)) {
            return false;
        }
        UserHasRolePK other = (UserHasRolePK) object;
        if (this.useridUser != other.useridUser) {
            return false;
        }
        if (this.roleidRole != other.roleidRole) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.UserHasRolePK[ useridUser=" + useridUser + ", roleidRole=" + roleidRole + " ]";
    }
    
}
