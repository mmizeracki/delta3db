package com.copsys.delta.entity;

import java.util.Comparator;

/**
 *
 * @author Boston Advanced Analytics
 */
public class PermissiontemplateComp implements Comparator<Permissiontemplate>{
    
    @Override
    public int compare(Permissiontemplate e1, Permissiontemplate e2) { 
        if(e1.getSequence() > e2.getSequence()){
            return 1;
        } else {
            return -1;
        }
    }
}
