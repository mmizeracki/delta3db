/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.entity.events;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Boston Advanced Analytics
 */
@Entity
@Table(name = "d3_alert")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Alert.findAll", query = "SELECT a FROM Alert a"),
    @NamedQuery(name = "Alert.findByIdAlert", query = "SELECT a FROM Alert a WHERE a.idAlert = :idAlert"),
    @NamedQuery(name = "Alert.findByName", query = "SELECT a FROM Alert a WHERE a.name = :name"),
    @NamedQuery(name = "Alert.findByDescription", query = "SELECT a FROM Alert a WHERE a.description = :description"),
    @NamedQuery(name = "Alert.findByType", query = "SELECT a FROM Alert a WHERE a.type = :type"),
    @NamedQuery(name = "Alert.findByRetryMax", query = "SELECT a FROM Alert a WHERE a.retryMax = :retryMax"),
    @NamedQuery(name = "Alert.findByTimeOut", query = "SELECT a FROM Alert a WHERE a.timeOut = :timeOut"),
    @NamedQuery(name = "Alert.findByAckRequired", query = "SELECT a FROM Alert a WHERE a.ackRequired = :ackRequired"),
    @NamedQuery(name = "Alert.findByCreatedBy", query = "SELECT a FROM Alert a WHERE a.createdBy = :createdBy"),
    @NamedQuery(name = "Alert.findByCreatedTS", query = "SELECT a FROM Alert a WHERE a.createdTS = :createdTS"),
    @NamedQuery(name = "Alert.findByUpdatedBy", query = "SELECT a FROM Alert a WHERE a.updatedBy = :updatedBy"),
    @NamedQuery(name = "Alert.findByUpdatedTS", query = "SELECT a FROM Alert a WHERE a.updatedTS = :updatedTS")})*/
public class Alert implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="ALERT_ID")
    @TableGenerator(name="ALERT_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="ALERT_ID", allocationSize=1)    
    @Column(name = "idAlert")
    private Integer idAlert;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;
    @Column(name = "retryMax")
    private Integer retryMax;
    @Column(name = "timeOut")
    private Integer timeOut;
    @Column(name = "ackRequired")
    private Boolean ackRequired;
    @Column(name = "sendEmail")
    private Boolean sendEmail;   
    @Column(name = "active")   
    private Boolean active;    
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "User_idUser")
    private Integer idUser;    
    @Column(name = "EventTemplate_idEventTemplate")
    private Integer idEventTemplate;    
    
    public Alert() {
    }

    public Alert(Integer idAlert) {
        this.idAlert = idAlert;
    }

    public Integer getIdAlert() {
        return idAlert;
    }

    public void setIdAlert(Integer idAlert) {
        this.idAlert = idAlert;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRetryMax() {
        return retryMax;
    }

    public void setRetryMax(Integer retryMax) {
        this.retryMax = retryMax;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public Boolean getAckRequired() {
        return ackRequired;
    }

    public void setAckRequired(Boolean ackRequired) {
        this.ackRequired = ackRequired;
    }

    public Boolean getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(Boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }    
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getIdUser() {
        return idUser;
    }
 
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdEventTemplate() {
        return idEventTemplate;
    }
 
    public void setIdEventTemplate(Integer idEventTemplate) {
        this.idEventTemplate = idEventTemplate;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlert != null ? idAlert.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alert)) {
            return false;
        }
        Alert other = (Alert) object;
        if ((this.idAlert == null && other.idAlert != null) || (this.idAlert != null && !this.idAlert.equals(other.idAlert))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.events.Alert[ idAlert=" + idAlert + " ]";
    }
    
}
