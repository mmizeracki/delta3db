/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.entity.events;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Boston Advanced Analytics
 */
@Entity
@Table(name = "d3_notification")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Notification.findAll", query = "SELECT n FROM Notification n"),
    @NamedQuery(name = "Notification.findByIdNotification", query = "SELECT n FROM Notification n WHERE n.idNotification = :idNotification"),
    @NamedQuery(name = "Notification.findByName", query = "SELECT n FROM Notification n WHERE n.name = :name"),
    @NamedQuery(name = "Notification.findByDescription", query = "SELECT n FROM Notification n WHERE n.description = :description"),
    @NamedQuery(name = "Notification.findByEventData", query = "SELECT n FROM Notification n WHERE n.eventData = :eventData"),
    @NamedQuery(name = "Notification.findByCreatedBy", query = "SELECT n FROM Notification n WHERE n.createdBy = :createdBy"),
    @NamedQuery(name = "Notification.findByCreatedTS", query = "SELECT n FROM Notification n WHERE n.createdTS = :createdTS"),
    @NamedQuery(name = "Notification.findByUpdatedBy", query = "SELECT n FROM Notification n WHERE n.updatedBy = :updatedBy"),
    @NamedQuery(name = "Notification.findByUpdatedTS", query = "SELECT n FROM Notification n WHERE n.updatedTS = :updatedTS"),
    @NamedQuery(name = "Notification.findByAcknowledged", query = "SELECT n FROM Notification n WHERE n.acknowledged = :acknowledged"),
    @NamedQuery(name = "Notification.findByRetryCount", query = "SELECT n FROM Notification n WHERE n.retryCount = :retryCount"),
    @NamedQuery(name = "Notification.findByTimeOut", query = "SELECT n FROM Notification n WHERE n.timeOut = :timeOut"),
    @NamedQuery(name = "Notification.findByAckRequired", query = "SELECT n FROM Notification n WHERE n.ackRequired = :ackRequired"),
    @NamedQuery(name = "Notification.findByActive", query = "SELECT n FROM Notification n WHERE n.active = :active")})*/
public class Notification implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="NOTIFICATION_ID")
    @TableGenerator(name="NOTIFICATION_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="NOTIFICATION_ID", allocationSize=1)    
    @Column(name = "idNotification")
    private Integer idNotification;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "eventData")
    private String eventData;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "acknowledged")
    private Boolean acknowledged;
    @Column(name = "retryCount")
    private Integer retryCount;
    @Column(name = "timeOut")
    private Integer timeOut;
    @Column(name = "ackRequired")
    private Boolean ackRequired;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "User_idUser")
    private Integer idUser;      
    @Column(name = "Event_idEvent")
    private Integer idEvent;  
    @Column(name = "NotificationTemplate_idNotificationTemplate")
    private Integer idNotificationTemplate;  
    
    public Notification() {
    }

    public Notification(Integer idNotification) {
        this.idNotification = idNotification;
    }

    public Integer getIdNotification() {
        return idNotification;
    }

    public void setIdNotification(Integer idNotification) {
        this.idNotification = idNotification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Boolean getAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(Boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public Boolean getAckRequired() {
        return ackRequired;
    }

    public void setAckRequired(Boolean ackRequired) {
        this.ackRequired = ackRequired;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getIdUser() {
        return idUser;
    }
 
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdEvent() {
        return idEvent;
    }
 
    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public Integer getIdNotificationTemplate() {
        return idNotificationTemplate;
    }
 
    public void setIdNotificationTemplate(Integer idNotificationTemplate) {
        this.idNotificationTemplate = idNotificationTemplate;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotification != null ? idNotification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notification)) {
            return false;
        }
        Notification other = (Notification) object;
        if ((this.idNotification == null && other.idNotification != null) || (this.idNotification != null && !this.idNotification.equals(other.idNotification))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.events.Notification[ idNotification=" + idNotification + " ]";
    }
    
}
