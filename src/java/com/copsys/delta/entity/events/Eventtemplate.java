/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.entity.events;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Boston Advanced Analytics
 */
@Entity
@Table(name = "d3_eventtemplate")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Eventtemplate.findAll", query = "SELECT e FROM Eventtemplate e"),
    @NamedQuery(name = "Eventtemplate.findByIdEventTemplate", query = "SELECT e FROM Eventtemplate e WHERE e.idEventTemplate = :idEventTemplate"),
    @NamedQuery(name = "Eventtemplate.findByName", query = "SELECT e FROM Eventtemplate e WHERE e.name = :name"),
    @NamedQuery(name = "Eventtemplate.findByDescription", query = "SELECT e FROM Eventtemplate e WHERE e.description = :description"),
    @NamedQuery(name = "Eventtemplate.findByType", query = "SELECT e FROM Eventtemplate e WHERE e.type = :type"),
    @NamedQuery(name = "Eventtemplate.findByActive", query = "SELECT e FROM Eventtemplate e WHERE e.active = :active"),
    @NamedQuery(name = "Eventtemplate.findByCreatedBy", query = "SELECT e FROM Eventtemplate e WHERE e.createdBy = :createdBy"),
    @NamedQuery(name = "Eventtemplate.findByCreatedTS", query = "SELECT e FROM Eventtemplate e WHERE e.createdTS = :createdTS"),
    @NamedQuery(name = "Eventtemplate.findByUpdatedBy", query = "SELECT e FROM Eventtemplate e WHERE e.updatedBy = :updatedBy"),
    @NamedQuery(name = "Eventtemplate.findByUpdatedTS", query = "SELECT e FROM Eventtemplate e WHERE e.updatedTS = :updatedTS")})*/
public class Eventtemplate implements Serializable {
    public static int SUBMIT_EVENT = 1;
    public static int RESULT_EVENT = 2;
    public static int CONFIG_METHOD_EVENT = 3;
    public static int CONFIG_DB_EVENT = 4;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="EVENTTEMPLATE_ID")
    @TableGenerator(name="EVENTTEMPLATE_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="EVENTTEMPLATE_ID", allocationSize=1)    
    @Column(name = "idEventTemplate")    
    private Integer idEventTemplate;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "Module_idModule")
    private Integer idModule;    

    public Eventtemplate() {
    }

    public Eventtemplate(Integer idEventTemplate) {
        this.idEventTemplate = idEventTemplate;
    }

    public Integer getIdEventTemplate() {
        return idEventTemplate;
    }

    public void setIdEventTemplate(Integer idEventTemplate) {
        this.idEventTemplate = idEventTemplate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }
    
    public Integer getIdModule() {
        return idModule;
    }

    public void setIdModule(Integer idModule) {
        this.idModule = idModule;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEventTemplate != null ? idEventTemplate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventtemplate)) {
            return false;
        }
        Eventtemplate other = (Eventtemplate) object;
        if ((this.idEventTemplate == null && other.idEventTemplate != null) || (this.idEventTemplate != null && !this.idEventTemplate.equals(other.idEventTemplate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.events.Eventtemplate[ idEventTemplate=" + idEventTemplate + " ]";
    }
    
}
