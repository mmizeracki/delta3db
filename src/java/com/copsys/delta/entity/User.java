/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_user")
@XmlRootElement
@NamedQueries({
    /*@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByIdUser", query = "SELECT u FROM User u WHERE u.idUser = :idUser"),
    @NamedQuery(name = "User.findByGuid", query = "SELECT u FROM User u WHERE u.guid = :guid"),
    @NamedQuery(name = "User.findByCreatedBy", query = "SELECT u FROM User u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "User.findByCreatedTS", query = "SELECT u FROM User u WHERE u.createdTS = :createdTS"),
    @NamedQuery(name = "User.findByUpdatedBy", query = "SELECT u FROM User u WHERE u.updatedBy = :updatedBy"),
    @NamedQuery(name = "User.findByUpdatedTS", query = "SELECT u FROM User u WHERE u.updatedTS = :updatedTS"),*/
    @NamedQuery(name = "User.findByAlias", query = "SELECT u FROM User u WHERE u.alias = :alias"),
    /*@NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByPassExpirationTS", query = "SELECT u FROM User u WHERE u.passExpirationTS = :passExpirationTS"),
    @NamedQuery(name = "User.findByPassChangedTS", query = "SELECT u FROM User u WHERE u.passChangedTS = :passChangedTS"),
    @NamedQuery(name = "User.findByPassFailureCount", query = "SELECT u FROM User u WHERE u.passFailureCount = :passFailureCount"),
    @NamedQuery(name = "User.findByPassFailureMax", query = "SELECT u FROM User u WHERE u.passFailureMax = :passFailureMax"),
    @NamedQuery(name = "User.findByActive", query = "SELECT u FROM User u WHERE u.active = :active"),
    @NamedQuery(name = "User.findByStatus", query = "SELECT u FROM User u WHERE u.status = :status"),
    @NamedQuery(name = "User.findByType", query = "SELECT u FROM User u WHERE u.type = :type"),
    @NamedQuery(name = "User.findByLocale", query = "SELECT u FROM User u WHERE u.locale = :locale"),    
    @NamedQuery(name = "User.findByEmailAddress1", query = "SELECT u FROM User u WHERE u.emailAddress1 = :emailAddress1"),
    @NamedQuery(name = "User.findByEmailAddress2", query = "SELECT u FROM User u WHERE u.emailAddress2 = :emailAddress2"),
    @NamedQuery(name = "User.findByUri1", query = "SELECT u FROM User u WHERE u.uri1 = :uri1"),
    @NamedQuery(name = "User.findByUri2", query = "SELECT u FROM User u WHERE u.uri2 = :uri2"),
    @NamedQuery(name = "User.findByPhoneNumber1", query = "SELECT u FROM User u WHERE u.phoneNumber1 = :phoneNumber1"),
    @NamedQuery(name = "User.findByPhoneNumber2", query = "SELECT u FROM User u WHERE u.phoneNumber2 = :phoneNumber2"),
    @NamedQuery(name = "User.findByOrganizationId", query = "SELECT u FROM User u WHERE u.organization = :organizationId"),    
    @NamedQuery(name = "User.findByAlertPreference1", query = "SELECT u FROM User u WHERE u.alertPreference1 = :alertPreference1"),
    @NamedQuery(name = "User.findByAlertPreference2", query = "SELECT u FROM User u WHERE u.alertPreference2 = :alertPreference2")*/})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="USER_ID")
    @TableGenerator(name="USER_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="USER_ID", allocationSize=1)
    @Column(name = "idUser")
    private Integer idUser;
    @Column(name = "GUID")
    private String guid;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "alias")
    private String alias;
    @Column(name = "password")
    private String password;
    @Column(name = "passExpirationTS")
    private Timestamp passExpirationTS;
    @Column(name = "passChangedTS")
    private Timestamp passChangedTS;    
    @Column(name = "passFailureCount")
    private Integer passFailureCount;
    @Column(name = "passFailureMax")
    private Integer passFailureMax;
    @Column(name = "securityHandler")
    private String securityHandler;    
    @Column(name = "active")
    private Boolean active;
    @Column(name = "status")
    private String status;
    @Column(name = "type")
    private String type;
    @Column(name = "locale")
    private String locale;    
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;    
    @Column(name = "emailAddress1")
    private String emailAddress1;
    @Column(name = "emailAddress2")
    private String emailAddress2;
    @Column(name = "uri1")
    private String uri1;
    @Column(name = "uri2")
    private String uri2;
    @Column(name = "phoneNumber1")
    private String phoneNumber1;
    @Column(name = "phoneNumber2")
    private String phoneNumber2;
    @Column(name = "alertPreference1")
    private String alertPreference1;
    @Column(name = "alertPreference2")
    private String alertPreference2;
    
    //@JoinColumn(name = "Person_idPerson", referencedColumnName = "idPerson", insertable = true, updatable = true)
    //@ManyToOne(optional = true)
    //private Person person;
    @Column(name = "Person_idPerson")
    private Integer person;    
    
    //@JoinColumn(name = "Organization_idOrganization", referencedColumnName = "idOrganization", insertable = true, updatable = true)
    //@ManyToOne(optional = true)
    //private Organization organization;
    @Column(name = "Organization_idOrganization")
    private Integer organization;
    
    //@JoinColumn(name = "Group_idGroup", referencedColumnName = "idGroup", insertable = true, updatable = true)
    //@ManyToOne(optional = true)
    //private GroupTable groupTable;
    @Column(name = "Group_idGroup")
    private Integer groupTable;    

    public User() {
    }

    public User(Integer userID) {
        this.idUser = userID;
    }

    public User(Integer userID, String email1, String password, Timestamp updatedTS, Integer updatedBy) {
        this.idUser = userID;
        this.emailAddress1 = email1;
        this.password = password;
        this.createdTS = (java.sql.Timestamp)updatedTS;
        this.createdBy = updatedBy;
        this.updatedTS = (java.sql.Timestamp)updatedTS;
        this.updatedBy = updatedBy;        
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int id) {
        this.idUser = id;
    }
    
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp)updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getPassExpirationTS() {
        return (java.sql.Timestamp)passExpirationTS;
    }

    public void setPassExpirationTS(java.sql.Timestamp passExpirationTS) {
        this.passExpirationTS = passExpirationTS;
    }

    
    public Timestamp getPassChangedTS() {
        return (java.sql.Timestamp)passChangedTS;
    }

    public void setPassChangedTS(java.sql.Timestamp passChangedTS) {
        this.passChangedTS = passChangedTS;
    }

    public Integer getPassFailureCount() {
        return passFailureCount;
    }

    public void setPassFailureCount(Integer passFailureCount) {
        this.passFailureCount = passFailureCount;
    }

    public Integer getPassFailureMax() {
        return passFailureMax;
    }

    public void setPassFailureMax(Integer passFailureMax) {
        this.passFailureMax = passFailureMax;
    }

    public String getSecurityHandler() {
        return securityHandler;
    }

    public void setSecurityHandler(String securityHandler) {
        this.securityHandler = securityHandler;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }    
    
    public String getEmailAddress1() {
        return emailAddress1;
    }

    public void setEmailAddress1(String emailAddress1) {
        this.emailAddress1 = emailAddress1;
    }

    public String getEmailAddress2() {
        return emailAddress2;
    }

    public void setEmailAddress2(String emailAddress2) {
        this.emailAddress2 = emailAddress2;
    }

    public String getUri1() {
        return uri1;
    }

    public void setUri1(String uri1) {
        this.uri1 = uri1;
    }

    public String getUri2() {
        return uri2;
    }

    public void setUri2(String uri2) {
        this.uri2 = uri2;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getAlertPreference1() {
        return alertPreference1;
    }

    public void setAlertPreference1(String alertPreference1) {
        this.alertPreference1 = alertPreference1;
    }

    public String getAlertPreference2() {
        return alertPreference2;
    }

    public void setAlertPreference2(String alertPreference2) {
        this.alertPreference2 = alertPreference2;
    }

    public Integer getPerson() {
        return person;
    }

    public void setPerson(Integer person) {
        this.person = person;
    }

    public Integer getOrganization() {
        return organization;
    }

    public void setOrganization(Integer organization) {
        this.organization = organization;
    }

    public Integer getGroupTable() {
        return groupTable;
    }

    public void setGroupTable(Integer groupTable) {
        this.groupTable = groupTable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if (this.idUser != other.idUser) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.User[ userPK=" + Integer.toString(idUser) + " ]";
    }
    
}
