/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.logregr;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_logregr_field")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "LogregrField.findAll", query = "SELECT l FROM LogregrField l"),
    @NamedQuery(name = "LogregrField.findByIdLogregrField", query = "SELECT l FROM LogregrField l WHERE l.idLogregrField = :idLogregrField"),
    @NamedQuery(name = "LogregrField.findByName", query = "SELECT l FROM LogregrField l WHERE l.name = :name"),
    @NamedQuery(name = "LogregrField.findByDescription", query = "SELECT l FROM LogregrField l WHERE l.description = :description"),
    @NamedQuery(name = "LogregrField.findByValue", query = "SELECT l FROM LogregrField l WHERE l.value = :value"),
    @NamedQuery(name = "LogregrField.findByModelColumnidRisk", query = "SELECT l FROM LogregrField l WHERE l.modelColumnidRisk = :modelColumnidRisk"),
    @NamedQuery(name = "LogregrField.findByIdLogregr", query = "SELECT l FROM LogregrField l WHERE l.idLogregr = :idLogregr"),
    @NamedQuery(name = "LogregrField.findByActive", query = "SELECT l FROM LogregrField l WHERE l.active = :active"),
    @NamedQuery(name = "LogregrField.findByCreatedBy", query = "SELECT l FROM LogregrField l WHERE l.createdBy = :createdBy"),
    @NamedQuery(name = "LogregrField.findByCreatedTS", query = "SELECT l FROM LogregrField l WHERE l.createdTS = :createdTS"),
    @NamedQuery(name = "LogregrField.findByUpdatedBy", query = "SELECT l FROM LogregrField l WHERE l.updatedBy = :updatedBy"),
    @NamedQuery(name = "LogregrField.findByUpdatedTS", query = "SELECT l FROM LogregrField l WHERE l.updatedTS = :updatedTS")})*/
public class LogregrField implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="LOGREGRFIELD_ID")
    @TableGenerator(name="LOGREGRFIELD_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="LOGREGRFIELD_ID", allocationSize=1)
    @Column(name = "idLogregrField")
    private Integer idLogregrField;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @Column(name = "ModelColumn_idRisk")
    private int modelColumnidRisk;
    @Column(name = "idLogregr")
    private Integer idLogregr;  
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public LogregrField() {
    }

    public LogregrField(Integer idLogregrField) {
        this.idLogregrField = idLogregrField;
    }

    public LogregrField(Integer idLogregrField, int modelColumnidRisk) {
        this.idLogregrField = idLogregrField;
        this.modelColumnidRisk = modelColumnidRisk;
    }

    public Integer getIdLogregrField() {
        return idLogregrField;
    }

    public void setIdLogregrField(Integer idLogregrField) {
        this.idLogregrField = idLogregrField;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getModelColumnidRisk() {
        return modelColumnidRisk;
    }

    public void setModelColumnidRisk(int modelColumnidRisk) {
        this.modelColumnidRisk = modelColumnidRisk;
    } 

    public Integer getIdLogregr() {
        return idLogregr;
    }

    public void setIdLogregr(Integer idLogregr) {
        this.idLogregr = idLogregr;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogregrField != null ? idLogregrField.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogregrField)) {
            return false;
        }
        LogregrField other = (LogregrField) object;
        if ((this.idLogregrField == null && other.idLogregrField != null) || (this.idLogregrField != null && !this.idLogregrField.equals(other.idLogregrField))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.logregr.LogregrField[ idLogregrField=" + idLogregrField + " ]";
    }
}
