package com.copsys.delta.entity.logregr;

import java.util.Comparator;

/**
 *
 * @author Boston Advanced Analytics
 */
public class LogregrDateFieldComp implements Comparator<LogregrDate>{
    
    @Override
    public int compare(LogregrDate e1, LogregrDate e2) { 
        if(e1.getIdLogregrField() < e2.getIdLogregrField()){
            return 1;
        } else {
            return -1;
        }
    }
}
