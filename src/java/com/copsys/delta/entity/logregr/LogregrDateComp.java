package com.copsys.delta.entity.logregr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

/**
 *
 * @author Boston2A
 */
public class LogregrDateComp implements Comparator<LogregrDate>{
    
    @Override
    public int compare(LogregrDate e1, LogregrDate e2) { 
        java.util.Date date1 = null;
        java.util.Date date2 = null;        
        try {
          SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
          String d1 = e1.getName();
          if ( d1 == null || "".equals(d1) ) {
              d1 = "01/01/0000";
          }
          String d2 = e2.getName();
          if ( d2 == null || "".equals(d2) ) {
              d2 = "01/01/0000";
          }          
          date1 = formatter.parse(d1);
          date2 = formatter.parse(d2);          
        } catch (ParseException e) {
          System.out.println(e.toString());
          e.printStackTrace();
        }
        return date2.compareTo(date1);
    }
}
