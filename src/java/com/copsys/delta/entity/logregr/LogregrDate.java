/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.logregr;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_logregr_date")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "LogregrDate.findAll", query = "SELECT l FROM LogregrDate l"),
    @NamedQuery(name = "LogregrDate.findByIdLogregrDate", query = "SELECT l FROM LogregrDate l WHERE l.idLogregrDate = :idLogregrDate"),
    @NamedQuery(name = "LogregrDate.findByName", query = "SELECT l FROM LogregrDate l WHERE l.name = :name"),
    @NamedQuery(name = "LogregrDate.findByDescription", query = "SELECT l FROM LogregrDate l WHERE l.description = :description"),
    @NamedQuery(name = "LogregrDate.findByValue", query = "SELECT l FROM LogregrDate l WHERE l.value = :value"),
    @NamedQuery(name = "LogregrDate.findByModelColumnidSequencer", query = "SELECT l FROM LogregrDate l WHERE l.modelColumnidSequencer = :modelColumnidSequencer"),
    @NamedQuery(name = "LogregrDate.findByIdLogregr", query = "SELECT l FROM LogregrDate l WHERE l.idLogregr = :idLogregr"),
    @NamedQuery(name = "LogregrDate.findByActive", query = "SELECT l FROM LogregrDate l WHERE l.active = :active"),
    @NamedQuery(name = "LogregrDate.findByCreatedBy", query = "SELECT l FROM LogregrDate l WHERE l.createdBy = :createdBy"),
    @NamedQuery(name = "LogregrDate.findByCreatedTS", query = "SELECT l FROM LogregrDate l WHERE l.createdTS = :createdTS"),
    @NamedQuery(name = "LogregrDate.findByUpdatedBy", query = "SELECT l FROM LogregrDate l WHERE l.updatedBy = :updatedBy"),
    @NamedQuery(name = "LogregrDate.findByUpdatedTS", query = "SELECT l FROM LogregrDate l WHERE l.updatedTS = :updatedTS")})*/
public class LogregrDate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="LOGREGRDATE_ID")
    @TableGenerator(name="LOGREGRDATE_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="LOGREGRDATE_ID", allocationSize=1)
    @Column(name = "idLogregrDate")
    private Integer idLogregrDate;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @Column(name = "ModelColumn_idSequencer")
    private int modelColumnidSequencer;
    @Column(name = "idLogregrField")
    private Integer idLogregrField;    
    @Column(name = "idLogregr")
    private Integer idLogregr;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public LogregrDate() {
    }

    public LogregrDate(Integer idLogregrDate) {
        this.idLogregrDate = idLogregrDate;
    }

    public LogregrDate(Integer idLogregrDate, int modelColumnidSequencer) {
        this.idLogregrDate = idLogregrDate;
        this.modelColumnidSequencer = modelColumnidSequencer;
    }

    public Integer getIdLogregrDate() {
        return idLogregrDate;
    }

    public void setIdLogregrDate(Integer idLogregrDate) {
        this.idLogregrDate = idLogregrDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getModelColumnidSequencer() {
        return modelColumnidSequencer;
    }

    public void setModelColumnidSequencer(int modelColumnidSequencer) {
        this.modelColumnidSequencer = modelColumnidSequencer;
    }

    public Integer getIdLogregrField() {
        return idLogregrField;
    }

    public void setIdLogregrField(Integer idLogregrField) {
        this.idLogregrField = idLogregrField;
    }    

    public Integer getIdLogregr() {
        return idLogregr;
    }

    public void setIdLogregr(Integer idLogregr) {
        this.idLogregr = idLogregr;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogregrDate != null ? idLogregrDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogregrDate)) {
            return false;
        }
        LogregrDate other = (LogregrDate) object;
        if ((this.idLogregrDate == null && other.idLogregrDate != null) || (this.idLogregrDate != null && !this.idLogregrDate.equals(other.idLogregrDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.logregr.LogregrDate[ idLogregrDate=" + idLogregrDate + " ]";
    }
}
