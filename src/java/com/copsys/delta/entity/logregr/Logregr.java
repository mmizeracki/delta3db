/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.logregr;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_logregr")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Logregr.findAll", query = "SELECT l FROM Logregr l"),
    @NamedQuery(name = "Logregr.findByIdLogregr", query = "SELECT l FROM Logregr l WHERE l.idLogregr = :idLogregr"),
    @NamedQuery(name = "Logregr.findByName", query = "SELECT l FROM Logregr l WHERE l.name = :name"),
    @NamedQuery(name = "Logregr.findByDescription", query = "SELECT l FROM Logregr l WHERE l.description = :description"),
    @NamedQuery(name = "Logregr.findByStatus", query = "SELECT l FROM Logregr l WHERE l.status = :status"),
    @NamedQuery(name = "Logregr.findByIdOrganization", query = "SELECT l FROM Logregr l WHERE l.idOrganization = :idOrganization"),
    @NamedQuery(name = "Logregr.findByIdModel", query = "SELECT l FROM Logregr l WHERE l.idModel = :idModel"),
    @NamedQuery(name = "Logregr.findByIdStudy", query = "SELECT l FROM Logregr l WHERE l.idStudy = :idStudy"),
    @NamedQuery(name = "Logregr.findByOriginalLogregrId", query = "SELECT l FROM Logregr l WHERE l.originalLogregrId = :originalLogregrId"),
    @NamedQuery(name = "Logregr.findByActive", query = "SELECT l FROM Logregr l WHERE l.active = :active"),
    @NamedQuery(name = "Logregr.findByCreatedBy", query = "SELECT l FROM Logregr l WHERE l.createdBy = :createdBy"),
    @NamedQuery(name = "Logregr.findByCreatedTS", query = "SELECT l FROM Logregr l WHERE l.createdTS = :createdTS"),
    @NamedQuery(name = "Logregr.findByUpdatedBy", query = "SELECT l FROM Logregr l WHERE l.updatedBy = :updatedBy"),
    @NamedQuery(name = "Logregr.findByUpdatedTS", query = "SELECT l FROM Logregr l WHERE l.updatedTS = :updatedTS")})*/
public class Logregr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="LOGREGR_ID")
    @TableGenerator(name="LOGREGR_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="LOGREGR_ID", allocationSize=1)
    @Column(name = "idLogregr")
    private Integer idLogregr;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "status")
    private String status;
    @Column(name = "intervalData")
    private String intervalData;   
    @Column(name = "intervalQuery")
    private String intervalQuery;       
    @Column(name = "formula")
    private String formula;    
    @Column(name = "idOrganization")
    private int idOrganization;
    @Column(name = "idModel")
    private Integer idModel;
    @Column(name = "ModelColumn_idSequencer")
    private int modelColumnidSequencer;    
    @Column(name = "idStudy")
    private Integer idStudy;
    @Column(name = "idProcess")
    private Integer idProcess;      
    @Column(name = "originalLogregrId")
    private Integer originalLogregrId;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public Logregr() {
    }

    public Logregr(Integer idLogregr) {
        this.idLogregr = idLogregr;
    }

    public Logregr(Integer idLogregr, int idOrganization) {
        this.idLogregr = idLogregr;
        this.idOrganization = idOrganization;
    }

    public Integer getIdLogregr() {
        return idLogregr;
    }

    public void setIdLogregr(Integer idLogregr) {
        this.idLogregr = idLogregr;
    }


    public Integer getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(Integer idProcess) {
        this.idProcess = idProcess;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIntervalData() {
        return intervalData;
    }

    public void setIntervalData(String intervalData) {
        this.intervalData = intervalData;
    }
 
    public String getIntervalQuery() {
        return intervalQuery;
    }

    public void setIntervalQuery(String intervalQuery) {
        this.intervalQuery = intervalQuery;
    }
 
    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
    
    public int getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(int idOrganization) {
        this.idOrganization = idOrganization;
    }

    public Integer getIdModel() {
        return idModel;
    }

    public void setIdModel(Integer idModel) {
        this.idModel = idModel;
    }

    public int getModelColumnidSequencer() {
        return modelColumnidSequencer;
    }

    public void setModelColumnidSequencer(int modelColumnidSequencer) {
        this.modelColumnidSequencer = modelColumnidSequencer;
    }

    public Integer getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(Integer idStudy) {
        this.idStudy = idStudy;
    }

    public Integer getOriginalLogregrId() {
        return originalLogregrId;
    }

    public void setOriginalLogregrId(Integer originalLogregrId) {
        this.originalLogregrId = originalLogregrId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogregr != null ? idLogregr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Logregr)) {
            return false;
        }
        Logregr other = (Logregr) object;
        if ((this.idLogregr == null && other.idLogregr != null) || (this.idLogregr != null && !this.idLogregr.equals(other.idLogregr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.logregr.Logregr[ idLogregr=" + idLogregr + " ]";
    }
    
}
