/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_audit")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Audit.findAll", query = "SELECT a FROM Audit a"),
    @NamedQuery(name = "Audit.findByIdAudit", query = "SELECT a FROM Audit a WHERE a.idAudit = :idAudit"),
    @NamedQuery(name = "Audit.findBySource", query = "SELECT a FROM Audit a WHERE a.source = :source"),
    @NamedQuery(name = "Audit.findByUserId", query = "SELECT a FROM Audit a WHERE a.userId = :userId"),
    @NamedQuery(name = "Audit.findByObjectType", query = "SELECT a FROM Audit a WHERE a.objectType = :objectType"),
    @NamedQuery(name = "Audit.findByObjectId", query = "SELECT a FROM Audit a WHERE a.objectId = :objectId"),
    @NamedQuery(name = "Audit.findByObjectContent", query = "SELECT a FROM Audit a WHERE a.objectContent = :objectContent"),
    @NamedQuery(name = "Audit.findByAction", query = "SELECT a FROM Audit a WHERE a.action = :action"),
    @NamedQuery(name = "Audit.findByCreatedTS", query = "SELECT a FROM Audit a WHERE a.createdTS = :createdTS")})*/
public class Audit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="AUDIT_ID")
    @TableGenerator(name="AUDIT_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="AUDIT_ID", allocationSize=1)    
    @Column(name = "idAudit")
    private Integer idAudit;
    @Column(name = "source")
    private String source;
    @Column(name = "userId")
    private Integer userId;
    @Column(name = "userAlias")
    private String userAlias;    
    @Column(name = "objectType")
    private String objectType;
    @Column(name = "objectId")
    private Integer objectId;
    @Column(name = "objectContent")
    private String objectContent;
    @Column(name = "action")
    private String action;
    @Column(name = "createdTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTS;

    public Audit() {
    }

    public Audit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public Integer getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

     public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }
   
    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getObjectContent() {
        return objectContent;
    }

    public void setObjectContent(String objectContent) {
        this.objectContent = objectContent;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Date createdTS) {
        this.createdTS = createdTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAudit != null ? idAudit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Audit)) {
            return false;
        }
        Audit other = (Audit) object;
        if ((this.idAudit == null && other.idAudit != null) || (this.idAudit != null && !this.idAudit.equals(other.idAudit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.Audit[ idAudit=" + idAudit + " ]";
    }
    
}
