/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_configuration")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Configuration.findAll", query = "SELECT c FROM Configuration c"),
    @NamedQuery(name = "Configuration.findByIdConfiguration", query = "SELECT c FROM Configuration c WHERE c.idConfiguration = :idConfiguration"),
    @NamedQuery(name = "Configuration.findByName", query = "SELECT c FROM Configuration c WHERE c.name = :name"),
    @NamedQuery(name = "Configuration.findByDescription", query = "SELECT c FROM Configuration c WHERE c.description = :description"),
    @NamedQuery(name = "Configuration.findByType", query = "SELECT c FROM Configuration c WHERE c.type = :type"),
    @NamedQuery(name = "Configuration.findByConnectionInfo", query = "SELECT c FROM Configuration c WHERE c.connectionInfo = :connectionInfo"),
    @NamedQuery(name = "Configuration.findByDbUser", query = "SELECT c FROM Configuration c WHERE c.dbUser = :dbUser"),
    @NamedQuery(name = "Configuration.findByDbPassword", query = "SELECT c FROM Configuration c WHERE c.dbPassword = :dbPassword"),
    @NamedQuery(name = "Configuration.findByActive", query = "SELECT c FROM Configuration c WHERE c.active = :active"),
    @NamedQuery(name = "Configuration.findByCreatedBy", query = "SELECT c FROM Configuration c WHERE c.createdBy = :createdBy"),
    @NamedQuery(name = "Configuration.findByCreatedTS", query = "SELECT c FROM Configuration c WHERE c.createdTS = :createdTS"),
    @NamedQuery(name = "Configuration.findByUpdatedBy", query = "SELECT c FROM Configuration c WHERE c.updatedBy = :updatedBy"),
    @NamedQuery(name = "Configuration.findByUpdatedTS", query = "SELECT c FROM Configuration c WHERE c.updatedTS = :updatedTS")})*/
    //@NamedQuery(name = "Configuration.findByOrganizationidOrganization", query = "SELECT c FROM Configuration c WHERE c.organizationidOrganization = :organizationidOrganization")})
public class Configuration implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="CONFIGURATION_ID")
    @TableGenerator(name="CONFIGURATION_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="CONFIGURATION_ID", allocationSize=1)
    @Column(name = "idConfiguration")
    private Integer idConfiguration;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;
    @Column(name = "connectionInfo")
    private String connectionInfo;
    @Column(name = "dbUser")
    private String dbUser;
    @Column(name = "dbPassword")
    private String dbPassword;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Basic(optional = false)
    @Column(name = "Organization_idOrganization")
    private Integer organization;

    public Configuration() {
    }

    public Configuration(Integer idConfiguration) {
        this.idConfiguration = idConfiguration;
    }

    public Configuration(Integer idConfiguration, int organizationidOrganization) {
        this.idConfiguration = idConfiguration;
        this.organization = organizationidOrganization;
    }

    public Integer getIdConfiguration() {
        return idConfiguration;
    }

    public void setIdConfiguration(Integer idConfiguration) {
        this.idConfiguration = idConfiguration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(String connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getOrganization() {
        return organization;
    }

    public void setOrganization(int organizationidOrganization) {
        this.organization = organizationidOrganization;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConfiguration != null ? idConfiguration.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Configuration)) {
            return false;
        }
        Configuration other = (Configuration) object;
        if ((this.idConfiguration == null && other.idConfiguration != null) || (this.idConfiguration != null && !this.idConfiguration.equals(other.idConfiguration))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.Configuration[ idConfiguration=" + idConfiguration + " ]";
    }
    
}
