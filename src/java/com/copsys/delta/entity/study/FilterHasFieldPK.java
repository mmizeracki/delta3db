/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Coping Systems Inc.
 */
@Embeddable
public class FilterHasFieldPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "Filter_idFilter")
    private int filteridFilter;
    @Basic(optional = false)
    @Column(name = "ModelColumn_idModelColumn")
    private int modelColumnidModelColumn;

    public FilterHasFieldPK() {
    }

    public FilterHasFieldPK(int filteridFilter, int modelColumnidModelColumn) {
        this.filteridFilter = filteridFilter;
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    public int getFilteridFilter() {
        return filteridFilter;
    }

    public void setFilteridFilter(int filteridFilter) {
        this.filteridFilter = filteridFilter;
    }

    public int getModelColumnidModelColumn() {
        return modelColumnidModelColumn;
    }

    public void setModelColumnidModelColumn(int modelColumnidModelColumn) {
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) filteridFilter;
        hash += (int) modelColumnidModelColumn;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilterHasFieldPK)) {
            return false;
        }
        FilterHasFieldPK other = (FilterHasFieldPK) object;
        if (this.filteridFilter != other.filteridFilter) {
            return false;
        }
        if (this.modelColumnidModelColumn != other.modelColumnidModelColumn) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.FilterHasFieldPK[ filteridFilter=" + filteridFilter + ", modelColumnidModelColumn=" + modelColumnidModelColumn + " ]";
    }
    
}
