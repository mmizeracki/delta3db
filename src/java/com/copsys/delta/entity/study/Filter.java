/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_filter")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Filter.findAll", query = "SELECT f FROM Filter f"),
    @NamedQuery(name = "Filter.findByIdFilter", query = "SELECT f FROM Filter f WHERE f.idFilter = :idFilter"),
    @NamedQuery(name = "Filter.findByCreatedBy", query = "SELECT f FROM Filter f WHERE f.createdBy = :createdBy"),
    @NamedQuery(name = "Filter.findByCreatedTS", query = "SELECT f FROM Filter f WHERE f.createdTS = :createdTS"),
    @NamedQuery(name = "Filter.findByUpdatedBy", query = "SELECT f FROM Filter f WHERE f.updatedBy = :updatedBy"),
    @NamedQuery(name = "Filter.findByUpdatedTS", query = "SELECT f FROM Filter f WHERE f.updatedTS = :updatedTS"),
    @NamedQuery(name = "Filter.findByName", query = "SELECT f FROM Filter f WHERE f.name = :name"),
    @NamedQuery(name = "Filter.findByDescription", query = "SELECT f FROM Filter f WHERE f.description = :description"),
    @NamedQuery(name = "Filter.findByType", query = "SELECT f FROM Filter f WHERE f.type = :type"),
    @NamedQuery(name = "Filter.findByGroupidGroup", query = "SELECT f FROM Filter f WHERE f.groupidGroup = :groupidGroup"),
    @NamedQuery(name = "Filter.findByActive", query = "SELECT f FROM Filter f WHERE f.active = :active"),
    @NamedQuery(name = "Filter.findByModelidModel", query = "SELECT f FROM Filter f WHERE f.modelidModel = :modelidModel")})*/
public class Filter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="FILTER_ID")
    @TableGenerator(name="FILTER_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="FILTER_ID", allocationSize=1)
    @Column(name = "idFilter")
    private Integer idFilter;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;
    @Column(name = "Organization_idOrganization")
    private Integer organizationidOrganization;    
    @Column(name = "Group_idGroup")
    private Integer groupidGroup;
    @Column(name = "active")
    private Boolean active;
    @Basic(optional = false)
    @Column(name = "Model_idModel")
    private int modelidModel;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "filter")
    //private Collection<FilterHasField> filterHasFieldCollection;

    public Filter() {
    }

    public Filter(Integer idFilter) {
        this.idFilter = idFilter;
    }

    public Filter(Integer idFilter, int modelidModel) {
        this.idFilter = idFilter;
        this.modelidModel = modelidModel;
    }

    public Integer getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(Integer idFilter) {
        this.idFilter = idFilter;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOrganizationidOrganization() {
        return organizationidOrganization;
    }

    public void setOrganizationidOrganization(Integer organizationidOrganization) {
        this.organizationidOrganization = organizationidOrganization;
    }    

    public Integer getGroupidGroup() {
        return groupidGroup;
    }

    public void setGroupidGroup(Integer groupidGroup) {
        this.groupidGroup = groupidGroup;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getModelidModel() {
        return modelidModel;
    }

    public void setModelidModel(int modelidModel) {
        this.modelidModel = modelidModel;
    }

    //@XmlTransient
    //@JsonIgnore
    //public Collection<FilterHasField> getFilterHasFieldCollection() {
    //    return filterHasFieldCollection;
    //}

    //public void setFilterHasFieldCollection(Collection<FilterHasField> filterHasFieldCollection) {
    //    this.filterHasFieldCollection = filterHasFieldCollection;
    //}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFilter != null ? idFilter.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filter)) {
            return false;
        }
        Filter other = (Filter) object;
        if ((this.idFilter == null && other.idFilter != null) || (this.idFilter != null && !this.idFilter.equals(other.idFilter))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.Filter[ idFilter=" + idFilter + " ]";
    }
    
}
