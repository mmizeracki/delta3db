/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_study_has_category")
@XmlRootElement
@NamedQueries({
    //@NamedQuery(name = "StudyHasCategory.findAll", query = "SELECT s FROM StudyHasCategory s"),
    @NamedQuery(name = "StudyHasCategory.findByStudyidStudy", query = "SELECT s FROM StudyHasCategory s WHERE s.studyHasCategoryPK.studyidStudy = :studyidStudy"),
    /*@NamedQuery(name = "StudyHasCategory.findByModelColumnidModelColumn", query = "SELECT s FROM StudyHasCategory s WHERE s.studyHasCategoryPK.modelColumnidModelColumn = :modelColumnidModelColumn"),
    @NamedQuery(name = "StudyHasCategory.findByType", query = "SELECT s FROM StudyHasCategory s WHERE s.type = :type"),
    @NamedQuery(name = "StudyHasCategory.findByCreatedBy", query = "SELECT s FROM StudyHasCategory s WHERE s.createdBy = :createdBy"),
    @NamedQuery(name = "StudyHasCategory.findByCreatedTS", query = "SELECT s FROM StudyHasCategory s WHERE s.createdTS = :createdTS"),
    @NamedQuery(name = "StudyHasCategory.findByUpdatedBy", query = "SELECT s FROM StudyHasCategory s WHERE s.updatedBy = :updatedBy"),
    @NamedQuery(name = "StudyHasCategory.findByUpdatedTS", query = "SELECT s FROM StudyHasCategory s WHERE s.updatedTS = :updatedTS")*/})
public class StudyHasCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected StudyHasCategoryPK studyHasCategoryPK;
    @Column(name = "type")
    private String type;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTS;

    public StudyHasCategory() {
    }

    public StudyHasCategory(StudyHasCategoryPK studyHasCategoryPK) {
        this.studyHasCategoryPK = studyHasCategoryPK;
    }

    public StudyHasCategory(int studyidStudy, int modelColumnidModelColumn) {
        this.studyHasCategoryPK = new StudyHasCategoryPK(studyidStudy, modelColumnidModelColumn);
    }

    public StudyHasCategoryPK getStudyHasCategoryPK() {
        return studyHasCategoryPK;
    }

    public void setStudyHasCategoryPK(StudyHasCategoryPK studyHasCategoryPK) {
        this.studyHasCategoryPK = studyHasCategoryPK;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Date createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Date updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studyHasCategoryPK != null ? studyHasCategoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyHasCategory)) {
            return false;
        }
        StudyHasCategory other = (StudyHasCategory) object;
        if ((this.studyHasCategoryPK == null && other.studyHasCategoryPK != null) || (this.studyHasCategoryPK != null && !this.studyHasCategoryPK.equals(other.studyHasCategoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.StudyHasCategory[ studyHasCategoryPK=" + studyHasCategoryPK + " ]";
    }
    
}
