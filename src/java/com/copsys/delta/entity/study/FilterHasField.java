/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import com.copsys.delta.entity.model.ModelColumn;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_filter_has_field")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "FilterHasField.findAll", query = "SELECT f FROM FilterHasField f"),
    @NamedQuery(name = "FilterHasField.findByFilteridFilter", query = "SELECT f FROM FilterHasField f WHERE f.filterHasFieldPK.filteridFilter = :filteridFilter"),
    @NamedQuery(name = "FilterHasField.findByModelColumnidModelColumn", query = "SELECT f FROM FilterHasField f WHERE f.filterHasFieldPK.modelColumnidModelColumn = :modelColumnidModelColumn"),
    @NamedQuery(name = "FilterHasField.findByType", query = "SELECT f FROM FilterHasField f WHERE f.type = :type"),
    @NamedQuery(name = "FilterHasField.findByCreatedBy", query = "SELECT f FROM FilterHasField f WHERE f.createdBy = :createdBy"),
    @NamedQuery(name = "FilterHasField.findByCreatedTS", query = "SELECT f FROM FilterHasField f WHERE f.createdTS = :createdTS"),
    @NamedQuery(name = "FilterHasField.findByUpdatedBy", query = "SELECT f FROM FilterHasField f WHERE f.updatedBy = :updatedBy"),
    @NamedQuery(name = "FilterHasField.findByUpdatedTS", query = "SELECT f FROM FilterHasField f WHERE f.updatedTS = :updatedTS")})*/
public class FilterHasField implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FilterHasFieldPK filterHasFieldPK;
    @Column(name = "type")
    private String type;
    @Column(name = "operator")
    private String operator;
    @Column(name = "formula")
    private String formula;    
    @Column(name = "negator")
    private Boolean negator;    
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @JoinColumn(name = "Filter_idFilter", referencedColumnName = "idFilter", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Filter filter;
    @JoinColumn(name = "ModelColumn_idModelColumn", referencedColumnName = "idModelColumn", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ModelColumn modelColumn;

    public FilterHasField() {
    }

    public FilterHasField(FilterHasFieldPK filterHasFieldPK) {
        this.filterHasFieldPK = filterHasFieldPK;
    }

    public FilterHasField(int filteridFilter, int modelColumnidModelColumn) {
        this.filterHasFieldPK = new FilterHasFieldPK(filteridFilter, modelColumnidModelColumn);
    }

    public FilterHasFieldPK getFilterHasFieldPK() {
        return filterHasFieldPK;
    }

    public void setFilterHasFieldPK(FilterHasFieldPK filterHasFieldPK) {
        this.filterHasFieldPK = filterHasFieldPK;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Boolean getNegator() {
        return negator;
    }

    public void setNegator(Boolean negator) {
        this.negator = negator;
    }
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public ModelColumn getModelColumn() {
        return modelColumn;
    }

    public void setModelColumn(ModelColumn modelColumn) {
        this.modelColumn = modelColumn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filterHasFieldPK != null ? filterHasFieldPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilterHasField)) {
            return false;
        }
        FilterHasField other = (FilterHasField) object;
        if ((this.filterHasFieldPK == null && other.filterHasFieldPK != null) || (this.filterHasFieldPK != null && !this.filterHasFieldPK.equals(other.filterHasFieldPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.FilterHasField[ filterHasFieldPK=" + filterHasFieldPK + " ]";
    }
    
}
