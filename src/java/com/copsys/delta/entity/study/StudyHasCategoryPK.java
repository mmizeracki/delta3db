/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Coping Systems Inc.
 */
@Embeddable
public class StudyHasCategoryPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "Study_idStudy")
    private int studyidStudy;
    @Basic(optional = false)
    @Column(name = "ModelColumn_idModelColumn")
    private int modelColumnidModelColumn;

    public StudyHasCategoryPK() {
    }

    public StudyHasCategoryPK(int studyidStudy, int modelColumnidModelColumn) {
        this.studyidStudy = studyidStudy;
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    public int getStudyidStudy() {
        return studyidStudy;
    }

    public void setStudyidStudy(int studyidStudy) {
        this.studyidStudy = studyidStudy;
    }

    public int getModelColumnidModelColumn() {
        return modelColumnidModelColumn;
    }

    public void setModelColumnidModelColumn(int modelColumnidModelColumn) {
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) studyidStudy;
        hash += (int) modelColumnidModelColumn;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyHasCategoryPK)) {
            return false;
        }
        StudyHasCategoryPK other = (StudyHasCategoryPK) object;
        if (this.studyidStudy != other.studyidStudy) {
            return false;
        }
        if (this.modelColumnidModelColumn != other.modelColumnidModelColumn) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.StudyHasCategoryPK[ studyidStudy=" + studyidStudy + ", modelColumnidModelColumn=" + modelColumnidModelColumn + " ]";
    }
    
}
