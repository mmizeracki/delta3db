/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.study;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_filter_field")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "FilterField.findAll", query = "SELECT f FROM FilterField f"),
    @NamedQuery(name = "FilterField.findByIdFilterField", query = "SELECT f FROM FilterField f WHERE f.idFilterField = :idFilterField"),
    @NamedQuery(name = "FilterField.findByFilteridFilter", query = "SELECT f FROM FilterField f WHERE f.filteridFilter = :filteridFilter"),
    @NamedQuery(name = "FilterField.findByModelColumnidModelColumn", query = "SELECT f FROM FilterField f WHERE f.modelColumnidModelColumn = :modelColumnidModelColumn"),
    @NamedQuery(name = "FilterField.findByFormula", query = "SELECT f FROM FilterField f WHERE f.formula = :formula"),
    @NamedQuery(name = "FilterField.findByNegator", query = "SELECT f FROM FilterField f WHERE f.negator = :negator"),
    @NamedQuery(name = "FilterField.findByType", query = "SELECT f FROM FilterField f WHERE f.type = :type"),
    @NamedQuery(name = "FilterField.findByOperator", query = "SELECT f FROM FilterField f WHERE f.operator = :operator"),
    @NamedQuery(name = "FilterField.findByCreatedBy", query = "SELECT f FROM FilterField f WHERE f.createdBy = :createdBy"),
    @NamedQuery(name = "FilterField.findByCreatedTS", query = "SELECT f FROM FilterField f WHERE f.createdTS = :createdTS"),
    @NamedQuery(name = "FilterField.findByUpdatedBy", query = "SELECT f FROM FilterField f WHERE f.updatedBy = :updatedBy"),
    @NamedQuery(name = "FilterField.findByUpdatedTS", query = "SELECT f FROM FilterField f WHERE f.updatedTS = :updatedTS")})*/
public class FilterField implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="FILTERFIELD_ID")
    @TableGenerator(name="FILTERFIELD_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="FILTERFIELD_ID", allocationSize=1)    
    @Column(name = "idFilterField")
    private Integer idFilterField;
    @Basic(optional = false)
    @Column(name = "Filter_idFilter")
    private int filteridFilter;
    @Basic(optional = false)
    @Column(name = "ModelColumn_idModelColumn")
    private int modelColumnidModelColumn;
    @Column(name = "formula")
    private String formula;
    @Column(name = "negator")
    private Short negator;
    @Column(name = "type")
    private String type;
    @Column(name = "operator")
    private String operator;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public FilterField() {
    }

    public FilterField(Integer idFilterField) {
        this.idFilterField = idFilterField;
    }

    public FilterField(Integer idFilterField, int filteridFilter, int modelColumnidModelColumn) {
        this.idFilterField = idFilterField;
        this.filteridFilter = filteridFilter;
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    public Integer getIdFilterField() {
        return idFilterField;
    }

    public void setIdFilterField(Integer idFilterField) {
        this.idFilterField = idFilterField;
    }

    public int getFilteridFilter() {
        return filteridFilter;
    }

    public void setFilteridFilter(int filteridFilter) {
        this.filteridFilter = filteridFilter;
    }

    public int getModelColumnidModelColumn() {
        return modelColumnidModelColumn;
    }

    public void setModelColumnidModelColumn(int modelColumnidModelColumn) {
        this.modelColumnidModelColumn = modelColumnidModelColumn;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Short getNegator() {
        return negator;
    }

    public void setNegator(Short negator) {
        this.negator = negator;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFilterField != null ? idFilterField.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilterField)) {
            return false;
        }
        FilterField other = (FilterField) object;
        if ((this.idFilterField == null && other.idFilterField != null) || (this.idFilterField != null && !this.idFilterField.equals(other.idFilterField))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.study.FilterField[ idFilterField=" + idFilterField + " ]";
    }
    
}
