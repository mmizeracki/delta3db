/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_permissiontemplate")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Permissiontemplate.findAll", query = "SELECT p FROM Permissiontemplate p"),
    @NamedQuery(name = "Permissiontemplate.findByIdPermissionTemplate", query = "SELECT p FROM Permissiontemplate p WHERE p.idPermissiontemplate = :idPermissiontemplate"),
    @NamedQuery(name = "Permissiontemplate.findByCreatedBy", query = "SELECT p FROM Permissiontemplate p WHERE p.createdBy = :createdBy"),
    @NamedQuery(name = "Permissiontemplate.findByCreatedTS", query = "SELECT p FROM Permissiontemplate p WHERE p.createdTS = :createdTS"),
    @NamedQuery(name = "Permissiontemplate.findByUpdatedBy", query = "SELECT p FROM Permissiontemplate p WHERE p.updatedBy = :updatedBy"),
    @NamedQuery(name = "Permissiontemplate.findByUpdatedTS", query = "SELECT p FROM Permissiontemplate p WHERE p.updatedTS = :updatedTS"),
    @NamedQuery(name = "Permissiontemplate.findByName", query = "SELECT p FROM Permissiontemplate p WHERE p.name = :name"),
    @NamedQuery(name = "Permissiontemplate.findByDescription", query = "SELECT p FROM Permissiontemplate p WHERE p.description = :description"),
    @NamedQuery(name = "Permissiontemplate.findByType", query = "SELECT p FROM Permissiontemplate p WHERE p.type = :type"),
    @NamedQuery(name = "Permissiontemplate.findByTag", query = "SELECT p FROM Permissiontemplate p WHERE p.tag = :tag")})*/
public class Permissiontemplate implements Serializable {
    @EmbeddedId
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="PERMTEMPLATE_ID")
    @TableGenerator(name="PERMTEMPLATE_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="PERMTEMPLATE_ID", allocationSize=1)    
    @Column(name = "idPermissiontemplate")
    private Integer idPermissiontemplate;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "sequence")
    private Integer sequence;      
    @Column(name = "type")
    private String type;
    @Column(name = "tag")
    private String tag;
    @Column(name = "active")
    private Boolean active;    
    @Column(name = "Module_idModule")
    private Integer module;  
    
    public Permissiontemplate() {
    }

    public Permissiontemplate(Integer idPermissiontemplate) {
        this.idPermissiontemplate = idPermissiontemplate;
    }

    public Integer getIdPermissiontemplate() {
        return idPermissiontemplate;
    }

    public void setIdPermissiontemplate(Integer idPermissiontemplate) {
        this.idPermissiontemplate = idPermissiontemplate;
    }
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    
    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPermissiontemplate != null ? idPermissiontemplate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permissiontemplate)) {
            return false;
        }
        Permissiontemplate other = (Permissiontemplate) object;
        if ((this.idPermissiontemplate == null && other.idPermissiontemplate != null) || (this.idPermissiontemplate != null && !this.idPermissiontemplate.equals(other.idPermissiontemplate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.Permissiontemplate[ permissiontemplatePK=" + idPermissiontemplate + " ]";
    }
    
}
