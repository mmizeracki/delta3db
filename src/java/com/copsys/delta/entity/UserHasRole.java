/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_user_has_role")
@XmlRootElement
@NamedQueries({
    /*@NamedQuery(name = "UserHasRole.findAll", query = "SELECT u FROM UserHasRole u"),*/
    @NamedQuery(name = "UserHasRole.findByUseridUser", query = "SELECT u FROM UserHasRole u WHERE u.userHasRolePK.useridUser = :useridUser"),
    /*@NamedQuery(name = "UserHasRole.findByRoleidRole", query = "SELECT u FROM UserHasRole u WHERE u.userHasRolePK.roleidRole = :roleidRole"),
    @NamedQuery(name = "UserHasRole.findByCreatedBy", query = "SELECT u FROM UserHasRole u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "UserHasRole.findByCreatedTS", query = "SELECT u FROM UserHasRole u WHERE u.createdTS = :createdTS"),
    @NamedQuery(name = "UserHasRole.findByUpdatedBy", query = "SELECT u FROM UserHasRole u WHERE u.updatedBy = :updatedBy"),
    @NamedQuery(name = "UserHasRole.findByUpdatedTS", query = "SELECT u FROM UserHasRole u WHERE u.updatedTS = :updatedTS")*/})
public class UserHasRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasRolePK userHasRolePK;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public UserHasRole() {
    }

    public UserHasRole(UserHasRolePK userHasRolePK) {
        this.userHasRolePK = userHasRolePK;
    }

    public UserHasRole(int useridUser, int roleidRole) {
        this.userHasRolePK = new UserHasRolePK(useridUser, roleidRole);
    }

    public UserHasRolePK getUserHasRolePK() {
        return userHasRolePK;
    }

    public void setUserHasRolePK(UserHasRolePK userHasRolePK) {
        this.userHasRolePK = userHasRolePK;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasRolePK != null ? userHasRolePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasRole)) {
            return false;
        }
        UserHasRole other = (UserHasRole) object;
        if ((this.userHasRolePK == null && other.userHasRolePK != null) || (this.userHasRolePK != null && !this.userHasRolePK.equals(other.userHasRolePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.UserHasRole[ userHasRolePK=" + userHasRolePK + " ]";
    }
    
}
