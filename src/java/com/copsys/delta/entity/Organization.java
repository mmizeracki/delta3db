/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_organization")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Organization.findAll", query = "SELECT o FROM Organization o"),
    @NamedQuery(name = "Organization.findByIdOrganization", query = "SELECT o FROM Organization o WHERE o.idOrganization = :idOrganization"),
    @NamedQuery(name = "Organization.findByGuid", query = "SELECT o FROM Organization o WHERE o.guid = :guid"),
    @NamedQuery(name = "Organization.findByCreatedBy", query = "SELECT o FROM Organization o WHERE o.createdBy = :createdBy"),
    @NamedQuery(name = "Organization.findByCreatedTS", query = "SELECT o FROM Organization o WHERE o.createdTS = :createdTS"),
    @NamedQuery(name = "Organization.findByUpdatedBy", query = "SELECT o FROM Organization o WHERE o.updatedBy = :updatedBy"),
    @NamedQuery(name = "Organization.findByUpdatedTS", query = "SELECT o FROM Organization o WHERE o.updatedTS = :updatedTS"),
    @NamedQuery(name = "Organization.findByName", query = "SELECT o FROM Organization o WHERE o.name = :name"),
    @NamedQuery(name = "Organization.findByLongName", query = "SELECT o FROM Organization o WHERE o.longName = :longName"),
    @NamedQuery(name = "Organization.findByType", query = "SELECT o FROM Organization o WHERE o.type = :type"),
    @NamedQuery(name = "Organization.findByParentId", query = "SELECT o FROM Organization o WHERE o.parentId = :parentId")})*/
public class Organization implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="ORGANIZATION_ID")
    @TableGenerator(name="ORGANIZATION_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="ORGANIZATION_ID", allocationSize=1)
    @Column(name = "idOrganization")
    private Integer idOrganization;
    @Column(name = "GUID")
    private String guid;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "name")
    private String name;
    @Column(name = "longName")
    private String longName;
    @Column(name = "type")
    private String type;
    @Column(name = "key1")
    private String key1;    
    @Column(name = "key2")
    private String key2; 
    @Column(name = "parentId")
    private Integer parentId;
    @Column(name = "active")
    private Boolean active;    

    public Organization() {
    }

    public Organization(Integer idOrganization) {
        this.idOrganization = idOrganization;
    }

    public Integer getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(Integer idOrganization) {
        this.idOrganization = idOrganization;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key) {
        this.key1 = key;
    }
    
    public String getKey2() {
        return key2;
    }

    public void setKey2(String key) {
        this.key2 = key;
    }
    
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //@XmlTransient
    //@JsonIgnore
   //public Collection<Location> getLocationCollection() {
    //    return locationCollection;
    //}

    //public void setLocationCollection(Collection<Location> locationCollection) {
    //    this.locationCollection = locationCollection;
    //}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrganization != null ? idOrganization.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organization)) {
            return false;
        }
        Organization other = (Organization) object;
        if ((this.idOrganization == null && other.idOrganization != null) || (this.idOrganization != null && !this.idOrganization.equals(other.idOrganization))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.Organization[ idOrganization=" + idOrganization + " ]";
    }

    //@XmlTransient
    //@JsonIgnore
    //public Collection<User> getUserCollection() {
    //    return userCollection;
    //}

    //public void setUserCollection(Collection<User> userCollection) {
    //    this.userCollection = userCollection;
    //}
    
}
