/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_permission")
@XmlRootElement
@NamedQueries({
    /*@NamedQuery(name = "Permission.findAll", query = "SELECT p FROM Permission p"),
    @NamedQuery(name = "Permission.findByCreatedBy", query = "SELECT p FROM Permission p WHERE p.createdBy = :createdBy"),
    @NamedQuery(name = "Permission.findByCreatedTS", query = "SELECT p FROM Permission p WHERE p.createdTS = :createdTS"),
    @NamedQuery(name = "Permission.findByUpdatedBy", query = "SELECT p FROM Permission p WHERE p.updatedBy = :updatedBy"),
    @NamedQuery(name = "Permission.findByUpdatedTS", query = "SELECT p FROM Permission p WHERE p.updatedTS = :updatedTS"),
    @NamedQuery(name = "Permission.findByPermTempidPermTemp", query = "SELECT p FROM Permission p WHERE p.permissionPK.permTempidPermTemp = :permTempidPermTemp"),*/
    @NamedQuery(name = "Permission.findByRoleidRole", query = "SELECT p FROM Permission p WHERE p.permissionPK.roleidRole = :roleidRole")})
public class Permission implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PermissionPK permissionPK;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public Permission() {
    }

    public Permission(PermissionPK permissionPK) {
        this.permissionPK = permissionPK;
    }

    public Permission(int permTempidPermTemp, int roleidRole) {
        this.permissionPK = new PermissionPK(permTempidPermTemp, roleidRole);
    }

    public PermissionPK getPermissionPK() {
        return permissionPK;
    }

    public void setPermissionPK(PermissionPK permissionPK) {
        this.permissionPK = permissionPK;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionPK != null ? permissionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permission)) {
            return false;
        }
        Permission other = (Permission) object;
        if ((this.permissionPK == null && other.permissionPK != null) || (this.permissionPK != null && !this.permissionPK.equals(other.permissionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.Permission[ permissionPK=" + permissionPK + " ]";
    }
    
}
