/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_group_table")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "GroupTable.findAll", query = "SELECT g FROM GroupTable g"),
    @NamedQuery(name = "GroupTable.findByIdGroup", query = "SELECT g FROM GroupTable g WHERE g.idGroup = :idGroup"),
    @NamedQuery(name = "GroupTable.findByCreatedBy", query = "SELECT g FROM GroupTable g WHERE g.createdBy = :createdBy"),
    @NamedQuery(name = "GroupTable.findByCreatedTS", query = "SELECT g FROM GroupTable g WHERE g.createdTS = :createdTS"),
    @NamedQuery(name = "GroupTable.findByUpdatedBy", query = "SELECT g FROM GroupTable g WHERE g.updatedBy = :updatedBy"),
    @NamedQuery(name = "GroupTable.findByUpdatedTS", query = "SELECT g FROM GroupTable g WHERE g.updatedTS = :updatedTS"),
    @NamedQuery(name = "GroupTable.findByType", query = "SELECT g FROM GroupTable g WHERE g.type = :type"),
    @NamedQuery(name = "GroupTable.findByName", query = "SELECT g FROM GroupTable g WHERE g.name = :name"),
    @NamedQuery(name = "GroupTable.findByDescription", query = "SELECT g FROM GroupTable g WHERE g.description = :description"),
    @NamedQuery(name = "GroupTable.findByParentId", query = "SELECT g FROM GroupTable g WHERE g.parentId = :parentId")})*/
public class GroupTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="GROUP_ID")
    @TableGenerator(name="GROUP_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="GROUP_ID", allocationSize=1)
    @Column(name = "idGroup")
    private Integer idGroup;    
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "type")
    private String type;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "parentId")
    private Integer parentId;
    @Column(name = "active")
    private Boolean active;
    
    public GroupTable() {
    }

    public GroupTable(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGroup != null ? idGroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupTable)) {
            return false;
        }
        GroupTable other = (GroupTable) object;
        if ((this.idGroup == null && other.idGroup != null) || (this.idGroup != null && !this.idGroup.equals(other.idGroup))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.GroupTable[ idGroup=" + idGroup + " ]";
    }
    
}
