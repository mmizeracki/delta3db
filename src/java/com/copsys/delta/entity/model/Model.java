/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_model")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Model.findByIdModel", query = "SELECT m FROM Model m WHERE m.idModel = :idModel"),
    @NamedQuery(name = "Model.findByName", query = "SELECT m FROM Model m WHERE m.name = :name")
    /*@NamedQuery(name = "Model.findByDescription", query = "SELECT m FROM Model m WHERE m.description = :description"),
    @NamedQuery(name = "Model.findByOutputName", query = "SELECT m FROM Model m WHERE m.outputName = :outputName"),
    @NamedQuery(name = "Model.findByActive", query = "SELECT m FROM Model m WHERE m.active = :active"),
    @NamedQuery(name = "Model.findByOrganizationId", query = "SELECT m FROM Model m WHERE m.organization = :organizationId"),     
    @NamedQuery(name = "Model.findByCreatedBy", query = "SELECT m FROM Model m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "Model.findByCreatedTS", query = "SELECT m FROM Model m WHERE m.createdTS = :createdTS"),
    @NamedQuery(name = "Model.findByUpdatedBy", query = "SELECT m FROM Model m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "Model.findByUpdatedTS", query = "SELECT m FROM Model m WHERE m.updatedTS = :updatedTS")*/})
public class Model implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MODEL_ID")
    @TableGenerator(name="MODEL_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MODEL_ID", allocationSize=1)
    @Column(name = "idModel")
    private Integer idModel;  
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "idConfiguration")
    private Integer idConfiguration;      
    @Column(name = "outputName")
    private String outputName;
    @Column(name = "status")
    private String status;    
    @Column(name = "active")
    private Boolean active;
    @Column(name = "isAtomicFinished")
    private Boolean isAtomicFinished;
    @Column(name = "isMissingFinished")
    private Boolean isMissingFinished;
    @Column(name = "isVirtualFinished")
    private Boolean isVirtualFinished;    
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "Organization_idOrganization")
    private Integer organization;
    
    public Model() {
    }

    public Model(Integer idModel) {
        this.idModel = idModel;
    }

    public Integer getIdModel() {
        return idModel;
    }

    public void setIdModel(Integer idModel) {
        this.idModel = idModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdConfiguration() {
        return idConfiguration;
    }

    public void setIdConfiguration(Integer idConnection) {
        this.idConfiguration = idConnection;
    }

    public String getOutputName() {
        return outputName;
    }

    public void setOutputName(String outputName) {
        this.outputName = outputName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Boolean getIsAtomicFinished() {
        return isAtomicFinished;
    }

    public void setIsAtomicFinished(Boolean isAtomicF) {
        this.isAtomicFinished = isAtomicF;
    }
    
    public Boolean getIsMissingFinished() {
        return isMissingFinished;
    }

    public void setIsMissingFinished(Boolean isMissingF) {
        this.isMissingFinished = isMissingF;
    }
    
    public Boolean getIsVirtualFinished() {
        return isVirtualFinished;
    }

    public void setIsVirtualFinished(Boolean isVirtualF) {
        this.isVirtualFinished = isVirtualF;
    }    
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp)updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }


    public Integer getOrganization() {
        return organization;
    }

    public void setOrganization(Integer organization) {
        this.organization = organization;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModel != null ? idModel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Model)) {
            return false;
        }
        Model other = (Model) object;
        if ((this.idModel == null && other.idModel != null) || (this.idModel != null && !this.idModel.equals(other.idModel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.model.Model[ idModel=" + idModel + " ]";
    }
    
}
