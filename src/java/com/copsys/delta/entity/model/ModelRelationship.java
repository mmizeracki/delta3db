/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_model_relationship")
@XmlRootElement
@NamedQueries({
    /*@NamedQuery(name = "ModelRelationship.findAll", query = "SELECT m FROM ModelRelationship m"),
    @NamedQuery(name = "ModelRelationship.findByIdModelRelationship", query = "SELECT m FROM ModelRelationship m WHERE m.idModelRelationship = :idModelRelationship"),
    @NamedQuery(name = "ModelRelationship.findByTable1", query = "SELECT m FROM ModelRelationship m WHERE m.table1 = :table1"),
    @NamedQuery(name = "ModelRelationship.findByKey1", query = "SELECT m FROM ModelRelationship m WHERE m.key1 = :key1"),
    @NamedQuery(name = "ModelRelationship.findByTable2", query = "SELECT m FROM ModelRelationship m WHERE m.table2 = :table2"),
    @NamedQuery(name = "ModelRelationship.findByKey2", query = "SELECT m FROM ModelRelationship m WHERE m.key2 = :key2"),
    @NamedQuery(name = "ModelRelationship.findByType", query = "SELECT m FROM ModelRelationship m WHERE m.type = :type"),
    @NamedQuery(name = "ModelRelationship.findByCreatedBy", query = "SELECT m FROM ModelRelationship m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "ModelRelationship.findByCreatedTS", query = "SELECT m FROM ModelRelationship m WHERE m.createdTS = :createdTS"),
    @NamedQuery(name = "ModelRelationship.findByUpdatedBy", query = "SELECT m FROM ModelRelationship m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "ModelRelationship.findByUpdatedTS", query = "SELECT m FROM ModelRelationship m WHERE m.updatedTS = :updatedTS"),*/
    @NamedQuery(name = "ModelRelationship.countRelationshipsByModelidModel", query = "SELECT count(m) FROM ModelRelationship m WHERE m.modelidModel = :modelidModel"),
    @NamedQuery(name = "ModelRelationship.findByModelidModel", query = "SELECT m FROM ModelRelationship m WHERE m.modelidModel = :modelidModel"),
    @NamedQuery(name = "ModelRelationship.findByModelTableid", query = "SELECT m FROM ModelRelationship m WHERE m.table1 = :modelTableid OR m.table2 = :modelTableid")})
public class ModelRelationship implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MODELRELATIONSHIP_ID")
    @TableGenerator(name="MODELRELATIONSHIP_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MODELRELATIONSHIP_ID", allocationSize=1)
    @Column(name = "idModelRelationship")
    private Integer idModelRelationship;
    @Column(name = "table1")
    private Integer table1;
    @Column(name = "key1")
    private Integer key1;
    @Column(name = "table2")
    private Integer table2;
    @Column(name = "key2")
    private Integer key2;
    @Column(name = "type")
    private String type;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "Model_idModel")
    private Integer modelidModel;

    public ModelRelationship() {
    }

    public ModelRelationship(Integer idModelRelationship) {
        this.idModelRelationship = idModelRelationship;
    }

    public Integer getIdModelRelationship() {
        return idModelRelationship;
    }

    public void setIdModelRelationship(Integer idModelRelationship) {
        this.idModelRelationship = idModelRelationship;
    }

    public Integer getTable1() {
        return table1;
    }

    public void setTable1(Integer table1) {
        this.table1 = table1;
    }

    public Integer getKey1() {
        return key1;
    }

    public void setKey1(Integer key1) {
        this.key1 = key1;
    }

    public Integer getTable2() {
        return table2;
    }

    public void setTable2(Integer table2) {
        this.table2 = table2;
    }

    public Integer getKey2() {
        return key2;
    }

    public void setKey2(Integer key2) {
        this.key2 = key2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp)updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getModelidModel() {
        return modelidModel;
    }

    public void setModelidModel(Integer modelidModel) {
        this.modelidModel = modelidModel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModelRelationship != null ? idModelRelationship.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelRelationship)) {
            return false;
        }
        ModelRelationship other = (ModelRelationship) object;
        if ((this.idModelRelationship == null && other.idModelRelationship != null) || (this.idModelRelationship != null && !this.idModelRelationship.equals(other.idModelRelationship))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.model.ModelRelationship[ idModelRelationship=" + idModelRelationship + " ]";
    }
    
}
