/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_model_column")
@XmlRootElement
@NamedQueries({
    /*@NamedQuery(name = "ModelColumn.findAll", query = "SELECT m FROM ModelColumn m"),
    @NamedQuery(name = "ModelColumn.findByIdModelColumn", query = "SELECT m FROM ModelColumn m WHERE m.idModelColumn = :idModelColumn"),
    @NamedQuery(name = "ModelColumn.findByName", query = "SELECT m FROM ModelColumn m WHERE m.name = :name"),   
    @NamedQuery(name = "ModelColumn.findByDescription", query = "SELECT m FROM ModelColumn m WHERE m.description = :description"),
    @NamedQuery(name = "ModelColumn.findByPhysicalName", query = "SELECT m FROM ModelColumn m WHERE m.physicalName = :physicalName"),
    @NamedQuery(name = "ModelColumn.findByType", query = "SELECT m FROM ModelColumn m WHERE m.type = :type"),
    @NamedQuery(name = "ModelColumn.findByDefaultValue", query = "SELECT m FROM ModelColumn m WHERE m.defaultValue = :defaultValue"),
    @NamedQuery(name = "ModelColumn.findByDefaultValueType", query = "SELECT m FROM ModelColumn m WHERE m.defaultValueType = :defaultValueType"),
    @NamedQuery(name = "ModelColumn.findByKeyField", query = "SELECT m FROM ModelColumn m WHERE m.keyField = :keyField"),
    @NamedQuery(name = "ModelColumn.findByInsertable", query = "SELECT m FROM ModelColumn m WHERE m.insertable = :insertable"),
    @NamedQuery(name = "ModelColumn.findByAtomic", query = "SELECT m FROM ModelColumn m WHERE m.atomic = :atomic"),
    @NamedQuery(name = "ModelColumn.findByActive", query = "SELECT m FROM ModelColumn m WHERE m.active = :active"),
    @NamedQuery(name = "ModelColumn.findByCreatedBy", query = "SELECT m FROM ModelColumn m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "ModelColumn.findByCreatedTS", query = "SELECT m FROM ModelColumn m WHERE m.createdTS = :createdTS"),
    @NamedQuery(name = "ModelColumn.findByUpdatedBy", query = "SELECT m FROM ModelColumn m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "ModelColumn.findByUpdatedTS", query = "SELECT m FROM ModelColumn m WHERE m.updatedTS = :updatedTS"),*/
    @NamedQuery(name = "ModelColumn.findByModelAndName", query = "SELECT m FROM ModelColumn m WHERE m.idModel = :idModel AND m.name = :name"),    
    @NamedQuery(name = "ModelColumn.findByModelTableidModelTable", query = "SELECT m FROM ModelColumn m WHERE m.modelTableidModelTable = :modelTableidModelTable")})
public class ModelColumn implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MODELCOLUMN_ID")
    @TableGenerator(name="MODELCOLUMN_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MODELCOLUMN_ID", allocationSize=1)
    @Column(name = "idModelColumn")
    private Integer idModelColumn;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "physicalName")
    private String physicalName;
    @Column(name = "type")
    private String type;
    @Column(name = "defaultValue")
    private String defaultValue;
    @Column(name = "defaultValueType")
    private String defaultValueType;
    @Column(name = "keyField")
    private Boolean keyField;
    @Column(name = "insertable")
    private Boolean insertable;
    @Column(name = "atomic")
    private Boolean atomic;
    @Column(name = "virtual")
    private Boolean virtual;   
    @Column(name = "sub")
    private Boolean sub;       
    @Column(name = "formula")
    private String formula;      
    @Column(name = "verified")
    private Boolean verified;    
    @Column(name = "fieldClass")
    private String fieldClass;       
    @Column(name = "fieldKind")
    private String fieldKind;        
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "ModelTable_idModelTable")
    private Integer modelTableidModelTable;
    @Column(name = "Model_idModel")    
    private Integer idModel;

    public ModelColumn() {
    }

    public ModelColumn(Integer idModelColumn) {
        this.idModelColumn = idModelColumn;
    }

    public Integer getIdModelColumn() {
        return idModelColumn;
    }

    public void setIdModelColumn(Integer idModelColumn) {
        this.idModelColumn = idModelColumn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValueType() {
        return defaultValueType;
    }

    public void setDefaultValueType(String defaultValueType) {
        this.defaultValueType = defaultValueType;
    }

    public Boolean getKeyField() {
        return keyField;
    }

    public void setKeyField(Boolean keyField) {
        this.keyField = keyField;
    }

    public Boolean getInsertable() {
        return insertable;
    }

    public void setInsertable(Boolean insertable) {
        this.insertable = insertable;
    }

    public Boolean getAtomic() {
        return atomic;
    }

    public void setAtomic(Boolean atomic) {
        this.atomic = atomic;
    }

    public Boolean getVirtual() {
        return virtual;
    }

    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }

    public Boolean getSub() {
        return sub;
    }

    public void setSub(Boolean sub) {
        this.sub = sub;
    }
    
    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
    
    public String getFieldClass() {
        return fieldClass;
    }

    public void setFieldClass(String fClass) {
        this.fieldClass = fClass;
    }

    
    public String getFieldKind() {
        return fieldKind;
    }

    public void setFieldKind(String fKind) {
        this.fieldKind = fKind;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Integer getIdModel() {
        return idModel;
    }

    public void setIdModel(Integer idModel) {
        this.idModel = idModel;
    }
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp) updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getModelTableidModelTable() {
        return modelTableidModelTable;
    }

    public void setModelTableidModelTable(Integer modelTableidModelTable) {
        this.modelTableidModelTable = modelTableidModelTable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModelColumn != null ? idModelColumn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelColumn)) {
            return false;
        }
        ModelColumn other = (ModelColumn) object;
        if ((this.idModelColumn == null && other.idModelColumn != null) || (this.idModelColumn != null && !this.idModelColumn.equals(other.idModelColumn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.model.ModelColumn[ idModelColumn=" + idModelColumn + " ]";
    }
    
}
