/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_model_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModelTable.findByIdModelTable", query = "SELECT m FROM ModelTable m WHERE m.idModelTable = :idModelTable"),
    /*@NamedQuery(name = "ModelTable.findByName", query = "SELECT m FROM ModelTable m WHERE m.name = :name"),
    @NamedQuery(name = "ModelTable.findByDescription", query = "SELECT m FROM ModelTable m WHERE m.description = :description"),
    @NamedQuery(name = "ModelTable.findByPhysicalName", query = "SELECT m FROM ModelTable m WHERE m.physicalName = :physicalName"),
    @NamedQuery(name = "ModelTable.findByActive", query = "SELECT m FROM ModelTable m WHERE m.active = :active"),
    @NamedQuery(name = "ModelTable.findByCreatedBy", query = "SELECT m FROM ModelTable m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "ModelTable.findByCreatedTS", query = "SELECT m FROM ModelTable m WHERE m.createdTS = :createdTS"),
    @NamedQuery(name = "ModelTable.findByUpdatedBy", query = "SELECT m FROM ModelTable m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "ModelTable.findByUpdatedTS", query = "SELECT m FROM ModelTable m WHERE m.updatedTS = :updatedTS"),*/
    @NamedQuery(name = "ModelTable.findByPrimary", query = "SELECT m FROM ModelTable m WHERE m.modelidModel = :modelidModel AND m.primaryTable = :primaryTable"),    
    @NamedQuery(name = "ModelTable.findByModelidModel", query = "SELECT m FROM ModelTable m WHERE m.modelidModel = :modelidModel")})
public class ModelTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MODELTABLE_ID")
    @TableGenerator(name="MODELTABLE_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MODELTABLE_ID", allocationSize=1)
    @Column(name = "idModelTable")
    private Integer idModelTable;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "physicalName")
    private String physicalName;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "primaryTable")
    private Boolean primaryTable;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;
    @Column(name = "Model_idModel")
    private Integer modelidModel;

    public ModelTable() {
    }

    public ModelTable(Integer idModelTable) {
        this.idModelTable = idModelTable;
    }

    public Integer getIdModelTable() {
        return idModelTable;
    }

    public void setIdModelTable(Integer idModelTable) {
        this.idModelTable = idModelTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getPrimaryTable() {
        return primaryTable;
    }

    public void setPrimaryTable(Boolean primaryTable) {
        this.primaryTable = primaryTable;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp)updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getModelidModel() {
        return modelidModel;
    }

    public void setModelidModel(Integer modelidModel) {
        this.modelidModel = modelidModel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModelTable != null ? idModelTable.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelTable)) {
            return false;
        }
        ModelTable other = (ModelTable) object;
        if ((this.idModelTable == null && other.idModelTable != null) || (this.idModelTable != null && !this.idModelTable.equals(other.idModelTable))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.model.ModelTable[ idModelTable=" + idModelTable + " ]";
    }
    
}
