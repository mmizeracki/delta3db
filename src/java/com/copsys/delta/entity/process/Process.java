/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.process;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_process")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Process.findAll", query = "SELECT p FROM Process p"),
    @NamedQuery(name = "Process.findByIdProcess", query = "SELECT p FROM Process p WHERE p.idProcess = :idProcess"),
    @NamedQuery(name = "Process.findByIdOrganization", query = "SELECT p FROM Process p WHERE p.idOrganization = :idOrganization"),
    @NamedQuery(name = "Process.findByIdModel", query = "SELECT p FROM Process p WHERE p.idModel = :idModel"),
    @NamedQuery(name = "Process.findByProgress", query = "SELECT p FROM Process p WHERE p.progress = :progress"),
    @NamedQuery(name = "Process.findByName", query = "SELECT p FROM Process p WHERE p.name = :name"),
    @NamedQuery(name = "Process.findByData", query = "SELECT p FROM Process p WHERE p.data = :data"),
    @NamedQuery(name = "Process.findByStatus", query = "SELECT p FROM Process p WHERE p.status = :status"),
    @NamedQuery(name = "Process.findByMessage", query = "SELECT p FROM Process p WHERE p.message = :message"),
    @NamedQuery(name = "Process.findByActive", query = "SELECT p FROM Process p WHERE p.active = :active"),
    @NamedQuery(name = "Process.findByCreatedBy", query = "SELECT p FROM Process p WHERE p.createdBy = :createdBy"),
    @NamedQuery(name = "Process.findByCreatedTS", query = "SELECT p FROM Process p WHERE p.createdTS = :createdTS"),
    @NamedQuery(name = "Process.findByUpdatedBy", query = "SELECT p FROM Process p WHERE p.updatedBy = :updatedBy"),
    @NamedQuery(name = "Process.findByUpdatedTS", query = "SELECT p FROM Process p WHERE p.updatedTS = :updatedTS")})*/
public class Process implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="PROCESS_ID")
    @TableGenerator(name="PROCESS_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="PROCESS_ID", allocationSize=1) 
    @Column(name = "idProcess")
    private Integer idProcess;
    @Column(name = "GUID")
    private String guid;    
    @Column(name = "idOrganization")
    private Integer idOrganization;
    @Column(name = "idModel")
    private Integer idModel;
    @Column(name = "idStudy")
    private Integer idStudy;    
    @Column(name = "progress")
    private Integer progress;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;    
    @Column(name = "data")
    private String data;
    @Column(name = "status")
    private String status;
    @Column(name = "message")
    private String message;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public Process() {
    }

    public Process(Integer idProcess) {
        this.idProcess = idProcess;
    }

    public Integer getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(Integer idProcess) {
        this.idProcess = idProcess;
    }
    
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
    
    public Integer getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(Integer idOrganization) {
        this.idOrganization = idOrganization;
    }

    public Integer getIdModel() {
        return idModel;
    }

    public void setIdModel(Integer idModel) {
        this.idModel = idModel;
    }

    public Integer getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(Integer idStudy) {
        this.idStudy = idStudy;
    }
    
    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcess != null ? idProcess.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Process)) {
            return false;
        }
        Process other = (Process) object;
        if ((this.idProcess == null && other.idProcess != null) || (this.idProcess != null && !this.idProcess.equals(other.idProcess))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.copsys.delta.entity.process.Process[ idProcess=" + idProcess + " ]";
    }
    
}
