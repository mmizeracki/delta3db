/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.entity.process;


/**
 *
 * @author Coping Systems Inc.
 */
public class JobRequest extends Process {

    private String memoryKey;
    private Integer columnCount;
    private String method;
    //private String inputs;
    private String columns;
    
    public JobRequest(Process p) {
        super();
        setActive(p.getActive());
        setCreatedBy(p.getCreatedBy());
        setCreatedTS(p.getCreatedTS());
        setData(p.getData());
        setGuid(p.getGuid());
        setIdModel(p.getIdModel());
        setIdOrganization(p.getIdOrganization());
        setIdProcess(p.getIdProcess());
        setStatus(p.getStatus());
        setUpdatedBy(p.getUpdatedBy());
        setUpdatedTS(p.getUpdatedTS());
    }
    
    public void setMemoryKey(String mk) {
        memoryKey = mk;
    }
    
    public String getMemoryKey() {
        return memoryKey;
    }
    
    public void setColumnCount(int c) {
        columnCount = c;
    }
    
    public Integer getColumnCount() {
        return columnCount;
    }
    
    public void setMethod(String m) {
        method = m;
    }
    
    public String getMethod() {
        return method;
    }   
    
    /*public void setInputs(String str) {
        inputs = str;
    }
    
    public String getInputs() {
        return inputs;
    }     */
    
    public void setColumns(String col) {
        columns = col;
    }
    
    public String getColumns() {
        return columns;
    }     
}
