/*
 * This class contains "business logic for calculationg stats for Dichotomous Fields
 */

package com.copsys.delta.db;

import com.copsys.delta.server.StatServiceImpl;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class Dichotomous {
    private static final Logger logger = Logger.getLogger(StatServiceImpl.class.getName());        
    private String name;
    private String tableName;
    private String trueValue;
    private boolean trueValueFromDb = false;
    private String falseValue;
    private boolean falseValueFromDb = false;
    private String nullValue;
    private String meanValue;
    private String stdValue;    
    private int trueCount;
    private int falseCount;
    private int nullCount;
    private final int totalCount;
   
    public Dichotomous (String tblName, String clmnName) throws Exception {          
        Query queryResult;  
        tableName = tblName;
        name = clmnName;
        DecimalFormat df = new DecimalFormat("#.##");

        /* obtain total count of all rows in target table */
        String sqlStmt = "select count(*) from " + tableName;        
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger); 
        List<Object[]> resultCounts = queryResult.getResultList(); 
        String tmp = String.valueOf(resultCounts.get(0));                        
        totalCount = Integer.parseInt(tmp);

        /* get all possible values for this field */
        sqlStmt = "select " + name + " from " + tableName + " GROUP BY " + name;     
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
        List<Object[]> resultValues = queryResult.getResultList(); 

        /* get the number of occurrences of those values, null will yield 0 */
        sqlStmt = "select count(" + name + ") from " + tableName + " GROUP BY " + name;  
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger); 
        resultCounts = queryResult.getResultList(); 

        if ( (resultValues.size() > 3) || (resultCounts.size()) > 3) {
           throw new NotDichotomousException("Field " + tableName + "." + name + " is not dichotomous.");
        }
        for ( int i =0; i < resultValues.size(); i++ ) {    
            if ( resultValues.get(i) == null ) {
                nullValue = "NULL";    
                tmp = String.valueOf(resultCounts.get(i));
                nullCount = Integer.parseInt(tmp);
            } else {
                String c = "";
                c += resultValues.get(i);
                if (c.toUpperCase().equals("Y") || 
                      c.equals("1") ||
                        c.equals("1.0") ||
                            c.toUpperCase().equals("TRUE") ||
                                c.toUpperCase().equals("T") ) {
                    trueValue = c;
                    tmp = String.valueOf(resultCounts.get(i));                        
                    trueCount = Integer.parseInt(tmp);
                    trueValueFromDb = true;
                } else {
                    falseValue = c;
                    tmp = String.valueOf(resultCounts.get(i));                        
                    falseCount = Integer.parseInt(tmp);
                    falseValueFromDb = true;
                }
            }
        }   
        nullCount = totalCount - trueCount - falseCount;
        // patch values in case only one was present in the data
        if ( (trueValue == null) && (falseValue != null)) {
            trueValue = getComplimentaryDichotomousValue(falseValue);
            logger.log(Level.INFO, "trueValue was patched for dichotomous field {0}, assigned value={1}", new Object[]{name, trueValue});    
        } 
        if ( (trueValue != null) && (falseValue == null)) {
            falseValue = getComplimentaryDichotomousValue(trueValue);
            logger.log(Level.INFO, "falseValue was patched for dichotomous field {0}, assigned value={1}", new Object[]{name, falseValue});                    
        }    
        
        /* get mean value for this field */
        sqlStmt = "select AVG(" + name + ") from " + tableName;             
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
        resultValues = queryResult.getResultList();      
        meanValue = String.valueOf(resultValues.get(0));
        meanValue = df.format(Double.parseDouble(meanValue));

        /* get standard dev value for this field */
        sqlStmt = "select STD(" + name + ") from " + tableName;            
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
        resultValues = queryResult.getResultList();      
        stdValue = String.valueOf(resultValues.get(0));
        stdValue = df.format(Double.parseDouble(stdValue));         
    }

    public String getName() {
        return name;
    }
    public String getTableName() {
        return tableName;
    }    
    public String getTrueValue() {
        return trueValue;
    }
    public String getFalseValue() {
        return falseValue;
    }
    public String getNullValue() {
        return nullValue;
    }    
    public boolean isFalseValueFromDb() {
        return falseValueFromDb;
    }
    public boolean isTrueValueFromDb() {
        return trueValueFromDb;
    }      
    public int getTrueCount() {
        return trueCount;
    }
    public int getFalseCount() {
        return falseCount;
    }
    public int getNullCount() {
        return nullCount;
    }      
    public int getTotalCount() {
        return totalCount;
    } 
    public String getMeanValue() {
        return meanValue;
    }
    public String getStdValue() {
        return stdValue;
    }      
    public static String getComplimentaryDichotomousValue(String input) {
        if ( "Y".equals(input) ) return "N";
        if ( "N".equals(input) ) return "Y";
        if ( "y".equals(input) ) return "n";
        if ( "n".equals(input) ) return "y";    
        if ( "True".equals(input) ) return "False";
        if ( "False".equals(input) ) return "True";  
        if ( "true".equals(input) ) return "false";
        if ( "false".equals(input) ) return "true";    
        if ( "TRUE".equals(input) ) return "FALSE";
        if ( "FALSE".equals(input) ) return "TRUE";    
        if ( "T".equals(input) ) return "F";
        if ( "F".equals(input) ) return "T";            
        if ( "0".equals(input) ) return "1";
        if ( "1".equals(input) ) return "0";     
        if ( "0.0".equals(input) ) return "1.0";
        if ( "1.0".equals(input) ) return "0.0";             
        return "dichotomous complimentary error";
    }
}
