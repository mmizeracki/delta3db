package com.copsys.delta.db;

/**
 *
 * @author Coping Systems Inc.
 */
import com.copsys.delta.entity.Configuration;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class DBHelper {
    
    private static final Logger logger = Logger.getLogger(DBHelper.class.getName());
    private static String[] typeS = {"SET","ENUM","VARBINARY","DECIMAL","BIGINT","INT","MEDIUMINT","SMALLINT","TINYINT","VARCHAR","CHAR"};
    
    public static String getMetadata(String dbType, String connString, String dbUser, String dbPass) throws Exception {
    //Map properties = new HashMap();
    //properties.put("JDBC_DRIVER", "oracle.jdbc.OracleDriver");
    //properties.put("JDBC_URL", "jdbc:oracle:thin:@localhost:1521:ORCL");
    //properties.put("JDBC_DRIVER", "com.mysql.jdbc.Driver");
    //properties.put("JDBC_URL", "jdbc:mysql://localhost:3306/delta3?autoReconnect=true");    
    //properties.put("JDBC_USER", "user-name");
    //properties.put("JDBC_PASSWORD", "password");
    //properties.put("JDBC_DRIVER", jdbcDriver);
    //properties.put("JDBC_URL", connString);    
    //properties.put("JDBC_USER", dbUser);
    //properties.put("JDBC_PASSWORD", dbPass);    

      String jdbcDriver = "";
      String dataString = "[";
      String schema = null;
      
      if ( dbType.equals("MySQL") ) { // the only supported at the moment
          jdbcDriver = "com.mysql.jdbc.Driver";
      } else {
        if ( dbType.equals("MS-SQL") ) { // supported?
            jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";   
            schema = "dbo";
        } else {
             throw new DatabaseNotSupportedException("Database type " + dbType + " is not supported.");
        }  
      }

      Connection conn = ConnectionCache.getInstance().getConnection(jdbcDriver, connString, dbUser, dbPass);
      Statement st = conn.createStatement();
      ResultSet mrs = conn.getMetaData().getTables(null, schema, null, new String[] { "TABLE" });
      int j = 0;
      while (mrs.next()) {
        if ( j++ != 0 ) {
           dataString += ",";
        }
        String tableName = mrs.getString(3);
        //logger.log(Level.INFO, "SQL Getting metadata for table: "+ tableName);
        dataString = dataString + "{\"text\": \""+ tableName + "\",\"qtip\":\"table\",\"leaf\":false,\"children\":[";

        ResultSet rs = st.executeQuery("select * from " + tableName);
        ResultSetMetaData metadata = rs.getMetaData();
        for (int i = 0; i < metadata.getColumnCount(); i++) {
          if ( i != 0 ) {
              dataString += ",";
          }
          //logger.log(Level.INFO, "SQL Name: "+ metadata.getColumnLabel(i + 1)+ ", Type: "+ metadata.getColumnTypeName(i + 1));
          dataString = dataString + "{\"leaf\":true,\"text\":\"" + metadata.getColumnLabel(i + 1) 
                  + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) 
                  + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) 
                  + "\",\"precision\":\"" + metadata.getPrecision(i + 1) 
                  + "\",\"scale\":\"" + metadata.getScale(i + 1)                   
                  + "\"}"; 
        }
        dataString += "]}";
        rs.close();
      }
      mrs.close();
      
      mrs = conn.getMetaData().getTables(null, schema, null, new String[] { "VIEW" });

      while (mrs.next()) {
        if ( j++ != 0 ) {
           dataString += ",";
        }
        String tableName = mrs.getString(3);
        logger.log(Level.INFO, "SQL Getting metadata for view: "+ tableName);
        dataString = dataString + "{\"text\": \""+ tableName + "\",\"qtip\":\"view\",\"leaf\":false,\"children\":[";

        ResultSet rs = st.executeQuery("select * from " + tableName);
        ResultSetMetaData metadata = rs.getMetaData();
        for (int i = 0; i < metadata.getColumnCount(); i++) {
          if ( i != 0 ) {
              dataString += ",";
          }
          //logger.log(Level.INFO, "SQL Name: "+ metadata.getColumnLabel(i + 1)+ ", Type: "+ metadata.getColumnTypeName(i + 1));
          dataString = dataString + "{\"leaf\":true,\"text\":\"" + metadata.getColumnLabel(i + 1) 
                  + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) 
                  + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) 
                  + "\",\"precision\":\"" + metadata.getPrecision(i + 1) 
                  + "\",\"scale\":\"" + metadata.getScale(i + 1)                   
                  + "\"}"; 
        }
        dataString += "]}";
        rs.close();
      }      
      dataString += "]";
      mrs.close();
      st.close();
      return dataString;
    } 
    
    public static int executeSqlStatement(String pu, String sql, Logger logger) throws Exception {
        //logger.log(Level.INFO, "SQL Executing statement: {0}", new Object[]{sql});        
        EntityManager em;  
        EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory(pu);  
        em = emf.createEntityManager();        
        try {   
            em.getTransaction().begin();
            Query q = em.createNativeQuery(sql);
            int result = q.executeUpdate();
            em.getTransaction().commit();
            return result;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "SQL Exception while executing statement {0} {1}", new Object[]{sql, ex});
            //em.close();
            throw ex;
        }
    }      
    
    public static int executeSqlStatement(EntityManagerFactory emf, String sql, Logger logger) throws Exception {
        //logger.log(Level.INFO, "SQL Executing statement: {0}", new Object[]{sql});        
        EntityManager em;  
        em = emf.createEntityManager();        
        try {   
            em.getTransaction().begin();
            Query q = em.createNativeQuery(sql);
            int result = q.executeUpdate();
            em.getTransaction().commit();
            return result;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "SQL Exception while executing statement {0} {1}", new Object[]{sql, ex});
            //em.close();
            throw ex;
        }
    }      
    
    public static Query executeSqlQuery(String pu, String sql, Logger logger) throws Exception {
        //logger.log(Level.INFO, "SQL Executing query: {0}", new Object[]{sql});            
        EntityManager em;  
        EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory(pu);   
        em = emf.createEntityManager();        
        try {   
            Query q = em.createNativeQuery(sql);
            return q;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "SQL Exception while executing query {0} {1}", new Object[]{sql, ex});
            //em.close();
            throw ex;
        }
    }    
    
    public static Query executeSqlQuery(EntityManagerFactory emf, String sql, Logger logger) throws Exception {
        //logger.log(Level.INFO, "SQL Executing query: {0}", new Object[]{sql});            
        EntityManager em;  
        em = emf.createEntityManager();        
        try {   
            Query q = em.createNativeQuery(sql);
            return q;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "SQL Exception while executing query {0} {1}", new Object[]{sql, ex});
            //em.close();
            throw ex;
        }
    }   
    
    public static String getTypeWithSize(String type, String size) {
        if ( Arrays.binarySearch(typeS,type) != -1  ) {
            return type + "(" + size + ")";
        } else {
            return type;
        }   
    }  
    
    public static String printSQLException(SQLException ex) {
        StringBuilder buffer = new StringBuilder();
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                buffer.append("SQLState: ").append (((SQLException)e).getSQLState());
                buffer.append(" Error Code: ").append (((SQLException)e).getErrorCode());
                buffer.append(" Message: ").append (e.getMessage());
                Throwable t = ex.getCause();
                while(t != null) {
                    buffer.append(" Cause: ").append (t);
                    t = t.getCause();
                }
            }
        }
        return buffer.toString();
    }    
    
    public static String printSQLGrammarException(SQLException ex) {
        StringBuilder buffer = new StringBuilder();
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                //e.printStackTrace(System.err);
                buffer.append("SQLState: ").append (((SQLException)e).getSQLState());
                buffer.append(" Error Code: ").append (((SQLException)e).getErrorCode());
                buffer.append(" Message: ").append (e.getMessage());
                Throwable t = ex.getCause();
                while(t != null) {
                    buffer.append(" Cause: ").append (t);
                    t = t.getCause();
                }
            }
        }
        return buffer.toString();
    }    

    public static String getDBObjectFields(String dbType, String connectionInfo, String dbUser, String dbPassword, String objectName) {
        
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        
        try {
            String jdbcDriver = "";
            String dataString = "[";
            String schema = null;

            if ( dbType.equals("MySQL") ) { // the only supported at the moment
                jdbcDriver = "com.mysql.jdbc.Driver";
            } else {
              if ( dbType.equals("MS-SQL") ) { // supported?
                  jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";   
                  schema = "dbo";
              } else {
                   throw new DatabaseNotSupportedException("Database type " + dbType + " is not supported.");
              }  
            }    

            conn = ConnectionCache.getInstance().getConnection(jdbcDriver, connectionInfo, dbUser, dbPassword);
            st = conn.createStatement();
            int j = 0;
            rs = st.executeQuery("select * from " + objectName);
            ResultSetMetaData metadata = rs.getMetaData();            
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                if ( i != 0 ) {
                    dataString += ",";
                }
                logger.log(Level.INFO, "SQL Name: "+ metadata.getColumnLabel(i + 1)+ ", Type: "+ metadata.getColumnTypeName(i + 1).toUpperCase());
                dataString = dataString + "{\"label\":\"" + metadata.getColumnLabel(i + 1) 
                        + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) 
                        + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) 
                        + "\",\"precision\":\"" + metadata.getPrecision(i + 1) 
                        + "\",\"scale\":\"" + metadata.getScale(i + 1)                   
                        + "\"}"; 
            }
            dataString += "]";              
            return dataString;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQL Exception while retrieving db info about DBObject: {0} {1}", new Object[]{objectName, ex});
        } catch (DatabaseNotSupportedException ex) {
            logger.log(Level.SEVERE, "SQL Exception while retrieving db info about DBObject: {0} {1}", new Object[]{objectName, ex});
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[]{objectName, ex});
            }
        }        
        return null;
    }

    public static String getObjectCount(Connection conn, String objectName, String filter) {
        
        Statement st = null;
        ResultSet rs = null;
        
        try {      
             st = conn.createStatement();
             if ( !"".equals(filter)) {
                rs = st.executeQuery("select count(*) from " + objectName + " where " + filter);                 
             } else {
                rs = st.executeQuery("select count(*) from " + objectName);
             }
             rs.next();
             int count = rs.getInt(1);  
             return Integer.toString(count);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[]{objectName, ex});
            }          
        }          
        return "Error getting table size";
    }
    
    public static ResultSet executeJdbcQuery(Configuration conConf, String sqlStmt, Logger logger) throws SQLException, DatabaseNotSupportedException {       
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        
        try {
            String jdbcDriver = "";
            String dataString = "[";
            String schema = null;

            if ( conConf.getType().equals("MySQL") ) { // the only supported at the moment
                jdbcDriver = "com.mysql.jdbc.Driver";
            } else {
              if ( conConf.getType().equals("MS-SQL") ) { // supported?
                  jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";   
                  schema = "dbo";
              } else {
                   throw new DatabaseNotSupportedException("Database type " + conConf.getType() + " is not supported.");
              }  
            }    
            conn = ConnectionCache.getInstance().getConnection(jdbcDriver, conConf.getConnectionInfo(), conConf.getDbUser(), conConf.getDbPassword());
            st = conn.createStatement();
            int j = 0;
            rs = st.executeQuery(sqlStmt);                      
            return rs;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQL Exception while JDBC executing query: {0} {1}", new Object[]{sqlStmt, ex});
            throw ex;
        } catch (DatabaseNotSupportedException ex) {
            logger.log(Level.SEVERE, "SQL Exception while JDBC executing query: {0} {1}", new Object[]{sqlStmt, ex});
            throw ex;
        } finally {
            /*try {
                rs.close(); // close after data is read
                st.close();
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "SQL Exception while closing db connection of query: {0} {1}", new Object[]{sqlStmt, ex});
            }*/
        }        
    }
 
    public static boolean executeJdbcStatement(Configuration conConf, String sqlStmt, Logger logger) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        
        try {
            String jdbcDriver = "";
            String dataString = "[";
            String schema = null;

            if ( conConf.getType().equals("MySQL") ) { // the only supported at the moment
                jdbcDriver = "com.mysql.jdbc.Driver";
            } else {
              if ( conConf.getType().equals("MS-SQL") ) { // supported?
                  jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";   
                  schema = "dbo";
              } else {
                   throw new DatabaseNotSupportedException("Database type " + conConf.getType() + " is not supported.");
              }  
            }    
            conn = ConnectionCache.getInstance().getConnection(jdbcDriver, conConf.getConnectionInfo(), conConf.getDbUser(), conConf.getDbPassword());
            st = conn.createStatement();
            int j = 0;
            return st.execute(sqlStmt);                      
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQL Exception while JDBC executing statement: {0} {1}", new Object[]{sqlStmt, ex});
        } catch (DatabaseNotSupportedException ex) {
            logger.log(Level.SEVERE, "SQL Exception while JDBC executing statement: {0} {1}", new Object[]{sqlStmt, ex});
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "SQL Exception while closing db connection of statment: {0} {1}", new Object[]{sqlStmt, ex});
            }
        }        
        return true;
    }    
}