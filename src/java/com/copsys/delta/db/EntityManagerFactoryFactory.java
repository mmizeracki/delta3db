/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.db;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 *
 * @author Coping Systems Inc.
 */
public class EntityManagerFactoryFactory {
  EntityManagerFactory emf = null;   
  
  public EntityManagerFactoryFactory (String userName, String password, String URI) {
    Map<String, String> properties = new HashMap<String, String>();
    properties.put("javax.persistence.jdbc.user", userName);
    properties.put("javax.persistence.jdbc.password", password);
    emf = Persistence.createEntityManagerFactory(URI, properties);   
  }
  
  public EntityManager getEntityManager() {
    return emf.createEntityManager();
  }
}
