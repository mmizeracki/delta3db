/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.db;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {

  private static final Logger logger = Logger.getLogger(PersistenceManager.class.getName());

  private static final PersistenceManager singleton = new PersistenceManager();

  protected static EntityManagerFactory emf;

  public static PersistenceManager getInstance() {

    return singleton;
  }

  private PersistenceManager() {
  }

  public EntityManagerFactory getEntityManagerFactory(String PU) {

    if (emf == null)
      createEntityManagerFactory(PU);
    return emf;
  }

  public static void closeEntityManagerFactory() {

    if (emf != null) {
      emf.close();
      emf = null;
      logger.log(Level.INFO, "Persistence Manager Factory destroyed");
    }
  }

  protected void createEntityManagerFactory(String PU) {

    logger.log(Level.SEVERE, "Creating Persistence Manager Factory for PU: {0}", PU);
    try {
        this.emf = Persistence.createEntityManagerFactory(PU);    
        logger.log(Level.INFO,"Persistence Manager Factory created");
    } catch (Exception ex) {
       logger.log(Level.SEVERE, "Persistence Manager Factory exception {0}\n{1}", new Object[]{ex.getMessage(), ex.toString()}); 
    }
  }
}
