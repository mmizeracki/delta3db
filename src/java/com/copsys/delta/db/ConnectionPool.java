package com.copsys.delta.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


/** A class for preallocating, recycling, and managing
 *  JDBC connections.
 *  based on Tomcat JDI Connection Pool DBCP library
 */

public class ConnectionPool {

  private static Logger logger = Logger.getLogger(ConnectionPool.class.getName());
  private static Context initialContext = null;
  private static final String DATASOURCE_CONTEXT = "java:comp/env/jdbc/mydb";
  private DataSource datasource = null;


    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance()
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder  {
      private static final ConnectionPool INSTANCE = new ConnectionPool();
    }

    public static ConnectionPool getInstance() {
      return SingletonHolder.INSTANCE;
    }

  private ConnectionPool() {
    logger.log(Level.INFO,"Creating initial Context and Datasource...");
    try {
        initialContext = new InitialContext();
        if ( initialContext == null){
            logger.log(Level.SEVERE,"JNDI problem. Cannot get InitialContext.");
        }
        datasource = (DataSource)initialContext.lookup(DATASOURCE_CONTEXT);
        if (datasource == null) {
            logger.log(Level.SEVERE,"Failed to lookup DataSource.");
        }
    } catch (NamingException ex) {
        logger.log(Level.SEVERE, "Exception while obtaining inital Context and DataSource: ", ex);
    }
  }

  public Connection getConnection() {
        try {
            return datasource.getConnection();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,"Cannot get connection: " + ex);
        }
        return null;
  }


  public void free(Connection connection) {
        try {
            connection.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error freeing Connection: ", ex);
        }
        connection = null;
  }

}
