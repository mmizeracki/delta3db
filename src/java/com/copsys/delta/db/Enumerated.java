/*
 * This class contains "business logic for calculationg stats for Dichotomous Fields
 */

package com.copsys.delta.db;

import com.copsys.delta.server.StatServiceImpl;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class Enumerated {
    private static int limit = 100;
    private static final Logger logger = Logger.getLogger(StatServiceImpl.class.getName());        
    private String name;
    private String tableName;
    private String[] value;
    private String nullValue;
    private int[] count;
    private int size;
    private int nullCount;
    private int totalCount;
   
    public Enumerated (String tblName, String clmnName) {          
        try {
            Query queryResult;  
            tableName = tblName;
            name = clmnName;
            
            /* obtain total count of all rows in target table */
            String sqlStmt = "select count(*) from " + tableName;        
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger); 
            List<Object[]> resultCounts = queryResult.getResultList(); 
            String tmp = String.valueOf(resultCounts.get(0));                        
            totalCount = Integer.parseInt(tmp);
            
            /* get all possible values for this field */
            sqlStmt = "select " + name + " from " + tableName + " GROUP BY " + name;     
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
            List<Object[]> resultValues = queryResult.getResultList(); 
            size = resultValues.size();
            if ( size > limit ) {
                size = limit;
            }
            value = new String[size];
            count = new int[size];
            
            /* get the number of occurrences of those values, null will yield 0 */
            sqlStmt = "select count(" + name + ") from " + tableName + " GROUP BY " + name;  
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger); 
            resultCounts = queryResult.getResultList(); 
            
            for ( int i =0; i < size; i++ ) {    
                if ( resultValues.get(i) == null ) {
                    nullValue = "NULL";    
                    tmp = String.valueOf(resultCounts.get(i));
                    nullCount = Integer.parseInt(tmp);
                } else {
                    String c = "";
                    c += resultValues.get(i);
                    value[i] = c;
                    tmp = String.valueOf(resultCounts.get(i));                        
                    count[i] = Integer.parseInt(tmp);
                }
            }   
        } catch (Exception ex) {
            Logger.getLogger(Enumerated.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public String getName() {
        return name;
    }
    public String getTableName() {
        return tableName;
    }    
    public String[] getValue() {
        return value;
    }
    public String getNullValue() {
        return nullValue;
    }    
    public int[] getCount() {
        return count;
    }
    public int getSize() {
        return size;
    }    
    public int getNullCount() {
        return nullCount;
    }      
    public int getTotalCount() {
        return totalCount;
    }     
}
