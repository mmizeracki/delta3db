package com.copsys.delta.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/* A class for caching single connection to database. 
 * And/or EnityManager
 * Connection string is the unique key.
 */

public class ConnectionCache {

  private static final Logger logger = Logger.getLogger(ConnectionCache.class.getName());
  private static Map<String, Connection> connCache = new HashMap<String, Connection>();
  private static Map<String, EntityManager> emCache = new HashMap<String, EntityManager>();


    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance()
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder  {
      private static final ConnectionCache INSTANCE = new ConnectionCache();
    }

    public static ConnectionCache getInstance() {
      return SingletonHolder.INSTANCE;
    }

  private ConnectionCache() {
    logger.log(Level.INFO,"Creating initial cache of db connection...");
    try {
    } catch (Exception ex) {
        logger.log(Level.SEVERE, "Exception while obtaining inital cache of db connections: ", ex);
    }
  }

  public Connection getConnection(String jdbcDriver, String connString, String dbUser, String dbPass) {
        try {
            if ( connCache.containsKey(connString + dbUser + dbPass)) {
                logger.log(Level.INFO,"Retrieving db connection from cache: " + connString + dbUser + "dbPass");                
                return connCache.get(connString + dbUser + dbPass );
            } else {
                logger.log(Level.INFO,"Adding db connection to cache: " + connString + dbUser + "dbPass");                    
                Class.forName(jdbcDriver);
                Connection conn = DriverManager.getConnection(connString, dbUser, dbPass);   
                connCache.put(connString + dbUser + dbPass, conn);
                return conn;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,"Cannot get connection: " + ex);
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "JDBC driver " + jdbcDriver + " not found: ", ex);
      }
        return null;
  }

  public EntityManager getEntitytManager(String jdbcDriver, String connString, String dbUser, String dbPass) {
        try {
            if ( emCache.containsKey(connString + dbUser + dbPass)) {
                logger.log(Level.INFO,"Retrieving db connection from cache: " + connString + dbUser + "dbPass");                   
                return emCache.get(connString + dbUser + dbPass);
            } else {
                logger.log(Level.INFO,"Adding db Entity Manager to cache: " + connString + dbUser + "dbPass");                    
                Map<String, String> properties = new HashMap<String, String>();
                properties.put("javax.persistence.jdbc.driver", jdbcDriver);
                properties.put("javax.persistence.jdbc.user", dbUser);
                properties.put("javax.persistence.jdbc.password", dbPass);
                EntityManagerFactory emf = Persistence.createEntityManagerFactory(connString, properties);   
                EntityManager em = emf.createEntityManager();
                emCache.put(connString + dbUser + dbPass, em);
                return em;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE,"Cannot get Entity Manager: " + ex);
        } 
        return null;
  }
  
  public void free(Connection connection) {
        try {
            //connCache.clear();
            connection.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error freeing Connection: ", ex);
        }
        connection = null;
  }

}
