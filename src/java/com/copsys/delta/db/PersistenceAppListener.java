/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.copsys.delta.db;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class PersistenceAppListener implements ServletContextListener {

  public void contextInitialized(ServletContextEvent evt) {
  }

  public void contextDestroyed(ServletContextEvent evt) {

    PersistenceManager.getInstance().closeEntityManagerFactory();
  }
}
