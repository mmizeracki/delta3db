/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.delta.db;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Boston2A Inc.
 */


public class NotDichotomousException extends Exception {
    private List<String> messages;

    NotDichotomousException(String msg) {
        messages = new ArrayList();
        messages.add(msg);
    }

    public List<String> getMessages() {
        return messages;
    }
    
    public String getMessage() {
        return messages.get(0);
    }    
}
