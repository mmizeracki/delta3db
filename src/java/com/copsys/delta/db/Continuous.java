/*
 * This class contains "business logic for calculationg stats for Continuous2 Fields
 * including formating decimals
 */
package com.copsys.delta.db;

import com.copsys.delta.server.StatServiceImpl;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class Continuous {
    private static final Logger logger = Logger.getLogger(StatServiceImpl.class.getName());        
    private final String name;
    private final String tableName;
    private final String minValue;
    private String meanValue;
    private final String medianValue;
    private String stdValue;
    private final String maxValue;
    private String nullValue;
    private final String value75;
    private final String value25;
    private final String filterString;
    private final int notNullCount;
    private final int nullCount;
    private final int totalCount;
    private final String notNullPercent;
   
    public Continuous (String tblName, String clmnName, String filter) throws Exception {          
        Query queryResult;  
        tableName = tblName;
        name = clmnName;
        filterString = filter;
        DecimalFormat df = new DecimalFormat("#.##");

        /* get totalCount, min, median, max etc for this field */
        String sqlStmt = "select " + name + " from " + tableName;    
        if ( !"".equals(filterString) ) {
           sqlStmt += " where " + filterString;     
        }    
        sqlStmt += " order by " + name;
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
        queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
        List<Object[]> resultValues = queryResult.getResultList();   
        totalCount = resultValues.size();
        
        int nullIdx = 0;
        // count and skip nulls, they are at the beginning of results list
        while ( (nullIdx < totalCount) && (resultValues.get(nullIdx) == null) ) {
            nullIdx++; 
        }
        nullCount = nullIdx;
        
        if ( resultValues.get(nullIdx) != null ) {
            minValue = df.format(resultValues.get(nullIdx));
        } else {
            minValue = " ";
        }

        if ( resultValues.get(totalCount-1) != null ) {            
            maxValue = df.format(resultValues.get(totalCount-1));
        } else {
            maxValue = " ";
        }

        notNullCount = totalCount - nullCount;
        if ( totalCount != 0 ) {
            notNullPercent = df.format(100 * notNullCount/totalCount);        
        } else {
            notNullPercent = df.format(0);
        }
               
        if ( nullCount != totalCount ) {
            int medianIdx = nullIdx + notNullCount /2;
            if ( notNullCount % 2 == 0 ) {
                Double medianValue1 = Double.valueOf(String.valueOf(resultValues.get(medianIdx - 1)));  
                Double medianValue2 = Double.valueOf(String.valueOf(resultValues.get(medianIdx)));   
                double medianMean = (medianValue1 + medianValue2)/2;
                medianValue = df.format(medianMean);                
            } else {
                medianValue = df.format(resultValues.get(medianIdx)); // table starts with 0 index!
            }

            int halfNotNullCount = notNullCount / 2;
            int median25Idx = nullIdx + halfNotNullCount/2;
            if ( median25Idx % 2 != 0 ) {
                Double mValue25_1 = Double.valueOf(String.valueOf(resultValues.get(median25Idx-1)));  
                Double mValue25_2 = Double.valueOf(String.valueOf(resultValues.get(median25Idx)));   
                double medianMean25 = (mValue25_1 + mValue25_2)/2;
                value25 = df.format(medianMean25);  
                Double mValue75_1 = Double.valueOf(String.valueOf(resultValues.get(medianIdx + halfNotNullCount/2)));  
                Double mValue75_2 = Double.valueOf(String.valueOf(resultValues.get(medianIdx + 1 + halfNotNullCount/2)));   
                double medianMean75 = (mValue75_1 + mValue75_2)/2;
                value75 = df.format(medianMean75);                 
            } else {
                value25 = df.format(resultValues.get(median25Idx));
                value75 = df.format(resultValues.get(totalCount - halfNotNullCount/2 - 1));
            }
            /* get mean value for this field */
            sqlStmt = "select AVG(" + name + ") from " + tableName;    
            if ( !"".equals(filterString) ) {
               sqlStmt += " where " + filterString;     
            }           
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
            resultValues = queryResult.getResultList();      
            meanValue = String.valueOf(resultValues.get(0));
            meanValue = df.format(Double.parseDouble(meanValue));

            /* get standard dev value for this field */
            sqlStmt = "select STD(" + name + ") from " + tableName;    
            if ( !"".equals(filterString) ) {
               sqlStmt += " where " + filterString;     
            }           
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
            queryResult = DBHelper.executeSqlQuery("DeltaPU",sqlStmt,logger);     
            resultValues = queryResult.getResultList();      
            stdValue = String.valueOf(resultValues.get(0));
            stdValue = df.format(Double.parseDouble(stdValue));              
        } else {
                medianValue = " ";             
                value25 = " ";
                value75 = " ";      
                meanValue = " ";
                stdValue = " ";
        }

              
    }
    
    public String getName() {
        return name;
    }
    public String getTableName() {
        return tableName;
    }    
    public String getMinValue() {
        return minValue;
    }
    public String getMaxValue() {
        return maxValue;
    }
    public String getMeanValue() {
        return meanValue;
    }
    public String getMedianValue() {
        return medianValue;
    }
    public String getStdValue() {
        return stdValue;
    }    
    public String getValue75() {
        return value75;
    }
    public String getValue25() {
        return value25;
    }
    public String getNullValue() {
        return nullValue;
    }    
    public String getFilterString() {
        return filterString;
    }  
    public int getNotNullCount() {
        return notNullCount;
    }     
    public int getNullCount() {
        return nullCount;
    }     
    public int getTotalCount() {
        return totalCount;
    }  
    public String getNotNullPercent() {
        return notNullPercent;
    }      
}
