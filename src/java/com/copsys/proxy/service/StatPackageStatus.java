package com.copsys.proxy.service;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class for keeping track of external statistical package status
 */
public class StatPackageStatus {

    private static final Logger logger = Logger.getLogger(StatPackageStatus.class.getName());
    private static String status = "Not connected";

    /**
     * SingletonHolder is loaded on the first execution of
     * Singleton.getInstance() or the first access to SingletonHolder.INSTANCE,
     * not before.
     */
    private static class SingletonHolder {

        private static final StatPackageStatus INSTANCE = new StatPackageStatus();
    }

    public static StatPackageStatus getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public StatPackageStatus() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String newStatus) {
        status = newStatus;
        logger.log(Level.SEVERE, "Status of external stat package changed to {0}", newStatus);
    }
}
