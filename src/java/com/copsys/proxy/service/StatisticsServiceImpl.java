/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.copsys.proxy.service;

import com.copsys.delta.entity.process.Process;
import com.copsys.delta.jpa.process.ProcessJpaController;
import com.copsys.delta.server.StatServiceImpl;
import com.copsys.delta.server.StudyServiceImpl;
import com.copsys.delta.util.JSON.JSONArray;
import com.copsys.delta.util.JSON.JSONException;
import com.copsys.delta.util.JSON.JSONObject;
import com.copsys.statproxy.services.StatisticsServiceDelta;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;


/**
 *
 * @author Coping Systems Inc.
 * This is an interface definition for calls made out of Statistical Package
 */
public class StatisticsServiceImpl implements StatisticsServiceDelta {
    private static final Logger logger = Logger.getLogger(StatisticsServiceImpl.class.getName());    
    private StatServiceImpl statService;
    private StudyServiceImpl studyService;    
    private ProcessJpaController processController;
    
    @Override
    // this method will be called by proxy to log in
    public int login(String userAlias, String password) {
        logger.log(Level.SEVERE, "login call to Proxy returned");
        return 0;
    };
 
    @Override
    public int loginCallBack(String string) {
        logger.log(Level.SEVERE, "DELTA3 loginCallBack call to Proxy returned");
        return 0;        
   }

    @Override
    public int getStatConfig() {
        logger.log(Level.SEVERE, "DELTA3 getStatConfig call to Proxy returned");
        return 0;        
   }

    @Override
    public int getStatConfigCallBack(String JSONString) {
        logger.log(Level.SEVERE, "DELTA3 getStatConfigCallBack call to Proxy returned: " + JSONString);
        if ( studyService == null ) {
            studyService = new StudyServiceImpl();
        }
        studyService.saveStudyMethods(JSONString, 1, 0); // defaults to org 1 and system user 0       
        return 0;        
    }

    @Override
    // this method will be used by proxy to inquire about process status
    public int getProcessStatus(String JSONString) {
        logger.log(Level.SEVERE, "DELTA3 getProcessStatus call to Proxy returned" + JSONString);                    
        return 0;        
    };   
    
    @Override
    public int getProcessStatusCallBack(String JSONString) {
        logger.log(Level.SEVERE, "DELTA3 getProcessStatusCallBack call to Proxy returned: " + JSONString);
        if ( "Proxy error".equals(JSONString) ) {
            return -1;
        }
        Integer userId = new Integer(0); // means "system"
        
        if ( processController == null ) {
            processController = new ProcessJpaController();
        } 
        
        try {
            JSONObject responseObject =  new JSONObject(JSONString);
            JSONArray processArray = responseObject.getJSONArray("processes");
            for (int i=0; i<processArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                Object subject = gson.fromJson(jString, Process.class);   
                Process originalProcess = ((ProcessJpaController)processController).findProcess(((Process)subject).getIdProcess());
                if ( originalProcess != null ) {
                    originalProcess.setProgress(((Process)subject).getProgress());
                    originalProcess.setStatus(((Process)subject).getStatus());     
                    originalProcess.setMessage(((Process)subject).getMessage());
                    subject = processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);       
                    if ( ((Process)subject).getProgress() == 100 ) {
                        if ( statService == null ) {
                            statService = new StatServiceImpl();
                        }      
                        statService.getStatResult(((Process)subject).getIdProcess().toString(), ((Process)subject).getGuid(), "system");
                    }
                } else {
                    logger.log(Level.SEVERE, "Received Process ID that does not match local database: {0}", new Object[]{((Process)subject).getIdProcess()});                       
                    }
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON Exception while creating new Object: {0}", new Object[]{ex});            
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while creating new Object: {0}", new Object[]{ex});
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0}", new Object[]{ex});
        }                 
       
        return 0;        
    }

    @Override
    public String storeDoubleArray(double[] doubles) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int storeDoubleArrayCallBack(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int sumbitStatJob(String string) {
        logger.log(Level.SEVERE, "DELTA3 submitStatJob call to Proxy returned: {0}", string);     
        return 0;
    }

    @Override
    public int sumbitStatJobCallBack(String JSONString) {
        logger.log(Level.SEVERE, "DELTA3 submitStatJobCallBack call to Proxy returned: {0}", JSONString);   
        if ( "Proxy error".equals(JSONString) ) {
            return -4;
        }  
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }
        try {
            return statService.receiveSubmit(JSONString);
        } catch (Exception ex) {
            logger.log(Level.SEVERE,  null, ex);
            return -3;
        }        
   }
    
    @Override
    public int getResults(String string) {
        logger.log(Level.SEVERE, "DELTA3 getResults call to Proxy returned: {0}", string); 
        return 0;
    }

    @Override
    public int getResultsCallBack(String JSONString) {
        logger.log(Level.SEVERE, "DELTA3 getResultsCallBack call to Proxy returned");  
        if ( "Proxy error".equals(JSONString) ) {
            return -4;
        }  
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }
        try {
            return statService.receiveResults(JSONString);
        } catch (Exception ex) {
            logger.log(Level.SEVERE,  null, ex);
            return -3;
        }
    }
}
