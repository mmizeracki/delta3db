-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.6.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_permissiontemplate`
--

DROP TABLE IF EXISTS `d3_permissiontemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_permissiontemplate` (
  `idPermissionTemplate` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `Module_idModule` int(11) NOT NULL,
  PRIMARY KEY (`idPermissionTemplate`,`Module_idModule`),
  KEY `fk_PermissionTemplate_Module1_idx` (`Module_idModule`),
  CONSTRAINT `fk_PermissionTemplate_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_permissiontemplate`
--

LOCK TABLES `d3_permissiontemplate` WRITE;
/*!40000 ALTER TABLE `d3_permissiontemplate` DISABLE KEYS */;
INSERT INTO `d3_permissiontemplate` VALUES (1,2,'2013-10-30 14:04:51',2,'2013-10-30 14:37:09','Admin Users','administer users',90,'menu','Users',1,1),(2,2,'2013-10-30 14:38:39',2,'2013-10-30 14:43:57','Admin Roles','administer roles',92,'menu','Roles',1,1),(3,2,'2013-10-30 14:41:16',2,'2013-10-30 14:44:08','Admin Groups','administer groups',94,'menu','Groups',0,1),(4,2,'2013-10-30 14:43:34',2,'2013-10-30 14:44:15','Admin Organizations','administer organizations',96,'menu','Organizations',1,1),(5,2,'2013-11-01 20:27:14',2,'2014-01-21 20:36:08','Results','user of statistical package',30,'menu','Processes',1,2),(6,2,'2014-01-20 17:00:00',2,'2014-01-21 20:36:08','Data Models','access to Model Builder',10,'menu','Models',1,2),(7,2,'2014-01-20 17:00:00',2,'2014-01-20 17:00:00','Permissions','administer permission templates',98,'menu','Permissions',1,1),(8,2,'2014-03-12 16:00:00',2,'2014-06-10 21:22:38','Table Viewer','access to Table Viewer',1001,'button','TableViewer',1,2),(9,2,'2014-06-30 16:00:00',2,'2014-06-30 16:00:00','Database Connections','program configuration',84,'menu','Configurations',1,1),(10,2,'2014-07-03 16:00:00',2,'2014-07-03 16:00:00','Studies','tools to create Studies',20,'menu','Studies',1,3),(11,2,'2014-07-03 17:00:00',2,'2014-07-03 17:00:01','Method Configurator','statistical method configuration',86,'menu','Methods',1,1),(12,1,'2014-07-11 17:41:00',1,'2014-07-11 17:41:00','Filters','administer filters for studies',1002,'button','Filters',1,3),(13,2,'2014-07-29 15:11:11',2,'2014-07-29 15:11:11','Delete Studies','allow deletion of studies',1003,'button','StudyDelete',1,3),(14,2,'2014-08-01 15:11:00',2,'2014-08-04 13:02:10','Logistic Regression','create and maintain log regression formulas',40,'menu','Logregrs',1,4),(15,2,'2014-09-08 16:42:38',2,'2014-09-08 16:42:38','Delete Processes','allow deletion of processes',1004,'button','ProcessDelete',1,1),(16,2,'2014-09-12 20:41:44',2,'2014-09-12 20:41:44','Events','events, alerts, notifications',70,'menu','Events',1,5),(17,2,'2014-09-15 17:29:49',2,'2014-09-15 17:29:49','Event Templates','administer event templates',80,'menu','Eventtemplates',1,5),(18,2,'2014-09-15 21:02:48',2,'2014-09-15 21:02:48','Alerts','set up alerts, receive notifications',50,'menu','Alerts',1,5),(19,2,'2014-09-17 19:37:57',2,'2014-09-17 19:37:57','Notifications','view and acknowledge notifications',60,'menu','Notifications',1,5),(20,2,'2014-10-24 20:59:39',2,'2014-10-24 21:00:37','Delete Models','allow deletion of models',1005,'button','ModelDelete',1,1),(21,2,'2014-11-11 13:36:35',2,'2014-11-11 13:36:35','Pseudo','become another user',1006,'button','Pseudo',1,1);
/*!40000 ALTER TABLE `d3_permissiontemplate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-25 13:56:27
