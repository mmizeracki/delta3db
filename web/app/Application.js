/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('delta3.Application', {
    extend: 'Ext.app.Application',
    name: "delta3",
    requires: [
        'delta3.utils.GlobalVars',
        'delta3.utils.GlobalFunc',
        'delta3.utils.Tooltips',        
        'Ext.window.MessageBox',
        'delta3.view.Viewport',
        'delta3.utils.GridPanel'
    ],
    controllers: ['delta3.controller.Main'],
    autoCreateViewport: false,
    launch: function() {
        Ext.create('delta3.utils.GlobalVars');
        Ext.create('delta3.utils.GlobalFunc');
        Ext.create('delta3.utils.Tooltips');        
        delta3.utils.GlobalFunc.getCurrentUserAuthInfo(delta3.utils.GlobalVars.currentUserAuthInfo);
        // pre-load in-memory data stores for fast lookups
        delta3.utils.GlobalVars.OrgStore = Ext.create('delta3.store.OrganizationStore');
        delta3.utils.GlobalVars.OrgStore.load();
        delta3.utils.GlobalVars.RoleStore = Ext.create('delta3.store.RoleStore');
        delta3.utils.GlobalVars.RoleStore.load();
        delta3.utils.GlobalVars.PermissionStore = Ext.create('delta3.store.PermissionStore');
        delta3.utils.GlobalVars.PermissionStore.load();
        delta3.utils.GlobalVars.FieldStore = Ext.create('delta3.store.FieldStore');
        delta3.utils.GlobalVars.FieldStore.load();
        delta3.utils.GlobalVars.UserStore = Ext.create('delta3.store.UserStore');
        delta3.utils.GlobalVars.UserStore.load();        
    }
});

//------------------------------------------------------------ general purpose UI utility functions
function doLogout()
{
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/admin/logout',
                        method: "POST",
                        disableCaching: true,
                        success: doLogoutSuccess,
                        failure: doLogoutFailed
                    }
            );
}

function doLogoutSuccess(response, options)
{
    setTimeout(gotoLoginPage, 1000);
}

function doLogoutFailed(response, options)
{
    setTimeout(gotoLoginPage, 1000);
}

function gotoLoginPage()
{
    window.location = './index.html';
}

