/* by sdruker, Druck-I.T. */
Ext.define('delta3.utils.GridPanel', {
    override: 'Ext.grid.GridPanel',
    requires: 'Ext.form.action.StandardSubmit',
    dateFormat : 'Y-m-d g:i',

    exportGrid: function(title) {
        if (!title) title = this.title;
        if (Ext.isIE) {
            //this._ieToExcel(this);
            var data = this._getCSV(this);
            var form = this.down('form#uploadForm');
            if (form) {
                form.destroy();
            }
            form = this.add({
                xtype: 'form',
                itemId: 'uploadForm',
                hidden: true,
                standardSubmit: true,
                url: 'http://webapps.figleaf.com/dataservices/Excel.cfc?method=echo&mimetype=application/vnd.ms-excel&filename=' + escape(title + ".csv"),
                items: [{
                    xtype: 'hiddenfield',
                    name: 'data',
                    value: data
                }]
            });
            form.getForm().submit();          
        } else {
            var data = this._getCSV(this);
            //window.location = 'data:text/csv;charset=utf8,' + encodeURIComponent(data);
            var location = 'data:text/csv;charset=utf8,' + encodeURIComponent(data); 
            var gridEl = this.getEl(); 
            var el = Ext.DomHelper.append(gridEl, {
                tag: "a",
                download: title + "-" + Ext.Date.format(new Date(), 'Y-m-d Hi') + '.csv',
                href: location
            });
            el.click();
            Ext.fly(el).destroy();            
        }
    },

    _escapeForCSV: function(string) {
        if (string.match(/,/)) {
            if (!string.match(/"/)) {
                string = '"' + string + '"';
            } else {
                string = string.replace(/,/g, ''); // comma's and quotes-- sorry, just loose the commas
            }
        }
        return string;
    },

    _getFieldText: function(fieldData) {
        var text;

        if (fieldData == null || fieldData == undefined) {
            text = '';
        } else if (fieldData._refObjectName && !fieldData.getMonth) {
            text = fieldData._refObjectName;
        } else if (fieldData instanceof Date) {
            text = Ext.Date.format(fieldData, this.dateFormat);
        } else if (typeof(fieldData) === "number" ) { 
            text = '' + fieldData; 
        } else if (typeof(fieldData) === "boolean" ) { 
            text = '' + fieldData; 
        } else if (!fieldData.match || fieldData === '&#160;') { // not a string or object we recognize... or action column
            text = '';
        } else {
            text = fieldData;
        }

        return text;
    },

    _getFieldTextAndEscape: function(fieldData) {
        var string  = this._getFieldText(fieldData);

        return '"' + this._escapeForCSV(string) + '"'; // added "" to eliminate "SYLK" issue https://support.microsoft.com/kb/215591/EN-US
    },

    _getCSV: function (grid) {
        var cols    = grid.columns;
        var store   = grid.store;
        var data    = '';

        var that = this;
        Ext.Array.each(cols, function(col, index) {
            if (col.hidden != true) {
                data += that._getFieldTextAndEscape(col.text) + ',';
            }
        });
        data += "\n";

        store.each(function(record) {
            var entry       = record.getData();
            Ext.Array.each(cols, function(col, index) {
                if (col.hidden != true) {
                    var fieldName   = col.dataIndex;
                    var text        = entry[fieldName];

                    data += that._getFieldTextAndEscape(text) + ',';
                }
            });
            data += "\n";
        });

        return data;
    },

    _ieGetGridData : function(grid, sheet) {
        var that            = this;
        var resourceItems   = grid.store.data.items;
        var cols            = grid.columns;

        Ext.Array.each(cols, function(col, colIndex) {
            if (col.hidden != true) {
                sheet.cells(1,colIndex + 1).value = col.text;
            }
        });

        var rowIndex = 2;
        grid.store.each(function(record) {
            var entry   = record.getData();

            Ext.Array.each(cols, function(col, colIndex) {
                if (col.hidden != true) {
                    var fieldName   = col.dataIndex;
                    var text        = entry[fieldName];
                    var value       = that._getFieldText(text);

                    sheet.cells(rowIndex, colIndex+1).value = value;
                }
            });
            rowIndex++;
        });
    },

    _ieToExcel: function (grid) {
        if (window.ActiveXObject){
            var  xlApp, xlBook;
            try {
                xlApp = new ActiveXObject("Excel.Application"); 
                xlBook = xlApp.Workbooks.Add();
            } catch (e) {
                Ext.Msg.alert('Error', 'For the export to work in IE, you have to enable a security setting called "Initialize and script ActiveX control not marked as safe" from Internet Options -> Security -> Custom level..."');
                return;
            }

            xlBook.worksheets("Sheet1").activate;
            var XlSheet = xlBook.activeSheet;
            xlApp.visible = true; 

           this._ieGetGridData(grid, XlSheet);
           XlSheet.columns.autofit; 
        }
    }
});