Ext.define('delta3.utils.GlobalFunc', {
        statics: {  
        jsonDeserializeHelper: function (key,value) {
          if ( typeof value === 'string' ) {
            var regexp;
            regexp = /^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\dZ$/.exec(value);
            if ( regexp ) {
              return new Date(value);
            }
          }
          return value;
        },      
        checkJDBCFieldNameValid: function (value) {
          if ( typeof value === 'string' ) {
            var regexp;
            regexp = /^[a-zA-Z_][a-zA-Z0-9_]*$/.exec(value);
            if ( regexp ) {
              return true;
            }
          }
          return false;
        },         
        //--------------------------------------  general purpose messages
        popupErrorRedirect: function(message) {
            Ext.Msg.alert("DELTA3 Error", message, function() {
                if ( message === "Session timeout.\nPlease login again." ) {
                    window.location = 'index.html'; 
                }
            });        
        },  
        showRemoteException: function (proxy, type, action, o, result, records, message) {
                if ( type === 'remote' ) {
                    Ext.Msg.alert("DELTA3. Could not " + action, result.raw.message);
                } else {
                    if ( type === 'response' ) {
                        Ext.Msg.alert("DELTA3. Could not " + action, "Server's response could not be decoded");
                    } else {
                        Ext.Msg.alert("DELTA3. Error", message);
                    }
                }
        },
        showDeltaMessage: function (message) {
                Ext.Msg.alert("DELTA3", message);
        },        
        //--------------------------------------  create generic comboBox store based on another store
        createComboBoxStore: function(store, fieldId) {
            var values = [];
            for ( var i=0; i<store.data.length; i++) {
                values[i] = {value: store.getAt(i).get(fieldId)};
            }
            var cbStore =  Ext.create('Ext.data.Store', {
                            fields: ['value'], 
                            data: values 
                        });
            return cbStore;
        },
        //------------------------------------------------------------ call to initialize stat config
        doInitializeConfig: function (remoteLocal)
        {		   
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/initStudyMethods',
                    method: "POST",
                    disableCaching: true,      
                    params: remoteLocal,
                    success: doInitializeConfigSuccess,
                    failure: doInitializeConfigFailed
                }
            );
            Ext.MessageBox.wait('Initializing Study Config ...');   
            return;
            
            function doInitializeConfigSuccess(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                } 
            }

            function doInitializeConfigFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }  
        },
        //------------------------------------------------------------ call to create and populate Flat Table
        doGetModelStatus: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelStatus',
                    method: "POST",
                    disableCaching: true,       
                    params: modelJSON,
                    success: doGetModelStatusSuccess,
                    failure: doGetModelStatusFailed
                }
            );
            return;
            
            function doGetModelStatusSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doGetModelStatusFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }      
        }, 
        //------------------------------------------------------------ call to create and populate Flat Table
        doSetModelStatus: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/setModelStatus',
                    method: "POST",
                    disableCaching: true,        
                    params: modelJSON,
                    success: doSetModelStatusSuccess,
                    failure: doSetModelStatusFailed
                }
            );
            Ext.MessageBox.wait('Updating Model Status ...');  
            return;
            
            function doSetModelStatusSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doSetModelStatusFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }      
        },         
        //------------------------------------------------------------ call to create and populate Atomic Fields
        doProcessFlatTableAll: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessFlatTableAll',
                    method: "POST",
                    disableCaching: true,         
                    params: { payload: modelJSON },
                    success: doProcessSuccess,
                    failure: doProcessFailed
                }
            );
            Ext.MessageBox.wait('Generating Flat Table ...');  
            return;
            
            function doProcessSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doProcessFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }      
        }, 
        //------------------------------------------------------------ call to create and populate Atomic Fields
        doProcessFlatTableAllNoMissing: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessFlatTableAllNoMissing',
                    method: "POST",
                    disableCaching: true,       
                    params: { payload: modelJSON },
                    success: doProcessSuccess,
                    failure: doProcessFailed
                }
            );
            Ext.MessageBox.wait('Generating Flat Table ...');  
            return;
            
            function doProcessSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doProcessFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }      
        },         
        //------------------------------------------------------------ call to create and populate Atomic Fields
        doProcessFlatTable: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessAtomicServlet',
                    method: "POST",
                    disableCaching: true,         
                    params: { payload: modelJSON },
                    success: doProcessFlatTableSuccess,
                    failure: doProcessFlatTableFailed
                }
            );
            Ext.MessageBox.wait('Generating Flat Table ...');  
            return;
            
            function doProcessFlatTableSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doProcessFlatTableFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }      
        },        
        //------------------------------------------------------------ call to delete Flat Table
        doDeleteFlatTable: function (modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/deleteDST',
                    method: "POST",
                    disableCaching: true,        
                    params: modelJSON,
                    success: doDeleteFlatTableSuccess,
                    failure: doDeleteFlatTableFailed
                }
            );
            Ext.MessageBox.wait('Deleting Denormalized Study Table ...');   
            return;
            
            function doDeleteFlatTableSuccess(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doDeleteFlatTableFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }  
        },

        //------------------------------------------------------------ call to create Flat Table
        doCreateFlatTable: function(modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/createFlatTable',
                    method: "POST",
                    disableCaching: true,
                    timeout       : 120000, //2 minutes, extra time needed
                    params: modelJSON,
                    success: doCreateFlatTableSuccess,
                    failure: doCreateFlatTableFailed
                }
            );
            Ext.MessageBox.wait('Creating Denormalized Study Table ...');  
            return;
            
            function doCreateFlatTableSuccess(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doCreateFlatTableFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },  

       //------------------------------------------------------------ call to create process virtual fields
        doProcessVirtualFields: function(modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessVirtualServlet',
                    method: "POST",
                    disableCaching: true,
                    params: { payload: modelJSON },
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Processing Virtual Fields ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },  
                
       //------------------------------------------------------------ call to process missing fields
        doProcessMissingData: function(modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessMissingServlet',
                    method: "POST",
                    disableCaching: true,
                    params: { payload: modelJSON },
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Processing Missing Data ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },                 
        //------------------------------------------------------------ call to create DST table
        doVerifyStatement: function(n, f, callbackFunction, rowIndex)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/verifyStatement',
                    method: "GET",
                    disableCaching: true,
                    params: {tableName: n, formula: f},                    
                    success: doVerifyStatementSuccess,
                    failure: doVerifyStatementFailed
                }
            );
            Ext.MessageBox.wait('Verifying formula statement ...');  
            return;
            
            function doVerifyStatementSuccess(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    callbackFunction(false, rowIndex);
                } else {
                    callbackFunction(true, rowIndex);
                }
            }

            function doVerifyStatementFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
                callbackFunction(false, rowIndex);
            }         
        },   
      //------------------------------------------------------------ call to export Model
        doExportModel: function(modelRecord)
        {		
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/exportModel',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Exporting models ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    var win = Ext.create('delta3.view.ExportPopup');
                    var textArea = win.down('#exportDataString');
                    textArea.setValue(response.responseText);
                    win.show();                                      
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },     
      //------------------------------------------------------------ call to import Model
        doImport: function(objectJSON)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/importObjects',
                    method: "POST",
                    disableCaching: true,
                    params: objectJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Importing object(s) ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },         
      //------------------------------------------------------------ call to clone Model
        doCloneModel: function(modelRecord, theGrid)
        {		
            modelRecord.name += ' Clone';
            var d = new Date();
            modelRecord.outputName = 'ft' + d.valueOf();
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/cloneModel',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },  
      //------------------------------------------------------------ call to clone Study
        doCloneStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/cloneStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();                    
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },         
      //------------------------------------------------------------ call to split Study
        doSplitStudy: function(studyRecord, newModelName)
        {		
            //modelRecord.name = newModelName;
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/splitStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Splitting studies ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },  
      //------------------------------------------------------------ call to execute Study
        doExecuteStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/executeStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    timeout: 360000, //6 minutes, extra time needed for very large selects 
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Submitting study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },      
      //------------------------------------------------------------ call to advance Study status
        doAdvanceStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/advanceStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Executing study status advance ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },              
      //------------------------------------------------------------ call to roll back Study status
        doRollbackStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/rollbackStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Executing study status rollback ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA3", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to get Study next step params
        /*doGetStudyNextStepParams: function(studyRecord, callbackFunction)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getStudyNextStepParams',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting study step parameters ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                callbackFunction(response, options);
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },     */      
      //------------------------------------------------------------ call to get Filter Usage (before delete for example)
        getFilterUsage: function(filter, callbackFunction, sm)
        {		
            var stringJSON = JSON.stringify(filter);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getFilterUsage',
                    method: "GET",
                    disableCaching: true,
                    params: {filter: stringJSON},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting filter usage ...');  
            return;

            function successFunction(response, options)
            {
                callbackFunction(response, options, sm);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ call to verify Filter syntax againt db
        validateFilter: function(filterId, filterName, sql, callbackFunction)
        {		
            var stringJSON = '{"filterFormulas":[{"filter":{"idFilter":' 
                    + filterId + ',"name":"' 
                    + filterName + '"},"condition":"' + sql +'"}]}';  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/validateFilter',
                    method: "POST",
                    disableCaching: true,
                    params: stringJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Verifying filter ...');  
            return;

            function successFunction(response, options)
            {
                callbackFunction(response, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ call to calculate LRF data chunks
        getLRFIntervals: function(studyId, type, chunking, store)
        {		

            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFIntervals',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId, type: type, chunking: chunking},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Calculating LRF data intervals ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                var intervalData = JSON.parse(response.responseText);
                store.setData(intervalData);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to get LRF string
        getLRF: function(logregrId, logregrName, callbackFunction)
        {		
            var stringJSON = '{"logregrs":[{"idLogregr":' + logregrId
                    + ',"name":"'  + logregrName + '"}]}';  
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRF',
                    method: "GET",
                    disableCaching: true,
                    params: {logregr: stringJSON},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp.status.message, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },      
      //------------------------------------------------------------ call to get LRF string based on studyId
        getLRFforStudy: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFforStudy',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },      
      //------------------------------------------------------------ call to get LRF string based on processId
        getLRFforProcess: function(processId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFforProcess',
                    method: "GET",
                    disableCaching: true,
                    params: {processId: processId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ call to get sudy details for studyId
        getStudyDetails: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getStudyDetails',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Study details...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to get LRF string based on studyId
        getResultsforStudy: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getResultsforStudy',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Results for Study ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to clone LRF
        doCloneLRF: function(data, logregrName, theGrid)
        {		
            var stringJSON = '{"logregrs":[{"idLogregr":' + data.idLogregr
                    + ',"name":"'  + data.name + '"}]}';  
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/cloneLRF',
                    method: "POST",
                    disableCaching: true,
                    params: stringJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning Logistic Regression Formula ...');  
            return;
            
            function successFunction(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                    theGrid.store.load();
                    theGrid.getView().refresh();                            
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },         
        //------------------------------------------------------------- generate where cluase string from Filter Fields
        getFilterWhereClause: function(formulas, fieldStore) {
            var counterMax = formulas.length;
            var orFlag = 0;
            var sqlOR = '';
            var sql = '';
            // build ORs first
            for (var i=0; i< counterMax; i++) {
                var r = formulas[i];
                if ( typeof r.data === 'undefined' ) {   
                    // this is to allow call to this function from Study Grid w/o reloading formulas
                    r.data = {};
                    r.data.idModelColumn = r.modelColumnidModelColumn;
                    r.data.formula = r.formula;
                    r.data.type = r.type;
                }
                if ( r.data.type === 'Include' ) {
                    var field = fieldStore.findRecord('idModelColumn', r.data.idModelColumn);
                    if ( field === null ) {
                        sql += '';
                    } else {
                        if ( orFlag > 0 ) {
                            sql += ' OR ';
                        }
                        orFlag++;
                        sql += '(' + field.get("name") + ' ' + r.data.formula + ')';       
                    }
                }
            }
            if ( orFlag > 1 ) {
                sqlOR = '(' + sql + ')';
            } else {
                sqlOR = sql;
            }
            // now built negative ANDs
            var andFlag = 0;
            sql = '';
            for (var i=0; i< counterMax; i++) {
                var r = formulas[i];
                if ( typeof r.data === 'undefined' ) {   
                    // this is allow call to this function from Study Grid w/o reloading formulas
                    r.data = {};
                    r.data.idModelColumn = r.modelColumnidModelcColumn;
                    r.data.formula = r.formula;
                    r.data.type = r.type;                    
                }                
                if ( r.data.type === 'Exclude' ) {
                    var field = fieldStore.findRecord('idModelColumn', r.data.idModelColumn);
                    if ( field === null ) {
                        sql += '';
                    } else {                    
                        if ( (orFlag > 0) || (andFlag > 0) ) {
                            sql += ' AND ';
                        }
                        andFlag++;
                        sql += 'NOT (' + field.get("name") + ' ' + r.data.formula + ')';      
                    }
                }
            }                    
            return sqlOR+sql;
        },
        //------------------------------------------------------------ call to create DST table
        doPseudo: function(userAlias, idUser, callbackFunction)
        {
            var userAuthInfoJSONString = '{"userAuth":[{"alias":"' + userAlias 
            + '", "idUser":"' + idUser
            + '"}]}';
    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/goPseudo',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,                    
                    success: doPseudoSuccess,
                    failure: doPseudoFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doPseudoSuccess(response, options)
            {
                Ext.MessageBox.hide();
                callbackFunction();
            }

            function doPseudoFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },
        //------------------------------------------------------------ call to create DST table
        doUpdateModelDropdowns: function()
        {
            var extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelDetails',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,                    
                    success: doUpdateModelDropdownsSuccess,
                    failure: doUpdateModelDropdownsFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doUpdateModelDropdownsSuccess(response, options)
            {
                Ext.MessageBox.hide();
                delta3.utils.GlobalFunc.updateModelBuilderDropdowns(Ext.decode(response.responseText));
            }

            function doUpdateModelDropdownsFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },        
        //------------------------------------------------------------ call to create DST table
        doUpdateTableTree: function()
        {
            var extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelDetails',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,                    
                    success: doUpdateTableTreeSuccess,
                    failure: doUpdateTableTreeFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doUpdateTableTreeSuccess(response, options)
            {
                Ext.MessageBox.hide();
                delta3.utils.GlobalFunc.updateTableTree(Ext.decode(response.responseText));
            }

            function doUpdateTableTreeFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA3", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA3", response.responseText);      
                }
            }         
        },             
        //------------------------------------------------------------ call to update MB Table Tree        
        updateTableTree: function(response) {
            if ((typeof response.modelTables !== 'undefined') && (typeof response.modelColumns !== 'undefined')) {
                delta3.utils.GlobalFunc.updateModelBuilderDropdowns(response);
                var treeModelString = '{"text":"' + ModelData.modelSelected.name + '","expanded":true,"children":[';
                //var treeModelString = '{"text":"' + ModelData.modelSelected.name + '","children":['; // collapsed version
                for (var i = 0; i < response.modelTables.length; i++) {
                    if (i !== 0) {
                        treeModelString += ',';
                    }
                    treeModelString += '{"text":"';
                    var newValue = response.modelTables[i].physicalName;
                    treeModelString += newValue + '","leaf": false';
                    if (response.modelTables[i].primaryTable === true) {
                        treeModelString += ',"qtip":"primary table"';
                    }
                    treeModelString += ',"allowDrop":false,"expanded":false,"allowDrag":false}';
                    var id = ModelData.tableStore.findRecord('physicalName', newValue); // store.findRecord
                    if ((typeof id !== 'undefined') && (id !== null)) {
                        id.data = response.modelTables[i];
                    }
                }
                treeModelString += ']}';
                var treeModel = JSON.parse(treeModelString);
                ModelData.relationshipTreeStore.setRootNode(treeModel);
                var digestedTreeModel = ModelData.relationshipTreeStore.getRootNode();
                var metadata = Ext.ComponentQuery.query('#metadataTree')[0].store.getRootNode();
                for (var x = 0; x < response.modelTables.length; x++) {
                    for (i = 0; i < metadata.childNodes.length; i++) {
                        if (response.modelTables[x].physicalName === metadata.childNodes[i].get('text')) {
                            for (j = 0; j < metadata.childNodes[i].childNodes.length; j++) {
                                digestedTreeModel.getChildAt(x).appendChild({
                                    text: metadata.childNodes[i].data.children[j].text,
                                    qtip: metadata.childNodes[i].data.children[j].type,
                                    allowDrop: false,
                                    iconCls: columnUsedInModel(response.modelColumns, response.modelTables[x].physicalName, metadata.childNodes[i].data.children[j].text),
                                    checked: false,
                                    leaf: true
                                });
                            }
                        }
                    }
                }
            }
            function columnUsedInModel(modelColumns, tableName, columnName) {
                for (var i = 0; i < modelColumns.length; i++) {
                    if ((modelColumns[i].tableName === tableName) && (modelColumns[i].physicalName === columnName)) {
                        return 'field-used';
                    }
                }
                return '';
            }            
        },        
        //------------------------------------------------------------- call to fill in KeyComboBoxes and tableName(s)
        updateModelBuilderDropdowns: function (response) 
        {
            var comboItemString;
            ModelData.modelTableComboStore.removeAll();
            for ( var j=0; j<response.modelTables.length; j++ ) {
                 // fill in table_name-->table_id map
                 ModelData.tableStore.tableToIdMap[response.modelTables[j].physicalName] = response.modelTables[j].idModelTable;
                 comboItemString = '{"idModelTable":"' + response.modelTables[j].idModelTable 
                         + '","physicalName":"' + response.modelTables[j].physicalName + '"}';
                 ModelData.modelTableComboStore.add(JSON.parse(comboItemString));                          
            }             
            ModelData.modelKey1ComboStore.removeAll();
            ModelData.modelKey2ComboStore.removeAll();            
            for ( var i=0; i<response.modelColumns.length; i++ ) {
                    // prepare 2 Key Combo Boxes for Relationships tab
                    if ( /*response.modelColumns[i].keyField === true &&*/
                        response.modelColumns[i].active === true &&
                        response.modelColumns[i].atomic === true ) {
                        comboItemString = '{"idModelColumn":"' + response.modelColumns[i].idModelColumn 
                                + '","name":"' + response.modelColumns[i].name
                                + '","modelTableidModelTable":"' + response.modelColumns[i].modelTableidModelTable + '"}';
                        ModelData.modelKey1ComboStore.add(JSON.parse(comboItemString));
                        ModelData.modelKey2ComboStore.add(JSON.parse(comboItemString));                                            
                    }                                    
                   for ( j=0; j<response.modelTables.length; j++ ) {
                       // fill in tableName in columns based on tables
                       if ( response.modelTables[j].idModelTable === response.modelColumns[i].modelTableidModelTable  ) {
                          response.modelColumns[i].tableName = response.modelTables[j].physicalName;
                       }
                   }                                
             }    
        },
        getNumberOfParams: function (stats) {
            var i = 0;
            while ( ( typeof stats[1].statistics[i] !== 'undefined' ) && (stats[1].statistics[i].filter === "") ) i++;
            return i;
        },       
        openTableViewerTab: function(processRecord) {		
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
            var tab = Ext.getCmp('tableViewerTab');
            if ( !Ext.isEmpty(tab) ) {
                theTabs.remove(tab);
            }
            theTabs.add(Ext.create('delta3.view.mb.TableViewerContainer', { 
                        title: 'Table Viewer: '+ processRecord.name,         
                        modelSelected: processRecord,       
                        id: 'tableViewerTab',
                        closable: true
                    })
                ).show();    
            theTabs.doLayout();              
        },
        openModelTab: function(processRecord, dataSourceName){		
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
            var tab = Ext.getCmp('modelBuilderTab');
            if ( !Ext.isEmpty(tab) ) {
                tab.removeAll();
                theTabs.remove(tab);
            }
            theTabs.add(Ext.create('delta3.view.mb.ModelBuilderContainer', { 
                        title: 'Model Builder: '+ processRecord.name,         
                        modelSelected: processRecord,
                        dataSourceName: dataSourceName,
                        id: 'modelBuilderTab',
                        closable: true
                    })
                ).show();    
            theTabs.doLayout();
        },
//------------------------------------------------------------------------------
        getCurrentUserAuthInfo: function () {	
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getCurrentUserAuthInfo',
                    method: "POST",
                    disableCaching: true,
                    success: getCurrentUserAuthInfoSuccess,
                    failure: getCurrentUserAuthInfoFailed
                }
            );

            function getCurrentUserAuthInfoSuccess(response, options)
            {
                delta3.utils.GlobalVars.currentUserAuthInfo = Ext.decode(response.responseText);
                Ext.create('delta3.view.Viewport'); 
            }

            function getCurrentUserAuthInfoFailed(response, options)
            {
                console.log(response.responseText);
            }
        },
//------------------------------------------------------------------------------                
        getCurrentUser: function(viewPort) {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getCurrentUser',
                    method: "GET",
                    disableCaching: true,
                    success: callSuccess,
                    failure: callFailed
                }
            );

            function callSuccess(response, options) {
               var view = Ext.ComponentQuery.query('#currentUserAlias')[0];
               view.setText(response.responseText);
               delta3.utils.GlobalVars.currentUser = response.responseText;
            }   

            function callFailed(response, options) {
                Ext.MessageBox.show
                (
                    {
                        title: 'DELTA3',
                        msg: '<b style="color:#cc0000;">Access denied!</b> Please check the credentials and try again<br/><br/><div style="padding-left:48px;">If you have forgotten your password, please click <a href="#">here</a>.<br/><br/>For any other assistance, <a href="mailto:admin@copingsystems.com">contact administrator</a> with your queries.',
                        progressText: 'Checking...',
                        width: 400,
                        wait: false,
                        closable: false,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.MessageBox.OK,
                        fn: function() { }
                    }
                );    
                viewPort.destroy();
            }
        },
//------------------------------------------------------------------------------
        getUserAuthInfo: function(userAlias, idUser, callback){		
            var userAuthInfoJSONString = '{"userAuth":[{"alias":"' + userAlias 
            + '", "idUser":"' + idUser
            + '"}]}';

            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getUserAuthInfo',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,
                    success: callback,
                    failure: getUserAuthInfoFailed
                }
            );

            function getUserAuthInfoFailed(response, options)
            {
                console.log('calling getUserAuthInfo failed ' + response.responseText);
            }
        },
//------------------------------------------------------------------------------
        changePassword: function(oldPass, newPass){		
            var userAuthInfoJSONString = '{"op":"' + oldPass + '", "np":"' + newPass + '"}';
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/changePassword',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,
                    success: changePasswordSuccess,
                    failure: changePasswordFailed
                }
            );
            function changePasswordSuccess(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                }
            }
            function changePasswordFailed(response, options)
            {
                console.log('calling changePassword failed ' + response.responseText);
            }
        },    
//------------------------------------------------------------------------------
        resetPassword: function(alias){		
            //var userInfoJSONString = '{"user":"' + alias + '"}';
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/resetPassword',
                    method: "POST",
                    disableCaching: true,
                    params: alias,
                    success: resetPasswordSuccess,
                    failure: resetPasswordFailed
                }
            );
            function resetPasswordSuccess(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                }
            }
            function resetPasswordFailed(response, options)
            {
                console.log('calling resetPassword failed ' + response.responseText);
            }
        },        
//------------------------------------------------------------------------------
        getRolePermissionInfo: function(name, idRole, callback){		
            var rolePermissionInfoJSONString = '{"rolePermissions":[{"name":"' + name 
            + '", "idRole":"' + idRole
            + '"}]}';

            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getRolePermissionInfo',
                    method: "POST",
                    disableCaching: true,
                    params: rolePermissionInfoJSONString,
                    success: callback,
                    failure: getRolePermissionInfoFailed
                }
            );

            function getRolePermissionInfoFailed(response, options)
            {
                console.log('calling getRolePermissionInfo failed ' + response.responseText);
            }
        }, 
//------------------------------------------------------------------------------            
        lookupUserAlias: function(val, cell, rec, r_idx, c_idx, store, idString, lookupStore) {
            var tableIndex = rec.data[idString]; 
            var model = lookupStore.findRecord( 'idUser', tableIndex);
            if ( model === null ) 
                return null;
            else  {
                return model.get("alias");
            }
        },
//------------------------------------------------------------------------------            
        lookupUserInfo: function(rec, idString, lookupStore) {
            var tableIndex = rec.data[idString]; 
            var model = lookupStore.findRecord( 'idUser', tableIndex);
            if ( model === null ) 
                return null;
            else  {
                return model.get("alias") + ', ' + model.get("firstName") + ' ' + model.get("lastName");
            }
        },        
//------------------------------------------------------------------------------            
        isPermitted: function(tag) {
            var userId = delta3.utils.GlobalVars.currentUser;
            //var authInfo = delta3.utils.GlobalVars.mainMenu;
            var roles = delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.roles;
            for (var i=0; i<roles.length; i++) {
                for (var j=0; j<roles[i].permissions.length; j++) {
                    if ( roles[i].permissions[j].tag === tag) {
                        return true;
                    }
                }
            }
            return false;
        },
        calcStandardDeviation: function(n, u, nsd) {
            var sd;
            var sd2;

            if (( n === null ) || ( u === null )) {
                return 0;
            }
            if ( nsd === null ) {
                nsd = 1;
            }
            sd2 = nsd*nsd;
            if ( (sd2 + 4*n*u*(1-u)) < 0 ) {
                return 0;
            }
            sd = 2*n*u + sd2 + nsd * Math.sqrt(sd2 + 4*n*u*(1-u));
            if ( (n + sd2) === 0 ) {
                return 0;
            }
            sd = sd/(2 * (n + sd2));
            if ( sd > 1 ) {
                sd = 1;
            }  
            return sd - u;
        },
        emphasizeString: function(string) {
            return '<span style="background-color: yellow; font-size: small">' + string + '</span>';
        },
        displayCentrallyBigString: function(string) {
            //return '<span style="color: blue; font-size: 300%; text-align: center">' + string + '</span>';
            return '<span id="centralInfo">' + string + '</span>';
        }           
    }  
});

