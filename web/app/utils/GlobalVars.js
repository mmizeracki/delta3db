Ext.define('delta3.utils.GlobalVars', {
    statics: {
        //browser_height: Ext.getBody().getViewSize().height,
        //browser_width: Ext.getBody().getViewSize().width,
        buildNumber: 0.80,
        MsgBox: Ext.MessageBox,
        currentUser: 'Not Authorized',
        logoutURL: 'index.html',
        currentUserAuthInfo: null,        
        mainMenu: [],
        tabHeight: 640,
        borderHeight: 640,        
        pageSize: 25,
        largePageSize: 1000,
        OrgStore: null,
        RoleStore: null,
        PermissionStore: null,
        MetadataStore: null,
        FieldStore: null,
        UserStore: null,
        loadSemaphore: 0,
        // following section is for Logregr grid
        idLength: 30,
        riskAdjLength: 120,
        dateLength: 70,
        initHeight: 96,
        initWidth: 192,
        dateColumnZero: 3,
        //------------------------------------------
        relationshipComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type','value'], 
                data: [ 
                    {type: 'One-to-One', value: 1}, 
                    {type: 'One-to-Many', value: 2}
                ] 
            }),            
        roleTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'USER'}, 
                    {type: 'OADMIN'},
                    {type: 'SADMIN'}
                ] 
            }),               
        formulaOperatorComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['operator'], 
                data: [ 
                    {operator: 'AND'}, 
                    {operator: 'OR'}                   
                ] 
            }),             
        formulaTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'Include'}, 
                    {type: 'Exclude'}                   
                ] 
            }),             
        fieldClassComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['classField'], 
                data: [ 
                    {classField: 'Case ID'}, 
                    {classField: 'Sequencer'}, 
                    {classField: 'Category'},
                    {classField: 'Risk Factor'},
                    {classField: 'Treatment'},
                    {classField: 'Outcome'},
                    {classField: 'Not Assigned'}                    
                ] 
            }),
        fieldKindComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['kind'], 
                data: [ 
                    {kind: 'Dichotomous'}, 
                    {kind: 'Continuous'}, 
                    {kind: 'Enumerated'},
                    {kind: 'Date'},
                    {kind: 'Not Assigned'}                    
                ] 
            })
    }
});

