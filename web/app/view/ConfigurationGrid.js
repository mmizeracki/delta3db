/* 
 * Configuration Grid
 */

Ext.define('delta3.view.ConfigurationGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.configuration',
    itemId: 'configurationGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.view.mb.ModelBuilderContainer',
        'delta3.view.mb.TableViewerContainer'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    tbar: [{
            text: 'Add Connection',
            iconCls: 'add_new_config-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.ConfigurationModel.create({
                    name: '',
                    longName: 'New Configuration',
                    type: '',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            itemId: 'refreshConfiguration',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0]
                thisGrid.store.load();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No configurations found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idConfiguration', width: 30},
            {text: 'Name', dataIndex: 'name', editor: 'textfield', width: 100},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 120},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield', width: 40},
            {text: 'Type', dataIndex: 'type', editor: 'textfield', width: 50},
            {text: 'Connection Information', dataIndex: 'connectionInfo', editor: 'textfield', width: 260},
            {text: 'Databse User', dataIndex: 'dbUser', editor: 'textfield', width: 120},
            {text: 'Database Password', dataIndex: 'dbPassword', editor: 'textfield', width: 120},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        return Ext.create('delta3.store.ConfigurationStore');
    }

});


