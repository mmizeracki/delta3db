/* 
 * Processes Container
 */

Ext.define('delta3.view.ProcessesContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.processes',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent ProcessesContainer");        
        me.items = {
                xtype:  'grid.process',
                region: 'center'
        },   
        me.callParent();
    }
});

