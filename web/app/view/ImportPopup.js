/* 
 * Import Popup Window
 */

Ext.define('delta3.view.ImportPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.import',
    layout: 'fit',
    width: 450,
    height: 360,
    itemId: 'importPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3 Import',
    items: [{
            xtype: 'textareafield',
            width: 440,
            height: 340,            
            grow: true,
            itemId: 'importDataString',
            value: '',
            allowBlank: true
        }],
    buttons: [
        {
            text: 'Import',
            handler: function() {
                var theString = Ext.ComponentQuery.query('#importDataString')[0].value;
                delta3.utils.GlobalFunc.doImport(theString);                
                this.up('#importPopup').destroy();
            }
        },
        {
            text: 'Close',
            handler: function() {
                this.up('#importPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});


