/* 
 * User Container
 */

Ext.define('delta3.view.UserContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.users',
    requires:   [
        'Ext.layout.container.Border',
        'Ext.resizer.BorderSplitterTracker'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent UserContainer");
        me.items = [{
                xtype:  'grid.user',
                region: 'center'
        }];    
        me.callParent();
    }
});

