/* 
 * Pasword Change Popup Window
 */

Ext.define('delta3.view.PasswordPopup', {
    extend: 'Ext.Window',
    layout: 'fit',
    width: 380,
    height: 198,
    itemId: 'passwordPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    oldPassword: '',
    newPassword1: '',
    newPassword2: '',
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#passwordPopup')[0];
                if (Ext.ComponentQuery.query('#fOldPassword')[0].value === '') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please enter old password.');
                }                  
                if (Ext.ComponentQuery.query('#fNewPassword1')[0].value === '') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please enter new password.');
                }                
                if (Ext.ComponentQuery.query('#fNewPassword1')[0].value !== Ext.ComponentQuery.query('#fNewPassword2')[0].value) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Passwords do not match.');
                } else {
                    delta3.utils.GlobalFunc.changePassword(
                            Ext.ComponentQuery.query('#fOldPassword')[0].value,
                            Ext.ComponentQuery.query('#fNewPassword1')[0].value);
                    thisWin.destroy();
                }
            }
        }, {
            text: 'Cancel',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#passwordPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 240,
                    labelAlign: 'right',
                    bodyStyle: 'padding:5px 5px 0',
                    width: 380,
                    height: 198,
                    title: 'Password Change',
                    autoScroll: true,
                    defaultType: 'displayfield',
                    items: [
                        {
                            fieldLabel: 'Old Password',
                            xtype: 'textfield',
                            inputType: 'password',
                            itemId: 'fOldPassword',
                            allowBlank: false,
                            value: me.oldPassword
                        }, {
                            fieldLabel: 'New Password',
                            xtype: 'textfield',
                            inputType: 'password',
                            itemId: 'fNewPassword1',
                            allowBlank: false,
                            value: me.newPassword1
                        }, {
                            fieldLabel: 'Re-type New Password',
                            xtype: 'textfield',
                            itemId: 'fNewPassword2',
                            inputType: 'password',
                            allowBlank: false,
                            value: me.newPassword2
                        }]
                });
        me.callParent();
    }
}
);

