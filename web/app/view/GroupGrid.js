/* 
 * Group Grid
 */

Ext.define('delta3.view.GroupGrid',{
	extend      : 'Ext.grid.Panel',
	alias       : 'widget.grid.group',
    itemId      : 'groupGrid',
    autoScroll  : false,
    renderTo    : document.body,
    height      : '100%',
    selType     : 'checkboxmodel',
	requires	: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',           
        'Ext.grid.plugin.RowEditing',
		'Ext.selection.RowModel',
		'Ext.grid.column.Column'
	],
    plugins     : [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0];    
                    thisGrid.store.sync();
                }
            }
        })
    ], 
	border		: false,
    tbar        : [{
            text: 'Add Group',
            iconCls: 'add_new-icon16',
            handler : function() {
                var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0];             
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.GroupModel.create({
                    name: '',
                    longName: 'New Group',
                    type: '',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});                   
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            itemId: 'removeGroup',
            text: 'Remove Group',
            iconCls: 'group-remove',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0]
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                thisGrid.store.remove(sm.getSelection());
                thisGrid.store.sync();
                sm.select(0);
            }
        }, {
            itemId: 'refreshGroup',
            text: 'Refresh',
            iconCls: 'group-refresh',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0]
                thisGrid.store.load();
            }
        }],    
        dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No organizations found',
            displayInfo: true
        }],    	
	initComponent   : function(){       
		var me = this;
		me.store = me.buildStore();
                me.dockedItems[0].store = me.store;
		me.columns = me.buildColumns();
		me.callParent();
		me.store.load();
	},

	buildColumns	: function(){
        var userStore = delta3.store.UserStore.create();
        userStore.load();   
		return [
			{text: 'ID',dataIndex: 'idGroup'},            
			{text: 'Name',dataIndex: 'name', editor: 'textfield'},
			{text: 'Description',dataIndex: 'description', editor: 'textfield'},
            {text: 'Active',disabled: true,xtype: 'checkcolumn',dataIndex: 'active', editor: 'checkboxfield'},
			{text: 'Type',dataIndex: 'type', editor: 'textfield'},            
            {text: 'Parent ID',dataIndex: 'parentId', editor: 'numberfield'},            
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type:'date', dateFormat:'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,                 
                 renderer: function(val, cell, rec, r_idx, c_idx, store) {return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);}},       
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type:'date', dateFormat:'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,      
                 renderer: function(val, cell, rec, r_idx, c_idx, store) {return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);}}    
		];
	},
    setActiveRecord: function(record){
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
	buildStore	: function(){  
       console.log("org grid building GroupStore");
	return Ext.create('delta3.store.GroupStore');
	}
           
});

