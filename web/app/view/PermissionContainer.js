/* 
 * Permission Container
 */

Ext.define('delta3.view.PermissionContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.permissions',
    requires:   [
        'Ext.layout.container.Border'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent PermissionContainer");
        me.items = {
                xtype:  'grid.permission',
                region: 'center'
        }, 
        me.callParent();
    }
});