/* 
 * Continuous/Dichotomous Grid
 */

Ext.define('delta3.view.ds.ContAndDichoGrid',{
	extend      : 'Ext.grid.Panel',
	alias       : 'widget.grid.contAndDichoGrid',
    itemId      : 'contAndDichoGrid',
    autoScroll  : false,
    renderTo    : document.body,
    region      : 'north',
    selType     : 'rowmodel',
	requires	: [
        'delta3.model.DStatsModel',        
        'Ext.data.Store',  
        'Ext.data.Model',
		'Ext.selection.RowModel',
		'Ext.grid.column.Column'
	],
	border		: false,   
    viewConfig  : {
        enableTextSelection: true,
        getRowClass: function(record, rowIndex, rowParams, store) { 
            if (record.get('0') === 'null') return 'value-null';        
            if ( rowIndex === 0 ) return 'value-positive';
            if ( rowIndex === 1 ) return 'value-negative';            
        }
    },
    dStats    : {},
    fieldStore: {},
	initComponent   : function(){       
		var me = this;
        var modelFields = me.buildGridModelFields();
		me.store = me.buildStore(me.dStats, modelFields);
        me.columns = me.buildGridColumns();
		me.callParent();
	},
	buildStore	: function(stats, modelFields){  
       console.log("org grid building ContDicho Stats Store");
       var data = stats[1].statistics;
       var store = new Ext.data.Store({
            autoLoad: true,
            model: 'delta3.model.DStatsModel',
            data : {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });    
        store.model.addFields(modelFields);
        store.loadRawData(data); // re-load data since metadata changed
        return store;
	},
    buildGridModelFields: function() {
            return [
                {name: 'filter', type: 'string'},            
                {name: 'min', type: 'string'},
                {name: 'value25', type: 'string'},
                {name: 'mean', type: 'string'},
                {name: 'value75', type: 'string'},
                {name: 'max', type: 'string'},
                {name: 'median', type: 'string'},
                {name: 'std', type: 'string'},            
                {name: 'count', type: 'int'},
                {name: 'nullCount', type: 'int'},            
                {name: 'notNullPercent', type: 'string'}
            ];
    },
    buildGridColumns: function() {
        return [
        {"text":"Filter", "dataIndex":"filter", "type":"string", "width":120},     
        {"text":"Graph", "dataIndex":"graph", "type":"string", "width":100},             
        {"text":"Min", "dataIndex":"min", "type":"string", "width":40},  
        {"text":"25%", "dataIndex":"value25", "type":"string", "width":40},  
        {"text":"Median", "dataIndex":"median", "type":"string", "width":40},    
        {"text":"75%", "dataIndex":"value75", "type":"string", "width":40}, 
        {"text":"Max", "dataIndex":"max", "type":"string", "width":40},  
        {"text":"Mean", "dataIndex":"mean", "type":"string", "width":40}, 
        {"text":"Std Dev", "dataIndex":"std", "type":"string", "width":50},      
        {"text":"Total Count", "dataIndex":"count", "type":"int", "width":70},  
        {"text":"Null Count", "dataIndex":"nullCount", "type":"int", "width":70},      
        {"text":"Not NULL %", "dataIndex":"notNullPercent", "type":"string", "width":70}    
        ];
    }            
});





/*var drawBox = Ext.create('Ext.draw.Component', {
    viewBox: false,
    items: [{
        type: 'rect',
        fill: '#ffc',
        x: 10,
        y: 6,
        width: 60,
        height: 10
    }]
});

var drawAxis = Ext.create('Ext.draw.Component', {
    viewBox: false,
    items: [{
        type: 'rect',
        fill: '#ffceef',
        x: 10,
        y: 6,
        width: 60,
        height: 1
    }]
});

var drawComponent = Ext.create('Ext.draw.Component', {
    viewBox: false,
    items: [{
        type: 'circle',
        fill: '#ffc',
        radius: 6,
        x: 8,
        y: 8
    }]
}); */

/*var myCircle = drawComponent.surface.add({
    type: 'circle',
    x: 8,
    y: 8,
    radius: 6,
    fill: '#cc5'
}).show(true);*/