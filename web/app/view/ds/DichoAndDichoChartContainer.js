/* 
 * DichoAndDichoChart Container
 */

Ext.define('delta3.view.ds.DichoAndDichoChartContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.dichoAndDichoChart',
    requires: [   
        'delta3.model.DStatsModel',        
        'Ext.layout.container.HBox',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Bar',
        //'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category'
    ],   
    itemId      : 'dichoAndDichoChart',    
    layout: 'fit',
    region: 'center',
    data: {},
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent DichoAndDichoChartContainer");           
        var modelFields = me.buildGridModelFields();        
        me.width = me.dStats[1].statistics.length * 100;
        var store = me.buildChartStore(me.dStats, me.fieldStore, modelFields);

        me.items = [{
            xtype: 'cartesian',
            width: me.width,
            height: 460,
            //interactions: ['itemhighlight'],
            store: store,
            insetPadding: {
                top: 10,
                left: 10,
                right: 10,
                bottom: 10
            },
            axes: [{
                type: 'numeric',
                position: 'left',
                fields: ['colTrue', 'colFalse'],
                grid: true,
                minimum: 0,
                maximum: 100
            }, {
                type: 'category',
                position: 'bottom',
                fields: ['colName'],
                label: {
                },   
                grid: true
            }],
            series: [{
                        type: 'bar',    
                        axis: 'left',
                        highlight: true,                           
                        stacked: true,
                        xField: 'colName',
                        yField: ['colTrue', 'colFalse']
                    }
            ]
        }];    
        me.doLayout();
        me.callParent();
    },
    buildGridModelFields: function() {
        var hString = '[';   
        hString += '{"name":"colName", "type":"string"},';
        hString += '{"name":"colTrue", "type":"int"},';   
        hString += '{"name":"colFalse", "type":"int"},';  
        hString += '{"name":"colNull", "type":"int"}';                
        return hString += ']';
    },
    buildChartStore: function(stats, fieldStore, modelFields){  
           console.log("org grid building Chart Stats Store");     
           var numParams = stats[1].statistics.length;
           var trueOrFalse;  
           var chartStore = {fields: [], data: []};
           var data = '[';
           var dataRow = '';

           for (var i=1; i<numParams; i++) {      
                if ( i > 1 ) {
                   dataRow += ',';
                }
                dataRow += '{';

                var cnString;
                if ( i > 1 ) { // extract field/filter column name
                     var j = Math.floor((i-2)/2);
                     var filterRecord = fieldStore.findRecord('idModelColumn', stats[0].filters[j]);    
                     if ( (i % 2) === 0 ) {
                         trueOrFalse = "true";
                     } else {
                         trueOrFalse = "false";
                     }
                     cnString = filterRecord.data.name + ' [' + trueOrFalse + ']';                      
                } else { // this obviously is i=1 and name of field under investigation needs to be retrieved
                     var fieldRecord = fieldStore.findRecord('idModelColumn', stats[0].fields[0]);
                     cnString = fieldRecord.data.name;
                }
                dataRow += '"colName":"' + cnString + '"';
                var totalCount = stats[1].statistics[i].FALSE + stats[1].statistics[i].TRUE;    
                if ( totalCount === 0 ) {
                    dataRow += ',"colTrue":0';
                    dataRow += ',"colFalse":0';   
                    dataRow += ',"colNull":0';  
                } else {
                    dataRow += ',"colTrue":' + Math.round(stats[1].statistics[i].TRUE/totalCount * 100)/1;
                    dataRow += ',"colFalse":' + Math.round(stats[1].statistics[i].FALSE/totalCount * 100)/1;  
                    if ( i === 1 ) { 
                       dataRow += ',"colNull":' + Math.round(stats[1].statistics[i].NULL/totalCount * 100)/1;        
                    } else {
                       dataRow += ',"colNull":0';                     
                    }                
                }

                dataRow += '}';
           }

           data += dataRow + ']';
           var storeFields = JSON.parse(modelFields);
           chartStore.fields = storeFields;
           var storeData = JSON.parse(data);
           chartStore.data = storeData;  
           return chartStore;
        }                
});





        