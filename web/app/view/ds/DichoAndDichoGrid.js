/* 
 * Dichotomous/Dichotomous Grid
 */

Ext.define('delta3.view.ds.DichoAndDichoGrid',{
	extend      : 'Ext.grid.Panel',
	alias       : 'widget.grid.dichoAndDichoGrid',
    itemId      : 'dichoAndDichoGrid',
    autoScroll  : false,
    renderTo    : document.body,
    region      : 'north',
    selType     : 'rowmodel',
	requires	: [
        'delta3.model.DStatsModel',        
        'Ext.data.Store',  
        'Ext.data.Model',
		'Ext.selection.RowModel',
		'Ext.grid.column.Column'
	],
	border		: false,   
    viewConfig  : {
        enableTextSelection: true,        
        getRowClass: function(record, rowIndex, rowParams, store) {
            //if (record.get('0') == 'Y') return 'value-positive';
            //if (record.get('0') == 'N') return 'value-negative';     
            if (record.get('0') === 'null') return 'value-null';        
            if ( rowIndex === 0 ) return 'value-negative';
            if ( rowIndex === 1 ) return 'value-positive';            
        }
    },
    dStats    : {},
    fieldStore: {},
	initComponent   : function(){       
		var me = this;
        var modelFields = me.buildGridModelFields(me.dStats[1].statistics.length);
		me.store = me.buildStore(me.dStats, modelFields);
        me.columns = eval ("(" + me.buildGridColumns(me.dStats, me.fieldStore) + ")");
		me.callParent();
	},
	buildStore	: function(stats, modelFields){  
       console.log("building DichoDicho Stats Store");
       //var totalCount = stats[1].statistics[1].FALSE + stats[1].statistics[1].TRUE + stats[1].statistics[1].NULL;
       var data = '{"dsData":[';
       var dataRow1 = '{';
       var dataRow2 = '{';
       var dataRow3 = '{';
       var dataRow4 = '{';
       
       for (var i=0; i<stats[1].statistics.length; i++) {      
           var totalCount = stats[1].statistics[i].FALSE + stats[1].statistics[i].TRUE + stats[1].statistics[i].NULL;           
           if ( i > 0 ) {
               dataRow1 += ',';
               dataRow2 += ',';
               dataRow3 += ',';   
               dataRow4 += ',';
               if ( totalCount === 0 ) {
                   dataRow1 += '"' + i + '":"' + stats[1].statistics[i].FALSE + '    (' + Math.round(stats[1].statistics[i].FALSE/1 * 1000)/10 + '%)"';
                   dataRow2 += '"' + i + '":"' + stats[1].statistics[i].TRUE + '    (' + Math.round(stats[1].statistics[i].TRUE/1 * 1000)/10 + '%)"';     
               } else {
                   dataRow1 += '"' + i + '":"' + stats[1].statistics[i].FALSE + '    (' + Math.round(stats[1].statistics[i].FALSE/totalCount * 1000)/10 + '%)"';
                   dataRow2 += '"' + i + '":"' + stats[1].statistics[i].TRUE + '    (' + Math.round(stats[1].statistics[i].TRUE/totalCount * 1000)/10 + '%)"';                   
               }
               if ( i === 1 ) {
                  if ( totalCount === 0 ) {
                    dataRow3 += '"' + i + '":"' + stats[1].statistics[i].NULL + '    (' + Math.round(stats[1].statistics[i].NULL/1 * 1000)/10 + '%)"';  
                  } else {
                    dataRow3 += '"' + i + '":"' + stats[1].statistics[i].NULL + '    (' + Math.round(stats[1].statistics[i].NULL/totalCount * 1000)/10 + '%)"';                        
                  }
               } else {
                  dataRow3 += '"' + i + '":"' + stats[1].statistics[i].NULL + '"';                     
               }
               dataRow4 += '"' + i + '":"' + totalCount + '"';
           } else {
                dataRow1 += '"' + i + '":"' + stats[1].statistics[i].FALSE + '"';
                dataRow2 += '"' + i + '":"' + stats[1].statistics[i].TRUE + '"';      
                dataRow3 += '"' + i + '":"' + stats[1].statistics[i].NULL + '"';    
                dataRow4 += '"' + i + '":"Total"';
           }
       }
       
       dataRow1 += '}';
       dataRow2 += '}';
       dataRow3 += '}';
       dataRow4 += '}';       
       data += dataRow1 + ',' + dataRow2 + ',' + dataRow3 + ',' + dataRow4 + ']}';

       var store = new Ext.data.Store({
            autoLoad: true,
            model: 'delta3.model.DStatsModel',
            data : {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });    
        //store.model.addFields(eval("(" + modelFields + ")"));
        store.model.addFields(modelFields);
        store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
        return store;
	},
    buildGridModelFields: function(length) {
        var hString = '[';
        for ( var i = 0; i < length; i++ ) {
            if ( i > 0 ) {
                hString += ',';
            }
            hString += '{"name":"' + i + '", "type":"string"}';
        }
        return hString += ']';
    },
    buildGridColumns: function(dStats, store) {
        var numParams = dStats[1].statistics.length;
        var trueOrFalse;
        var hString = '[';
        hString += '{"text":"Value", "dataIndex":"0", "type":"string", "width":60, tdCls: "x-value-cell"},';     
        hString += '{"text":"Count", "dataIndex":"1", "type":"string"}';             
        for ( var i = 2; i < numParams; i++ ) {
            if ( i > 0 ) {
                hString += ',';
            }
            var j = Math.floor((i-2)/2);
            var filterRecord = store.findRecord('idModelColumn', dStats[0].filters[j]);    
            if ( (i % 2) === 0 ) {
                trueOrFalse = "true";
            } else {
                trueOrFalse = "false";
            }
            hString += '{"text":"' + filterRecord.data.name + ' [' + trueOrFalse + ']", "dataIndex":"' + i + '", "type":"string", "width": 100}';     
        }
        return hString += ']';
    }            
});

Ext.define('dsData', {
    extend: 'Ext.data.Model',
    fields: []
});




