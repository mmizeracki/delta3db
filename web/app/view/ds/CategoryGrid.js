/* 
 * Dichotomous/Dichotomous Grid
 */

Ext.define('delta3.view.ds.CategoryGrid',{
	extend      : 'Ext.grid.Panel',
	alias       : 'widget.grid.categoryGrid',
    itemId      : 'categoryGrid',
    autoScroll  : false,
    renderTo    : document.body,
    region      : 'north',
    selType     : 'rowmodel',
	requires	: [
        'delta3.model.DStatsModel',        
        'Ext.data.Store',  
        'Ext.data.Model',
		'Ext.selection.RowModel',
		'Ext.grid.column.Column'
	],
	border		: false,   
    viewConfig  : {
        enableTextSelection: true,        
        getRowClass: function(record, rowIndex, rowParams, store) {  
            if (record.get('0') === 'null') return 'value-null';        
            if ( rowIndex === 0 ) return 'value-positive';
            if ( rowIndex === 1 ) return 'value-negative';            
        }
    },
    dStats    : {},
    fieldStore: {},
	initComponent   : function(){       
		var me = this;
        var modelFields = me.buildGridModelFields(me.dStats);
		me.store = me.buildStore(me.dStats, modelFields);
        me.columns = eval ("(" + me.buildGridColumns(me.dStats, me.fieldStore) + ")");
		me.callParent();
	},
	buildStore	: function(stats, modelFields){  
       console.log("org grid building DichoDicho Stats Store");
       var totalCount = 0;
       var numberOfParams = delta3.utils.GlobalFunc.getNumberOfParams(stats);
 
       for (var i=0; i<numberOfParams; i++) {              
            totalCount += stats[1].statistics[i].count;
       }
       var data = '{"dsData":[';
       var dataRow1 = '{"0":"",';
       var dataRowX = '';
       
       for (var i=0; i<numberOfParams; i++) {      
           if ( i > 0 ) {
               dataRow1 += ',';
           }
           dataRow1 += '"' + (i+1) + '":"' + stats[1].statistics[i].count + '    (' + Math.round(stats[1].statistics[i].count/totalCount * 1000)/10 + '%)"';         
       }
       
       dataRow1 += '}';    
       data += dataRow1;
       
       if ( stats[1].statistics.length > numberOfParams ) {  // there are filters
           for (var i=numberOfParams; i<stats[1].statistics.length;) {   
               for ( var j = 0; j < numberOfParams; j++ ) {
                    if ( j === 0 ) {
                        dataRowX = ',{';                  
                        dataRowX += '"' + j + '":"' + stats[1].statistics[i].filter + '",';
                        dataRowX += '"' + (j+1) + '":"' + stats[1].statistics[i].count + '    (' + Math.round(stats[1].statistics[i].count/totalCount * 1000)/10 + '%)"';                 
                    } else {                    
                        dataRowX += ',"' + (j+1) + '":"' + stats[1].statistics[i].count + '    (' + Math.round(stats[1].statistics[i].count/totalCount * 1000)/10 + '%)"';                 
                    }  
                    i++;
                }
                dataRowX += '}';
                data += dataRowX;
          }         
       }
        
       data += ']}';

       var store = new Ext.data.Store({
            autoLoad: true,
            model: 'delta3.model.DStatsModel',
            data : {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });    
        store.model.addFields(eval("(" + modelFields + ")"));
        store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
        return store;
	},
    buildGridModelFields: function(stats) {
        var numberOfParams = delta3.utils.GlobalFunc.getNumberOfParams(stats);    
        var hString = '[';
        for ( var i = 0; i < numberOfParams+1; i++ ) {
            if ( i > 0 ) {
                hString += ',';
            }
            hString += '{"name":"' + i + '", "type":"string"}';
        }
        return hString += ']';
    },
    buildGridColumns: function(stats, store) {
        var numberOfParams = delta3.utils.GlobalFunc.getNumberOfParams(stats);
        var hString = '[';       
        hString += '{"text":"Filter", "dataIndex":"0", "type":"string", "width": 100},';     
        for ( var i = 0; i < numberOfParams; i++ ) {
            if ( i > 0 ) {
                hString += ',';
            }
            hString += '{"text":"' + stats[1].statistics[i].value  + '", "dataIndex":"' + (i+1) + '", "type":"string", "width": 100}';     
        }
        return hString += ']';
    }            
});





