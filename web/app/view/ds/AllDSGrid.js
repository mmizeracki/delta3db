/* 
 * All Descriptive Stats Grid
 */

Ext.define('delta3.view.ds.AllDSGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.allDSGrid',
    itemId: 'allDSGrid',
    autoScroll: false,
    renderTo: document.body,
    region: 'north',
    selType: 'rowmodel',
    requires: [
        'delta3.model.DStatsModel',
        'Ext.data.Store',
        'Ext.data.Model',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column'
    ],
    border: false,
    viewConfig: {
        enableTextSelection: true,
        getRowClass: function(record, rowIndex, rowParams, store) {
            if (record.get('0') === 'null')
                return 'value-null';
            if (rowIndex === 0)
                return 'value-positive';
            if (rowIndex === 1)
                return 'value-negative';
        }
    },
    dStats: {},
    fieldStore: {},
    initComponent: function() {
        var me = this;
        var modelFields = me.buildGridModelFields();
        me.store = me.buildStore(me.dStats, modelFields);
        me.columns = me.buildGridColumns();
        me.callParent();
    },
    buildStore: function(stats, modelFields) {
        console.log("org grid building All Descriptive Stats Store");
        var data = stats[1].statistics;
        var store = new Ext.data.Store({
            autoLoad: true,
            model: 'delta3.model.DStatsModel',
            data: {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });
        store.model.addFields(modelFields);
        store.loadRawData(data); // re-load data since metadata changed
        return store;
    },
    buildGridModelFields: function() {
        return [
            {name: 'field', type: 'string'},
            {name: 'class', type: 'string'},
            {name: 'kind', type: 'string'},
            {name: 'min', type: 'string'},
            {name: 'value25', type: 'string'},
            {name: 'mean', type: 'string'},
            {name: 'value75', type: 'string'},
            {name: 'max', type: 'string'},
            {name: 'median', type: 'string'},
            {name: 'std', type: 'string'},
            {name: 'count', type: 'int'},
            {name: 'nullCount', type: 'int'},
            {name: 'notNullPercent', type: 'string'}
        ];
    },
    buildGridColumns: function() {
        return [
            {"text": "Field", "dataIndex": "field", "type": "string", "width": 100},
            {"text": "Class", "dataIndex": "class", "type": "string", "width": 90},
            {"text": "Kind", "dataIndex": "kind", "type": "string", "width": 90},
            {"text": "Min", "dataIndex": "min", "type": "string", "width": 40},
            {"text": "25%", "dataIndex": "value25", "type": "string", "width": 40},
            {"text": "Median", "dataIndex": "median", "type": "string", "width": 40},
            {"text": "75%", "dataIndex": "value75", "type": "string", "width": 40},
            {"text": "Max", "dataIndex": "max", "type": "string", "width": 40},
            {"text": "Mean", "dataIndex": "mean", "type": "string", "width": 40},
            {"text": "STD", "dataIndex": "std", "type": "string", "width": 40},
            {"text": "Total Count", "dataIndex": "count", "type": "int", "width": 70},
            {"text": "Null Count", "dataIndex": "nullCount", "type": "int", "width": 70},
            {"text": "Not NULL %", "dataIndex": "notNullPercent", "type": "string", "width": 70}
        ];
    }
});




