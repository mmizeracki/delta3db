/* 
 * CategoryChart Container
 */

Ext.define('delta3.view.ds.CategoryChartContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.categoryChart',
    requires: [   
        'delta3.model.DStatsModel',        
        'Ext.layout.container.HBox',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate',
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.PolarChart'        
    ],    
    itemId      : 'categoryChart',
    layout: {
        type: 'hbox',
        align: 'left'
    },
    margin: 0,
    region: 'center',
    data: {},
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent CategoryChartContainer");   
        var modelFields = me.buildCategoryModelFields();
        var numberOfParams = delta3.utils.GlobalFunc.getNumberOfParams(me.dStats);
        var numberOfCharts = me.dStats[1].statistics.length / numberOfParams;        
        me.items = me.createChartPanel(me.dStats, me.fieldStore, modelFields, numberOfCharts);
        me.doLayout();
        me.callParent();
    },
    buildCategoryModelFields: function() {
        var hString = '[';   
        hString += '{"name":"name", "type":"string"},';
        hString += '{"name":"data", "type":"int"}';             
        return hString += ']';
    },
    buildChartStore: function(stats, modelFields, index, numberOfParams){     
           var startIndex = index * numberOfParams;
           var stopIndex = startIndex + numberOfParams;
           var chartStore = {fields: ['name','data'], data: []};
           var data = '[';
           var dataRow = '';

           for (var i=startIndex; i<stopIndex; i++) {      
                if ( i > startIndex ) {
                   dataRow += ',';
                }
                dataRow += '{"name": "' + stats[1].statistics[i].value + '", "data": ' + stats[1].statistics[i].count + '}';         
           }

           data += dataRow + ']';
           var storeFields = JSON.parse(modelFields);
           chartStore.fields = storeFields;
           var storeData = JSON.parse(data);
           chartStore.data = storeData;   
           return chartStore;
        },
    createChartPanel: function(stats, fieldStore, modelFields) {    
        var numberOfParams = delta3.utils.GlobalFunc.getNumberOfParams(stats);
        var numberOfCharts = stats[1].statistics.length / numberOfParams;     
        var chartArray = [];

        for (var i = 0; i < numberOfCharts; i++ ) {
            var store = this.buildChartStore(stats, modelFields, i, numberOfParams);
            var chartC = new Ext.chart.PolarChart({
                        animate: false,
                        //renderTo: Ext.getBody(),      
                        //showInLegend: true,
                        //legend: {
                        //    docked: 'bottom'
                        //},
                        interactions: ['rotate','itemhighlight'],                        
                        height: 112,
                        width: 140,                    
                        store: store,
                        sprites: [{
                            type: 'text',
                            text: stats[1].statistics[i*numberOfParams].filter,
                            font: 'bold 10px Arial',
                            x: 10,
                            y: 10,
                            width: 100,
                            height: 10 
                        }],
                        series: [{
                                    type: 'pie',
                                    //angleField: 'data',
                                    xField: 'data',
                                    highlight: false,                                    
                                    label: {
                                        field: 'name',
                                        display: 'inside',
                                        //calloutLine: {
                                        //    length: 4,
                                        //    width: 2
                                            // specifying 'color' is also possible here
                                        //    },                                        
                                        contrast: true,
                                        font: '10px Arial'
                                    },
                                    tooltip: {
                                        trackMouse: true,
                                        style: 'background: #fff',
                                        renderer: function(storeItem, item) {
                                            this.setHtml(storeItem.get('data'));
                                        }
                                    }
                                }]       
                    });
            chartArray[i] = chartC;
        }   
        return chartArray;
    }                
});





      