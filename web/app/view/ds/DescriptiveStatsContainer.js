/* 
 * DescriptiveStats Container
 */

Ext.define('delta3.view.ds.DescriptiveStatsContainer', {
    extend: 'Ext.container.Container',
    requires	: [
        'delta3.view.ds.ContAndDichoGrid',        
        'delta3.view.ds.DichoAndDichoGrid',
        'delta3.view.ds.DichoAndDichoChartContainer',   
        'delta3.view.ds.CategoryGrid',
        'delta3.view.ds.CategoryChartContainer',  
        'delta3.view.ds.AllDSGrid',           
        'Ext.data.Store',
        'Ext.toolbar.Paging',           
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.form.Label',
        'Ext.grid.column.Column'
    ],    
    alias:  'widget.container.descriptiveStats',
    dStats: {},
    fieldStore: {},
    layout: {
        type: 'vbox',
        align: 'center'
    },
    items: [],
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent DescriptiveStatsContainer type " + me.dStats[1].type);     
        var fieldInfo = {
                    xtype:  'panel',
                    width: '100%',
                    layout: {
                           type: 'hbox',
                           align: 'center'
                    },   
                    items: [{
                        xtype: 'label',
                        forId: 'myFieldId',
                        text: 'Field: ' + me.selectedRecord.name + ' (' + me.selectedRecord.description + '), Type: ' 
                                + me.selectedRecord.type + ', Class: ' + me.selectedRecord.fieldClass 
                                + ', Kind: ' + me.selectedRecord.fieldKind,
                        margins: '10 10 10 10'
                    }]
            };
        if ( me.dStats[1].type === "DD" ) {
            me.items = [fieldInfo, {
                    xtype:  'grid.dichoAndDichoGrid',
                    dStats: me.dStats,
                    width: '100%',
                    flex: 1,
                    fieldStore: me.fieldStore
            },{
                    xtype:  'container.dichoAndDichoChart',
                    dStats: me.dStats,
                    width: '100%',
                    flex: 1,                
                    fieldStore: me.fieldStore
            }];
        }
        if ( me.dStats[1].type === "CD" ) {
            me.items = [fieldInfo, {
                    xtype:  'grid.contAndDichoGrid',
                    dStats: me.dStats,
                    width: '100%',
                    flex: 1,
                    fieldStore: me.fieldStore
            }];
        }        
        if ( me.dStats[1].type === "CAT" ) {
            me.items[0] = fieldInfo;
            me.items[1] = {
                    xtype:  'grid.categoryGrid',
                    dStats: me.dStats,
                    width: '100%',
                    flex: 1,
                    fieldStore: me.fieldStore
            };
            me.items[2] = {
                    xtype:  'container.categoryChart',
                    dStats: me.dStats,
                    width: '100%',
                    margin: 10,
                    flex: 1,                
                    fieldStore: me.fieldStore
            };  
        }  
        if ( me.dStats[1].type === "ALL" ) {
            me.items = [fieldInfo, {
                    xtype:  'grid.allDSGrid',
                    dStats: me.dStats,
                    width: '100%',
                    flex: 1,
                    fieldStore: me.fieldStore
            }];
        }          
        me.callParent();
    }
});

