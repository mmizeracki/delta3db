/* 
 * Flat Table Field Grid
 */

Ext.define('delta3.view.mb.FieldGrid', {
    title: delta3.utils.GlobalFunc.emphasizeString('Data Model:'),
    extend: 'Ext.grid.Panel',
    columnLines: true,
    //enableLocking: true,     // this is causing major problems with window   
    region: 'south',
    height: 2 * delta3.utils.GlobalVars.tabHeight / 3,
    minSize: 175,
    maxSize: delta3.utils.GlobalVars.tabHeight - 100,
    sortableColumns: true,
    selModel: {selType: 'checkboxmodel', mode: 'SINGLE', allowDeselect: true, toggleOnClick: true},
    itemId: 'fieldGrid',
    autoScroll: true,
    requires: [
        'delta3.view.ds.DescriptiveStatsContainer',
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.plugin.DragDrop',
        'Ext.form.RadioGroup',
        'Ext.grid.column.Action',
        'Ext.menu.Menu',
        'delta3.store.ModelFieldStore',
        'delta3.view.filter.FilterPopup',
        'delta3.utils.GridFilter'
    ],
    stripeRows: true,
    listeners: {
        beforedestroy: function(panel, eOpt) {
            var mButton = panel.down('#monitorButton');
            clearInterval(mButton.periodicModelStatusCheck);
        }
    },
    tbar: [],
    dockedItems: [{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        emptyMsg: 'No fields found',
        displayInfo: true
    }],
    initComponent: function() {
        var me = this;
        delta3.utils.GlobalVars.FieldStore.load();
        me.fieldStore = delta3.utils.GlobalVars.FieldStore;
        me.store = me.buildStore();
        me.columns = me.buildColumns();
        me.viewConfig = me.buildViewConfig();
        me.plugins = me.buildPlugins();
        // optional, permission driven buttons hookup follows
        var tableViewer;
        if (delta3.utils.GlobalFunc.isPermitted("TableViewer") === true) {
            tableViewer = {
                itemId: 'TableViewer',
                text: 'Table Viewer',
                iconCls: 'tableViewer-icon16',
                tooltip: delta3.utils.Tooltips.mbBtnTableViewer,                    
                handler: function() {
                    delta3.utils.GlobalFunc.openTableViewerTab(ModelData.modelSelected);
                }
            };
        }
        var filters;
        if (delta3.utils.GlobalFunc.isPermitted("Filters") === true) {
            filters = {
                itemId: 'Filters',
                text: 'Filters',
                iconCls: 'filter-icon16',
                tooltip: delta3.utils.Tooltips.mbBtnFilters,                    
                handler: function() {
                    var win = new delta3.view.filter.FilterPopup({
                        modelId: ModelData.modelSelected.idModel,
                        isStudyMode: false,
                        subTitle: 'Filter definition and selection for model "' + ModelData.modelSelected.name + '"'
                    });
                    win.show();
                }
            };
        }        
        me.tbar = [];
        var x = 0;
        me.tbar[x++] = {
            text: 'Step 2: Specs/Edit',
            iconCls: 'spec-icon16',
            //tooltip: 'Step 2',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [{
                        text: 'Add Virtual Field',
                        iconCls: 'add_new-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnAddField,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                            // get first table Id for virt field persistence
                            var rec = thisGrid.getStore().getAt(0);
                            var r = delta3.model.FieldModel.create({
                                physicalName: 'virtual',
                                tableName: ModelData.modelSelected.outputName,
                                modelTableidModelTable: rec.get('modelTableidModelTable'),
                                fieldClass: 'Not Assigned',
                                fieldKind: 'Not Assigned',
                                active: true,
                                atomic: false,
                                keyField: false,
                                insertable: true,
                                virtual: true,
                                sub: false,
                                idModel: ModelData.modelSelected.idModel,
                                createdTS: '0000-00-00 00:00:00.0',
                                updatedTS: '0000-00-00 00:00:00.0'
                            });
                            thisGrid.store.add(r);   
                            thisGrid.plugins[0].startEdit(r);
                        }
                    }, {
                        text: 'Verify Formula',
                        iconCls: 'check_good-icon',
                        tooltip: delta3.utils.Tooltips.mbBtnVerify,
                        handler: function(grid, rowIndex, colIndex) {
                            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];                           
                            var rec = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof rec === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Field first.');
                                return;
                            }  
                            if (rec.get('formula') === '') {
                                Ext.Msg.show
                                        ({
                                            title: 'DELTA Error',
                                            message: 'Formula is empty',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.ERROR
                                        });
                            } else {
                                if ((rec.get('virtual') === true) || (rec.get('sub') === true)) {
                                    delta3.utils.GlobalFunc.doVerifyStatement(ModelData.modelSelected.outputName, rec.get('formula'), setVerifyFlag, rec);
                                } else {
                                    delta3.utils.GlobalFunc.doVerifyStatement(getTableName(rec), rec.get('formula'), setVerifyFlag, rec);
                                }
                                thisGrid.plugins[0].cancelEdit();
                            }
                        }
                    }, {
                        text: 'Edit Field',
                        iconCls: 'edit-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnEdit,                        
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];                           
                            var sel = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof sel === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Field first.');
                                return;
                            }                            
                            thisGrid.plugins[0].startEdit(sel, 0);
                        }
                    }, {
                        text: 'Delete Field',
                        iconCls: 'delete-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnDelete,                        
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Field first.');
                                return;
                            }                   
                            Ext.Msg.show({
                                title:'DELTA3',
                                message: 'You are about to delete Field. Would you like to proceed?',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.Msg.QUESTION,
                                fn: function(btn) {
                                    if (btn === 'yes') {
                                        if (sm.getSelection()[0].data.physicalName !== 'virtual') {
                                            removeUsedIconInTableTreePanel(sm.getSelection()[0].data.tableName, sm.getSelection()[0].data.physicalName);
                                        }
                                        thisGrid.store.remove(sm.getSelection());
                                        thisGrid.store.sync();
                                        sm.select(0);
                                        thisGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing")); 
                                    }
                                }
                            });                    
                        }                        
                    }, filters, 
                    {
                        itemId: 'recordInfo',
                        text: 'View Properties',
                        iconCls: 'recordInfo-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnProperties,                        
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0]
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Field first.');
                                return;
                            }                            
                            var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Model Field"});
                            win.show();
                        }
                    }]
            })
        };
        me.tbar[x++] = {
            text: 'Step 3: Build',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [ {
                    itemId: 'prepareFaltTableAll',
                    text: 'Process All',
                    iconCls: 'generateDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnProcessAtomic,                    
                    handler: function() {
                        delta3.utils.GlobalFunc.doProcessFlatTableAll(ModelData.modelSelected);
                    }
                }, {
                    itemId: 'prepareFlatTableNoMissing',
                    text: 'Process All no Missing',
                    iconCls: 'generateDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnProcessAtomic,                    
                    handler: function() {
                        delta3.utils.GlobalFunc.doProcessFlatTableAllNoMissing(ModelData.modelSelected);
                    }
                },{
                    itemId: 'prepareDST',
                    text: 'Step 3A: Process Atomic',
                    iconCls: 'generateDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnProcessAtomic,                    
                    handler: function() {
                        delta3.utils.GlobalFunc.doProcessFlatTable(ModelData.modelSelected);
                    }
                }, {
                    itemId: 'prepareMissingData',
                    text: 'Step 3B: Process Missing',
                    iconCls: 'generateDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnProcessMissing,                        
                    handler: function() {
                        delta3.utils.GlobalFunc.doProcessMissingData(ModelData.modelSelected);
                    }
                }, {
                    itemId: 'prepareVirtualFields',
                    text: 'Step 3C: Process Virtual',
                    iconCls: 'generateDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnProcessVirtual,                        
                    handler: function() {
                        var verResults = virtFieldVerification(Ext.ComponentQuery.query('#fieldGrid')[0]);
                        if (verResults !== '') {
                            Ext.Msg.show
                                    ({
                                        title: 'DELTA Error',
                                        message: verResults,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.ERROR
                                    });
                        } else {
                            delta3.utils.GlobalFunc.doProcessVirtualFields(ModelData.modelSelected);
                        }
                    }
                }, {
                    itemId: 'deleteDST',
                    text: 'Delete Flat Table',
                    iconCls: 'deleteDST-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnDeleteFT,                        
                    handler: function() {
                        Ext.Msg.show({
                            title:'DELTA3',
                            message: 'You are about to delete Flat Table. Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                    delta3.utils.GlobalFunc.doDeleteFlatTable(ModelData.modelSelected);
                                    var dsWindow = Ext.ComponentQuery.query('#descriptiveStatistics')[0];
                                    dsWindow.removeAll();
                                }
                            }
                        });
                    }
                }]
            })
        };
        me.tbar[x++] = {
            text: 'Step 4: Review',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                    itemId: 'allDescStats',
                    text: 'All Stats',
                    iconCls: 'add_allstats-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnAllStats,                        
                    handler: function() {
                        getDescriptiveStats(ModelData.modelSelected, null, null);
                    }
                }, {
                    itemId: 'descStats',
                    text: 'Field Stats',
                    iconCls: 'add_stats-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnFieldStats,                        
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        if (typeof sm.getSelection()[0] !== 'undefined') {
                            if ((sm.getSelection()[0].data.fieldClass === 'Not Assigned') || (sm.getSelection()[0].data.fieldKind === 'Not Assigned')) {
                                delta3.utils.GlobalFunc.showDeltaMessage("Please specify field Class and Kind");
                            } else {
                                popupFieldStats(sm.getSelection()[0].data, thisGrid.store);
                            }
                        } else {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Field first.');
                        }
                    }
                }, tableViewer, 
                {
                    text: 'Export to CSV',
                    iconCls: 'exportCSV-icon16',
                    tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
                    handler: function(b, e) {
                        b.up('grid').exportGrid(ModelData.modelSelected.name);
                    }
                }]
            })
        };
        me.tbar[x++] = {
            text: 'Start Monitor',
            iconCls: 'monitor-icon16',
            itemId: 'monitorButton',
            tooltip: delta3.utils.Tooltips.mbBtnMonitor,                
            periodicModelStatusCheck: {},
            handler: function() {
                var me = this;
                if (this.text === 'Start Monitor') {
                    me.executeMonitorPulse(me);
                    this.periodicModelStatusCheck = setInterval(function() {
                            me.executeMonitorPulse(me);
                        }, 4000); // every 4 sec (plus 1 sec for red-eye pulse)
                    this.setText('Stop Monitor');
                } else {
                    clearInterval(me.periodicModelStatusCheck);
                    this.setText('Start Monitor');
                }
            },
            executeMonitorPulse: function(button) {
                button.setIconCls('monitor_on-icon16');
                setTimeout(function() {
                    button.callToGetModelStatus(button)
                }, 1000);
            },
            callToGetModelStatus: function(button) {
                delta3.utils.GlobalFunc.doGetModelStatus(ModelData.modelSelected);
                button.setIconCls('monitor-icon16');
            }
        };      
        me.tbar[x++] = {
            itemId: 'refreshModelDetail',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnRefresh,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                thisGrid.store.load();
            }
            /*}, {
             itemId: 'createFlatTable',
             text: 'Create Flat Table',
             iconCls: 'createDST-icon16',
             handler: function() {
             delta3.utils.GlobalFunc.doCreateFlatTable(ModelData.modelSelected);
             }*/
        };
        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me});  
        me.dockedItems[0].store = me.store;
        me.callParent();
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idModelColumn', width: 40, tooltip: delta3.utils.Tooltips.mbGridId},
            {text: 'Name', dataIndex: 'name', editor: 'textfield', width: 90, tooltip: delta3.utils.Tooltips.mbGridField},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 130, tooltip: delta3.utils.Tooltips.mbGridDescription},
            {text: 'Formula', dataIndex: 'formula', editor: 'textfield', width: 200, tooltip: delta3.utils.Tooltips.mbGridFormula},
            {text: 'Verified', dataIndex: 'verified', disabled: true, xtype: 'checkcolumn', width: 60, tooltip: delta3.utils.Tooltips.mbGridVerifyStatus},
            {text: 'Class', dataIndex: 'fieldClass', width: 100, tooltip: delta3.utils.Tooltips.mbGridClass, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.fieldClassComboBoxStore,
                            displayField: 'classField',
                            valueField: 'classField',
                            emptyText: 'Not Assigned',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('fieldClass', h);
                                }
                            }
                        })
            },
            {text: 'Kind', dataIndex: 'fieldKind', width: 100, tooltip: delta3.utils.Tooltips.mbGridKind, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.fieldKindComboBoxStore,
                            displayField: 'kind',
                            valueField: 'kind',
                            emptyText: 'Not Assigned',
                            queryMode: 'local',
                            forceSelection: false,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('fieldKind', h);
                                }
                            }
                        })
            },
            {text: 'Source Table', dataIndex: 'tableName', width: 130, sortable: false, tooltip: delta3.utils.Tooltips.mbGridTable,
                renderer: function(value, metaData, record, row, col, store, gridView) {
                    return getTableName(record);
                }
            },
            {text: 'Source Column', dataIndex: 'physicalName', width: 100, tooltip: delta3.utils.Tooltips.mbGridColumn},            
            {text: 'Source Type', dataIndex: 'type', width: 90, tooltip: delta3.utils.Tooltips.mbGridType},            
            {text: 'Use', dataIndex: 'insertable', editor: 'checkboxfield', disabled: true, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.mbGridInsertable},
            //{text: 'Atomic', dataIndex: 'atomic', disabled: true, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.mbGridAtomic},
            //{text: 'Virtual', dataIndex: 'virtual', disabled: true, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.mbGridVirtual},
            {text: 'Missing', dataIndex: 'sub', editor: 'checkboxfield', disabled: true, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.mbGridSubstitute},
            {text: 'Key', dataIndex: 'keyField', disabled: true, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.mbGridKey},
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.ModelFieldStore');
    },
    buildViewConfig: function() {
        return {
            plugins: {
                ptype: 'gridviewdragdrop',
                dropGroup: 'gridColumnDDGroup'
            },
            getRowClass: function(record, rowIndex, rowParams, store) {
                if (record.get('virtual') === true)
                    return 'rowVirtualField';
            },
            listeners: {
                beforedrop: function(node, data) {
                    var tableID;
                    var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                    if (data.records[0].parentNode.data.text.toLowerCase() === ModelData.modelSelected.outputName.toLowerCase()) {
                        return false;
                    }
                    if (data.records[0].parentNode.data.text === 'Database') {
                        return false;
                    }
                    if (data.records[0].data.leaf === false) {
                        return false;
                    }
                    if (data.records[0].parentNode.data.text in ModelData.tableStore.tableToIdMap) {
                        tableID = ModelData.tableStore.tableToIdMap[data.records[0].parentNode.data.text];
                    } else {
                        tableID = 0;
                    }
                    var r = delta3.model.FieldModel.create({
                        physicalName: data.records[0].get('text'),
                        tableName: data.records[0].parentNode.data.text,
                        name: data.records[0].get('text'),
                        type: data.records[0].data.qtip,
                        modelTableidModelTable: tableID,
                        idModel: ModelData.modelSelected.idModel,
                        fieldClass: 'Not Assigned',
                        fieldKind: 'Not Assigned',
                        active: true,
                        atomic: true,
                        keyField: true,
                        insertable: true,
                        virtual: false,
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'
                    });
                    var valid = true;
                    thisGrid.store.each(function (record, index) {
                        //validating uniqueness of new field name 
                        if (record.data.name === r.data.name) {
                             valid = false;
                             return;
                         }
                    });
                    if ( valid === false ) {
                        //delta3.utils.GlobalFunc.showDeltaMessage('Field names must be unique: ' + r.data.name);
                        var newName = '';
                        var win = new Ext.Window(
                                {
                                    layout: 'fit',
                                    width: 400,
                                    height: 200,
                                    itemId: 'uniqueNamePopup',
                                    title: 'DELTA3',
                                    modal: true,
                                    closeAction: 'hide',
                                    items: new Ext.Panel(
                                            {
                                                frame: true,
                                                labelWidth: 180,
                                                labelAlign: 'right',
                                                title: 'Field name change',
                                                bodyStyle: 'padding:5px 5px 0',
                                                width: 400,
                                                height: 200,
                                                autoScroll: true,
                                                itemCls: 'form_row',
                                                defaultType: 'displayfield',
                                                buttons: [
                                                    {text: 'Accept',
                                                        handler: function() {
                                                            newName = Ext.ComponentQuery.query('#newFieldName')[0].value;
                                                            if ( r.data.name.toUpperCase() !== newName.toUpperCase() ) {
                                                                r.data.name = newName;
                                                                thisGrid.store.add(r);                    
                                                                thisGrid.store.sync();
                                                                setUsedIconInTableTreePanel(data.records[0].parentNode.data.text, data.records[0].get('text'));
                                                                thisGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
                                                                Ext.ComponentQuery.query('#uniqueNamePopup')[0].destroy();
                                                                return false;
                                                            } else {
                                                                delta3.utils.GlobalFunc.showDeltaMessage('Field names must be unique: ' + r.data.name);
                                                            }
                                                        }
                                                    },
                                                    {text: 'Cancel',
                                                        handler: function() {
                                                            Ext.ComponentQuery.query('#uniqueNamePopup')[0].destroy();
                                                            return false;
                                                        }
                                                    }
                                                ],
                                                items: [{
                                                        fieldLabel: 'Current field name',
                                                        name: 'currentFieldName',
                                                        allowBlank: false,
                                                        value: r.data.name
                                                    },{
                                                        xtype: 'textfield',
                                                        fieldLabel: 'New field name',
                                                        itemId: 'newFieldName',
                                                        allowBlank: true,
                                                        value: newName
                                                    }
                                                ]
                                            })
                                });
                        win.show();                        
                        return false;                            
                    }   
                    thisGrid.store.add(r);                    
                    thisGrid.store.sync();
                    setUsedIconInTableTreePanel(data.records[0].parentNode.data.text, data.records[0].get('text'));
                    thisGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
                    return false;
                }
            }
        };
    },
    buildPlugins: function() {
        return [
            Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2,
                listeners: {
                    edit: function(rowEditor, context, eOpt) {
                        var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                        if ( delta3.utils.GlobalFunc.checkJDBCFieldNameValid(context.newValues.name) === false) {
                            delta3.utils.GlobalFunc.showDeltaMessage('Field name contains not allowed characters');
                            return;                              
                        }
                        var valid = true;
                        thisGrid.store.each(function (record, index) {
                            //validating uniqueness of new field name 
                            if (index !== context.rowIdx && record.data.name === context.newValues.name) {
                                 valid = false;
                                 return;
                             }
                        });
                        if ( valid === false ) {
                            delta3.utils.GlobalFunc.showDeltaMessage('Field names must be unique.');
                            return;                            
                        }
                        var frmla = context.record.get('formula');                        
                        if ((frmla === '') && (context.record.get('virtual') === true)) {
                            delta3.utils.GlobalFunc.showDeltaMessage('Formula for virtual field is missing.');
                        } else {
                            if (frmla !== '') {
                                context.record.set('verified', false);
                                thisGrid.getView().refresh();
                            }
                        thisGrid.store.sync();
                        thisGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing")); 
                        }
                    }
                }
            })
        ];
    }
});

function  popupFieldStats(selectedRecord, theStore) {

    var win = new Ext.Window(
            {
                layout: 'fit',
                width: 720,
                height: 300,
                itemId: 'descStatsPopup',
                modal: true,
                closeAction: 'destroy',
                title: 'DELTA3',
                filterSelection: {},
                items: new Ext.Panel(
                        {
                            frame: true,
                            labelWidth: 90,
                            labelAlign: 'right',
                            title: 'Decriptive Statistics for field ' + selectedRecord.name,
                            bodyStyle: 'padding:5px 5px 0',
                            width: 720,
                            height: 300,
                            autoScroll: true,
                            itemCls: 'form_row',
                            defaultType: 'displayfield',
                            buttons: [
                                {text: 'Analyze ' + selectedRecord.name,
                                    handler: function() {
                                        getDescriptiveStats(ModelData.modelSelected, selectedRecord, win.filterSelection);
                                        win.destroy();
                                    }
                                }, {text: 'Cancel',
                                    handler: function() {
                                        comboBox = null;
                                        win.destroy();
                                    }
                                }
                            ],
                            items: [{
                                    fieldLabel: 'Description',
                                    name: 'fDescription',
                                    allowBlank: false,
                                    value: selectedRecord.description
                                }, {
                                    fieldLabel: 'Type',
                                    name: 'fType',
                                    allowBlank: false,
                                    value: selectedRecord.type
                                }, {
                                    fieldLabel: 'Class',
                                    name: 'fClass',
                                    allowBlank: false,
                                    value: selectedRecord.fieldClass
                                }, {
                                    fieldLabel: 'Kind',
                                    name: 'fKind',
                                    allowBlank: false,
                                    value: selectedRecord.fieldKind
                                }, {
                                    xtype: 'radiogroup',
                                    width: 680,
                                    fieldLabel: 'Filter by',
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            id: 'radio0filter',
                                            name: 'filterType',
                                            boxLabel: 'No Filters',
                                            checked: true,
                                            inputValue: 'None'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            id: 'radio1filter',
                                            name: 'filterType',
                                            boxLabel: 'Category',
                                            inputValue: 'Category'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            id: 'radio2filter',
                                            name: 'filterType',
                                            boxLabel: 'Risk Factor',
                                            inputValue: 'Risk Factor'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            id: 'radio3filter',
                                            name: 'filterType',
                                            boxLabel: 'Treatment',
                                            inputValue: 'Treatment'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            id: 'radio4filter',
                                            name: 'filterType',
                                            boxLabel: 'Outcome',
                                            inputValue: 'Outcome'
                                        }
                                    ],
                                    listeners: {
                                        change: function(field, newValue, oldValue) {
                                            var value = newValue.filterType;
                                            var cb = Ext.getCmp('dStatsComboBox');
                                            updateComboBoxFilterStore(theStore, value, cb.store);
                                        }
                                    }
                                }, new Ext.form.ComboBox({
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ['filterField', 'filterId'],
                                        data: []
                                    }),
                                    id: 'dStatsComboBox',
                                    width: '200',
                                    fieldLabel: 'Filter Field',
                                    displayField: 'filterField',
                                    valueField: 'filterId',
                                    queryMode: 'local',
                                    multiSelect: true,
                                    forceSelection: true,
                                    listeners: {
                                        'select': function(cmb, rec, idx) {
                                            win.filterSelection = cmb.getValue();
                                        }
                                    }
                                })
                            ]
                        })
            });
    win.show();
}

function getDescriptiveStats(modelRecord, selectedRecord, filterselection) {
    MsgBox = Ext.MessageBox;
    var JSONString = '';
    if (selectedRecord !== null) {
        if (Ext.getCmp('radio0filter').getValue() !== true) {
            JSONString = '{"dStats":[{"models":[' + JSON.stringify(modelRecord)
                    + '], "fields":[' + selectedRecord.idModelColumn
                    + '], "filters":[' + filterselection
                    + ']}]}';
        } else {
            JSONString = '{"dStats":[{"models":[' + JSON.stringify(modelRecord)
                    + '], "fields":[' + selectedRecord.idModelColumn
                    + '], "filters":[]}]}';
        }
    } else {
        JSONString = '{"dStats":[{"models":[' + JSON.stringify(modelRecord)
                + '], "fields":[],"filters":[]}]}';
        selectedRecord = {"idModelColumn": 0, "name": "All"};
    }

    MsgBox.wait('Obtaining Descriptive Statistics... Please wait...', 'DELTA3');
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/process/getDStats',
                        method: "POST",
                        disableCaching: true,
                        params: JSONString,
                        success: dStatsCallSuccess,
                        failure: dStatsCallFailed
                    }
            );

    function dStatsCallSuccess(response, options) {
        try {
            var returnValue = Ext.decode(response.responseText);
        } catch (err) {
            // if server not wrapping messages correctly
            dStatsCallFailed(response, options);
            return;
        }
        if (returnValue.success === true) {
            MsgBox.hide();
            var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
            var dsWindow = Ext.ComponentQuery.query('#descriptiveStatistics')[0];
            dsWindow.expand(true);
            // remove descriptive stats tab for this field if exists
            var tab = Ext.getCmp('dsTab-' + selectedRecord.idModelColumn);
            if (tab !== null) {
                dsWindow.remove(tab);
            }
            dsWindow.add(Ext.create('delta3.view.ds.DescriptiveStatsContainer', {
                title: selectedRecord.name,
                id: 'dsTab-' + selectedRecord.idModelColumn,
                selectedRecord: selectedRecord,
                dStats: returnValue.dStats,
                fieldStore: thisGrid.store,
                closable: true
            })).show();
            dsWindow.doLayout();
        }
        else {
            delta3.utils.GlobalFunc.showDeltaMessage(returnValue.status.message);
        }
    }

    function dStatsCallFailed(response, options) {
        Ext.MessageBox.hide();
        if (response.timedout === true) {
            delta3.utils.GlobalFunc.showDeltaMessage(response.statusText);
        } else {
            delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);
        }
    }

}

function updateComboBoxFilterStore(store, filterClass, comboBoxStore) {
    comboBoxStore.removeAll();
    for (var i = 0; i < store.data.length; i++) {
        var fClass = store.data.items[i].get('fieldClass');
        var fKind = store.data.items[i].get('fieldKind');
        if ((fClass === filterClass) && ((fKind !== 'Date') || (fKind !== 'x'))) {
            var fName = store.data.items[i].get('name');
            var fId = store.data.items[i].get('idModelColumn');
            comboBoxStore.add(JSON.parse('{"filterField":"' + fName + '","filterId":"' + fId + '"}'));
        }
    }
    return comboBoxStore;
}

function setUsedIconInTableTreePanel(tableName, columnName) {
    var root = ModelData.relationshipTreeStore.getRootNode();
    var branch = root.findChild('text', tableName);
    branch.findChild('text', columnName).set('iconCls', 'field-used');
}

function removeUsedIconInTableTreePanel(tableName, columnName) {
    var root = ModelData.relationshipTreeStore.getRootNode();
    var branch = root.findChild('text', tableName);
    if ( typeof branch !== 'undefined' && branch !== null ) {
        branch.findChild('text', columnName).set('iconCls', '');
    }
}

function setVerifyFlag(state, rec) {
    var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
    rec.set('verified', state);
    thisGrid.store.sync();
}

function getTableName(record) {
 
    if (record.data.virtual === true) {
        return "";
    } else {
        var tableIndex = record.data['modelTableidModelTable'];
        var tableRecord = ModelData.tableStore.findRecord('idModelTable', tableIndex);
        if (tableRecord === null) 
            return "";
        else
            return tableRecord.get("name");   
    }
}

function virtFieldVerification(grid) {
    var resultString = '';
    var errorFlag = 0;
    var dataStore = grid.store;
    for (i = 0; i < dataStore.data.length; i++) {
        var rec = dataStore.getAt(i);
        if ((rec.get('virtual') === true) && (rec.get('formula') === '' || rec.get('verified') === false)) {
            if (errorFlag === 0) {
                var resultString = 'Following virtual fields require formula or formula validation: ' + rec.get('name');
            } else {
                resultString += ', ' + rec.get('name');
            }
            errorFlag++;
        }
    }
    return resultString;
}
