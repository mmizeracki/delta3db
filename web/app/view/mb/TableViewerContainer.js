/* 
 * Table Viewer Container
 */

Ext.define('delta3.view.mb.TableViewerContainer', {
    extend: 'Ext.container.Container',
    requires: [
        'delta3.view.mb.TableViewerGrid'
    ],
    alias:  'widget.container.tableViewer',
    itemId: 'tableViewerContainer',
    layout: 'fit',   
    initComponent:  function(){   
        this.items = {
                xtype:  'grid.tableViewer',
                idModel: this.modelSelected.idModel,
                region: 'center'
        };                 
        this.callParent();
    }
});