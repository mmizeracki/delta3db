/* 
 * Model Container
 */

Ext.define('delta3.view.mb.ModelContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.models',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent ModelContainer");        
        me.items = {
                xtype:  'grid.model',
                region: 'center'
        },   
        me.callParent();
    }
});