/* 
 * Table Tree Panel
 */

Ext.define('delta3.view.mb.TableTreePanel', {
    title: "Step 1B: Atomic Field Selection",
    extend: 'Ext.tree.TreePanel',
    itemId: 'modelTableTree',
    tbar: [{
            text: 'Select All',
            iconCls: 'selectAll-icon16',
            tooltip: delta3.utils.Tooltips.atomicBtnSelect,                
            handler: function() {
                var thisTree = Ext.ComponentQuery.query('#modelTableTree')[0];
                var r = thisTree.getSelectionModel().getSelection();
                var root = thisTree.getRootNode();
                var child = root.findChild('text', r[0].get('text'));
                child.cascadeBy(function(n) {
                    if ((typeof n.get('checked') !== 'undefined') && (n.parentNode.id !== 'root')) { // avoid table node
                        n.set('checked', true);
                    }
                });
            }
        }, {
            text: 'Add Field to Model',
            iconCls: 'add_new-icon16',
            tooltip: delta3.utils.Tooltips.atomicBtnAddField,                           
            handler: function() {
                var fieldGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                var msgBoxFlag = 0;         
                ModelData.relationshipTreeStore.getRootNode().cascadeBy(function(n) {
                    if ((n.get('checked') === true) && (n.parentNode.id !== 'root')) {
                        if ( msgBoxFlag === 0 ) {
                            Ext.MessageBox.wait('Populating field list ...');
                            msgBoxFlag = 1;
                        }
                        var tableID;
                        if (n.parentNode.data.text in ModelData.tableStore.tableToIdMap) {
                            tableID = ModelData.tableStore.tableToIdMap[n.parentNode.data.text];
                        } else {
                            tableID = 0;
                        }
                        var r = delta3.model.FieldModel.create({
                            physicalName: n.get('text'),
                            tableName: n.parentNode.data.text,
                            name: n.get('text'),
                            type: n.data.qtip,
                            modelTableidModelTable: tableID,
                            idModel: ModelData.modelSelected.idModel,
                            description: '',
                            formula: '',
                            fieldClass: 'Not Assigned',
                            fieldKind: 'Not Assigned',
                            active: true,
                            atomic: true,
                            keyField: true,
                            insertable: true,
                            virtual: false,
                            createdTS: '0000-00-00 00:00:00.0',
                            updatedTS: '0000-00-00 00:00:00.0'
                        });
                        var valid = true;
                        fieldGrid.store.each(function (record, index) {
                            //validating uniqueness of new field name 
                            if (record.data.name === r.data.name) {
                                 valid = false;
                                 return;
                             }
                        });
                        if ( valid === false ) {
                            msgBoxFlag = 1;
                            var newName = '';
                            var win = new Ext.Window(
                                    {
                                        layout: 'fit',
                                        width: 400,
                                        height: 200,
                                        itemId: 'uniqueNamePopup2',
                                        title: 'DELTA3',
                                        modal: true,
                                        closeAction: 'hide',
                                        items: new Ext.Panel(
                                                {
                                                    frame: true,
                                                    labelWidth: 180,
                                                    labelAlign: 'right',
                                                    title: 'Field name change',
                                                    bodyStyle: 'padding:5px 5px 0',
                                                    width: 400,
                                                    height: 200,
                                                    autoScroll: true,
                                                    itemCls: 'form_row',
                                                    defaultType: 'displayfield',
                                                    buttons: [
                                                        {text: 'Accept',
                                                            handler: function() {
                                                                //newName = Ext.ComponentQuery.query('#newFieldName')[0].value;
                                                                newName = this.up('panel').down('#newFieldName').value;
                                                                if ( r.data.name.toUpperCase() !== newName.toUpperCase() ) {
                                                                    r.data.name = newName;
                                                                    fieldGrid.store.add(r);                    
                                                                    fieldGrid.store.sync();
                                                                    fieldGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
                                                                    n.set('checked', false);
                                                                    n.set('iconCls', 'field-used');
                                                                    win.destroy();
                                                                    return false;
                                                                } else {
                                                                    delta3.utils.GlobalFunc.showDeltaMessage('Field names must be unique: ' + r.data.name);
                                                                }
                                                            }
                                                        },
                                                        {text: 'Cancel',
                                                            handler: function() {
                                                                Ext.ComponentQuery.query('#uniqueNamePopup')[0].destroy();
                                                                return false;
                                                            }
                                                        }
                                                    ],
                                                    items: [{
                                                            fieldLabel: 'Current field name',
                                                            name: 'currentFieldName',
                                                            allowBlank: false,
                                                            value: r.data.name
                                                        },{
                                                            xtype: 'textfield',
                                                            fieldLabel: 'New field name',
                                                            itemId: 'newFieldName',
                                                            allowBlank: true,
                                                            value: newName
                                                        }
                                                    ]
                                                })
                                    });
                            win.show();                        
                            return false;                            
                        }                           
                        fieldGrid.store.add(r);
                        n.set('checked', false);
                        n.set('iconCls', 'field-used');
                    }
                });
                if ( msgBoxFlag === 1) {
                    Ext.MessageBox.hide();
                }
                fieldGrid.store.sync();
                fieldGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
            }
        }, {
            text: 'Set Primary Table',
            iconCls: 'primary-icon16',
            tooltip: delta3.utils.Tooltips.atomicBtnSetPrimary,                           
            handler: function() {
                var thisTree = Ext.ComponentQuery.query('#modelTableTree')[0];
                var r = thisTree.getSelectionModel().getSelection();
                ModelData.tableStore.setPrimary(r[0].data.text);
            }
        }, {
            text: 'Delete',
            iconCls: 'delete-icon16',
            tooltip: delta3.utils.Tooltips.atomicBtnDelete,                           
            handler: function() {
                Ext.Msg.show({
                    title:'DELTA3',
                    message: 'You are about to delete Table from Model. All associated fields will be deleted.<br> Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            var thisTree = Ext.ComponentQuery.query('#modelTableTree')[0];
                            var r = thisTree.getSelectionModel().getSelection();
                            var dr = ModelData.tableStore.findRecord('physicalName', r[0].get('text'));
                            ModelData.tableStore.remove(dr);
                            ModelData.tableStore.sync();
                            var root = thisTree.getRootNode();
                            var child = root.findChild('text', r[0].get('text'));
                            root.removeChild(child); // from UI
                        }
                    }
                });                 
            }
        }, {
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.atomicBtnRefresh,                           
            handler: function() {
                ModelData.tableStore.load();              
            }
        }],
    viewConfig: {
        allowCopy: true,
        copy: true,
        plugins: {
            ptype: 'treeviewdragdrop',
            dropGroup: 'treeTableDDGroup',
            dragGroup: 'gridColumnDDGroup',
            containerScroll: true
        },
        listeners: {
            beforedrop: function(node, data) {
                if (data.records[0].parentNode.data.text.toLowerCase() === ModelData.modelSelected.outputName.toLowerCase()) {
                    return false;
                }
                if (data.records[0].data.text === 'Database') {
                    return false;
                }
            },
            drop: function(node, data, dropRec, dropPosition) {
                var r = delta3.model.ModelTableModel.create({
                    name: data.records[0].get('text'),
                    physicalName: data.records[0].get('text'),
                    idModelTable: 0,
                    active: true,
                    modelidModel: ModelData.modelSelected.idModel,
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'
                });
                var root = ModelData.relationshipTreeStore.getRootNode();
                if ( root.childNodes.length === 1 ) {
                    r.set('primaryTable', true);
                }                   
                ModelData.tableStore.insert(node.rowIndex, r);
                var root = ModelData.relationshipTreeStore.getRootNode();
                root.insertChild(0, data.records[0]);
                this.up().addCheckBoxes(root, data.records[0].get('text'));
                ModelData.tableStore.sync();
            }
        }
    },
    addCheckBoxes: function(root, tableName) {
        var branch = root.findChild('text', tableName);
        for (var i = 0; i < branch.childNodes.length; i++) {
            branch.getChildAt(i).set('checked', false);
        }
    },
    initComponent: function() {
        var me = this;
        var root = me.getRootNode();
        me.callParent();
    }
});
