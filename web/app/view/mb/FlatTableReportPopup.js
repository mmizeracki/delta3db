/* 
 * Flat Table Status Report Popup Window - not used
 */

Ext.define('delta3.view.logregr.FlatTableReportPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.ftReport',
    layout: 'fit',
    width: 520,
    height: 300,
    itemId: 'ftReportPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    statusString: {},
    items: [{xtype: 'panel',
            frame: true,
            labelWidth: 20,
            labelAlign: 'right',
            bodyStyle: 'padding:5px 5px 0',
            width: 520,
            height: 410,
            title: 'Flat Table Status',
            autoScroll: true,
            defaultType: 'displayfield',
            items: [{
                    //fieldLabel: 'LRF:',
                    name: 'fField',
                    value: ''
                }]
        }],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#ftReportPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.items[0].items[0].value = me.statusString;
        me.callParent();
    }
});
