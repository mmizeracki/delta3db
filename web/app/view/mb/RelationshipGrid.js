/* 
 * Relationship Grid
 */

Ext.define('delta3.view.mb.RelationshipGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Step 1C: Select Relationships',
    requires: [
        'delta3.store.RelationshipStore'
    ],
    height: 364,
    width: 400,
    itemId: 'relationshipGrid',
    listeners: {
        beforeshow: function(node, data) {
            var me = this;
            me.store.idModel = ModelData.modelSelected.idModel;
            me.store.proxy.extraParams.model = ModelData.modelSelected.idModel;
            me.store.load();
        }
    },
    initComponent: function() {
        var me = this;
        me.store = new Ext.create('delta3.store.RelationshipStore');
        me.plugins = me.buildPlugins();
        me.columns = me.buildColumns();
        me.callParent();
    },
    tbar: [{
            text: 'Add Relationship',
            iconCls: 'add_new-icon16',
            tooltip: delta3.utils.Tooltips.relBtnAdd,                 
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                ModelData.modelKey1ComboStore.clearFilter(true);
                ModelData.modelKey2ComboStore.clearFilter(true);
                var cTable1 = Ext.ComponentQuery.query('#relationshipComboTable1')[0];
                var cKey1 = Ext.ComponentQuery.query('#relationshipComboKey1')[0];
                var cTable2 = Ext.ComponentQuery.query('#relationshipComboTable2')[0];
                var cKey2 = Ext.ComponentQuery.query('#relationshipComboKey2')[0];
                cTable1.reset();
                cKey1.reset();
                cTable2.reset();
                cKey2.reset();
                var r = delta3.model.ModelRelationshipModel.create({
                    table1: '1',
                    key1: '1',
                    table2: '1',
                    key2: '1',
                    type: '1',
                    modelidModel: ModelData.modelSelected.idModel,
                    createdTS: '0000-00-00 00:00:00.0',
                    createdBy: '0',
                    updatedTS: '0000-00-00 00:00:00.0',
                    updatedBy: '0'
                });
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            text: 'Delete',
            iconCls: 'delete-icon16',
            tooltip: delta3.utils.Tooltips.relBtnDelete,                 
            handler: function() {
                Ext.Msg.show({
                    title:'DELTA3',
                    message: 'You are about to delete Relationship from Model. Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            ModelData.modelKey1ComboStore.clearFilter(true);
                            ModelData.modelKey2ComboStore.clearFilter(true);                            
                            var fieldGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                            var k1 = sm.getSelection()[0].get('key1');
                            var k2 = sm.getSelection()[0].get('key2');
                            if ( thisGrid.isKeyUsedMoreThanOnce(thisGrid.store, k1) < 2 ) {
                                fieldGrid.store.findRecord('idModelColumn', k1).set('keyField', false);
                            }
                            if ( thisGrid.isKeyUsedMoreThanOnce(thisGrid.store, k2) < 2 ) {
                                fieldGrid.store.findRecord('idModelColumn', k2).set('keyField', false);  
                            }
                            fieldGrid.store.sync();
                            thisGrid.store.remove(sm.getSelection());
                            thisGrid.store.sync();                         
                            sm.select(0);
                        }
                    }
                });                
            }
        }, {
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.relBtnRefresh,                           
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                ModelData.modelKey1ComboStore.clearFilter(true);
                ModelData.modelKey2ComboStore.clearFilter(true);
                thisGrid.store.load();
            }
        }],
    buildColumns: function() {
        return [
            {text: 'Table1', dataIndex: 'table1', type: 'int', width: 140, tooltip: delta3.utils.Tooltips.relGridTable1, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelTableComboStore, // use pre-populated in ModelTableStore
                            displayField: 'physicalName',
                            valueField: 'idModelTable',
                            queryMode: 'local',
                            itemId: 'relationshipComboTable1',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    ModelData.modelKey1ComboStore.clearFilter(true);
                                    ModelData.modelKey1ComboStore.filter("modelTableidModelTable", h);
                                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                                    //var root = ModelData.metadataStore.tree.root;
                                    var r = thisGrid.store.getNewRecords();
                                    if (r.length > 0) { // process only new record
                                        r[0].data.table1 = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['table1'];
                    var model = ModelData.modelTableComboStore.findRecord('idModelTable', tableIndex);
                    if (model === null)
                        return null;
                    else {
                        return model.get("physicalName");
                    }
                }},
            {text: 'Key1', dataIndex: 'key1', type: 'int', width: 120, tooltip: delta3.utils.Tooltips.relGridKey1, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelKey1ComboStore, // use pre-populated
                            displayField: 'name',
                            valueField: 'idModelColumn',
                            itemId: 'relationshipComboKey1',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                                    var r = thisGrid.store.getNewRecords();
                                    if (r.length > 0) { // process only new record
                                        r[0].data.key1 = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = ModelData.modelKey1ComboStore.findRecord('idModelColumn', rec.data['key1']);
                    if (model === null)
                        return null;
                    else
                        return model.get("name");
                }},
            {text: 'Table2', dataIndex: 'table2', type: 'int',  width: 140, tooltip: delta3.utils.Tooltips.relGridTable2, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelTableComboStore, // use pre-populated
                            displayField: 'physicalName',
                            valueField: 'idModelTable',
                            itemId: 'relationshipComboTable2',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    ModelData.modelKey2ComboStore.clearFilter(true);
                                    ModelData.modelKey2ComboStore.filter("modelTableidModelTable", h);
                                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                                    var r = thisGrid.store.getNewRecords();
                                    if (r.length > 0) { // process only new record
                                        r[0].data.table2 = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['table2'];
                    var model = ModelData.modelTableComboStore.findRecord('idModelTable', tableIndex);                  
                    if (model === null)
                        return null;
                    else {
                        return model.get("physicalName");
                    }
                }},
            {text: 'Key2', dataIndex: 'key2', type: 'int',  width: 120, tooltip: delta3.utils.Tooltips.relGridKey2, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelKey2ComboStore, // use pre-populated
                            displayField: 'name',
                            valueField: 'idModelColumn',
                            itemId: 'relationshipComboKey2',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                                    var r = thisGrid.store.getNewRecords();
                                    if (r.length > 0) { // process only new record
                                        r[0].data.key2 = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = ModelData.modelKey2ComboStore.findRecord('idModelColumn', rec.data['key2']);
                    if (model === null)
                        return null;
                    else
                        return model.get("name");
                }},
            //{text: 'Type', dataIndex: 'type', type: 'int', editor: 'numberfield', width: 44}      
            {text: 'Type', dataIndex: 'type', width: 100, tooltip: delta3.utils.Tooltips.relGridType, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.relationshipComboBoxStore,
                            displayField: 'type',
                            valueField: 'value',
                            emptyText: '',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var typeString = delta3.utils.GlobalVars.relationshipComboBoxStore.findRecord('value', rec.data['type']);
                    if (typeString === null)
                        return 'error';
                    else
                        return typeString.get('type');
                }
            }
        ];
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 0,
            autoCancel: true,
            errorSummary: true,
            listeners: {
                beforeedit: function(editor, context, eOpts) {
                    ModelData.modelKey1ComboStore.clearFilter(true);
                    ModelData.modelKey1ComboStore.filter("modelTableidModelTable", context.record.data.table1);
                    ModelData.modelKey2ComboStore.clearFilter(true);
                    ModelData.modelKey2ComboStore.filter("modelTableidModelTable", context.record.data.table2);                    
                },                        
                afteredit: function(editor, context, eOpts) {
                    var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                    thisGrid.store.sync();
                    ModelData.modelKey1ComboStore.clearFilter(true);
                    ModelData.modelKey2ComboStore.clearFilter(true);
                    var fieldGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                    fieldGrid.store.findRecord('idModelColumn', context.record.data.key1).set('keyField', true);
                    fieldGrid.store.findRecord('idModelColumn', context.record.data.key2).set('keyField', true);
                    fieldGrid.store.sync();
                }
            }
        })];
    },
    isKeyUsedMoreThanOnce: function(store, key) {
        var count = 0;
        store.each(function (record, index) {
            if (record.data.key1 === key) {
                 count++;;
            }
            if (record.data.key2 === key) {
                 count++;;
            }            
        });    
        return count;
    }
});

