/* 
 * TableViewer Grid
 */

Ext.define('delta3.view.mb.TableViewerGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.tableViewer',
    itemId: 'tableViewerGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'rowmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.column.Column'
    ],
    border: false,
    viewConfig: {
        enableTextSelection: true
    },
    listeners: {
        sortchange: function(ct, column, direction, eOpts) {
            var me = this;
            if ((me.store.proxy.extraParams.orderBy !== column.dataIndex)
                    || (me.store.proxy.extraParams.direction !== direction)) {
                me.store.proxy.extraParams.orderBy = column.dataIndex;
                me.store.proxy.extraParams.direction = direction;
                me.store.proxy.extraParams.filterId = me.selectedFilterId;
                me.store.load();
            }
        }
    },
    tbar: [{xtype: 'tbfill'}, 'Filters:', {},
        {
            itemId: 'applyFilter',
            text: 'Apply Filter',
            iconCls: 'filter-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#tableViewerGrid')[0];
                thisGrid.store.proxy.extraParams.modelId = thisGrid.idModel;
                thisGrid.store.proxy.extraParams.filterId = thisGrid.selectedFilterId;
                thisGrid.store.load();
            }
        },
        {
            itemId: 'removeFilter',
            text: 'Remove Filter',
            iconCls: 'no_filter-icon16',
            handler: function() {
                var filterComboBox = Ext.ComponentQuery.query('#filterComboBox')[0];
                filterComboBox.clearValue();
                var thisGrid = Ext.ComponentQuery.query('#tableViewerGrid')[0];
                thisGrid.selectedFilterId = 0;
                thisGrid.store.proxy.extraParams.modelId = thisGrid.idModel;
                thisGrid.store.proxy.extraParams.filterId = thisGrid.selectedFilterId;
                thisGrid.store.load();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No data found',
            displayInfo: true
        }],
    columns: [],
    selectedFilterId: 0,
    initComponent: function() {
        var me = this;
        var dropDown = new Ext.form.ComboBox({
            store: new delta3.store.FilterStore({idModel: me.idModel}),
            itemId: 'filterComboBox',
            displayField: 'name',
            valueField: 'idFilter',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.selectedFilterId = cmb.getValue();
                }
            }
        });
        dropDown.store.load();
        me.tbar[2] = dropDown;    // this index has to match content of tool bar
        me.store = me.buildStore();
        me.store.proxy.extraParams.modelId = me.idModel;
        me.store.proxy.extraParams.direction = '';
        me.store.proxy.extraParams.orderBy = '';
        me.dockedItems[0].store = me.store;
        me.callParent();
        me.store.load();
    },
    buildStore: function() {
        return Ext.create('delta3.store.TableViewerStore');
    }

});



