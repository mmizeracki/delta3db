/* 
 * Model Grid
 */

Ext.define('delta3.view.mb.ModelGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.model',
    itemId: 'modelGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selModel: {selType: 'checkboxmodel',
        mode: 'SINGLE'},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.view.mb.ModelBuilderContainer',
        'delta3.view.mb.TableViewerContainer',
        'delta3.view.ImportPopup',
        'delta3.view.ImportFilePopup',
        'delta3.view.RecordInfoPopup',
        'delta3.utils.GridFilter'
    ],
    configStore: {},
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                edit: function(rowEditor, context, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var valid = true;
                    var counter = 0;
                    thisGrid.store.each(function (record, index) {
                        //validating uniqueness of new study name 
                        if (record.data.name === context.newValues.name) {
                            counter++;
                            if ( counter > 1 ) {
                                valid = false;
                                return;
                            }
                         }
                    });
                    if ( valid === false ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Data Model names must be unique.');
                        return;                            
                    }                    
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No models found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.configStore = delta3.store.ConfigurationStore.create();
        me.configStore.load();        
        var deleteModel;
        if (delta3.utils.GlobalFunc.isPermitted("ModelDelete") === true) {
            deleteModel = {
                itemId: 'ModelDelete',
                text: 'Delete Model',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.modelBtnDelete,            
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                        return;
                    }                      
                    Ext.Msg.show({
                        title:'DELTA3',
                        message: 'You are about to delete Model. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });                    
                }
            };
        }        
        me.tbar = [];
        var x = 0;
        me.tbar[x++] = {
            text: 'Step 1: Manage Model',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [{
                        text: 'Add Model',
                        iconCls: 'add_new-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnAdd,                            
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            // Create a new record instance
                            var d = new Date();
                            var r = delta3.model.ModelModel.create({
                                name: '',
                                longName: 'New Model',
                                type: '',
                                status: 'New',
                                outputName: 'ft' + d.valueOf(),
                                createdTS: '0000-00-00 00:00:00.0',
                                updatedTS: '0000-00-00 00:00:00.0'});
                            thisGrid.store.add(r);   
                            thisGrid.plugins[0].startEdit(r);                            
                        }
                    }, {
                        text: 'Edit Model',
                        iconCls: 'edit-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnEdit,                            
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];                           
                            var sel = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof sel === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }                                     
                            thisGrid.plugins[0].startEdit(sel, 0);
                        }  
                    }, {
                        itemId: 'cloneModel',
                        text: 'Clone Model',
                        iconCls: 'cloneModel-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnClone,                            
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0]
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }                    
                            delta3.utils.GlobalFunc.doCloneModel(sm.getSelection()[0].data, thisGrid);
                        }
                    }, {
                        itemId: 'recordInfo',
                        text: 'View Properties',
                        iconCls: 'recordInfo-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnProperties,                            
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0]
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }                            
                            var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Data Model"});
                            win.show();
                        }
                    }, deleteModel]
            })
        };        
        me.tbar[x++] = {
            itemId: 'editModel',
            text: 'Step 2: Build Model Structure',
            iconCls: 'modelbuilder-icon16',
            tooltip: delta3.utils.Tooltips.modelBtnMB,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0]
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                    return;
                }                   
                var dataSourceName = '';
                var confRec = thisGrid.configStore.findRecord('idConfiguration', sm.getSelection()[0].data.idConfiguration);
                if (confRec !== null) {
                    dataSourceName = confRec.get("name");
                }
                delta3.utils.GlobalFunc.openModelTab(sm.getSelection()[0].data, dataSourceName);
                Ext.MessageBox.wait('Loading Data Model ' + sm.getSelection()[0].data.name + ' ...');  
            }
        };       
        me.tbar[x++] = {
            text: 'Import/Export',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                        itemId: 'exportModel',
                        text: 'Export',
                        iconCls: 'exportModel-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnExport,                
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }                    
                            delta3.utils.GlobalFunc.doExportModel(sm.getSelection()[0].data);
                        }
                    }, {
                        itemId: 'importModel',
                        text: 'Quick Import',
                        iconCls: 'importModel-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnImport,                
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.ImportPopup();
                            win.show();
                        }
                    }, {
                        itemId: 'importModelFromFile',
                        text: 'Import',
                        iconCls: 'importModel-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnImport,                
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.ImportFilePopup();
                            win.show();
                        }
                    }, {
                        text: 'Export Grid to CSV',
                        iconCls: 'exportCSV-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
                        handler: function(b, e) {
                            b.up('grid').exportGrid('DELTA3 Data Model List');
                        }
                    }]
            })
        };                
        me.tbar[x++] = {
            itemId: 'refreshModel',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.modelBtnRefresh,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                thisGrid.store.load();
            }
        };        
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns(me);
        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me});   
        me.callParent();
        me.store.load();
    },
    buildColumns: function(me) {
        return [
            {text: 'ID', dataIndex: 'idModel', width: 40, locked: true, tooltip: delta3.utils.Tooltips.modelGridId},
            {text: 'Name', dataIndex: 'name', locked: true, tooltip: delta3.utils.Tooltips.modelGridName, editor: 'textfield', width: 90},     
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 300, tooltip: delta3.utils.Tooltips.modelGridDescription},
            {text: 'Source', dataIndex: 'idConfiguration', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.modelGridSource,
                editor: new Ext.form.ComboBox({
                    store: me.configStore,
                    itemId: 'configurationComboBox',
                    displayField: 'name',
                    valueField: 'idConfiguration',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idConfiguration'];
                    var confRec = me.configStore.findRecord('idConfiguration', tableIndex);
                    if (confRec === null)
                        return null;
                    else {
                        return confRec.get("name");
                    }
                }},
            {text: 'Status', dataIndex: 'status', width: 300, tooltip: delta3.utils.Tooltips.modelGridStatus},
            {text: 'Flat Table Name', dataIndex: 'outputName', width: 120, tooltip: delta3.utils.Tooltips.modelGridFTName}            
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        return Ext.create('delta3.store.ModelStore');
    }

});

