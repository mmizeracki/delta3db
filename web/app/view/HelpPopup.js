/* 
 * Help Popup Window
 */

Ext.define('delta3.view.HelpPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.help',
    layout: 'fit',
    width: 450,
    height: 360,
    itemId: 'helpPopup',
    helpSource: {},
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3 Help',
    bbar: { items: [
             '->'
            , { text: 'Close', handler: function () { Ext.ComponentQuery.query('#helpPopup')[0].close(); } }
            ]
    },
    initComponent: function() {
        var me = this;
        Ext.Ajax.request({
            url: me.helpSource,
            success: function(response, options) {
                if ( response.statusText === 'OK') {
                    var panelHTML = Ext.ComponentQuery.query('#helpPopup')[0];  
                    var RT = response.responseText;
                    panelHTML.body.update(RT);
                    panelHTML.doLayout();     
                    }
                }
        });        
        me.callParent();
    }
});


