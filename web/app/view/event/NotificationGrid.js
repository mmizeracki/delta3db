/* 
 * Notification Grid
 */

Ext.define('delta3.view.event.NotificationGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.notification',
    itemId: 'notificationGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.store.NotificationStore'
    ],
    border: false,
    tbar: [{
                itemId: 'NotificationDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    Ext.Msg.show({
                        title:'DELTA3',
                        message: 'You are about to delete Notification. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                var thisGrid = Ext.ComponentQuery.query('#notificationGrid')[0]
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });                    
                }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No events found',
            displayInfo: true
        }],
    modelSelected: {
        idModel: 0,
        name: ""
    },
    fieldStore: {},
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.plugins = me.buildPlugins();     
        me.columns = me.buildColumns(me.fieldStore);
        var x = me.tbar.length;
        if (delta3.utils.GlobalFunc.isPermitted("NotificationDelete") === true) {
            me.tbar[x++] = {
                itemId: 'NotificationDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#notificationGrid')[0]
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    thisGrid.store.remove(sm.getSelection());
                    thisGrid.store.sync();
                }
            };
        }
        me.callParent();
        me.store.load();
    },
    buildColumns: function(fieldStore) {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        var eventtemplateStore = delta3.store.EventtemplateStore.create();
        eventtemplateStore.load();        
        return [
            {text: 'ID', dataIndex: 'idNotification', width: 30},           
            {text: 'Name', dataIndex: 'name', width: 100, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 200},       
            {text: 'Event Data', dataIndex: 'eventData', width: 120},    
            {text: 'Acknowledged', disabled: true, xtype: 'checkcolumn', dataIndex: 'acknowledged', width: 80, editor: 'checkboxfield'},  
            {text: 'Retry Count', dataindex: 'retryCount', width: 60},
            //{text: 'Time Out', dataIndex: 'timeOut', width: 60, editor: 'textfield'},               
            {text: 'Event ID', dataindex: 'idEvent', width: 60},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50, editor: 'checkboxfield'},    
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 80,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 80,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    buildStore: function() {
        console.log("Notification grid building NotificationStore");
        return Ext.create('delta3.store.NotificationStore');
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'notificationGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#notificationGrid')[0];
                    thisGrid.store.save();
                }
            }
            })
        ];
    }    
});

