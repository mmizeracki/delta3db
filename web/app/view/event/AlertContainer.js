/* 
 * Alert Container
 */

Ext.define('delta3.view.event.AlertContainer', {
    extend: 'Ext.container.Container',
    itemId: 'alertContainer',
    alias:  'widget.container.alerts',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent AlertContainer");        
        me.items = {
                xtype:  'grid.alert',
                region: 'center'
        },   
        me.callParent();
    }
});

