/* 
 * Notification Container
 */

Ext.define('delta3.view.event.NotificationContainer', {
    extend: 'Ext.container.Container',
    itemId: 'notificationContainer',
    alias:  'widget.container.notifications',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent NotificationContainer");        
        me.items = {
                xtype:  'grid.notification',
                region: 'center'
        },   
        me.callParent();
    }
});

