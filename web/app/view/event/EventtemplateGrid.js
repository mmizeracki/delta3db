/* 
 * Eventtemplate Grid
 */

Ext.define('delta3.view.event.EventtemplateGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.eventtemplate',
    itemId: 'eventtemplateGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.store.EventtemplateStore'
    ],
    border: false,
    tbar: [{
            itemId: 'addEventtemplate',
            text: 'Add Event Template',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#eventtemplateGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.EventtemplateModel.create({
                    active: true,
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No event templates found',
            displayInfo: true
        }],
    modelSelected: {
        idModel: 0,
        name: ""
    },
    fieldStore: {},
    initComponent: function() {
        var me = this;
        //delta3.utils.GlobalVars.FieldStore.load();    
        me.fieldStore = delta3.utils.GlobalVars.FieldStore;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.plugins = me.buildPlugins();     
        me.columns = me.buildColumns(me.fieldStore);
        var x = me.tbar.length;
        if (delta3.utils.GlobalFunc.isPermitted("EventtemplateDelete") === true) {
            me.tbar[x++] = {
                itemId: 'EventtemplateDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#eventtemplateGrid')[0]
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    thisGrid.store.remove(sm.getSelection());
                    thisGrid.store.sync();
                }
            };
        }
        me.callParent();
        me.store.load();
    },
    buildColumns: function(fieldStore) {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idEventTemplate', width: 30},
            {text: 'Name', dataIndex: 'name', width: 100, editor: 'textfield'},
            //{text: 'Name',  xtype:'templatecolumn', tpl:'<b>{name}</b>'},
            {text: 'Description', dataIndex: 'description', width: 100, editor: 'textfield'},       
            {text: 'Type', dataIndex: 'type', width: 100, editor: 'textfield'},    
            {text: 'Module ID', dataIndex: 'idModule', width: 60, type: 'int', editor: 'numberfield'},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50, editor: 'checkboxfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    buildStore: function() {
        console.log("Eventtemplate grid building EventtemplateStore");
        return Ext.create('delta3.store.EventtemplateStore');
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'eventtemplateGridEditor',
            listeners: {
                beforeedit: function(rowEditor, context, r) {
                    var thisGrid = Ext.ComponentQuery.query('#eventtemplateGrid')[0];
                    thisGrid.fieldStore.clearFilter(true);
                },
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#eventtemplateGrid')[0];
                    thisGrid.store.save();
                }
            }
            })
        ];
    }    
});

