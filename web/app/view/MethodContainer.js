/* 
 * Method Container
 */

Ext.define('delta3.view.MethodContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.methods',
    requires:   [
        'Ext.layout.container.Border'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent MethodContainer");
        me.items = {
                xtype:  'grid.method',
                region: 'center'
        }, 
        me.callParent();
    }
});