/* 
 * User Container
 */

Ext.define('delta3.view.RoleContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.roles',
    requires:   [
        'Ext.layout.container.Border',
        'Ext.resizer.BorderSplitterTracker'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent RoleContainer");
        me.items = {
                xtype:  'grid.role',
                region: 'center'
        },  
        me.callParent();
    }
});



