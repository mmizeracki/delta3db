/* 
 * User Grid
 */

Ext.define('delta3.view.UserGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.user',
    itemId: 'userGrid',
    autoScroll: true,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'Ext.toolbar.Paging',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.form.ComboBox',
        'delta3.utils.GridFilter'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                    thisGrid.store.save();
                },
                validateedit: function(editor, e) {
                    console.log('field ' + e.field + ' has value ' + e.value);
                    if (e.field === 'organization') {
                        e.record.data['organization'] = Ext.ComponentQuery.query('#orgComboBox')[0].getValue();
                    }
                }
            }
        })
    ],
    border: false,
    tbar: [{
            text: 'Add User',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.UserModel.create({
                    organization: 1,
                    person: 1,
                    passChangedTS: '0000-00-00 00:00:00.0',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        },{
            text: 'Edit User',
            iconCls: 'edit-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];                           
                var sel = thisGrid.getSelectionModel().getSelection()[0];
                if (typeof sel === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                    return;
                }                            
                thisGrid.plugins[0].startEdit(sel, 0);
            }
        }, {
            itemId: 'assignUserRole',
            text: 'User Roles',
            iconCls: 'roles-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                if (typeof thisGrid.getSelectionModel().getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                    return;
                }
                var currUserRoles = "";          // comma delimited string of existing roles
                var currRoleId = new Array();    // array to hold ids of existing roles
                var idRole = null;               // array of new selected role ids         
                var idUser = thisGrid.getSelectionModel().getSelection()[0].data.idUser;
                var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                delta3.utils.GlobalFunc.getUserAuthInfo(alias, idUser, function(response, options) {
                    var responseObject = JSON.parse(response.responseText);
                    if (responseObject.success === true) {
                        for (i = 0; i < responseObject.userAuth.roles.length; i++) {
                            if (i > 0) {
                                currUserRoles += ", ";
                            }
                            currUserRoles += responseObject.userAuth.roles[i].name;
                            currRoleId[i] = responseObject.userAuth.roles[i].idRole;
                        }
                    }
                    delta3.utils.GlobalVars.RoleStore.filterBy(function(record, scope)
                    {
                        if (thisGrid.getSelectionModel().getSelection()[0].data.type === 'SADMIN') {
                            return true; // sys admin can pick any role
                        }
                        if (thisGrid.getSelectionModel().getSelection()[0].data.type === 'OADMIN') {
                            if (record.get("type") === 'SADMIN') {
                                return false; // org admin can pick any role except sys admin
                            }
                            return true;
                        }
                        return (record.get("type") === thisGrid.getSelectionModel().getSelection()[0].data.type);
                    });
                    var win = new Ext.Window(
                            {
                                layout: 'fit',
                                width: 400,
                                height: 200,
                                itemId: 'rolePopup',
                                modal: true,
                                closeAction: 'hide',
                                items: new Ext.Panel(
                                        {
                                            frame: true,
                                            labelWidth: 90,
                                            labelAlign: 'right',
                                            title: 'Select Roles for the User',
                                            bodyStyle: 'padding:5px 5px 0',
                                            width: 400,
                                            height: 200,
                                            autoScroll: true,
                                            itemCls: 'form_row',
                                            defaultType: 'displayfield',
                                            buttons: [
                                                {text: 'Save',
                                                    handler: function() {
                                                        //Ext.Msg.confirm('Confirm', 'Do you want to save?');
                                                        if (idUser !== null && alias !== null && idRole !== null) {
                                                            saveRoleUserRelationship(alias, idUser, idRole, function() {
                                                                console.log('calling saveRoleUserRelationship success ' + response.responseText);
                                                                Ext.ComponentQuery.query('#rolePopup')[0].destroy();
                                                            });
                                                        }
                                                    }
                                                },
                                                {text: 'Remove',
                                                    handler: function() {
                                                        //Ext.Msg.confirm('Confirm', 'Do you want to remove all Roles for this User?');
                                                        if (idUser !== null && alias !== null) {
                                                            removeRoleUserRelationship(alias, idUser, currRoleId, function() {
                                                                console.log('calling removeRoleUserRelationship success ' + response.responseText);
                                                                Ext.ComponentQuery.query('#rolePopup')[0].destroy();
                                                            });
                                                        }
                                                    }
                                                },
                                                {text: 'Cancel',
                                                    handler: function() {
                                                        Ext.ComponentQuery.query('#rolePopup')[0].destroy();
                                                    }
                                                }
                                            ],
                                            items: [{
                                                    fieldLabel: 'User',
                                                    name: 'userAlias',
                                                    allowBlank: false,
                                                    value: alias
                                                },
                                                new Ext.form.ComboBox({
                                                    store: delta3.utils.GlobalVars.RoleStore, // use pre-loaded
                                                    itemId: 'roleComboBox',
                                                    fieldLabel: 'Add Role',
                                                    displayField: 'name',
                                                    valueField: 'idRole',
                                                    queryMode: 'local',
                                                    multiSelect: true,
                                                    forceSelection: true,
                                                    listeners: {
                                                        'select': function(cmb, rec, idx) {
                                                            idRole = cmb.getValue();
                                                        }
                                                    }
                                                }), {
                                                    fieldLabel: 'Roles',
                                                    name: 'userRoles',
                                                    allowBlank: true,
                                                    value: currUserRoles
                                                }
                                            ]
                                        })
                            });
                    win.show();
                });
            }
        }, {
            text: 'Reset Password',
            iconCls: 'pass_reset-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                if (typeof thisGrid.getSelectionModel().getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                    return;
                }                
                var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                delta3.utils.GlobalFunc.resetPassword(alias);
            }
        }, {
            itemId: 'recordInfo',
            text: 'View Properties',
            iconCls: 'recordInfo-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#userGrid')[0]
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                    return;
                }                            
                var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "User"});
                win.show();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No users found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        // optional, permission driven buttons hookup follows
        var x = me.tbar.length;
        if (delta3.utils.GlobalFunc.isPermitted("Pseudo") === true) {
            me.tbar[x++] = {
                itemId: 'Pseudo',
                text: 'Pseudo',
                iconCls: 'pseudo-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0]
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    var idUser = thisGrid.getSelectionModel().getSelection()[0].data.idUser;
                    var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                    delta3.utils.GlobalFunc.doPseudo(alias, idUser, function() {
                        window.location = './mainLanding.html';
                    }
                    );
                }
            };
        }          
        me.store = me.buildStore();
        me.store.load();         
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns(me.store);
        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me});            
        me.callParent(); 
        this.renderer = (this.editor && this.editor.triggerAction) ? ComboBoxRenderer(this.editor, this.gridId) : function(value) {
            return value;
        };
    },
    buildColumns: function(userStore) {
        delta3.utils.GlobalVars.roleTypeComboBoxStore.filterBy(function(record, scope)
        {
            for (var i = 0; i < delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.roles.length; i++) {
                if (delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.roles[0].type === 'SADMIN') {
                    return true; // sys admin can pick any type
                }
            }
            if (record.get("type") === 'SADMIN') {
                return false; // org admin can pick any type except sys admin
            }
            return true;
        });
        return [
            {text: 'ID', dataIndex: 'idUser', locked: true},
            {text: 'Alias', dataIndex: 'alias', locked: true, editor: 'textfield'},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield'},
            {text: 'Organization', dataIndex: 'organization', editor: //'numberfield'},
                        new Ext.form.ComboBox({
                            //store: 'OrganizationStore',
                            store: delta3.utils.GlobalVars.OrgStore, // use pre-loaded
                            itemId: 'orgComboBox',
                            displayField: 'name',
                            valueField: 'idOrganization',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                                    var rec = thisGrid.store.getNewRecords();
                                    if (rec.length > 0) { // process only new record
                                        rec[0].data.organization = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = delta3.utils.GlobalVars.OrgStore.findRecord('idOrganization', rec.data['organization']);
                    return model.get("name");
                }},
            {text: 'Type', dataIndex: 'type', width: 100, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.roleTypeComboBoxStore,
                            displayField: 'type',
                            valueField: 'type',
                            emptyText: '',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                                }
                            }
                        })
            },
            //{text: "Passord Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', field: 'datefield', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
            {text: "Password Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u', editor: 'datefield', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
            //{text: "Passord Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', type:'date', dateFormat:'Y-m-d H:i:s',editor: 'textfield'},
            {text: "Password Changed TS", width: 120, sortable: true, dataIndex: 'passChangedTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
            {text: 'Pass Failure Max', dataIndex: 'passFailureMax', type: 'int', editor: 'numberfield'},
            {text: 'Pass Failure Count', dataIndex: 'passFailureCount', type: 'int', editor: 'numberfield'},
            //{text: 'GUID', dataIndex: 'guid'},
            {text: 'Security Handle', dataIndex: 'securityHandler', editor: 'numberfield'},
            {text: 'Status', dataIndex: 'status', editor: 'textfield'},
            {text: 'Locale', dataIndex: 'locale', editor: 'textfield'},
            {text: 'First Name', dataIndex: 'firstName', editor: 'textfield'},
            {text: 'Last Name', dataIndex: 'lastName', editor: 'textfield'},
            {text: 'Email Address 1', dataIndex: 'emailAddress1', editor: 'textfield'},
            {text: 'Email Address 2', dataIndex: 'emailAddress2', editor: 'textfield'},
            {text: 'URI1', dataIndex: 'uri1', editor: 'textfield'},
            {text: 'URI2', dataIndex: 'uri2', editor: 'textfield'},
            {text: 'Phone Number 1', dataIndex: 'phoneNumber1', editor: 'textfield'},
            {text: 'Phone Number 2', dataIndex: 'phoneNumber2', editor: 'textfield'},
            {text: 'Alert Prefernce 1', dataIndex: 'alertPreference1', editor: 'textfield'},
            {text: 'Alert Preference 2', dataIndex: 'alertPreference2', editor: 'textfield'}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.UserStore');
    }

});

//------------------------------------------------------------------------------ Supporting functions
function saveRoleUserRelationship(userAlias, idUser, idRole, callback)
{
    var userRoleJSONString = '{"users":[{"alias":"' + userAlias
            + '", "idUser":"' + idUser
            + '"}], "roles":' + JSON.stringify(idRole)
            + '}';

    console.log('calling saveRoleUserRelationship: ' + userAlias);
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/admin/addUserRole',
                        method: "POST",
                        disableCaching: true,
                        params: userRoleJSONString,
                        success: callback,
                        failure: saveRoleUserRelationshipFailed
                    }
            );
}

function saveRoleUserRelationshipFailed(response, options)
{
    console.log('calling saveRoleUserRelationship failed ' + response.responseText);
}


//------------------------------------------------------------------------------
function removeRoleUserRelationship(userAlias, idUser, idRole, callback)
{
    var userRoleJSONString = '{"users":[{"alias":"' + userAlias
            + '", "idUser":"' + idUser
            + '"}], "roles":' + JSON.stringify(idRole)
            + '}';

    console.log('calling removeRoleUserRelationship: ' + userAlias);
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/admin/removeUserRole',
                        method: "POST",
                        disableCaching: true,
                        params: userRoleJSONString,
                        success: callback,
                        failure: removeRoleUserRelationshipFailed
                    }
            );
}

function removeRoleUserRelationshipFailed(response, options)
{
    console.log('calling removeRoleUserRelationship failed ' + response.responseText);
}
