/* 
 * Study Grid
 */

Ext.define('delta3.view.study.StudyGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.study',
    itemId: 'studyGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    filterStore: {},
    modelStore: {},
    methodStore: {},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'Ext.grid.column.Action',        
        'Ext.form.DateField',
        'delta3.store.FieldStore',
        'delta3.store.StudyStore',
        'delta3.store.FilterStore',
        'delta3.view.study.OutcomePopup',
        'delta3.view.study.TreatmentPopup',
        'delta3.view.study.CategoryPopup',
        'delta3.view.study.DatePopup',
        'delta3.view.study.KeyPopup',        
        'delta3.view.study.LogisticRegrContainer',      
        'delta3.view.study.PropensityPopup',    
        'delta3.view.study.SPRTPopup',        
        'delta3.view.filter.FilterPopup',
        'delta3.utils.GridFilter'
    ],
    border: false,
    listeners: {
        beforedestroy: function(panel, eOpt) {
            var mButton = panel.down('#monitorStudyButton');
            clearInterval(mButton.periodicModelStatusCheck);
        }
    },     
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No studies found',
            displayInfo: true
        }],
    modelSelected: {
        idModel: 0,
        name: ""
    },
    initComponent: function() {
        var me = this;
        me.filterStore = delta3.store.FilterStore.create({idModel: 0});
        me.filterStore.load();    
        delta3.utils.GlobalVars.FieldStore.load();         
        me.modelStore = delta3.store.ModelStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        me.modelStore.load();
        me.methodStore = delta3.store.MethodStore.create();
        me.methodStore.load();        
        me.store = me.buildStore();    
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns(me);        
        me.plugins = me.buildPlugins();    
        var deleteStudy;
        if (delta3.utils.GlobalFunc.isPermitted("StudyDelete") === true) {
            deleteStudy = {
                itemId: 'StudyDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.studyBtnDelete,               
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                        return;
                    }                     
                    Ext.Msg.show({
                        title:'DELTA3',
                        message: 'You are about to delete Study. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });                    
                }
            };
        }         
        me.tbar = [];
        var x = 0;
        me.tbar[x++] = {
            itemId: 'addStudy',
            text: 'Step 1: Add Study',
            iconCls: 'add_new-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnAdd,               
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.StudyModel.create({
                    name: '',
                    description: '',
                    isMissingDataUsed: true,
                    confidenceInterval: '95',
                    active: true,
                    status: 'New',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.add(r);   
                thisGrid.plugins[0].startEdit(r);                
            }
        };        
        me.tbar[x++] = {
            text: 'Step 2: Edit Study',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [{
                    text: 'Edit Study',
                    iconCls: 'edit-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEdit,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        thisGrid.plugins[0].startEdit(sel, 0);
                    }
                }, {
                    text: 'Edit Parameters',
                    iconCls: 'edit_params-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEditParam,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        thisGrid.paramPopupDispatcher(sel, me);   
                    }
                }, {
                    text: 'Clear Parameters',
                    iconCls: 'clear_params-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnClearParam,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }          
                        Ext.Msg.show({
                            title:'DELTA3',
                            message: 'You are about to clear Study Parameters. Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                    sel.data.methodParams = ""; 
                                    sel.dirty = true; 
                                    thisGrid.store.sync();
                                }
                            }
                        });                        
                    }
                }, {
                    text: 'Study Filter',
                    iconCls: 'filter-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEditFilter,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        var win = new delta3.view.filter.FilterPopup({
                            fieldStore: delta3.utils.GlobalVars.FieldStore,
                            modelId: sel.data.idModel,
                            subTitle: 'Filter selection for study "' + sel.get('name') + '"',
                            isStudyMode: true,
                            studyId: sel.data.idStudy});
                        win.show();                        
                    }
                },{
                    itemId: 'cloneStudy',
                    text: 'Clone Study',
                    iconCls: 'cloneStudy-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnClone,                       
                    handler: function() {            
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }  
                        delta3.utils.GlobalFunc.doCloneStudy(sm.getSelection()[0].data, thisGrid);
                    }
                }, {
                    itemId: 'splitStudy',
                    text: 'Split Study',
                    iconCls: 'splitStudy-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnSplit,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }  
                        delta3.utils.GlobalFunc.doSplitStudy(sm.getSelection()[0].data, 'testNewStudy');          
                    }
                }, deleteStudy, 
                {
                    itemId: 'recordInfo',
                    text: 'View Properties',
                    iconCls: 'recordInfo-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnProperties,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0]
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Study"});
                        win.show();
                    }
                }]
            })
        };        
        me.tbar[x++] = {
            text: 'Step 3: Run Study',
            iconCls: 'spec-icon16',
            //tooltip: 'Step 2',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [{
                    itemId: 'executeStudy',
                    text: 'Run Current Stage',
                    iconCls: 'study-executeStep-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnRun,                   
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        delta3.utils.GlobalFunc.doExecuteStudy(sm.getSelection()[0].data, thisGrid);                        
                    }
                }, {
                    itemId: 'nextStepStudy',
                    text: 'Go To Next Stage',
                    iconCls: 'study-nextStep-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnGotoNext,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        //thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        delta3.utils.GlobalFunc.doAdvanceStudy(sm.getSelection()[0].data, thisGrid);                        
                    }
                }, {
                    itemId: 'previousStepStudy',
                    text: 'Go To Previous Stage',
                    iconCls: 'study-previousStep-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnGotoPrevious,                   
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        //thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        delta3.utils.GlobalFunc.doRollbackStudy(sm.getSelection()[0].data, thisGrid);                   
                    }
                }, {
                    itemId: 'showStudyResultsPopup',
                    text: 'Show Results',
                    iconCls: 'show_results-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnShowResults,                   
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        delta3.utils.GlobalFunc.getResultsforStudy(sm.getSelection()[0].data.idStudy, showResults);   
                        function showResults(result, options) {
                            if ( result.success === true ) {
                                var theTabs = Ext.ComponentQuery.query('#maintabs')[0];
                                theTabs.add(Ext.create('delta3.view.rv.ResultsContainer', {
                                    title: 'Process results [' + result.process[0].name + ', ' + result.process[0].idProcess + '], Model [' + result.process[0].idModel + ']',
                                    resultsData: result.process[0].data,
                                    processId: result.process[0].idProcess,
                                    name: result.process[0].name,
                                    desacription: result.process[0].description,
                                    closable: true
                                })).show();
                                theTabs.doLayout();
                            } else {
                                delta3.utils.GlobalFunc.showDeltaMessage(result.status.message);
                            }
                        }
                    }
                }]
            })
        };          
        me.tbar[x++] = {
            text: 'Start Monitor',
            iconCls: 'monitor-icon16',
            itemId: 'monitorStudyButton',
            tooltip: delta3.utils.Tooltips.studyBtnMonitor,               
            periodicModelStatusCheck: {},
            handler: function() {
                var me = this;
                if ( this.text === 'Start Monitor' ) { 
                    me.executeMonitorPulse(me);
                    this.periodicModelStatusCheck = setInterval(function () 
                        {me.executeMonitorPulse(me);}, 
                        9000); // every 9 sec (plus 1 sec for red-eye pulse)
                    this.setText('Stop Monitor');
                } else {
                    clearInterval(me.periodicModelStatusCheck);
                    this.setText('Start Monitor');
                }
            },
            executeMonitorPulse: function(button) {
                button.setIconCls('monitor_on-icon16');
                setTimeout(function(){button.callToGetModelStatus(button)},1000);
            },
            callToGetModelStatus: function(button) {
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                thisGrid.store.load();
                button.setIconCls('monitor-icon16');    
            }            
        };
        me.tbar[x++] = {
            itemId: 'refreshStudy',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnRefresh,               
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                thisGrid.store.load();
            }
        };        
        me.tbar[x++] = {
            text: 'Export to CSV',
            iconCls: 'exportCSV-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
            handler: function(b, e) {
                b.up('grid').exportGrid('DELTA3 Studies');
            }
        };
        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me});   
        me.callParent();
        me.store.load();
    },
    buildColumns: function(thisGrid) {
        return [               
            {text: 'ID', dataIndex: 'idStudy', width: 30, locked: true, tooltip: delta3.utils.Tooltips.studyGridId},
            {text: 'Name', dataIndex: 'name', width: 100, locked: true, tooltip: delta3.utils.Tooltips.studyGridName, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 130, tooltip: delta3.utils.Tooltips.studyGridDescription, editor: 'textfield'},
            {text: 'Data Model', dataIndex: 'idModel', width: 100, sortable: false, tooltip: delta3.utils.Tooltips.studyGridModel,
                editor: new Ext.form.ComboBox({
                    store: thisGrid.modelStore,
                    itemId: 'modelComboBox',
                    displayField: 'name',
                    valueField: 'idModel',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, eOpt) {
                            thisGrid.modelSelected = cmb.displayTplData[0];                                                      
                        }
                    }
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModel'];
                    var model = thisGrid.modelStore.findRecord('idModel', tableIndex);
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                }},
            {text: "Start", width: 90, sortable: true, dataIndex: 'startTS', type: 'date',
                dateFormat: 'Y-m-d H:i:s',
                tooltip: delta3.utils.Tooltips.studyGridStart,
                editor: 'datefield',
                renderer: Ext.util.Format.dateRenderer('m/d/Y')},
            {text: "End", width: 90, sortable: true, dataIndex: 'endTS', type: 'date',
                dateFormat: 'Y-m-d H:i:s',
                tooltip: delta3.utils.Tooltips.studyGridEnd,
                editor: 'datefield',
                renderer: Ext.util.Format.dateRenderer('m/d/Y')},         
            {text: 'CI Primary', dataIndex: 'confidenceInterval', width: 75, tooltip: delta3.utils.Tooltips.studyGridCIPrimary, editor: 'numberfield'},
            {text: 'CI Secondary', dataIndex: 'confidenceInterval2', width: 85, tooltip: delta3.utils.Tooltips.studyGridCISecondary, editor: 'numberfield'},                                 
            {text: 'Stage Status', dataIndex: 'status', width: 120, tooltip: delta3.utils.Tooltips.studyGridStatus},
            {text: 'Analysis Method', dataIndex: 'idMethod', width: 130, sortable: false, tooltip: delta3.utils.Tooltips.studyGridMethod,
                editor: new Ext.form.ComboBox({
                    store: thisGrid.methodStore,
                    itemId: 'methodComboBox',
                    displayField: 'name',
                    valueField: 'idMethod',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var r = thisGrid.store.getNewRecords();
                            if (r.length > 0) { // process only new record
                                r[0].data.idMethod = h;
                            }
                        }
                    }
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idMethod'];
                    var method = thisGrid.methodStore.findRecord('idMethod', tableIndex);
                    if (method === null)
                        return null;
                    else {
                        return method.get("name");
                    }
                }},            
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridOutcomeButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.OutcomePopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            {text: 'Outcome', dataIndex: 'idOutcome', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridOutcome,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idOutcome'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
              }},
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridTreatmentButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.TreatmentPopup({fieldStore: delta3.utils.GlobalVars.FieldStore, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            {text: 'Treatment', dataIndex: 'idTreatment', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridTreatment,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idTreatment'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridCategoryButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.CategoryPopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },            
            {text: 'Categories', dataIndex: 'idAlert', width: 180, sortable: false, tooltip: delta3.utils.Tooltips.studyGridCategory,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var cat = rec.data['categories'];
                    if ((typeof cat === 'undefined') || (cat.length === 0)) {
                        return null;
                    } else {
                        var catString = '';
                        for (var i = 0; i < cat.length; i++) {
                            if (i > 0)
                                catString += ', ';
                            catString += cat[i].name;
                        }
                        return catString;
                    }
                }},              
            {text: 'Filter', dataIndex: 'idFilter', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridFilter,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {          
                    var tableIndex = rec.data['idFilter'];                  
                    var filter = thisGrid.filterStore.findRecord('idFilter', tableIndex);                
                    if (filter === null)
                        return null;
                    else {
                        var tooltip = '';
                        var fields = filter.data['fields'];
                        var tooltip = '';
                        for (var i = 0; i < fields.length; i++) {
                            if (i > 0) {
                                tooltip += ', ';
                            }
                            tooltip = delta3.utils.GlobalFunc.getFilterWhereClause(fields, delta3.utils.GlobalVars.FieldStore);
                        }
                        cell.tdAttr = 'data-qtip="' + tooltip + '"';
                        return filter.get("name");
                    }
                }},                
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridKeyButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.KeyPopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            {text: 'Key Field', dataIndex: 'idDate', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridKey,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idKey'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},            
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridSequenceButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.DatePopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            {text: 'Sequence Field', dataIndex: 'idDate', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridSequence,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idDate'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},                            
            {text: 'Original Study', dataIndex: 'originalStudyId', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridModel,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['originalStudyId'];
                    var study = thisGrid.store.findRecord('idStudy', tableIndex);
                    if (study === null)
                        return null;
                    else {
                        return study.get("name");
                    }
                }}            
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.StudyStore');
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'studyGridEditor',
            listeners: {
                beforeedit: function(rowEditor, context, r) {
                    delta3.utils.GlobalVars.FieldStore.clearFilter(true);
                },
                edit: function(rowEditor, context, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var valid = true;
                    var counter = 0;
                    thisGrid.store.each(function (record, index) {
                        //validating uniqueness of new study name 
                        if (record.data.name === context.newValues.name) {
                            counter++;
                            if ( counter > 1 ) {
                                valid = false;
                                return;
                            }
                         }
                    });
                    if ( valid === false ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Study names must be unique.');
                        return;                            
                    }                       
                    if ( (context.newValues.idModel !== null) && 
                            (context.newValues.idModel !== context.originalValues.idModel) ) {
                        Ext.Msg.show({
                            title:'DELTA3',
                            message: 'You are about to change Data Model for the existing Study.<br> Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                   thisGrid.store.save();
                                } else {
                                   context.newValues.idModel = context.originalValues.idModel;
                                }
                            }
                        });                             
                    } else {
                        thisGrid.checkAndSaveAdvancedParams(context, thisGrid);
                        thisGrid.store.save();
                    }
                }
            }
            })
        ];
    },
    paramPopupDispatcher: function(rec, thisGrid) {
        var win;
        var popupTypeIndex = thisGrid.methodStore.find('idMethod',rec.data.idMethod);
        switch ( thisGrid.methodStore.getAt(popupTypeIndex).get('name') ) {                               
            case "logisticRegression":
                win = new delta3.view.study.LogisticRegrContainer({
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec});
                win.show();
                break;
            case "propensityScoreAnalysis":
                win = new delta3.view.study.PropensityPopup({
                    step: 0,
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});
                win.show();
                break;  
            case "riskAdjustedSPRT":
                win = new delta3.view.study.SPRTPopup({
                    step: 0,
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});
                win.show();
                break;         
            default:
                delta3.utils.GlobalFunc.showDeltaMessage(
                        'Method not supported: ' + thisGrid.methodStore.getAt(popupTypeIndex).get('name')
                        );
        }                            
    },
    checkAndSaveAdvancedParams: function(context, theGrid) {
        var studyParams = context.record.data.methodParams;
        var params;
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            params = JSON.parse(studyParams);          
        } else {
            // empty params JSON - do nothing
            return;
        }        
        if ( typeof context.record.modified.startTS !== 'undefined' ) {
            var startDate = context.newValues.startTS.toISOString();
        } 
        if ( typeof context.record.modified.endTS !== 'undefined' ) {
            var endDate = context.newValues.endTS.toISOString();
        }
        if ( typeof context.record.modified.confidenceInterval !== 'undefined' ) {
            var ciValue = context.newValues.confidenceInterval;
        }  
        if ( typeof context.record.modified.confidenceInterval2 !== 'undefined' ) {
            var ci2Value = context.newValues.confidenceInterval2;
        }          
        if ( typeof context.record.modified.idOutcome !== 'undefined' ) {
            var outcomeRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idDate);       
            var outcomeValue = outcomeRecord.get("name");  
        }     
        if ( typeof context.record.modified.idKey !== 'undefined' ) {
            var uniqueRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idKey); 
            var uniqueValue = uniqueRecord.get("name");
        }   
        if ( typeof context.record.modified.idDate !== 'undefined' ) {
            var seqRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idDate);       
            var sequenceValue = seqRecord.get("name");   
        }    
        
        var popupTypeIndex = theGrid.methodStore.find('idMethod', context.record.data.idMethod);
        
        switch ( theGrid.methodStore.getAt(popupTypeIndex).get('name') ) {                               
            case "logisticRegression":
                if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                if ( typeof uniqueValue !== 'undefined' ) params[1].inputs.studyUniqueId.values[0] = uniqueValue;
                if ( typeof outcomeValue !== 'undefined' ) params[1].inputs.dependentVariableSelection.values[0] = outcomeValue;                  
                if ( typeof startDate !== 'undefined' ) params[1].inputs.studyStartDate.values[0] = startDate;
                if ( typeof endDate !== 'undefined' ) params[1].inputs.studyEndDate.values[0] = endDate;
                if ( typeof sequenceValue !== 'undefined' ) params[1].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                if ( typeof ciValue !== 'undefined' ) params[1].inputs.confidenceIntervalList.values[0].value = ciValue;;   
                if ( typeof ci2Value !== 'undefined' ) params[1].inputs.confidenceIntervalList.values[1].value = ci2Value;                                  
                break;
            case "propensityScoreAnalysis":
                if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                break;  
            case "riskAdjustedSPRT":
                if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                break;         
            default:
                delta3.utils.GlobalFunc.showDeltaMessage(
                        'Method not supported: ' + thisGrid.methodStore.getAt(popupTypeIndex).get('name')
                        );
        }  
        var newParams = JSON.stringify(params);
        context.record.data.methodParams = newParams;
    }
});

