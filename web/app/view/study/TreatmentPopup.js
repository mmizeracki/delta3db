/* 
 * Treatment Popup Window
 */

Ext.define('delta3.view.study.TreatmentPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.treatment',
    layout: 'fit',
    width: 420,
    height: 260,
    itemId: 'treatmentPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    fieldId: 0,
    newFieldId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#treatmentPopup')[0];
                thisWin.fieldStore.clearFilter(true);
                if (thisWin.fieldId !== thisWin.newFieldId) {
                    var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var updatedRecord = studyGrid.store.findRecord('idStudy', thisWin.studyId);
                    updatedRecord.set('idTreatment',thisWin.newFieldId);
                    studyGrid.store.sync();
                }
                thisWin.destroy();
            }
        }, {
            text: 'Cancel',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#treatmentPopup')[0];
                thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        me.selectedStudyRecord = studyGrid.store.findRecord('idStudy', me.studyId);        
        var modelId = me.selectedStudyRecord.get('idModel');
        me.fieldId = me.selectedStudyRecord.get('idTreatment');
        var title = 'Treatment field selection for study "' + me.selectedStudyRecord.get('name') + '"';
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        me.fieldStore.filter('fieldClass', 'Treatment');
        me.fieldStore.filter('idModel', modelId);
        me.fieldStore.filter('insertable', true);            
        var dropDown = new Ext.form.ComboBox({
            store: me.fieldStore,
            itemId: 'popupTreatmentComboBox',
            displayField: 'name',
            valueField: 'idModelColumn',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        var name, description, type, fieldKind;
        if (me.selectedFieldRecord !== null) {
            name = me.selectedFieldRecord.data.name;
            description = me.selectedFieldRecord.data.description;
            type = me.selectedFieldRecord.data.type;
            fieldKind = me.selectedFieldRecord.data.fieldKind;
        }
        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 90,
                    labelAlign: 'right',
                    bodyStyle: 'padding:5px 5px 0',
                    width: 420,
                    height: 260,
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield',
                    items: [
                        {
                            fieldLabel: 'Field',
                            name: 'fField',
                            allowBlank: false,
                            value: name
                        }, {
                            fieldLabel: 'Description',
                            name: 'fDescription',
                            allowBlank: false,
                            value: description
                        }, {
                            fieldLabel: 'Type',
                            name: 'fType',
                            allowBlank: false,
                            value: type
                        }, {
                            fieldLabel: 'Kind',
                            name: 'fKind',
                            allowBlank: false,
                            value: fieldKind
                        },
                        dropDown]
                });
        me.callParent();
    }
}
);

