/* 
 * Study Container
 */

Ext.define('delta3.view.study.StudyContainer', {
    extend: 'Ext.container.Container',
    itemId: 'studyContainer',
    alias:  'widget.container.studies',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.study',
                region: 'center'
        },   
        me.callParent();
    }
});

