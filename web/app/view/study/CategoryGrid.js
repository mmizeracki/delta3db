/* 
 * Category Grid for Studies
 */

Ext.define('delta3.view.study.CategoryGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.studyCategory',
    itemId: 'studyCategoryGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.data.Model',
        'Ext.selection.RowModel',
        'delta3.model.FieldModel',
        'delta3.store.CategoryStudyStore',
        'Ext.grid.column.Template'
    ],
    border: false,
    columnLines: true,
    hideHeaders: true,
    tbar: [{},
        {
            itemId: 'addCategory',
            text: 'Add Category',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#studyCategoryGrid')[0];
                if (thisGrid.newFieldId > 0) {
                    var r = thisGrid.fieldStore.findRecord('idModelColumn', thisGrid.newFieldId);
                    thisGrid.store.insert(0, r);
                    thisGrid.saveRelationship(thisGrid.studyName, thisGrid.studyId, r.data.idModelColumn);
                    thisGrid.newFieldId = 0;
                    var delay = 1000;
                    setTimeout(function() {
                        // wait for update to hit db
                        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        studyGrid.store.reload();
                    }, delay);
                }
                //thisGrid.store.reload();
                thisGrid.getView().refresh();
            }
        }, {
            itemId: 'filterDelete',
            text: 'Delete',
            iconCls: 'delete-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#studyCategoryGrid')[0];
                var sm = thisGrid.getSelectionModel();
                var selection = sm.getSelection();
                thisGrid.store.remove(selection);
                for (var i = 0; i < selection.length; i++) {
                    thisGrid.removeRelationship(thisGrid.studyName, thisGrid.studyId, selection[i].data.idModelColumn);
                }
                if (selection.length > 0) {
                    var delay = 1000;
                    setTimeout(function() {
                        // wait for update to hit db
                        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        studyGrid.store.reload();
                    }, delay);
                }
                //thisGrid.store.reload();
                thisGrid.getView().refresh();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No categories found',
            displayInfo: false
        }],
    fieldStore: {},
    fieldId: [],
    newField: 0,
    initComponent: function() {
        var me = this;
        me.columns = me.buildColumns();
        me.store = me.buildStore(me.studyName, me.studyId);
        me.dockedItems[0].store = me.store;
        var dropDown = new Ext.form.ComboBox({
            store: me.fieldStore,
            itemId: 'gridCategoryComboBox',
            displayField: 'name',
            valueField: 'idModelColumn',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        me.tbar[0] = dropDown;
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        return [
            {text: 'Name', dataIndex: 'name', width: 90},
            //{text: 'Name',  xtype:'templatecolumn', tpl:'<b>{name}</b>', width: 110},           
            {text: 'Description', dataIndex: 'description', width: 140},
            {text: 'Type', dataIndex: 'type', width: 68},
            {text: 'Kind', dataIndex: 'fieldKind', width: 90,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModelColumn'];
                    var thisGrid = Ext.ComponentQuery.query('#studyCategoryGrid')[0];
                    var field = thisGrid.fieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        var value = field.get("fieldKind");
                        switch (value) {
                            case 'Dichotomous':
                                cell.tdCls = 'dicho-icon16';
                                break;
                            case 'Continuous':
                                cell.tdCls = 'cont-icon16';
                                break;
                            case 'Enumerated':
                                cell.tdCls = 'enum-icon16';
                                break;
                            case 'Date':
                                cell.tdCls = 'date-icon16';
                                break;
                        }
                        return value;
                    }
                }}
        ];
    },
    buildStore: function(studyName, studyId) {
        return new delta3.store.CategoryStudyStore({studyName: studyName, idStudy: studyId});
    },
    saveRelationship: function(studyName, idStudy, idCategory) {
        var outputJSONString = '{"studies":[{"name":"' + studyName
                + '", "idStudy":"' + idStudy
                + '"}], "categories":[' + JSON.stringify(idCategory)
                + ']}';

        console.log('calling saveStudyCategoryRelationship: ' + studyName);
        Ext.Ajax.request
                (
                        {
                            url: '/Delta3/webresources/study/addStudyCategory',
                            method: "POST",
                            disableCaching: true,
                            params: outputJSONString,
                            success: saveRelationshipReturn,
                            failure: saveRelationshipReturn
                        }
                );

        function saveRelationshipReturn(response, options)
        {
            var returnValue = Ext.decode(response.responseText);
            if (typeof returnValue === 'undefined') {
                message = 'General server error';
            } else {
                message = returnValue.status.message;
            }
            console.log('calling saveStudyCategoryRelationship returned ' + message);
            if (response.status !== 200 || returnValue.success === false) {
                delta3.utils.GlobalVars.MsgBox.hide();
                var thisGrid = Ext.ComponentQuery.query('#studyCategoryGrid')[0];
                thisGrid.store.removeAt(0);
                thisGrid.getView().refresh();
                delta3.utils.GlobalVars.MsgBox.show
                        (
                                {
                                    title: 'DELTA3 Web Services',
                                    message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                    progressText: 'Checking...',
                                    width: 400,
                                    wait: false,
                                    closable: false,
                                    icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                    buttons: delta3.utils.GlobalVars.MsgBox.OK
                                }
                        );
            }
        }
    },
    removeRelationship: function(studyName, idStudy, idCategory) {
        var outputJSONString = '{"studies":[{"name":"' + studyName
                + '", "idStudy":"' + idStudy
                + '"}], "categories":[' + JSON.stringify(idCategory)
                + ']}';

        console.log('calling removeStudyCategoryRelationship: ' + studyName);
        Ext.Ajax.request
                (
                        {
                            url: '/Delta3/webresources/study/removeStudyCategory',
                            method: "POST",
                            disableCaching: true,
                            params: outputJSONString,
                            success: removeRelationshipReturn,
                            failure: removeRelationshipReturn
                        }
                );
        function removeRelationshipReturn(response, options)
        {
            var returnValue = Ext.decode(response.responseText);
            if (typeof returnValue === 'undefined') {
                message = 'General server error';
            } else {
                message = returnValue.status.message;
            }
            console.log('calling removeStudyCategoryRelationship returned ' + message);
            if (response.status !== 200 || returnValue.success === false) {
                delta3.utils.GlobalVars.MsgBox.hide();
                delta3.utils.GlobalVars.MsgBox.show
                        (
                                {
                                    title: 'DELTA3 Web Services',
                                    message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                    progressText: 'Checking...',
                                    width: 400,
                                    wait: false,
                                    closable: false,
                                    icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                    buttons: delta3.utils.GlobalVars.MsgBox.OK
                                }
                        );
            }
        }
    }
});

