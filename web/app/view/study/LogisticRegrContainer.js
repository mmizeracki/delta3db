/* 
 * Logistic Regression Param Container
 * Coping Sysytems, Inc
 */

Ext.define('delta3.view.study.LogisticRegrContainer', {
    extend: 'Ext.Window',
    itemId: 'logisticRegrContainer',
    alias: 'widget.container.logisticRegr',
    requires: [
        'delta3.view.study.LogisticRegrPopup1',  
        'delta3.view.study.LogisticRegrPopup2'
    ],
    layout: 'fit',
    width: 480,
    height: 550,
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',    
    items: [{
                xtype: 'tabpanel',
                itemId: 'regrPopupTab',
                collapsible: false,
                region: 'center',
                layout: 'fit',              
                defaults: {autoScroll: true},
                items: []
            }],
    buttons: [{
            text: 'Cancel',
            handler: function() {
                var theTabs = Ext.ComponentQuery.query('#regrPopupTab')[0];    
                var tab = Ext.ComponentQuery.query('#methodLRStep1Popup')[0];
                if ( !Ext.isEmpty(tab) ) {
                    tab.removeAll();
                    theTabs.remove(tab);
                    tab = null;
                } 
                tab = Ext.ComponentQuery.query('#methodLRStep2Panel')[0];
                if ( !Ext.isEmpty(tab) ) {
                    // clean up necessary after multiple sub panel allocations while switching tabs
                    while ( Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel').length > 0 ) {
                        var thePanel = Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel')[0];
                        thePanel.close();
                        thePanel = null;     
                    }
                    tab.removeAll();
                    theTabs.remove(tab);
                    tab = null;
                }           
                theTabs = null;
                var thisWin = Ext.ComponentQuery.query('#logisticRegrContainer')[0];
                thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],        
    fieldStore: {},
    selectedStudyRecord: {},
    params: [],
    initComponent: function() {
        var me = this;
        me.title = 'DELTA3 Logistic Regression parameters for study "' + me.selectedStudyRecord.get('name') + '"'; 
        
        var studyParams = me.selectedStudyRecord.get('methodParams');
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null)  && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
        }  else {
            var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
            var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));            
            // read empty params string from template in tab panel
            me.params = JSON.parse(methodRec.data.methodParams);
        }
            
        me.callParent();
        var theTabs = Ext.ComponentQuery.query('#regrPopupTab')[0];
        theTabs.add(new delta3.view.study.LogisticRegrPopup1({
                        step: 0,
                        params: me.params[0],
                        methodParams: me.methodParams,
                        fieldStore: me.fieldStore, 
                        selectedStudyRecord: me.selectedStudyRecord, 
                        modelId: me.selectedStudyRecord.data.idModel,
                        studyId: me.selectedStudyRecord.data.idStudy})  );
        theTabs.add(new delta3.view.study.LogisticRegrPopup2({
                        step: 1,
                        params: me.params[1],         
                        methodParams: me.methodParams,
                        fieldStore: me.fieldStore, 
                        selectedStudyRecord: me.selectedStudyRecord, 
                        modelId: me.selectedStudyRecord.data.idModel,
                        studyId: me.selectedStudyRecord.data.idStudy}) );       
        theTabs.setActiveTab(0);
        me.callParent();   
    },
    saveParams: function() {
        var me = this;
        var newParams = JSON.stringify(me.params);
        me.selectedStudyRecord.data.methodParams = newParams;
        me.selectedStudyRecord.dirty = true;        
        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        studyGrid.getStore().sync();  
        studyGrid.setLoading();
        // Clear the loading mask after 1.5 seconds
        setTimeout(function (target) {
            target.setLoading(false);
        }, 1500, studyGrid);
    }
});
