/* 
 * Category Popup Window
 */

Ext.define('delta3.view.study.CategoryPopup', {
	extend: 'Ext.Window',
	alias: 'widget.popup.category',    
    requires: ['delta3.view.study.CategoryGrid'],
    layout: 'fit',
    width: 450,
    height: 260,
    itemId: 'categoryPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3', 
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    fieldId: [],
    newFieldId: [],
    studyId: {},
    items: [],
    buttons: [
        { 
               text: 'Close',
               handler: function() {
                     var thisWin = Ext.ComponentQuery.query('#categoryPopup')[0]; 
                     thisWin.fieldStore.clearFilter(true);                 
                     thisWin.destroy();
               }
       }],  
    initComponent: function() {
        var me = this;
        var modelId = me.selectedStudyRecord.get('idModel');
        //me.fieldId = me.selectedStudyRecord.get('idCategory');
        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        var updatedRecord = studyGrid.store.findRecord('idStudy', me.studyId);            
        var title = 'Category field selection for study "' + me.selectedStudyRecord.get('name') + '"';
        me.selectedFieldRecord = me.fieldStore.findRecord( 'idModelColumn', me.fieldId);
        me.fieldStore.filter('fieldClass','Category');
        me.fieldStore.filter('idModel',modelId);  
        me.items[0] = new delta3.view.study.CategoryGrid(
               {fieldStore  : me.fieldStore, 
                fieldId     : me.fieldId, 
                title       : title,
                studyName   : updatedRecord.data.name,
                studyId     : me.studyId
                });         
        me.callParent();
    }     
});
