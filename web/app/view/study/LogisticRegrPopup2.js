/* 
 * Logistic Regression Method Step 2 Popup Window
 */

Ext.define('delta3.view.study.LogisticRegrPopup2', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.popup.methodLRStep2',
    layout: 'fit',
    width: 440,
    height: 520,
    labelWidth: 160,
    labelAlign: 'left',       
    itemId: 'methodLRStep2Panel',
    title: 'LR Step2',    
    modal: true,
    closeAction: 'destroy',
    step: 1,
    startDate: {},
    endDate: {},
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],    
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodLRStep2Panel')[0];               
                var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];              
                if ( genericIdCheckbox.value === true ) {
                    thisWin.params.inputs.studyUniqueId.values[0] = 'id';
                } else {
                    var index = thisWin.selectedStudyRecord.get('idKey');
                    thisWin.fieldStore.clearFilter(true);
                    var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);                    
                    thisWin.params.inputs.studyUniqueId.values[0] = uniqueField.get("name");
                }
                thisWin.params.inputs.dependentVariableSelection.values[0] = thisWin.selectedFieldRecord.data.name;  
                thisWin.params.inputs.studyStartDate.values[0] = Ext.ComponentQuery.query('#startDate')[0].value;
                thisWin.params.inputs.studyEndDate.values[0] = Ext.ComponentQuery.query('#endDate')[0].value;
                thisWin.params.inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
                thisWin.params.inputs.reportingPeriod.values[0] = Ext.ComponentQuery.query('#period')[0].value;                      
                thisWin.params.inputs.confidenceIntervalList.values[0].value = Ext.ComponentQuery.query('#primaryCI')[0].value;   
                thisWin.params.inputs.confidenceIntervalList.values[1].value = Ext.ComponentQuery.query('#secondaryCI')[0].value;                  
                //thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);                  
                Ext.ComponentQuery.query('#logisticRegrContainer')[0].saveParams();
            }
        }],
    initComponent: function() {
        var me = this;  
        me.fieldStore.clearFilter(true);        
        // check if all necessary data is set
        me.fieldId = me.selectedStudyRecord.get('idOutcome');
        if ( me.fieldId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Outcome to the study first.");
            me.destroy();
        }        
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        }          
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.destroy();
        }
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }        
        // if params empty retrieve all method parameters, not only for this step
        if ( (typeof me.params === 'undefined') || (me.params === null) || (me.params === "") ) {        
            // initialize from empty params string based on Method template
            var paramArray = JSON.parse(methodRec.data.methodParams);
            me.params = paramArray[1];
        }
        // prepare panel, which will be used to attach UI components
        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    itemId: 'popupMethodLRStep2SubPanel',
                    bodyStyle: 'padding:5px 5px 0',
                    autoScroll: true,
                    defaultType: 'displayfield'                   
                });               
        if ( me.params.inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                fieldLabel: me.params.inputs.studyUniqueId.inputText,
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                itemId: 'studyId',
                allowBlank: true,
                value: uniqueField.get("name")
            });
        }        
        if ( me.params.inputs.dependentVariableSelection !== 'undefined' ) {
            me.items[0].add({
                fieldLabel: me.params.inputs.dependentVariableSelection.inputText,
                itemId: 'depVariable',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                allowBlank: true,
                value: me.selectedFieldRecord.data.name
            });
        }              
        if ( me.params.inputs.studyStartDate !== 'undefined' ) {     
            if ( (typeof me.params.inputs.studyStartDate.values[0] !== 'undefined') 
                    && (me.params.inputs.studyStartDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                me.startDate = new Date(me.params.inputs.studyStartDate.values[0]);
            } else {
                me.startDate = me.selectedStudyRecord.get('startTS');
                if ( me.startDate === null ) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Study Start Date is not defined.");
                    me.destroy();    
                }                  
            }             
            me.items[0].add({
                xtype: 'datefield',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                format: 'm/d/Y',
                submitFormat: 'Y-m-d H:i:s.u',                            
                fieldLabel: me.params.inputs.studyStartDate.inputText,
                itemId: 'startDate',
                readOnly: true,
                allowBlank: true,
                value: me.startDate
            });
        }        
        if ( me.params.inputs.studyEndDate !== 'undefined' ) {
            if ( (typeof me.params.inputs.studyEndDate.values[0] !== 'undefined') 
                    && (me.params.inputs.studyEndDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                me.endDate = new Date(me.params.inputs.studyEndDate.values[0]);
            } else {
                me.endDate = me.selectedStudyRecord.get('endTS');
                if ( me.endDate === null ) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Study End Date is not defined.");
                    me.destroy();    
                }                   
            }               
            me.items[0].add({
                xtype: 'datefield',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                submitFormat: 'Y-m-d H:i:s.u',
                format: 'm/d/Y',
                fieldLabel: me.params.inputs.studyEndDate.inputText,
                itemId: 'endDate',
                allowBlank: true,
                readOnly: true,
                value: me.endDate
            });
        }     
        //me.fieldStore.filter('fieldClass', 'Sequencer');
        //me.fieldStore.filter('idModel', me.modelId);
        //me.fieldStore.filter('insertable', true);             
        if ( me.params.inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);    
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exits.");
                me.destroy();    
            }            
            var currValue = sequenceField.get("name");                      
            me.items[0].add( {
                    fieldLabel: me.params.inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    allowBlank: true,
                    value: currValue
                });   
        }  
        if ( me.params.inputs.confidenceIntervalList !== 'undefined' ) {
            me.items[0].add({
                //xtype: 'textfield',
                fieldLabel: 'Primary CI',
                itemId: 'primaryCI',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: me.selectedStudyRecord.get('confidenceInterval'),
                allowBlank: true
            });
            me.items[0].add({
                //xtype: 'textfield',
                fieldLabel: 'Secondary CI',
                itemId: 'secondaryCI',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,    
                value: me.selectedStudyRecord.get('confidenceInterval2'),
                allowBlank: true
            });
        }         
        if ( me.params.inputs.reportingPeriod !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params.inputs.reportingPeriod.values[0] !== 'undefined') && (me.params.inputs.reportingPeriod.values[0] !== 'value0') ) {     
                currValue = me.params.inputs.reportingPeriod.values[0];
            }        
            if ( (me.params.inputs.reportingPeriod.type !== 'undefined') && (me.params.inputs.reportingPeriod.type === "MultipleUnique") ) {
                var values = me.params.inputs.reportingPeriod.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params.inputs.reportingPeriod.inputText,
                store: dropdownValues,
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                itemId: 'period',
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }           
        if ( me.params.inputs.model !== 'undefined' ) {
            delta3.utils.GlobalFunc.getLRFforStudy(me.selectedStudyRecord.get('idStudy'), showModel);   
            function showModel(result, options) {
                me.params.inputs.model = result.model;
                var panels = Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel');
                var thePanel = Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel')[panels.length-1];
                thePanel.add({
                            fieldLabel: 'Formula model',
                            itemId: 'formulaModelHeader',
                            labelWidth: me.labelWidth,
                            labelAlign: me.labelAlign                             
                        });
                for (var i=0; i< me.params.inputs.model.values.length; i++ ) {    
                    thePanel.add({
                        //xtype: 'textfield',
                        itemId: 'formulaModel' +i,
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                           
                        fieldLabel: me.params.inputs.model.values[i].name,
                        value: me.params.inputs.model.values[i].estimate
                    });                                
                }
                thePanel.doLayout();       
            }
        } 
        // set filter for first tab
        me.fieldStore.clearFilter(true);     
        me.fieldStore.filter('fieldClass', 'Risk Factor');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);
        me.callParent();   
        me.doLayout();
    }
}
);

