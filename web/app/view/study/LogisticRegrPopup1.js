/* 
 * Logistic Regression Method Popup Window
 */

Ext.define('delta3.view.study.LogisticRegrPopup1', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.popup.methodLRStep1',
    layout: 'fit',
    width: 480,
    height: 360,
    labelWidth: 160,
    labelAlign: 'left',      
    itemId: 'methodLRStep1Panel',
    title: 'LR Step1',
    modal: true,
    closeAction: 'destroy',
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    listeners: {
        beforeshow: function(comp, eOpts) {        
            var thisPanel = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
            thisPanel.fieldStore.clearFilter(true);     
            thisPanel.fieldStore.filter('fieldClass', 'Risk Factor');
            thisPanel.fieldStore.filter('idModel', thisPanel.modelId);
            thisPanel.fieldStore.filter('insertable', true);
        }
    },    
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
                var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
                //var theCheckbox = Ext.ComponentQuery.query('#chunkingCheckbox')[0];
                var theChunkRadio = Ext.ComponentQuery.query('#chunkRadiogroup')[0];
                var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];
                var listOfVariables = theItems.getRawValue();
                var arrayOfVariables = listOfVariables.split(", ");
                thisWin.params.inputs.independentVariableSelection.values = [];              
                for (var i = 0; i < arrayOfVariables.length; i++) {
                    thisWin.params.inputs.independentVariableSelection.values[i] = arrayOfVariables[i];
                }
                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);
                var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
                if ( genericIdCheckbox.value === true ) {
                    thisWin.params.inputs.studyUniqueId.values[0] = 'id';
                } else {
                    thisWin.params.inputs.studyUniqueId.values[0] = uniqueField.get("name");
                }
                thisWin.params.inputs.dependentVariableSelection.values[0] = thisWin.selectedFieldRecord.data.name;                
                thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);                  
                var temp = theChunkRadio.getValue();
                thisWin.selectedStudyRecord.set('chunkingBy',temp.chunkingBy);
                if ( typeof Ext.ComponentQuery.query('#avs')[0] !== 'undefined' ) {
                    thisWin.params.inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#avs')[0].value;  
                }                
                //thisWin.selectedStudyRecord.set('isChunking',theCheckbox.value);        
                Ext.ComponentQuery.query('#logisticRegrContainer')[0].saveParams();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);        
        me.fieldId = me.selectedStudyRecord.get('idOutcome');
        if ( me.fieldId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Outcome to the study first.");
            me.destroy();
        }        
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        }     
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.destroy();
        }        
        me.fieldStore.filter('fieldClass', 'Risk Factor');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);            
        var dropDown = new Ext.form.ComboBox({
            store: me.fieldStore,
            displayField: 'name',
            valueField: 'name',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        // if params empty retrieve all method parameters, not only for this step
        if ( (typeof me.params === 'undefined') || (me.params === null) || (me.params === "") ) {        
            // initialize from empty params string based on Method template
            var paramArray = JSON.parse(methodRec.data.methodParams);
            me.params = paramArray[0];
        }        
        var indepVariables = '';
        for (var i = 0; i < me.params.inputs.independentVariableSelection.values.length; i++) {
            if ( i > 0 ) {
                indepVariables += ', ';
            }
            indepVariables += me.params.inputs.independentVariableSelection.values[i];
        }
        // following code for AVS allows study to retroactively pick this new parameter w/o clearing all params
        var useAVS = null;
        if ( typeof me.params.inputs.automaticVariableSelection !== 'undefined' ) {                
            var currValue = '';            
            if ( typeof me.params.inputs.automaticVariableSelection.values[0] !== 'undefined' ) {     
                currValue = me.params.inputs.automaticVariableSelection.values[0];
            }             
            useAVS = {
                xtype: 'checkbox',
                fieldLabel: me.params.inputs.automaticVariableSelection.inputText,
                itemId: 'avs',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue,
                allowBlank: true
            };
        } else {
            if ( typeof me.methodParams[0].inputs.automaticVariableSelection !== 'undefined' ) {   
                me.params.inputs.automaticVariableSelection = me.methodParams[0].inputs.automaticVariableSelection;
                var currValue = '';            
                if ( typeof me.params.inputs.automaticVariableSelection.values[0] !== 'undefined' ) {     
                    currValue = me.params.inputs.automaticVariableSelection.values[0];
                }             
                useAVS = {
                    xtype: 'checkbox',
                    fieldLabel: me.params.inputs.automaticVariableSelection.inputText,
                    itemId: 'avs',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                   
                    value: currValue,
                    allowBlank: true
                };                
            }
        }                           

        me.items[0] = new Ext.Panel(
            {
                frame: true,
                labelWidth: 100,
                labelAlign: 'right',
                bodyStyle: 'padding:5px 5px 0',
                autoScroll: true,
                defaultType: 'displayfield',
                items: [
                    {
                        fieldLabel: me.params.inputs.studyUniqueId.inputText,
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                              
                        name: 'studyId',
                        allowBlank: true,
                        value: uniqueField.get("name")
                    }, {
                        xtype: 'checkbox',
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                              
                        fieldLabel: 'Use generic Id',
                        name: 'genericId',
                        inputValue: true,
                        value: me.selectedStudyRecord.get('isGenericId'),
                        itemId: 'genericIdCheckbox'                           
                    }, {
                        fieldLabel: me.params.inputs.dependentVariableSelection.inputText,
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                                
                        name: 'depVariable',
                        allowBlank: true,
                        value: me.selectedFieldRecord.data.name
                    }, {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'Risk Factor',
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                                
                        layout: 'hbox',
                        items: [
                            dropDown,
                            {
                                xtype: 'button',
                                iconCls: 'add_new-icon16',
                                text: 'Add',
                                handler: function() {
                                    var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
                                    var listOfVariables = theItems.getRawValue();
                                    if (listOfVariables !== '') {
                                        listOfVariables += ', '
                                    }
                                    listOfVariables += me.newFieldId;
                                    theItems.setRawValue(listOfVariables);
                                }
                            }, {
                                xtype: 'button',
                                iconCls: 'delete-icon16',
                                text: 'Clear',
                                handler: function() {
                                    var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
                                    theItems.setRawValue('');
                                }
                            }
                        ]
                    }, {
                        fieldLabel: me.params.inputs.independentVariableSelection.inputText,
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,    
                        margin: '5 5 28 0',                            
                        name: 'indepVariable',
                        itemId: 'indepVariable',
                        allowBlank: true,
                        value: indepVariables
                    },/*{
                        xtype: 'checkbox',
                        fieldLabel: 'Chunking on',
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                                
                        name: 'chunking',
                        inputValue: me.selectedStudyRecord.get('isChunking'),
                        value: me.selectedStudyRecord.get('isChunking'),
                        itemId: 'chunkingCheckbox'                         
                    },*/{
                        xtype: 'radiogroup',
                        fieldLabel: 'Initial Chunking Period',
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                                
                        columns: 3,
                        itemId: 'chunkRadiogroup',      
                        anchor: '100%',
                        defaults: {xtype: "radio",name: "chunkingBy"},
                        vertical: false,
                        items: [{
                            boxLabel: 'Year&nbsp;&nbsp;&nbsp;',
                            inputValue: 'year'
                        }, {
                            boxLabel: 'Quarter&nbsp;&nbsp;&nbsp;',
                            inputValue: 'quarter'
                        }, {
                            boxLabel: 'Month',
                            inputValue: 'month'
                        }]
                    }, useAVS 
                ]
            }); 
        var chunkRadios = Ext.ComponentQuery.query('#chunkRadiogroup')[0];
        // now set the value of the radio button(s) that match the key/value pair
        chunkRadios.setValue({chunkingBy:me.selectedStudyRecord.get('chunkingBy')});                
        me.callParent();
    }
}
);

