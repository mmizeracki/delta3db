/* 
 * Propensity Method Popup Window
 */

Ext.define('delta3.view.study.PropensityPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.methodPropensity',
    layout: 'fit',
    width: 540,
    height: 580,
    itemId: 'methodPropensityPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    labelWidth: 160,
    labelAlign: 'left',   
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityPopup')[0];
                var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];       
                
                var theIndepItems = Ext.ComponentQuery.query('#indepVariable')[0];        
                var listOfIndepVariables = theIndepItems.getRawValue();
                var arrayOfIndepVariables = listOfIndepVariables.split(",");
                thisWin.params[thisWin.step].inputs.independentVariableSelection.values = [];              
                for (var i = 0; i < arrayOfIndepVariables.length; i++) {
                    thisWin.params[thisWin.step].inputs.independentVariableSelection.values[i] = arrayOfIndepVariables[i];
                }
                
                var theExpItems = Ext.ComponentQuery.query('#expVariable')[0];                
                var listOfExpVariables = theExpItems.getRawValue();
                var arrayOfExpVariables = listOfExpVariables.split(",");
                thisWin.params[thisWin.step].inputs.exposureVariableSelection.values = [];              
                for (var i = 0; i < arrayOfExpVariables.length; i++) {
                    thisWin.params[thisWin.step].inputs.exposureVariableSelection.values[i] = arrayOfExpVariables[i];
                }                

                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);
                var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
                if ( genericIdCheckbox.value === true ) {
                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = 'id';
                } else {
                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = uniqueField.get("name");
                }
                thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);  
                
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.selectedFieldRecord.data.name;  
                thisWin.params[thisWin.step].inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
                thisWin.params[thisWin.step].inputs.reportingPeriod.values[0] = Ext.ComponentQuery.query('#reportingPeriod')[0].value;             

                thisWin.params[thisWin.step].inputs.dataSplitOption.values[0] = Ext.ComponentQuery.query('#dataSplitOption')[0].value;  
                
                thisWin.params[thisWin.step].inputs.calipers.values[0] = Ext.ComponentQuery.query('#calipers')[0].value;
                thisWin.params[thisWin.step].inputs.caliperType.values[0] = Ext.ComponentQuery.query('#caliperType')[0].value;                                

                thisWin.params[thisWin.step].inputs.matchNumber.values[0] = Ext.ComponentQuery.query('#matchNumber')[0].value;
                thisWin.params[thisWin.step].inputs.matchType.values[0] = Ext.ComponentQuery.query('#matchType')[0].value;  
                thisWin.params[thisWin.step].inputs.matchOrder.values[0] = Ext.ComponentQuery.query('#matchOrder')[0].value;  

                thisWin.params[thisWin.step].inputs.alphaError.values[0] = Ext.ComponentQuery.query('#alphaError')[0].value;
                
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                studyGrid.getStore().sync();
                thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();                
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Cancel',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityPopup')[0];
                thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }        
        me.fieldId = me.selectedStudyRecord.get('idOutcome');
        if ( me.fieldId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Outcome to the study first.");
            me.destroy();
        }        
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }
        
        index = me.selectedStudyRecord.get('idTreatment');
        var expVariables = me.fieldStore.findRecord('idModelColumn', index);
        if ( expVariables === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Treatment Field to the study first.");
            me.destroy();            
        }        
        
        me.fieldStore.filter('fieldClass', 'Risk Factor');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);      
        var riskAdjustmentStore = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore,'name');
        var dropDownRiskAdjustment = new Ext.form.ComboBox({
            store: riskAdjustmentStore,
            displayField: 'value',
            valueField: 'value',
            queryMode: 'local',
            multiSelect: true,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        
        //var title = methodRec.data.name + ' parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var title = 'PA parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        var indepVariables = '';
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
            for (var i = 0; i < me.params[me.step].inputs.independentVariableSelection.values.length; i++) {
                if ( i > 0 ) {
                    indepVariables += ',';
                }
                indepVariables += me.params[me.step].inputs.independentVariableSelection.values[i];
            }           
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 400,
                    labelAlign: 'right',                    
                    itemId: 'methodPropStep2Panel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

        if ( typeof me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                allowBlank: true,
                value: uniqueField.get("name")
            });
            me.items[0].add({
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                 
                xtype: 'checkbox',
                fieldLabel: 'Use generic Id',
                inputValue: true,
                value: me.selectedStudyRecord.get('isGenericId'),
                itemId: 'genericIdCheckbox'                           
            });
        }        
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            me.items[0].add({
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                itemId: 'depVariable',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                allowBlank: true,
                value: me.selectedFieldRecord.data.name
            });
        } 
        if ( typeof me.params[me.step].inputs.exposureVariableSelection !== 'undefined' ) {
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.exposureVariableSelection.inputText,
                    name: 'expVariable',
                    itemId: 'expVariable',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    allowBlank: true,
                    value: expVariables.data.name
                });            
        }         
        
        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( typeof me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exits.");
                me.destroy();    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    allowBlank: true,
                    value: currValue
                });   
        }
        if ( typeof me.params[me.step].inputs.alphaError !== 'undefined' ) {              
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.alphaError.inputText,
                itemId: 'alphaError',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: me.params[me.step].inputs.alphaError.values[0],
                allowBlank: true
            });
        }         
        if ( typeof me.params[me.step].inputs.reportingPeriod !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.reportingPeriod.values[0] !== 'undefined') && (me.params[me.step].inputs.reportingPeriod.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.reportingPeriod.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.reportingPeriod.type !== 'undefined') && (me.params[me.step].inputs.reportingPeriod.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.reportingPeriod.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.reportingPeriod.inputText,
                store: dropdownValues,
                itemId: 'reportingPeriod',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }   
        if ( typeof me.params[me.step].inputs.dataSplitOption !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.dataSplitOption.values[0] !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.dataSplitOption.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.dataSplitOption.type !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.dataSplitOption.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dataSplitOption.inputText,
                store: dropdownValues,
                itemId: 'dataSplitOption',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }         
        if ( typeof me.params[me.step].inputs.calipers !== 'undefined' ) {                
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.calipers.values[0] !== 'undefined') && (me.params[me.step].inputs.calipers.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.calipers.values[0];
            }             
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.calipers.inputText,
                itemId: 'calipers',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue,
                allowBlank: true
            });
        }        
        if ( typeof me.params[me.step].inputs.caliperType !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.caliperType.values[0] !== 'undefined') && (me.params[me.step].inputs.caliperType.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.caliperType.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.caliperType.type !== 'undefined') && (me.params[me.step].inputs.caliperType.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.caliperType.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.caliperType.inputText,
                store: dropdownValues,
                itemId: 'caliperType',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        } 
        if ( typeof me.params[me.step].inputs.matchNumber !== 'undefined' ) {
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.matchNumber.values[0] !== 'undefined') && (me.params[me.step].inputs.matchNumber.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchNumber.values[0];
            }               
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.matchNumber.inputText,
                itemId: 'matchNumber',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue,
                allowBlank: true
            });
        }         
        if ( typeof me.params[me.step].inputs.matchType !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.matchType.values[0] !== 'undefined') && (me.params[me.step].inputs.matchType.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchType.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.matchType.type !== 'undefined') && (me.params[me.step].inputs.matchType.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.matchType.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.matchType.inputText,
                store: dropdownValues,
                itemId: 'matchType',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }        
        if ( typeof me.params[me.step].inputs.matchOrder !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.matchOrder.values[0] !== 'undefined') && (me.params[me.step].inputs.matchOrder.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchOrder.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.matchOrder.type !== 'undefined') && (me.params[me.step].inputs.matchOrder.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.matchOrder.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.matchOrder.inputText,
                store: dropdownValues,
                itemId: 'matchOrder',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }   
        if ( typeof me.params[me.step].inputs.independentVariableSelection !== 'undefined' ) {
            me.items[0].add({
                xtype: 'fieldcontainer',
                fieldLabel: 'Risk Factor',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                layout: 'hbox',
                items: [
                    dropDownRiskAdjustment,
                    {
                        xtype: 'button',
                        iconCls: 'add_new-icon16',
                        text: 'Add',
                        handler: function() {
                            var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
                            var listOfVariables = theItems.getRawValue();
                            if (listOfVariables !== '') {
                                listOfVariables += ','
                            }
                            listOfVariables += me.newFieldId;
                            theItems.setRawValue(listOfVariables);
                        }
                    }, {
                        xtype: 'button',
                        iconCls: 'delete-icon16',
                        text: 'Clear',
                        handler: function() {
                            var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
                            theItems.setRawValue('');
                        }
                    }
                ]});     
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.independentVariableSelection.inputText,
                    name: 'indepVariable',
                    itemId: 'indepVariable',
                    labelWidth: me.labelWidth,
                    maxHeight: 24,
                    margin: '5 5 28 0',
                    labelAlign: me.labelAlign,                       
                    allowBlank: true,
                    value: indepVariables
                });
        }        
        me.items[0].doLayout();
        me.callParent();    
    }
}
);

