/* 
 * Results Container
 */

Ext.define('delta3.view.ResultsContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.results',
    requires: [       
        'Ext.layout.container.HBox',  
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.draw.modifier.Highlight',
        'Ext.chart.axis.Time',
        'Ext.chart.theme.Sky',
        'Ext.chart.interactions.ItemHighlight'
    ],    
    layout: {
        type: 'hbox',
        align: 'stretch'        
    },
    resultsData: {},
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent ResultsContainer");   
        var chart1 = createNewcombeWilsonChartPanel(me.config.resultsData);
        var chart2 = createObservedExpectedChartPanel(me.config.resultsData);
        var grid1 = createObservedExpectedGridPanel(me.config.resultsData);        
        me.items = [chart1, chart2, grid1];
        me.doLayout();
        me.callParent();
    }
});

var tipWidth = 152;
var tipHeight = 46;
//var embelishmentSteps = 12;

function createNewcombeWilsonChartPanel(data) {       
    var storeMaterial = JSON.parse(data);

    var chartC = new Ext.chart.CartesianChart({
                xtype: 'cartesian',
                animate: true,
                id: 'newcombeWilsonChart',
                renderTo: Ext.getBody(),       
                store: convertData(storeMaterial.newcombeWilsonScore),
                theme: 'Sky',
                insetPadding: 20,                
                innerPadding: 20,
                background: 'white',
                interactions: [
                    'panzoom',
                    'itemhighlight'
                ],
                //legend: {
                //    position: 'bottom'
                //},                
                axes: [{
                    type: 'numeric',
                    position: 'left',
                    fields: ['0', '1', '2', '3'],
                    grid: true,
                    minimum: -1,
                    maximum: 1
                }, {
                    type: 'category',
                    position: 'bottom',
                    fields: ['time'],
                    title: 'Time',
                    grid: true
                }],
                series: [{
                            type: 'line',
                            //fill: true,
                            style: {
                                smooth: true,
                                opacity: 0.7,
                                //miterLimit: 3,
                                //lineCap: 'miter',
                                //strokeOpacity: 1,
                                //fillOpacity: 0.7,
                                lineWidth: 4
                            },
                            title: 'Lower Confidence Limit',
                            highlight: {
                                scale: 0.9
                            },
                            marker: {
                                type: 'image',
                                src: 'resources/images/square.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },
                            xField: 'time',
                            yField: '0'
                        },
                        {
                            type: 'line',
                            style: {
                                smooth: true,
                                opacity: 0.7,
                                lineWidth: 4
                            },
                            title: 'Mean',
                            highlight: {
                                scale: 0.9
                            },
                            marker: {
                                type: 'image',
                                src: 'resources/images/circle.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },                    
                            xField: 'time',
                            yField: '1'
                        },{
                            type: 'line',
                            style: {
                                smooth: true,
                                opacity: 0.7,
                                lineWidth: 4
                            },
                            title: 'Upper Confidence Limit',
                            highlight: {
                                scale: 0.9
                            },
                            marker: {
                                type: 'image',
                                src: 'resources/images/pentagon.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },                           
                            xField: 'time',
                            yField: '2'
                        },{
                            type: 'line', // artificial zero line    
                            style: {
                                stroke: "#CCC0000", 
                                opacity: 0.7,
                                lineWidth: 2
                            },
                            //title: 'Zero',
                            highlight: {
                                scale: 0.9
                            },                          
                            xField: 'time',
                            yField: '3'
                        }]       
            });

    var panel = Ext.create('Ext.panel.Panel', {   
        title: 'Newcombe-Wilson Score',
        width: 400,
        layout: 'fit',
        margin: '6 6 6 6',            
        items: [chartC]
     });

 return panel;
}

function createObservedExpectedChartPanel(data) {       
    var storeMaterial = JSON.parse(data);

    var chartC = new Ext.chart.CartesianChart({
                animate: true,
                id: 'observedExpectedChart',
                renderTo: Ext.getBody(),       
                background: 'white',
                insetPadding: 20,
                innerPadding: 20,
                interactions: [
                     {
                         type: 'panzoom',
                         enabled: false,
                         zoomOnPanGesture: false,
                         axes: {
                             left: {
                                 allowPan: false,
                                 allowZoom: false
                             },
                             bottom: {
                                 allowPan: true,
                                 allowZoom: true
                             }
                         }
                     },
                     {
                         type: 'crosshair'
                     }
                ],                
                store: convertExpectedObservedDataForChart(storeMaterial.observedExpectedAnalytics),
                axes: [{
                    type: 'numeric',
                    position: 'left',
                    fields: ['0', '1', '2', '3', '4', '5'],
                    grid: true,
                    minimum: 0,
                    maximum: 100
                }, {
                    type: 'category',
                    position: 'bottom',
                    fields: ['time'],
                    title: 'Time',
                    grid: false
                }],
                series: [
                    {
                            type: 'line',
                            fill: true,
                            style: {
                                smooth: true,
                                opacity: 0.7,
                                fill: "#3399FF",
                                stroke: "#3399FF",
                                lineWidth: 4
                            },
                            title: 'Upper Confidence Limit',
                            highlight: {
                                scale: 0.9
                            },
                            marker: {
                                type: 'image',
                                src: 'resources/images/square.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },
                            tooltip: {
                                trackMouse: true,
                                width: tipWidth,
                                height: tipHeight,
                                renderer: function (storeItem, item) {
                                    displayExpObsTip(this, storeItem, item);
                                }
                            },                            
                            xField: 'time',
                            yField: '2'
                        },{
                            type: 'line',
                            fill: false,
                            style: {
                                smooth: true,
                                opacity: 0.7,
                                fill: "#3399FF",
                                stroke: "#3399FF",                                
                                lineWidth: 4
                            },
                            title: 'Mean Confidence',
                            highlight: {
                                scale: 0.9
                            },
                            tooltip: {
                                trackMouse: true,
                                width: tipWidth,
                                height: tipHeight,
                                renderer: function (storeItem, item) {
                                    displayExpObsTip(this, storeItem, item);
                                }
                            },                                     
                            marker: {
                                type: 'image',
                                src: 'resources/images/square.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },
                            xField: 'time',
                            yField: '1'
                        },{
                            type: 'line',
                            fill: true,
                            style: {
                                smooth: true,
                                opacity: 1,
                                fill: "#FFFFFF",
                                stroke: "#3399FF",                                
                                lineWidth: 4
                            },
                            title: 'Lower Confidence Limit',
                            highlight: {
                                scale: 0.9
                            },
                            tooltip: {
                                trackMouse: true,
                                width: tipWidth,
                                height: tipHeight,
                                renderer: function (storeItem, item) {
                                    displayExpObsTip(this, storeItem, item);
                                }
                            },                                     
                            marker: {
                                type: 'image',
                                src: 'resources/images/square.png',
                                width: 16,
                                height: 16,
                                x: -8,
                                y: -8,
                                scale: 0.7,
                                fx: {
                                    duration: 200
                                }
                            },
                            xField: 'time',
                            yField: '0'
                        },                    
                        {
                            type: 'candlestick',
                            xField: 'time',
                            openField: '4',
                            highField: '5',
                            lowField: '3',
                            closeField: '4',
                            style: {
                                barWidth: 20,
                                barHeight: 10,
                                opacity: 0.9,
                                //ohlcType: 'ohlc',
                                raiseStyle: {
                                    fill: "#003366",
                                    stroke: "#003366",
                                    'stroke-width': 6
                                },
                                dropStyle: {
                                    fill: "#663300",
                                    stroke: "#663300",
                                    'stroke-width': 6
                                }
                            },
                            aggregator: {
                                strategy: 'time'
                            }
                        }
                    ]            
                });

    var panel = Ext.create('Ext.panel.Panel', {   
        title: 'Observed vs. Expected Chart',
        width: 400,
        layout: 'fit',
        margin: '6 6 6 6',               
        items: [chartC]
     });
 
 return panel;
}

function createObservedExpectedGridPanel(data) {       
    var storeMaterial = JSON.parse(data);

    var resultGrid = new Ext.grid.Panel({
        alias       : 'widget.grid.user',
        itemId      : 'userGrid',
        autoScroll  : true,
        renderTo    : document.body,
        height      : '100%',
        requires	: [
            'Ext.data.Store',
            'Ext.toolbar.Paging',        
            'Ext.selection.RowModel',
            'Ext.grid.column.Column'
        ],     
        store: convertDataForGrid(storeMaterial),
        viewConfig  : {
            getRowClass: function(record, rowIndex, rowParams, store) {
                if (record.get('alertNW')=== true) return 'rowAlert';
                if (record.get('alert')=== true) return 'rowAlert';                
            }
        },
        columns: [
			{header: 'Time',dataIndex: 'time', width: 60},  
			{header: 'NW[low]',dataIndex: 'nwl', width: 50},  
			{header: 'NW[med]',dataIndex: 'nwm', width: 50},  
			{header: 'NW[high]',dataIndex: 'nwh', width: 50},   
            {header: 'Alert', dataIndex: 'alertNW', width: 40},            
			{header: 'Exp',dataIndex: 'n', width: 30},
            {header: 'Exp[%]',dataIndex: 'npercent', width: 50},
            {header: 'Exp[std]',dataIndex: 'nstd', width: 50},
			{header: 'Exp[lci]',dataIndex: 'nlci', width: 50},          
            {header: 'Exp[hci]',dataIndex: 'nhci', width: 50},            
            {header: 'Obs',dataIndex: 'q', width: 30},
            {header: 'Obs[%]', dataIndex: 'qpercent', width: 50},       
            {header: 'Obs[std]', dataIndex: 'qstd', width: 50},
            {header: 'Obs[lci]', dataIndex: 'qlci', width: 50},
            {header: 'Obs[hci]', dataIndex: 'qhci', width: 50},
            {header: 'Alert', dataIndex: 'alert', width: 40}
		]
    });

    var panel = Ext.create('Ext.panel.Panel', {   
        title: 'Data',
        width: 662,
        layout: 'fit',
        margin: '6 6 6 6',               
        items: [resultGrid]
     });

 return panel;
}

function convertData(data)
{
    var chartStore = {fields: [],data: []};
    var fieldString = '["time"';
    var dataString = '[';
    var numberOfColumns;

    Object.getOwnPropertyNames(data).sort().forEach(function(val, idx, array) {
      console.log(val + " -> " + data[val]);
      if ( idx !== 0 ) {
          dataString += ',';
      }
      fieldString += ',"' + idx + '"';
      numberOfColumns = idx;
      dataString += '{"time":"' + array[idx] + '"'; 
      var record = data[val];
      for (var i=0; i<record.length; i++) {
           dataString += ',"' + i + '":' + record[i]; 
      }
      dataString += ',"' + i + '":0'; // add "zero" line      
      dataString += '}';
    });

    numberOfColumns++;
    fieldString += ',"' + numberOfColumns + '"';  // "zero" line  
    fieldString += ']';
    dataString += ']';

    var storeFields = JSON.parse(fieldString);
    chartStore.fields = storeFields;
    var storeData = JSON.parse(dataString);
    chartStore.data = storeData;
    return chartStore;
}
 
function convertExpectedObservedDataForChart(data)
{
    var resultStore = {fields: [],data: []};    
    var fieldString = '["time","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"]';    
    var dataString = '[';  
    
    Object.getOwnPropertyNames(data).sort().forEach(function(val, idx, array) {
      console.log(val + " -> " + data[val]);
      if ( idx !== 0 ) {
          dataString += ',';
      }
      dataString += '{"time":"' + array[idx] + '"'; 
      var record = data[val];
        dataString += ',"1":' + Math.round(record[1]/record[0] * 1000)/10; 
        var std95n = delta3.utils.GlobalFunc.calcStandardDeviation(record[0],record[1]/record[0],0.95);   
        var nlci = (record[1]/record[0] - 1.96*std95n);
        if ( nlci < 0 ) nlci = 0;        
        dataString += ',"0":' + Math.round(nlci * 1000)/10;
        dataString += ',"2":' + Math.round((record[1]/record[0] + 1.96*std95n) *1000)/10;        
        dataString += ',"4":' + Math.round(record[3]/record[2] * 1000)/10;        
        var std95q = delta3.utils.GlobalFunc.calcStandardDeviation(record[2],record[3]/record[2],0.95);   
        var qlci = (record[3]/record[2] - 1.96*std95q);
        if ( qlci < 0 ) qlci = 0;
        dataString += ',"3":' + Math.round(qlci * 1000)/10;
        dataString += ',"5":' + Math.round((record[3]/record[2] + 1.96*std95q) * 1000)/10;     
        dataString += '}';
    });   
    dataString += ']';

    var storeFields = JSON.parse(fieldString);
    resultStore.fields = storeFields;
    var storeData = JSON.parse(dataString);
    resultStore.data = storeData;
    return resultStore;
}
  
function convertDataForGrid(data)
{
    var resultStore = {fields: [],data: []};    
    var fieldString = '["time","nwl","nwm","nwh","alertNW","n","npercent","nstd","nlci","nhci","q","qpercent","qstd","qlci","qhci","alert"';     
    var dataString = '[';    
    
    Object.getOwnPropertyNames(data.observedExpectedAnalytics).sort().forEach(function(val, idx, array) {
      console.log(val + " -> " + data.observedExpectedAnalytics[val]);
      if ( idx !== 0 ) {
          dataString += ',';
      }
      dataString += '{"time":"' + array[idx] + '"'; 
      var record = data.observedExpectedAnalytics[val];
        dataString += ',"nwl": "1"'; // leave empty spots for future Newcombe-Wilson results
        dataString += ',"nwm": "1"';
        dataString += ',"nwh": "1"'; 
        dataString += ',"alertNW": false';
        dataString += ',"n":' + record[0]; // expected
        dataString += ',"npercent":' + Math.round(record[1]/record[0] * 1000)/10; 
        var std95n = delta3.utils.GlobalFunc.calcStandardDeviation(record[0],record[1]/record[0],0.95);        
        dataString += ',"nstd":' + Math.round(std95n *1000)/10;   
        var nlci = (record[1]/record[0] - 1.96*std95n);
        if ( nlci < 0 ) nlci = 0;
        dataString += ',"nlci":' + Math.round(nlci * 1000)/10;
        dataString += ',"nhci":' + Math.round((record[1]/record[0] + 1.96*std95n) * 1000)/10;        
        dataString += ',"q":' + record[2]; // observed
        dataString += ',"qpercent":' + Math.round(record[3]/record[2] * 1000)/10;        
        var std95q = delta3.utils.GlobalFunc.calcStandardDeviation(record[2],record[3]/record[2],0.95);            
        dataString += ',"qstd":' + Math.round(std95q * 1000)/10; 
        var qlci = (record[3]/record[2] - 1.96*std95q);
        if ( qlci < 0 ) qlci = 0;
        dataString += ',"qlci":' + Math.round(qlci * 1000)/10;
        dataString += ',"qhci":' + Math.round((record[3]/record[2] + 1.96*std95q) * 1000)/10;   
        if ( (record[3]/record[2] > (record[1]/record[0] + 1.96*std95n))
               || (record[3]/record[2] < (record[1]/record[0] - 1.96*std95n)) ) {
            dataString += ',"alert":true';
        }
        else {
            dataString += ',"alert":false';      
        }
      dataString += '}';
    });
    
    fieldString += ']';    
    dataString += ']';

    var storeFields = JSON.parse(fieldString);
    resultStore.fields = storeFields;
    var storeData = JSON.parse(dataString);
    
    Object.getOwnPropertyNames(data.newcombeWilsonScore).sort().forEach(function(val, idx, array) {
      console.log(val + " -> " + data.newcombeWilsonScore[val]);
      var record = data.newcombeWilsonScore[val];
        storeData[idx].nwl = record[0];
        storeData[idx].nwm = record[1];
        storeData[idx].nwh = record[2];       
        if ( record[0] > 0 ) storeData[idx].alertNW = true;
        if ( record[2] < 0 ) storeData[idx].alertNW = true;        
    });    
    
    resultStore.data = storeData;
    return resultStore;
}

function displayExpObsTip (tip, storeItem, item) {
   tip.setTitle(storeItem.get('time') + '<br>  Exp: ' 
           + storeItem.get('0') + '% '   
           + storeItem.get('1') + '% '     
           + storeItem.get('2') + '% '
           + '<br>  Obs: ' 
           + storeItem.get('3') + '% '   
           + storeItem.get('4') + '% '     
           + storeItem.get('5') + '% '
       );
}
function calcStandardDeviation(n, u, nsd) {
    var sd;
    var sd2;
    
    if (( n === null ) || ( u === null )) {
        return 0;
    }
    if ( nsd === null ) {
        nsd = 1;
    }
    sd2 = nsd*nsd;
    if ( (sd2 + 4*n*u*(1-u)) < 0 ) {
        return 0;
    }
    sd = 2*n*u + sd2 + nsd * Math.sqrt(sd2 + 4*n*u*(1-u));
    if ( (n + sd2) === 0 ) {
        return 0;
    }
    sd = sd/(2 * (n + sd2));
    if ( sd > 1 ) {
        sd = 1;
    }  
    return sd - u;
}


/* markers
 * circle,line,square,triangle,diamond,cross,plus,arrow
 *  */