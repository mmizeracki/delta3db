/* 
 * Configuration Container
 */

Ext.define('delta3.view.ConfigurationContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.configurations',
    requires:   [
        'Ext.layout.container.Border'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent ConfigurationContainer");
        me.items = {
                xtype:  'grid.configuration',
                region: 'center'
        }, 
        me.callParent();
    }
});