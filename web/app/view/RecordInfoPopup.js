/* 
 * Record Info Popup Window - displays record time stamp information
 */

Ext.define('delta3.view.RecordInfoPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.recordInfo',
    layout: 'fit',
    width: 420,
    height: 260,
    itemId: 'recordInfoPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    recordType: {},
    record: {},
    items: [],
    buttons: [
        {
            text: 'Cancel',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#recordInfoPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;   
        var name = me.record.get('name');
        if ( typeof name === 'undefined' ) {
           name = '';
        }
        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 90,
                    labelAlign: 'right',
                    bodyStyle: 'padding:5px 5px 0',
                    width: 420,
                    height: 260,
                    title: me.recordType + ' Properties: ' + me.record.get('name'),
                    autoScroll: true,
                    defaultType: 'displayfield',
                    items: [
                        {
                            fieldLabel: 'Created by',
                            name: 'createdBy',
                            allowBlank: false,
                            value: delta3.utils.GlobalFunc.lookupUserInfo(me.record, 'createdBy', delta3.utils.GlobalVars.UserStore)
                        }, {
                            fieldLabel: 'Created on',
                            name: 'createdTS',
                            allowBlank: false,
                            value: me.record.get('createdTS')
                        }, {
                            fieldLabel: 'Updated by',
                            name: 'updatedBy',
                            allowBlank: false,
                            value: delta3.utils.GlobalFunc.lookupUserInfo(me.record, 'updatedBy', delta3.utils.GlobalVars.UserStore)
                        }, {
                            fieldLabel: 'Update on',
                            name: 'updatedTS',
                            allowBlank: false,
                            value: me.record.get('updatedTS')
                        }]
                });
        me.callParent();
    }
}
);


