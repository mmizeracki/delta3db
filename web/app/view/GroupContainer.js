/* 
 * Group Container
 */

Ext.define('delta3.view.GroupContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.groups',
    //layout: 'border',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent GroupContainer");        
        me.items = {
                xtype:  'grid.group',
                region: 'center'
        },   
        me.callParent();
    }
});