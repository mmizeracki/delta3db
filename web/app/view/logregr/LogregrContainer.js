/* 
 * Logistic Regression Container
 */

Ext.define('delta3.view.logregr.LogregrContainer', {
    extend: 'Ext.container.Container',
    itemId: 'logregrContainer',
    alias:  'widget.container.logregrs',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent LogregrContainer");        
        me.items = {
                xtype:  'grid.logregr',
                region: 'center'
        },   
        me.callParent();
    }
});

