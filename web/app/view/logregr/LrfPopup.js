/* 
 * Logistic Regression Formula Popup Window
 */

Ext.define('delta3.view.logregr.LrfPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.lrf',
    layout: 'fit',
    width: 520,
    height: 300,
    itemId: 'lrfPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    lrfString: {},
    items: [{xtype: 'panel',
            frame: true,
            labelWidth: 20,
            labelAlign: 'right',
            bodyStyle: 'padding:5px 5px 0',
            width: 520,
            height: 410,
            title: 'Logistic Regression Formula',
            autoScroll: true,
            defaultType: 'displayfield',
            items: [{
                    fieldLabel: 'LRF:',
                    name: 'fField',
                    value: ''
                }]
        }],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#lrfPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.items[0].items[0].value = me.lrfString;
        me.callParent();
    }
});
