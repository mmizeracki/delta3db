/* 
 * Logistic Regression Formula Field Popup Window
 */

Ext.define('delta3.view.logregr.LogregrFieldPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.logregrField',
    requires: ['Ext.grid.column.Column',
        'delta3.view.logregr.LogregrFieldGrid',
        'delta3.store.LogregrDateStore',
        'delta3.view.logregr.LrfPopup'],
    layout: 'fit',
    width: 560,
    height: 460,
    itemId: 'logregrFieldPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    fieldStore: {},
    logregrId: {},
    modelId: {},
    sequencerId: {},
    logregrName: {},
    logregrFormula: {},
    newFieldId: {},
    fieldGrid: {},
    dateStore: {},
    stats: {},
    items: [],
    buttons: [{
                text: 'Sort & Refresh',
                handler: function() {                  
                    var dateStore = Ext.StoreMgr.lookup("logregrDateStore");   
                    dateStore.load();                 
                }
            },{
                text: 'Show LRF',
                handler: function() {
                    var thisWin = Ext.ComponentQuery.query('#logregrFieldPopup')[0];                   
                    delta3.utils.GlobalFunc.getLRF(thisWin.logregrId, thisWin.logregrName, thisWin.showLRFWindow);                                
                }
            },{
                text: 'Close',
                handler: function() {
                    var thisWin = Ext.ComponentQuery.query('#logregrFieldPopup')[0];
                    thisWin.fieldStore.clearFilter(true);    
                    thisWin.destroy();
                }
            }/*,{
                text: 'Print',
                handler: function() {
                    var thisWin = Ext.ComponentQuery.query('#logregrFieldPopup')[0];
                    var html = Ext.dom.Query.selectNode('#popup-logregrField-1076-body');
                    plugin.Printer.print(html,true);                    
                    //thisWin.destroy();
                }
            }*/],
    initComponent: function() {
        var me = this;
        me.fieldStore.filter('fieldClass','Risk Factor');
        me.fieldStore.filter('idModel',me.modelId);        
        dateStore = new delta3.store.LogregrDateStore({logregrId: me.logregrId, logregrName: me.logregrName});
        dateStore.load();
        var dropDown = new Ext.form.ComboBox({
            store: me.fieldStore,
            itemId: 'popupLogregrFieldComboBox',
            displayField: 'name',
            valueField: 'idModelColumn',
            queryMode: 'local',
            multiSelect: true,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        var name, date;
        me.fieldGrid = new delta3.view.logregr.LogregrFieldGrid(
                {
                    margin: "10 0 10 10",       // top, right, bottom, left                    
                    fieldStore: me.fieldStore,
                    logregrId: me.logregrId,
                    logregrName: me.logregrName
                });
        if ( typeof me.logregrFormula !== 'undefined' ) {
            var statsResults = JSON.parse(me.logregrFormula);
            var tableWidth = statsResults.results[0].columns.length;
            me.stats = {
                    xtype: 'fieldcontainer',            
                    renderTo: Ext.getBody(),
                    margin: "10 0 10 10",       // top, right, bottom, left               
                    width: 520,
                    layout: {
                        type: 'table',
                        bodyStyle: 'padding:10px 10px 10px',
                        columns: tableWidth,
                        tableAttrs: {
                            style: {
                                width: '100%',
                                'font-size': '12px;!important'
                            }
                        }
                    },
                    items: []
                    };   
            var labelIndx = 0;
            for (var i=0; i<tableWidth; i++) {
                me.stats.items[labelIndx++] = {
                        xtype: 'label',
                        text: statsResults.results[0].columns[i].name,
                        forId: 'label' + labelIndx
                    };
            }
            for ( var j=0; j<statsResults.results[0].data.length; j++) {
                for (var i=0; i<tableWidth; i++) {
                    me.stats.items[labelIndx++] = {
                            xtype: 'label',
                            text: statsResults.results[0].data[j][i],
                            forId: 'label' + labelIndx
                        };
                }        
            }
        }
        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 90,
                    labelAlign: 'right',
                    bodyStyle: 'padding:5px 5px 0',
                    width: 220,
                    height: 260,
                    title: 'Logistic Regression Formula Definition',
                    autoScroll: true,
                    defaultType: 'displayfield',
                    items: [
                            {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            margin: "10 10 10 10",                                  
                            items: [{
                                    xtype: 'datefield',
                                    fieldLabel: 'Date',
                                    id: 'fDate',
                                    name: 'fDate',
                                    allowBlank: true,
                                    value: date
                                }, {
                                    xtype: 'button',
                                    iconCls: 'add_new-icon16',
                                    text: 'Add',
                                    handler: function() {
                                        me.fieldGrid.addColumn(me);
                                        /*var df = Ext.getCmp("fDate");
                                        var regrFieldGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
                                        if (regrFieldGrid.store.data.length > 0) {
                                            // do this only if at least one field is defined                                        
                                            var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                                            var val = df.getValue().toLocaleDateString();
                                            for (var i = 0; i < regrFieldGrid.store.data.length; i++) {
                                                var rec = regrFieldGrid.getStore().getAt(i);
                                                var v = rec.get("value");
                                                if ( v === 'C' ) v = '0.0';
                                                var r = delta3.model.LogregrDateModel.create({
                                                    active: true,
                                                    name: val,
                                                    value: v,
                                                    idLogregr: me.logregrId,
                                                    modelColumnidSequencer: me.sequencerId,
                                                    idLogregrField: rec.get("idLogregrField"),
                                                    createdTS: '0000-00-00 00:00:00.0',
                                                    updatedTS: '0000-00-00 00:00:00.0'});
                                                dateStore.insert(dateStore.getCount(), r);
                                            }
                                            dateStore.sync();
                                            //var len = regrFieldGrid.columns.length;
                                            var len = regrFieldGrid.headerCt.gridDataColumns.length;
                                            var column = new Ext.grid.column.Column({text: val, dataIndex: 'value' + len, width: delta3.utils.GlobalVars.dateLength, editor: 'textfield'});
                                            regrFieldGrid.headerCt.insert(len, column);
                                            regrFieldGrid.width += delta3.utils.GlobalVars.dateLength;
                                            regrFieldGrid.getView().refresh();
                                            me.doLayout();
                                        }*/
                                    }
                                }
                            ]
                        }, {
                            xtype: 'fieldcontainer',
                            margin: "10 10 10 10",                            
                            fieldLabel: 'Risk Factor',
                            layout: 'hbox',
                            items: [
                                dropDown,
                                {
                                    xtype: 'button',
                                    iconCls: 'add_new-icon16',
                                    text: 'Add',
                                    handler: function() {
                                        for (var i=0; i<me.newFieldId.length; i++) {
                                            var r = me.fieldGrid.fieldStore.findRecord('idModelColumn', me.newFieldId[i]);
                                            me.fieldGrid.addField(r.data.name, me.newFieldId[i]);
                                        }
                                    }
                                }
                            ]
                        },
                        me.fieldGrid,
                        me.stats                      
                    ]
                });
        me.callParent();
    },
    showLRFWindow: function(result) {
        var win = new delta3.view.logregr.LrfPopup({lrfString: result});
        win.show();            
    }
});
