/* 
 * Logistic Regression Grid
 */

Ext.define('delta3.view.logregr.LogregrGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.logregr',
    itemId: 'logregrGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'Ext.form.DateField',
        'delta3.view.logregr.LogregrFieldPopup',
        'delta3.utils.GridFilter'
    ],
    border: false,
    tbar: [{
            itemId: 'addLRF',
            text: 'Add LR Formula',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.LogregrModel.create({
                    active: true,
                    idStudy: 0,
                    idModel: 0,
                    name: '',
                    description: '',
                    status: 'manual',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            itemId: 'cloneLRF',
            text: 'Clone Formula',
            iconCls: 'cloneLRF-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Logistic Regression Formula first.');
                    return;
                }                  
                delta3.utils.GlobalFunc.doCloneLRF(sm.getSelection()[0].data, sm.getSelection()[0].data.name + ' Clone', thisGrid);
            }
        }, {
            itemId: 'deleteLRF',
            text: 'Delete',
            iconCls: 'delete-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];                
                thisGrid.plugins[0].cancelEdit();
                var sm = thisGrid.getSelectionModel();                
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Logistic Regression Formula first.');
                    return;
                }                    
                Ext.Msg.show({
                    title:'DELTA3',
                    message: 'You are about to delete Logistic Regression Formula. Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            thisGrid.store.remove(sm.getSelection());
                            thisGrid.store.sync();
                        }
                    }
                });                  
            }
        }, {
            itemId: 'recordInfo',
            text: 'View Properties',
            iconCls: 'recordInfo-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select logistic regression formula first.');
                    return;
                }                            
                var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Logistic Regression Formula"});
                win.show();
            }
        }
    ],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No formulas found',
            displayInfo: true
        }],
    fieldStore: {},
    studyStore: {},
    modelStore: {},
    initComponent: function() {
        var me = this;  
        me.fieldStore = delta3.utils.GlobalVars.FieldStore; 
        me.modelStore = delta3.store.ModelStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        me.modelStore.load();        
        me.studyStore = delta3.store.StudyStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        me.studyStore.load();
        me.store = me.buildStore();
        me.plugins = me.buildPlugins();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns(me);
        me.tbar[me.tbar.length] = delta3.utils.GridFilter.create({gridToSearch: me});   
        me.callParent();
        me.store.load();      
    },
    buildColumns: function(thisGrid) {
        return [
            {text: 'ID', dataIndex: 'idLogregr', width: 40, locked: true, tooltip: delta3.utils.Tooltips.logregrGridId},
            {text: 'Name', dataIndex: 'name', width: 130, locked: true, tooltip: delta3.utils.Tooltips.logregrGridName, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 160, tooltip: delta3.utils.Tooltips.logregrGridDescription, editor: 'textfield'},
            {text: 'Type', dataIndex: 'status', width: 50, tooltip: delta3.utils.Tooltips.logregrGridType},
            {text: 'Model', dataIndex: 'idModel', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridModel,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var id = rec.data['idModel'];
                    var model = thisGrid.modelStore.findRecord( 'idModel', id);
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                }},
            {text: 'Date Field', dataIndex: 'modelColumnidSequencer', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridDate,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var id = rec.data['modelColumnidSequencer'];
                    var field = thisGrid.fieldStore.findRecord( 'idModelColumn', id);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},            
            /*{text: 'Study', dataIndex: 'idStudy', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridStudy,
                editor: new Ext.form.ComboBox({
                    store: thisGrid.studyStore,
                    itemId: 'studyComboBox',
                    displayField: 'name',
                    valueField: 'idStudy',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            //ModelData.modelSelected = cmb.displayTplData[0];
                        }
                    }
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var id = rec.data['idStudy'];
                    var study = thisGrid.studyStore.findRecord( 'idStudy', id);
                    if (study === null)
                        return null;
                    else {
                        //rec.data['modelColumnidSequencer'] = study.get("idDate");
                        return study.get("name");
                    }
                }},*/
            {xtype: 'actioncolumn',
                text: 'Formula',   
                width: 60,
                tooltip: delta3.utils.Tooltips.logregrGridFormula,
                items: [{
                        //iconCls: '.check_good-icon',
                        icon: 'resources/images/page_link16.png',
                        tooltip: 'Click to define or edit Logistic Regression Formula',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var study = thisGrid.studyStore.findRecord('idStudy', rec.data.idStudy);
                            if ( study === null ) {
                               delta3.utils.GlobalFunc.showDeltaMessage("Data integrity problem. Cannot find study with ID " + rec.data.idStudy);
                               return;                                    
                            }
                            var win = new delta3.view.logregr.LogregrFieldPopup({fieldStore: thisGrid.fieldStore, 
                                logregrId: rec.data.idLogregr, 
                                logregrName: rec.data.name,
                                logregrFormula: rec.data.formula,
                                modelId: rec.data.idModel,
                                sequencerId: study.get('idDate')
                            });
                            win.show();
                        }
                    }]
            },
            //{text: 'Process ID', dataIndex: 'idProcess', width: 80, tooltip: delta3.utils.Tooltips.logregrGridId},      
            //{text: 'Interval Start', dataIndex: 'intervalData', width: 80, tooltip: delta3.utils.Tooltips.logregrGridInterval},        
            //{text: 'IntervalQuery', dataIndex: 'intervalQuery', width: 100},                
            {text: 'Original Formula', dataIndex: 'originalLogregrId', width: 100, tooltip: delta3.utils.Tooltips.logregrGridOriginId}
            //{text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50, editor: 'checkboxfield'}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.LogregrStore');
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'logregrGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                    var recSource = thisGrid.studyStore.findRecord('idStudy',changes.record.data.idStudy);
                    var m = recSource.get('idModel');
                    var d = recSource.get('idDate');
                    var recDest = thisGrid.store.findRecord('idStudy',changes.record.data.idStudy);        
                    recDest.set('idModel',m);
                    recDest.set('modelColumnidSequencer',d);
                    thisGrid.store.save();
                }
            }
        })];
    }
});

