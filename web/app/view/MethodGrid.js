/* 
 * Method Grid
 */

Ext.define('delta3.view.MethodGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.method',
    itemId: 'methodGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.view.mb.ModelBuilderContainer',
        'delta3.view.mb.TableViewerContainer'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#methodGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    tbar: [{
            text: 'Add Method',
            iconCls: 'add_new_config-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#methodGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.MethodModel.create({
                    name: '',
                    description: 'New Method',
                    methodParams: '',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            itemId: 'initializeMethodRemote',
            text: 'Initialize Remote',
            iconCls: 'initialize-icon16',
            handler: function() {
                Ext.Msg.show({
                    title:'DELTA3',
                    message: 'You are about to initialize DELTA configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            delta3.utils.GlobalFunc.doInitializeConfig("remote");
                        }
                    }
                });                
            }
        }, {
            itemId: 'initializeMethodLocal',
            text: 'Initialize Local',
            iconCls: 'initialize-icon16',
            handler: function() {
                Ext.Msg.show({
                    title:'DELTA3',
                    message: 'You are about to initialize DELTA configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            delta3.utils.GlobalFunc.doInitializeConfig("local");
                        }
                    }
                }); 
            }
        }, {
            itemId: 'refreshMethod',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#methodGrid')[0]
                thisGrid.store.load();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No methods found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idMethod', width: 40, locked: true},
            {text: 'Name', dataIndex: 'name', editor: 'textfield', width: 120, locked: true},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 120},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield', width: 40},
            {text: 'Parameters', dataIndex: 'methodParams', editor: 'textfield', width: 50},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        return Ext.create('delta3.store.MethodStore');
    }

});


