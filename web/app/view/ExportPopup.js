/* 
 * Export Popup Window
 */

Ext.define('delta3.view.ExportPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.export',
    layout: 'fit',
    width: 450,
    height: 360,
    itemId: 'exportPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3 Export',
    items: [{
            xtype: 'textareafield',
            width: 440,
            height: 340,            
            grow: true,
            itemId: 'exportDataString',
            value: '',
            allowBlank: true
        }],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                this.up('#exportPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});


