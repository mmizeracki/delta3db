var currentUser = 'Not Authorized';
var currentUserAuthInfo = null;

Ext.define('delta3.view.Viewport', {
    extend: 'Ext.container.Viewport',
    autoDestroy: true,
    requires: [
        'Ext.tab.Panel',
        'Ext.Toolbar',
        'Ext.window.MessageBox',
        'delta3.utils.GlobalVars',
        'delta3.controller.Methods',
        'delta3.controller.Logregrs',
        'delta3.controller.Configurations',
        'delta3.controller.Processes',
        'delta3.controller.Studies',
        'delta3.controller.Models',
        'delta3.controller.Organizations',
        'delta3.controller.Permissions',
        'delta3.controller.Groups',
        'delta3.controller.Roles',
        'delta3.controller.Users',
        'delta3.controller.Events',
        'delta3.controller.Eventtemplates',
        'delta3.controller.Alerts',
        'delta3.controller.Notifications',
        'delta3.view.PasswordPopup',
        'delta3.view.HelpPopup',        
        'delta3.store.FieldStore',
        'delta3.utils.GridFilter'],
    //layout: 'fit',
    title: 'D E L T A  3',
    initComponent: function() {
        var me = this;
        console.log('Initializing DELTA3 version ' + deltaVersion);
        delta3.utils.GlobalFunc.getCurrentUser(me);
        delta3.utils.GlobalVars.mainMenu = Ext.decode(delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu);
        var rightTopMenu = Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Change Password',
                    iconCls: 'password-icon16',
                    handler: function() {
                        var win = new delta3.view.PasswordPopup();
                        win.show();
                    }
                }, {
                    text: 'Logout',
                    iconCls: 'logout-icon16',
                    handler: function() {
                        doLogout();
                    }
                } , {
                    text: 'Help',
                    iconCls: 'help-icon16',
                    handler: function() {
                        var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
                        var t = theTabs.getActiveTab();
                        var win;
                        if ( t !== null ) {
                            win = new delta3.view.HelpPopup({helpSource: t.title + 'Help.html'});
                        } else {
                            win = new delta3.view.HelpPopup({helpSource: 'help.html'});
                        }
                        win.show();
                    }
                }]
        });
        me.items = [{
                xtype: 'panel',
                scrollable: true,
                autoScroll: true,
                title: '<p style="text-align:center"><span style="font-size: small">DELTA3 [ver. ' + deltaVersion + ']  Boston Advanced Analytics Inc.</span></p>',
                dockedItems: [{
                        xtype: 'toolbar',
                        docked: 'top',
                        itemId: 'mainmenu',
                        items: [{
                                text: 'DELTA3 Menu',
                                iconCls: 'home-icon16',
                                itemId: 'startbutton',
                                menu: delta3.utils.GlobalVars.mainMenu
                            }, {
                                xtype: 'tbfill'
                            }, {
                                xtype: 'button',
                                itemId: 'extPackageIndicator',
                                iconCls: 'bullet_black',
                                text: 'OCEANS',
                                listeners: { 'click' : function() {
                                        var thisButton = this;
                                        thisButton.setIconCls('bullet_black');
                                        thisButton.setDisabled(true);                                   
                                        Ext.Ajax.request({
                                            url: '/Delta3/webresources/process/initializeStatPackage',
                                            method: "GET",
                                            disableCaching: true,
                                            success: function(response, options) {
                                                thisButton.setDisabled(false);
                                                if (response.responseText === 'OK') {
                                                    thisButton.setIconCls('bullet_green');
                                                } else {
                                                    thisButton.setIconCls('bullet_red');
                                                }
                                            },
                                            failure: function(response) {
                                                console.log("call to OCEANS intit servlet failed");
                                                thisButton.setDisabled(false);
                                                thisButton.setIconCls('bullet_red');
                                            }
                                        });
                                    }
                                }
                            }, {
                                text: delta3.utils.GlobalVars.currentUser,
                                itemId: 'currentUserAlias',
                                menu: rightTopMenu,
                                icon: 'resources/images/user_suit.png'
                            }]
                    },
                    {
                        xtype: 'tabpanel',
                        itemId: 'maintabs',
                        layout: 'fit',
                        border: true,
                        //bodyPadding: 260,
                        //html: delta3.utils.GlobalFunc.displayCentrallyBigString('Click DELTA3 Menu to start'),
                        hidden: true
                    }]
            }];
        me.callParent();
        var b = me.down('#extPackageIndicator');
        b.fireEvent('click');  
    }
});




