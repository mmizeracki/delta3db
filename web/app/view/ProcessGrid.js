/* 
 * Process (Result) Grid
 */

Ext.define('delta3.view.ProcessGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.process',
    itemId: 'processGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.view.rv.ResultsContainer',
        'delta3.view.ResultsContainer',
        'delta3.store.ProcessStore',
        'delta3.utils.GridFilter'        
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'processGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No models found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
        var deleteProcess;
        if (delta3.utils.GlobalFunc.isPermitted("ProcessDelete") === true) {
            deleteProcess = {
                itemId: 'ProcessDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.resultBtnDelete,                     
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];                            
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                        return;
                    }                        
                    Ext.Msg.show({
                        title:'DELTA3',
                        message: 'You are about to delete Results. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {                          
                            if (btn === 'yes') {
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });                    
                }
            };
        }        
        me.tbar = [];
        var x = 0;   
        me.tbar[x++] = {
            itemId: 'showResults',
            text: 'Show Results',
            iconCls: 'show_results-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnShowResults,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                var sel = thisGrid.getSelectionModel().getSelection()[0];
                if (typeof sel === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                    return;
                }                  
                openResultsTab(sel.data);
            }
        };
        me.tbar[x++] = {
            text: 'Edit',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                    text: 'Edit Result',
                    iconCls: 'edit-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnEdit,                        
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        thisGrid.plugins[0].startEdit(sel, 0);
                    }
                }, deleteProcess, 
                {
                    itemId: 'recordInfo',
                    text: 'View Properties',
                    iconCls: 'recordInfo-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnProperties,                    
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0]
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        var win = Ext.create('delta3.view.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Process"});
                        win.show();
                    }
                }]
            })
        };       
        me.tbar[x++] = {
            text: 'Advanced',
            iconCls: 'spec-icon16',
            //tooltip: 'Step 2',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                    itemId: 'getStatus',
                    text: 'Get Stage Status',
                    iconCls: 'statProxy-getStatus-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnGetStatus,                        
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        doGetStatus(sm.getSelection());    
                        //openResultsTabOld(sm.getSelection()[0].data);
                    }
                },  {
                    itemId: 'retrieveResults',
                    text: 'Retrieve Results',
                    iconCls: 'statProxy-getResults-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnRetrieveResults,                    
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        doGetStatResults(sm.getSelection());
                    }
                }]
            })
        };   
        me.tbar[x++] = {
            itemId: 'refreshStudy',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnRefresh,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                thisGrid.store.load();
            }
        };          
        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me});   
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        var modelStore = delta3.store.ModelStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        modelStore.load();        
        return [
            {text: 'ID', dataIndex: 'idProcess', width: 30},
            {text: 'Study Stage', dataIndex: 'name', width: 140, tooltip: delta3.utils.Tooltips.resultGridName},
            {text: 'Status', dataIndex: 'status', width: 70, tooltip: delta3.utils.Tooltips.resultGridStatus},
            {text: 'Progress %', dataIndex: 'progress', width: 70, tooltip: delta3.utils.Tooltips.resultGridProgress},                 
            {text: 'Data', dataIndex: 'data', width: 280, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridData},
            {text: 'Message', dataIndex: 'message', width: 160, tooltip: delta3.utils.Tooltips.resultGridMessage},
            {text: 'Description', dataIndex: 'description', width: 280, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridDescription},                   
            {text: 'Data Model', dataIndex: 'idModel', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.resultGridModel,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModel'];
                    var model = modelStore.findRecord('idModel', tableIndex);
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                }},
            {text: "Processed on", width: 128, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'}            
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.ProcessStore');
    }
});

//------------------------------------------------------------ call to get results
function doGetStatResults(selections)
{
    var processes = '';
    var guids = '';

    for (var i = 0; i < selections.length; i++) {
        if (i !== 0) {
            processes += ',';
            guids += ',';
        }
        processes += selections[i].data.idProcess;
        guids += selections[i].data.guid;
    }
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/process/getStatResult',
                        method: "GET",
                        disableCaching: true,
                        params: {processId: processes, guid: guids},
                        success: doGetStatResultsSuccess,
                        failure: doGetStatResultsFailed
                    }
            );
    function doGetStatResultsSuccess(response, options)
    {
        console.log('calling getStatResults success ' + response.responseText);
        //Ext.Msg.alert("DELTA3", response.responseText);
    }

    function doGetStatResultsFailed(response, options)
    {
        console.log('calling getStatResults failed ' + response.responseText);
        Ext.Msg.alert("DELTA3", response.responseText);
    }    
}

//------------------------------------------------------------ call to get jobs status
function doGetStatus(selections)
{
    var processes = '';
    var guids = '';

    for (var i = 0; i < selections.length; i++) {
        if (i !== 0) {
            processes += ',';
            guids += ',';
        }
        processes += selections[i].data.idProcess;
        guids += selections[i].data.guid;
        console.log('calling OCEANS getStatus for process id ' + selections[i].data.idProcess);
    }

    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/process/getJobStatus',
                        method: "GET",
                        disableCaching: true,
                        params: {processId: processes, guid: guids},
                        success: doGetStatusSuccess,
                        failure: doGetStatusFailed
                    }
            );
    function doGetStatusSuccess(response, options)
    {
        console.log('calling getStatus success ' + response.responseText);
    }

    function doGetStatusFailed(response, options)
    {
        console.log('calling getStatus failed ' + response.responseText);
        Ext.Msg.alert("DELTA3", response.responseText);
    }    
}

function openResultsTab(processRecord)
{
    delta3.utils.GlobalFunc.getStudyDetails(processRecord.idStudy, showTab);
    
    function showTab(resp, options) {
        var theTabs = Ext.ComponentQuery.query('#maintabs')[0];
        theTabs.add(Ext.create('delta3.view.rv.ResultsContainer', {
            title: 'Results [' + processRecord.name + ', ID ' + processRecord.idProcess + ']',
            resultsData: processRecord.data,
            processId: processRecord.idProcess,
            name: processRecord.name,
            description: processRecord.description,
            outcomeName: resp.studyDetails[0].outcome.name,
            closable: true
        })).show();
        theTabs.doLayout();
    }
}

function openResultsTabOld(processRecord)
{
    var theTabs = Ext.ComponentQuery.query('#maintabs')[0];
    theTabs.add(Ext.create('delta3.view.ResultsContainer', {
        title: 'Results: ' + processRecord.name + ' [' + processRecord.idModel + '][' + processRecord.idProcess + ']',
        resultsData: processRecord.data,
        closable: true
    })
            ).show();
    theTabs.doLayout();
}
