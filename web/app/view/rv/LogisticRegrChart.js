/**
 * Logistic Regression Chart uses candle stick chart
 */

Ext.define('delta3.view.rv.LogisticRegrChart', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chart.logisticRegrChart',
    requires: [       
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category'
    ],    
    height: 400,
    width: 800,
    insetPadding: 20,
    innerPadding: 20,  
    initComponent: function() {
        var me = this;
        me.axes[0].setMinimum(me.calcStoreMin(me.store));
        me.axes[0].setMaximum(me.calcStoreMax(me.store));      
        //me.axes[0].set("log-scale", true);
        //chartModel.getYAxis().set("log-scale", true);
        me.callParent();
    },    
    axes: [{
        type: 'numeric',
        position: 'left',
        fields: ['low', 'medium', 'high', 'open', 'close', 'zero'],
        scale: 'log-scale',
        grid: true
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['covariate'],
        title: 'Covariate'
    }],
    series: [
        {
            type: 'candlestick',
            xField: 'covariate',
            openField: 'open',
            highField: 'high',
            lowField: 'low',
            closeField: 'close',
            tooltip: {
                trackMouse: true,
                renderer: function (storeItem, item) {
                  if ( storeItem.get('alert') !== '' ) {
                        this.width = 140;
                        this.hight = 60;
                        this.setHtml(storeItem.get('alert') + '<br>' 
                                + 'Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));                      
                  } else {
                        this.width = 100;
                        this.hight = 50;                      
                        this.setHtml('Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));
                    }
                }                
            },          
            style: {
                barWidth: 15,
                barHeight: 10,
                opacity: 0.9,
                dropStyle: {
                    fill: '#ff0000',
                    stroke: '#ff0000',
                    'stroke-width': 4
                },
                raiseStyle: {
                    fill: '#333333',
                    stroke: '#333333',
                    'stroke-width': 4               
                }
            }
        },{
            type: 'line', // artificial red zero line    
            style: {
                stroke: '#FF0000', 
                opacity: 0.7,
                lineWidth: 1
            },
            highlight: {
                scale: 0.9
            },                          
            xField: 'covariate',
            yField: 'zero'
           }
    ],
    calcStoreMin: function (store) {
        var bottomMargin = 0.2;
        var tmp = store.getAt(0).data.low;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.low < tmp) {
                tmp = store.getAt(i).data.low;
            }
        }
        return (tmp - bottomMargin);
    },
    calcStoreMax: function (store) {
        var topMargin = 0.2;
        var tmp = store.getAt(0).data.high;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.high > tmp) {
                tmp = store.getAt(i).data.high;
            }
        }
        return (tmp + topMargin);
    }     
});