/**
 * Proportional Difference Chart uses candle stick chart
 */
//var tipWidth = 152;
//var tipHeight = 46;

Ext.define('delta3.view.rv.PropDiffChart', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chart.propDiffChart',
    requires: [
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.draw.Sprite'
    ],
    itemId: 'propDiffChart',
    height: 400,
    width: 800,
    insetPadding: 20,
    innerPadding: 20,
    axes_min: {},
    axes_max: {},
    sprites: [{
        id: 'spriteTitle',
        type  : 'text',
        font: '16px Arial',
        text: 'Chart Title',
        x: 200,
        y: 40,
        width: 200,
        height: 40 
    }],
    initComponent: function() {
        var me = this;
        me.axes[0].setMinimum(me.calcStoreMin(me.store));
        me.axes[0].setMaximum(me.calcStoreMax(me.store));
        me.callParent();
    },
    axes: [{
            type: 'numeric',
            position: 'left',
            fields: ['low', 'medium', 'high', 'open', 'close', 'zero'],
            grid: true
        }, {
            type: 'category',
            position: 'bottom',
            fields: ['time'],
            title: 'Time period'
        }],
    series: [
        {
            type: 'candlestick',
            xField: 'time',
            openField: 'open',
            highField: 'high',
            lowField: 'low',
            closeField: 'close',
            tooltip: {
                trackMouse: true,
                renderer: function(storeItem, item) {
                    if (storeItem.get('alert') !== '') {
                        this.width = 140;
                        this.hight = 60;
                        this.setHtml(storeItem.get('alert') + '<br>'
                                + 'Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));
                    } else {
                        this.width = 100;
                        this.hight = 50;
                        this.setHtml('Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));
                    }
                }
            },
            style: {
                barWidth: 10,
                barHeight: 10,
                opacity: 0.9,
                dropStyle: {
                    fill: '#ff0000',
                    stroke: '#ff0000',
                    'stroke-width': 2
                },
                raiseStyle: {
                    fill: '#333333',
                    stroke: '#333333',
                    'stroke-width': 2
                }
            },
            aggregator: {
                strategy: 'time'
            }
        }, {
            type: 'line', // artificial red zero line    
            style: {
                stroke: '#FF0000',
                opacity: 0.7,
                lineWidth: 1
            },
            highlight: {
                scale: 0.9
            },
            xField: 'time',
            yField: 'zero'
        }
    ],
    calcStoreMin: function (store) {
        var bottomMargin = 0.2;
        var tmp = store.getAt(0).data.low;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.low < tmp) {
                tmp = store.getAt(i).data.low;
            }
        }
        return (tmp - bottomMargin);
    },
    calcStoreMax: function (store) {
        var topMargin = 0.2;
        var tmp = store.getAt(0).data.high;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.high > tmp) {
                tmp = store.getAt(i).data.high;
            }
        }
        return (tmp + topMargin);
    } 
});