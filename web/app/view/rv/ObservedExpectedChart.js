/**
 * Observed Expected Chart
 */
var tipWidth = 180;
var tipHeight = 46;

Ext.define('delta3.view.rv.ObservedExpectedChart', {
    animate: true,
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chart.observedExpectedChart',
    requires: [       
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.interactions.PanZoom',
        'Ext.chart.interactions.Crosshair'
    ],    
    height: 400,
    width: 800,
    insetPadding: 20,
    innerPadding: 20,    
    yAxisTitle: "Cumulative ",
    sprites: [{
        id: 'spriteTitle',
        type  : 'text',
        font: '16px Arial',
        text: 'Chart Title',
        x: 200,
        y: 40,
        width: 200,
        height: 40 
    }],        
    background: 'white',
    interactions: [
         {
             type: 'panzoom',
             enabled: false,
             zoomOnPanGesture: false,
             axes: {
                 left: {
                     allowPan: false,
                     allowZoom: false
                 },
                 bottom: {
                     allowPan: true,
                     allowZoom: true
                 }
             }
         },
         {
             type: 'crosshair'
         }
    ],             
    initComponent: function() {
        var me = this;
        me.axes[0].setTitle(me.yAxisTitle);
        me.callParent();
    },
    axes: [{
        type: 'numeric',
        position: 'left',
        fields: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        grid: true,
        minimum: 0,
        maximum: 100
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['time'],
        title: 'Time',
        grid: false
    }],
    series: [
        {
                type: 'line',
                fill: true,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    fill: "#CCE6FF",
                    stroke: "#3399FF",
                    lineWidth: 1
                },
                title: 'Upper Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                marker: {
                    scale: 0.7,
                    fx: {
                        duration: 200
                    }
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: '2'
            },{
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    fill: "#CCE6FF",
                    stroke: "#3399FF",                                
                    lineWidth: 1
                },
                title: 'Mean Confidence',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                                     
                marker: {
                    scale: 0.7,
                    fx: {
                        duration: 200
                    }
                },
                xField: 'time',
                yField: '1'
            },{
                type: 'line',
                fill: true,
                style: {
                    smooth: true,
                    opacity: 1,
                    fill: "#FFFFFF",
                    stroke: "#3399FF",                                
                    lineWidth: 1
                },
                title: 'Lower Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                                     
                marker: {
                    scale: 0.7,
                    fx: {
                        duration: 200
                    }
                },
                xField: 'time',
                yField: '0'
            }, {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#CCE6FF",
                    stroke: "#FF99CC",
                    lineWidth: 1
                },
                title: 'Upper Secondary Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                marker: {
                    scale: 0.7,
                    fx: {
                        duration: 200
                    }
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: '6'
            }, {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#CCE6FF",
                    stroke: "#FF99CC",
                    lineWidth: 1
                },
                title: 'Lower Secondary Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                marker: {
                    scale: 0.7,
                    fx: {
                        duration: 200
                    }
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: '7'
            }, {
                type: 'candlestick',
                xField: 'time',
                openField: '4',
                highField: '5',
                lowField: '3',
                closeField: '4',
                style: {
                    barWidth: 10,
                    barHeight: 10,
                    opacity: 0.9,
                    //ohlcType: 'ohlc',
                    raiseStyle: {
                        fill: "#003366",
                        stroke: "#003366",
                        'stroke-width': 2
                    },
                    dropStyle: {
                        fill: "#663300",
                        stroke: "#663300",
                        'stroke-width': 2
                    }
                },
                aggregator: {
                    strategy: 'time'
                }
            }
        ]            
    });
 
function displayExpObsTip (tip, storeItem, item) {
   tip.setTitle(storeItem.get('time') + '<br>  Exp: ' 
           + storeItem.get('0') + ', '   
           + storeItem.get('1') + ', '     
           + storeItem.get('2') + ' '
           + '<br>  Obs: ' 
           + storeItem.get('3') + ', '   
           + storeItem.get('4') + ', '     
           + storeItem.get('5') + ' '
       );
}