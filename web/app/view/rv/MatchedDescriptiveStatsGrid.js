/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('delta3.view.rv.MatchedDescriptiveStatsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.matchedDescriptiveStats',
    scrollable: true,
    resultsData: {},
    height: delta3.utils.GlobalVars.tabHeight,
    requires: [
        'Ext.data.Store',
        'Ext.data.Model'
    ],
    layout: 'fit',
    width: '100%',
    columnLines: true,
    items: [],
    viewConfig: {
        enableTextSelection: true,
        getRowClass: function(record, rowIndex, rowParams, store) {
            if (rowIndex === 0)
                return 'firstRow';
        }
    },
    initComponent: function() {
        var me = this;
        var modelFields = me.buildGridModelFields(me.resultsData);
        me.store = me.buildStore(me.resultsData, modelFields);
        me.columns = eval("(" + me.buildGridColumns(me.resultsData) + ")");
        me.tbar = []; 
        me.tbar[0] = {
            text: 'Export to CSV',
            iconCls: 'exportCSV-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
            handler: function(b, e) {
                b.up('grid').exportGrid('DELTA3 ' + me.title);
            }
        };        
        me.callParent();
    },
    buildGridModelFields: function(resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var j, i;
        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === 'PropensityMatchDescriptiveStats') {
                var hString = '[';
                for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                    if (i > 0) {
                        hString += ',';
                    }
                    hString += '{"name":"' + i + '", "type":"string"}';
                    //hString += '{"name":"' + resultsArray.results[j].columns[i].name + '", "type":"string"}';                    
                }
                return hString += ']';
            }
        }
        return null;
    },
    buildStore: function(resultsData, modelFields) {
        var resultsArray = JSON.parse(resultsData);
        var data = '{"dsData":[';
        var dataRow;

        for (var i = 0; i < resultsArray.results.length; i++) {
            if (resultsArray.results[i].name === 'PropensityMatchDescriptiveStats') {
                for (var j = 0; j < resultsArray.results[i].data.length; j++) {
                    if (j > 0) {
                        data += ',';
                    }
                    dataRow = '[';
                    for (var z = 0; z < resultsArray.results[i].data[j].length; z++) {
                        if (z > 0) {
                            dataRow += ',';
                        }
                        if ( resultsArray.results[i].data[j][z] === 'null' ) {
                            resultsArray.results[i].data[j][z] = '';
                        }                        
                        dataRow += '"' + resultsArray.results[i].data[j][z] + '"';
                    }
                    dataRow += ']';
                    data += dataRow;
                }
            }
        }
        data += ']}';

        var fields = eval("(" + modelFields + ")");
        var model = Ext.define(null, {
            extend: 'Ext.data.Model',
            fields: fields
        });

        var store = new Ext.data.Store({
            autoLoad: true,
            model: model,
            data: {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });

        store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
        return store;
    },
    buildGridColumns: function(resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var hString = '[';
        var j, i;
        var topColumnArray = [];
        var middleColumnArray = [];
        var bottomColumnArray = [];
        var columnProcessed = [];

        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === 'PropensityMatchDescriptiveStats') {
                var size = resultsArray.results[j].columns.length;

                // pre-processor loop
                for (i = 0; i < size; i++) {
                    if ((typeof resultsArray.results[j].columns[i].parentcolumn !== 'undefined') && (resultsArray.results[j].columns[i].parentcolumn.name !== "")) {
                        if ((typeof resultsArray.results[j].columns[i].parentcolumn.parentcolumn !== 'undefined') && (resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name !== "")) {
                            topColumnArray[i] = resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name;
                            middleColumnArray[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                            bottomColumnArray[i] = resultsArray.results[j].columns[i].name;
                        } else {
                            middleColumnArray[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                            bottomColumnArray[i] = resultsArray.results[j].columns[i].name;
                        }
                    } else {
                        if ((typeof resultsArray.results[j].columns[i].parentcolumn !== 'undefined') && (typeof resultsArray.results[j].columns[i].parentcolumn.parentcolumn !== 'undefined')) {
                            // the case of missing middle column name
                            topColumnArray[i] = resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name;
                            middleColumnArray[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                            bottomColumnArray[i] = resultsArray.results[j].columns[i].name;
                        } else {
                            // regular column with no middle or top grouping
                            bottomColumnArray[i] = resultsArray.results[j].columns[i].name;
                        }
                    }
                    columnProcessed[i] = 'n';
                }

                // loop creating JSON
                var hString = '[';
                var separatorI = 0;
                for (i = 0; i < size; i++) {
                    //var dataIndex = resultsArray.results[j].columns[i].name;
                    if ((typeof topColumnArray[i] !== 'undefined') && (columnProcessed[i] === 'n')) {
                        if (separatorI > 0) {
                            hString += ',';
                        }
                        separatorI++;
                        hString += '{"text":"' + topColumnArray[i] + '","columns":[';
                        var separatorZ = 0;
                        for (var z = 0; z < size; z++) {
                            if ((columnProcessed[z] === 'n') && (topColumnArray[z] === topColumnArray[i])) {
                                if (separatorZ > 0) {
                                    hString += ',';
                                }
                                columnProcessed[z] = 'y';
                                separatorZ++;
                                hString += '{"text":"' + middleColumnArray[z] + '","columns":[';
                                var separatorY = 0;
                                for (var y = 0; y < size; y++) {
                                    if ((middleColumnArray[y] === middleColumnArray[z]) && (topColumnArray[y] === topColumnArray[z])) {
                                        if (separatorY > 0) {
                                            hString += ',';
                                        }
                                        columnProcessed[y] = 'y';
                                        separatorY++;
                                        switch (topColumnArray[y]) {
                                            case "Prior To Match":
                                                hString += '{"text":"' + bottomColumnArray[y] + '", "dataIndex":"' + y + '", "type":"string", "width": 70, tdCls: "custom-column-blue"}';
                                                break;
                                            case "After Match":
                                                hString += '{"text":"' + bottomColumnArray[y] + '", "dataIndex":"' + y + '", "type":"string", "width": 70, tdCls: "custom-column-yellow"}';
                                                break;
                                            case "Unmatched Exposures":
                                                hString += '{"text":"' + bottomColumnArray[y] + '", "dataIndex":"' + y + '", "type":"string", "width": 70, tdCls: "custom-column-orange"}';
                                                break;
                                            default:
                                                hString += '{"text":"' + bottomColumnArray[y] + '", "dataIndex":"' + y + '", "type":"string", "width": 120}';
                                                break;
                                        }
                                        //hString += '{"text":"'+ bottomColumnArray[y] + '", "dataIndex":"' + y + '", "type":"string", "width": 70, tdCls: "custom-column"}';                                        
                                    }
                                }
                                hString += ']}';
                            }
                        }
                        hString += ']}';
                    } else {
                        if ((columnProcessed[i] === 'n')) {
                            if (separatorI > 0) {
                                hString += ',';
                            }
                            separatorI++;
                            //hString += '{"text":"' + resultsArray.results[j].columns[i].name  + '", "dataIndex":"' + resultsArray.results[j].columns[i].name + '", "type":"string", "width": 70}';  
                            hString += '{"text":"' + resultsArray.results[j].columns[i].name + '", "dataIndex":"' + i + '", "type":"string", "width": 70}';
                            columnProcessed[i] = 'y';
                        }
                    }
                }
                return hString += ']';
            }
        }
        return null;
    }
});


