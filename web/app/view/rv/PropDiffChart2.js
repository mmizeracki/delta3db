/**
 * Proportional Difference Chart Zingchart version uses candle stick chart
 */


Ext.define('delta3.view.rv.PropDiffChart2', {
    extend: 'Ext.tab.Panel',    
    alias: 'widget.chart.propDiffChart2',
    html: '<div id="propDiffChartId"></div>',
    itemId: 'propDiffChart2',
    height : 400,
    width : 800,   
    axes_min: {},
    axes_max: {},    
    store: {},
    listeners: {
        afterrender: function() {
            var me = Ext.ComponentQuery.query('#propDiffChart2')[0]; 
            console.log("afterrender listener!");
            var series = [
                                {
                                    "values":[
                                    []
                                    ]
                                }
                            ]; 
            for (var i=0; i < me.store.data.length; i++) {
                series[0].values[i] = [
                me.store.data[i].medium,
                me.store.data[i].high,
                me.store.data[i].low,
                me.store.data[i].medium];

            }            
            var myChart = {"graphset":[
                    {
                        "type":"stock",
                        "plot":{
                                "aspect":"whisker"
                            },                        
                        "series": series
                    }
                ]
            };
            zingchart.render({
                    id : "propDiffChartId",
                    height : 400,
                    width : 800,
                    data : myChart
                    });            
        }
    },    
    initComponent: function() {
        var me = this;
	me.callParent();        
    }            
});