/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('delta3.view.rv.DescriptiveStatsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.descriptiveStats',
    scrollable: true,
    viewConfig: {
        enableTextSelection: true
    },
    resultsData: {},
    height: delta3.utils.GlobalVars.tabHeight,
    requires: [
        'Ext.data.Store',
        'Ext.data.Model'
    ],
    layout: 'fit',
    width: '100%',
    columnLines: true,
    items: [],
    initComponent: function() {
        var me = this;
        var modelFields = me.buildGridModelFields(me.resultsData);
        me.store = me.buildStore(me.resultsData, modelFields);
        me.columns = eval("(" + me.buildGridColumns(me.resultsData) + ")");
        me.tbar = []; 
        me.tbar[0] = {
            text: 'Export to CSV',
            iconCls: 'exportCSV-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
            handler: function(b, e) {
                b.up('grid').exportGrid('DELTA3 ' + me.title);
            }
        };        
        me.callParent();
    },
    buildGridModelFields: function(resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var j, i;
        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === 'DescriptiveStatisticsOutput') {
                var hString = '[';
                for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                    if (i > 0) {
                        hString += ',';
                    }
                    hString += '{"name":"' + resultsArray.results[j].columns[i].name + '", "type":"string"}';
                }
                return hString += ']';
            }
        }
        return null;
    },
    buildStore: function(resultsData, modelFields) {
        var resultsArray = JSON.parse(resultsData);
        var data = '{"dsData":[';
        var dataRow;

        for (var i = 0; i < resultsArray.results.length; i++) {
            if (resultsArray.results[i].name === 'DescriptiveStatisticsOutput') {
                for (var j = 0; j < resultsArray.results[i].data.length; j++) {
                    if (j > 0) {
                        data += ',';
                    }
                    dataRow = '[';
                    for (var z = 0; z < resultsArray.results[i].data[j].length; z++) {
                        if (z > 0) {
                            dataRow += ',';
                        }
                        if ( resultsArray.results[i].data[j][z] === 'null' ) {
                            resultsArray.results[i].data[j][z] = '';
                        }
                        dataRow += '"' + resultsArray.results[i].data[j][z] + '"';
                    }
                    dataRow += ']';
                    data += dataRow;
                }
            }
        }
        data += ']}';

        var fields = eval("(" + modelFields + ")");
        var model = Ext.define(null, {
            extend: 'Ext.data.Model',
            fields: fields
        });

        var store = new Ext.data.Store({
            autoLoad: true,
            model: model,
            data: {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });

        store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
        return store;
    },
    buildGridColumns: function(resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var hString = '[';
        var j, i;
        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === 'DescriptiveStatisticsOutput') {
                var hString = '[';
                for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                    if (i > 0) {
                        hString += ',';
                    }
                    hString += '{"text":"' + resultsArray.results[j].columns[i].name + '", "dataIndex":"' + resultsArray.results[j].columns[i].name + '", "type":"string", "width": 100}';
                }
                return hString += ']';
            }
        }
        return null;
    }
});


