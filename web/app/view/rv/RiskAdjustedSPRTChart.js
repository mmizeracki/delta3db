/**
 * Risk Adjusted SPRT Chart
 */
var tipWidth = 152;
var tipHeight = 30;

Ext.define('delta3.view.rv.RiskAdjustedSPRTChart', {
    animate: true,
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chart.riskAdjustedSPRTChart',
    requires: [       
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.interactions.PanZoom',
        'Ext.chart.interactions.Crosshair'
    ],    
    height: 400,
    width: 800,
    insetPadding: 20,
    innerPadding: 20,    
    sprites: [{
        id: 'spriteTitle',
        type  : 'text',
        font: '16px Arial',
        text: 'Chart Title',
        x: 200,
        y: 40,
        width: 200,
        height: 40 
    }],    
    initComponent: function() {
        var me = this;
        me.axes[0].setMinimum(me.calcStoreMin(me.store));
        me.axes[0].setMaximum(me.calcStoreMax(me.store));
        me.callParent();
    },     
    background: 'white',
    interactions: [
         {
             type: 'panzoom',
             enabled: false,
             zoomOnPanGesture: false,
             axes: {
                 left: {
                     allowPan: false,
                     allowZoom: false
                 },
                 bottom: {
                     allowPan: true,
                     allowZoom: true
                 }
             }
         },
         {
             type: 'crosshair'
         }
    ],               
    axes: [{
        type: 'numeric',
        position: 'left',
        fields: ['riskAdjustedValue', 'upper', 'lower'],
        title: 'Cumulative Log-Likelihood Ratio',
        grid: true
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['RowId'],
        title: 'Procedure Number',
        grid: true
    }],
    series: [
        {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#3399FF",
                    stroke: "#CC00CC",
                    lineWidth: 1
                },
                title: 'Accept Hypothesis',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displaySPRTTip(this, storeItem, item);
                    }
                },                            
                xField: 'RowId',
                yField: 'upper'
            },{
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#3399FF",
                    stroke: "#000000",                                
                    lineWidth: 1
                },
                title: 'Risk Adjusted Value',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displaySPRTTip(this, storeItem, item);
                    }
                },                                     
                xField: 'RowId',
                yField: 'riskAdjustedValue'
            },{
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 1,
                    //fill: "#FFFFFF",
                    stroke: "#3399FF",                                
                    lineWidth: 1
                },
                title: 'Reject Hypothesis',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidth,
                    height: tipHeight,
                    renderer: function (storeItem, item) {
                        displaySPRTTip(this, storeItem, item);
                    }
                },
                xField: 'RowId',
                yField: 'lower'
            }
        ],
    calcStoreMin: function (store) {
        var bottomMargin = 0.2;
        var tmp = store.getAt(0).data.lower;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.riskAdjustedValue < tmp) {
                tmp = store.getAt(i).data.riskAdjustedValue;
            }
        }
        return (tmp - bottomMargin);
    },
    calcStoreMax: function (store) {
        var topMargin = 0.2;
        var tmp = store.getAt(0).data.upper;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.riskAdjustedValue > tmp) {
                tmp = store.getAt(i).data.riskAdjustedValue;
            }
        }
        return (tmp + topMargin);
    }        
    });

function displaySPRTTip (tip, storeItem, item) {
        tip.setTitle('Procedure Number: ' + storeItem.get('RowId') 
        + '<br>Ratio: ' + storeItem.get('riskAdjustedValue')
       );
}