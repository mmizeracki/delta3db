/* 
 * (C) Boston Advanced Analytics
 * Results Container
 */
Ext.define('delta3.view.rv.ResultsContainer', {
    extend: 'Ext.tab.Panel',
    autoDestroy: true,
    requires: [
        'Ext.tab.Panel',
        'delta3.view.rv.DescriptiveStatsGrid',
        'delta3.view.rv.MatchedDescriptiveStatsGrid',
        'delta3.view.rv.LogisticRegrChart',
        'delta3.view.rv.ObservedExpectedChart', 
        'delta3.view.rv.ObservedExpectedGrid',
        'delta3.view.rv.RiskAdjustedSPRTChart',   
        'delta3.view.rv.RiskAdjustedSPRTGrid',           
        'delta3.view.rv.PropDiffChart',
        'delta3.view.rv.PropDiffGrid',
        'delta3.view.rv.LogisticRegressionFormulaGrid'
    ],
    items: [],
    resultsData: {},
    processId: {},
    outcomeName: {},
    initComponent: function () { 
        var me = this;
        if ( (me.resultsData === '') || (typeof me.resultsData === 'undefined') ) {
            delta3.utils.GlobalFunc.showDeltaMessage("There are no results for this study");
            me.destroy();
            this.callParent();  
            return;
        }
        var logisticRegrStore = me.buildLogisticRegrChartStore(me.resultsData);
        var propDiffStore = me.buildPropDiffChartStore(me.resultsData);
        var observedExpectedStore = me.buildObservedExpectedChartStore(me.resultsData);        
        var riskAdjustedSPRTStore = me.buildRiskAdjustedSPRTChartStore(me.resultsData);    
        if ( typeof me.description === 'undefined' ) {
            me.description = '';
        }        
        var itemsCount = 0;
        me.items = [];
        if ( logisticRegrStore.data.length > 0 ) {
            me.items[itemsCount++] = { xtype:  'chart.logisticRegrChart',
                            title: 'Cumulative Standard Graph',
                            store: logisticRegrStore
                    };
        }
        if ( propDiffStore.data.length > 0 ) {        
            var c = Ext.create('delta3.view.rv.PropDiffChart',{
                            xtype:  'chart.propDiffChart',
                            title: 'Cumulative Proportional Graph', 
                            store: propDiffStore
                    });
            me.items[itemsCount++] = c;              
            var surface = c.getSurface('chart');
            var sprite = surface.get('spriteTitle');
            sprite.setText(me.name + ' - ' + me.description);    
            me.items[itemsCount++] = {
                            xtype:  'grid.propDiff',
                            title: 'Cumulative Proportional Table',
                            resultsData: this.resultsData
                    };               
        }   
        if ( observedExpectedStore.data.length > 0 ) {        
            var c = Ext.create('delta3.view.rv.ObservedExpectedChart',{
                            xtype:  'chart.observedExpectedChart',
                            title: 'Cumulative Standard Graph',    
                            yAxisTitle: 'Cumulative ' + me.outcomeName,
                            store: observedExpectedStore
                    });
            me.items[itemsCount++] = c;              
            var surface = c.getSurface('chart');
            var sprite = surface.get('spriteTitle');
            sprite.setText(me.name + ' - ' + me.description);    
            me.items[itemsCount++] = {
                            xtype:  'grid.observedExpected',
                            title: 'Cumulative Standard Table',
                            resultsData: this.resultsData
                    };               
        }    
        if ( riskAdjustedSPRTStore.data.length > 0 ) {
            var c = Ext.create('delta3.view.rv.RiskAdjustedSPRTChart', {
                            title: 'Risk Adjusted SPRT Graph',                        
                            store: riskAdjustedSPRTStore
                    });
            me.items[itemsCount++] = c;      
            var surface = c.getSurface('chart');
            var sprite = surface.get('spriteTitle');
            sprite.setText(me.name + ' - ' + me.description);     
            me.items[itemsCount++] = {
                            xtype:  'grid.riskAdjustedSPRT',
                            title: 'Risk Adjusted SPRT Table',
                            resultsData: this.resultsData
                    };              
        }       
        if ( me.checkIfResultsPresent(me.resultsData,'DescriptiveStatisticsOutput') === true ) {
            me.items[itemsCount++] = {
                            xtype:  'grid.descriptiveStats',
                            title: 'Descriptive Statistics Table',
                            resultsData: this.resultsData
                    };         
        } 
        if ( me.checkIfResultsPresent(me.resultsData,'PropensityMatchDescriptiveStats') === true ) {        
            me.items[itemsCount++] = {
                                xtype:  'grid.matchedDescriptiveStats',
                                title: 'Matched Data Descriptive Statistics Table', 
                                resultsData: this.resultsData
                        };   
        }
        if ( me.checkIfResultsPresent(me.resultsData,'LogisticRegressionFormulaOutput') === true ) {        
            /*delta3.utils.GlobalFunc.getLRFforProcess(me.processId, showModel);   
            function showModel(result, options) {
                var win = new delta3.view.logregr.LrfPopup({lrfString: result.status.message});
                win.show();
                me.destroy();
            }*/
            me.items[itemsCount++] = {
                                xtype:  'grid.logisticRegressionFormula',
                                title: 'Logistic Regression Formula Table', 
                                resultsData: this.resultsData
                        };               
        }        
        this.callParent();       
    },
    buildLogisticRegrChartStore: function(resultsData) {
       var modelFields = '["id","covariate","low","medium","high","zero","alert"]';  
       var storeFields = JSON.parse(modelFields);             
       var resultStore = {fields: []};            
       var resultsArray = JSON.parse(resultsData);
       var data = '[';
       var dataRow;
       
       for (var i=0; i<resultsArray.results.length; i++) {      
           if (resultsArray.results[i].name === 'LogisticRegressionGraphOutput') { 
                for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                     if ( j > 0 ) {
                         data += ',';
                     }
                     dataRow = '{';
                     for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                        if ( z > 0 ) {
                            dataRow += ',';
                        }  
                        if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                            resultsArray.results[i].data[j][z] = '""';
                        }
                        if  ( storeFields[z] === "covariate" ) {
                            dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                        } else {
                            dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                        }
                     }
                     dataRow += ',"zero":0,"open":0,"close":0,"alert":""}';
                     data += dataRow;
                }
           }
       }
       data += ']';
       
       var storeData = JSON.parse(data);
       resultStore.data = storeData;
       // following loop is to notify viewer about unusual record
       // candle graph, used out of the box, requires this artificial operation 
       for (var y=0; y<resultStore.data.length; y++) {
           if ( resultStore.data[y].low < 0 ) {
               resultStore.data[y].open = resultStore.data[y].medium - 0.001;
               resultStore.data[y].close = resultStore.data[y].medium + 0.001;
           } else {
               resultStore.data[y].open = resultStore.data[y].medium + 0.001;
               resultStore.data[y].close = resultStore.data[y].medium - 0.001; 
               resultStore.data[y].alert = 'Out of expected range';
           }
       }       
       return resultStore;        
    },             
    buildPropDiffChartStore: function(resultsData) {
       var modelFields = '["id","time","low","medium","high","alert"]';  
       var storeFields = JSON.parse(modelFields);             
       var resultStore = {fields: []};            
       var resultsArray = JSON.parse(resultsData);
       var data = '[';
       var dataRow;
       
       for (var i=0; i<resultsArray.results.length; i++) {      
           if (resultsArray.results[i].name === 'ProportionalDifferenceGraphOutput') { 
                for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                     if ( j > 0 ) {
                         data += ',';
                     }
                     dataRow = '{';
                     for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                        if ( z > 0 ) {
                            dataRow += ',';
                        }  
                        if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                            resultsArray.results[i].data[j][z] = '""';
                        }                        
                        if  ( storeFields[z] === "time" ) {
                            dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                        } else {
                            dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                        }
                     }
                     dataRow += ',"zero":0,"open":0,"close":0,"alert":""}';
                     data += dataRow;
                }
           }
       }
       data += ']';
       
       var storeData = JSON.parse(data);
       resultStore.data = storeData;
       // following loops is to notify viewer about unusual record
       // candle graph, used out of the box, requires this artificial operation 
       for (var y=0; y<resultStore.data.length; y++) {
           if ( resultStore.data[y].low < 0 ) {
               resultStore.data[y].open = resultStore.data[y].medium - 0.001;
               resultStore.data[y].close = resultStore.data[y].medium + 0.001;
           } else {
               resultStore.data[y].open = resultStore.data[y].medium + 0.001;
               resultStore.data[y].close = resultStore.data[y].medium - 0.001;     
               resultStore.data[y].alert = 'Out of expected range';               
           }
       }
       return resultStore;        
    },
    buildObservedExpectedChartStore: function(resultsData) {
       var modelFields = '["id","time","obsCount","obsEvent","5","3","4","expCount","expEvent","2","0","1","6","7","8","9"]';  
       var storeFields = JSON.parse(modelFields);             
       var resultStore = {fields: []};     
       
       var resultsArray = JSON.parse(resultsData);
       var data = '[';
       var dataRow;
       
       for (var i=0; i<resultsArray.results.length; i++) {      
           if (resultsArray.results[i].name === 'ObservedExpectedGraphOutput') { 
                for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                     if ( j > 0 ) {
                         data += ',';
                     }
                     dataRow = '{';
                     for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                        if ( z > 0 ) {
                            dataRow += ',';
                        }  
                        if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                            resultsArray.results[i].data[j][z] = '""';
                        }                        
                        if  ( storeFields[z] === "time" ) {
                            dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                        } else {
                            dataRow += '"' + storeFields[z] + '":' + Math.round(1000 * resultsArray.results[i].data[j][z])/10;  
                        }
                     }
                     dataRow += ',"zero":0,"open":0,"close":0,"alert":""}';
                     data += dataRow;
                }
           }
       }
       data += ']';
       
       var storeData = JSON.parse(data);
       resultStore.data = storeData;
       return resultStore;        
    },      
    buildRiskAdjustedSPRTChartStore: function(resultsData) {        
       var resultStore = {fields: []};     
       var resultsArray = JSON.parse(resultsData);
       var data = '[';
       
        for (var i=0; i<resultsArray.results.length; i++) {               
           if (resultsArray.results[i].name === 'RaSprtGraphOutput') {              
                for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                    if ( j > 0 ) {
                        data += ',';
                    }
                    data += '{"RowId":';
                    data += resultsArray.results[i].data[j][0];
                    var lower = resultsArray.results[i].columns[1].metaData.lowerBound;
                    var upper = resultsArray.results[i].columns[1].metaData.upperBound;
                    data += ',"riskAdjustedValue":' + resultsArray.results[i].data[j][1];
                    data += ',"upper":' + upper + ',"lower":' + lower + ',"alert":""}';
                }
           }
       }
       data += ']';
       
       var storeData = JSON.parse(data);
       resultStore.data = storeData;
       return resultStore;        
    },         
    checkIfResultsPresent: function(resultsData, resultType) {
       var resultsArray = JSON.parse(resultsData);     
       for (var i=0; i<resultsArray.results.length; i++) {      
           if (resultsArray.results[i].name === resultType) { 
               return true;
           }
       }
       return false;             
    }
});



