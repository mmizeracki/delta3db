/* 
 * Filter Popup Window
 */

Ext.define('delta3.view.filter.FilterPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.filter',
    requires: ['delta3.view.filter.FilterGrid'],
    layout: 'fit',
    width: 460,
    height: 460,
    itemId: 'filterPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    fieldStore: {},
    modelId: {},
    subTitle: {},
    fieldId: [],
    newFieldId: [],
    isStudyMode: false,
    studyId: {},
    items: [],
    buttons: [{
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#filterPopup')[0];
                if ( thisWin.isStudyMode === true ) {
                    var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    studyGrid.filterStore.load(); 
                }
                thisWin.fieldStore = null;
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore = Ext.create('delta3.store.FieldStore');
        me.fieldStore.load();
        me.fieldStore.filter('idModel', me.modelId);
        me.items[0] = new delta3.view.filter.FilterGrid(
                {fieldStore: me.fieldStore,
                    modelId: me.modelId,
                    title: me.subTitle,
                    isStudyMode: me.isStudyMode,
                    studyId: me.studyId
                });
        me.callParent();
    }
});
