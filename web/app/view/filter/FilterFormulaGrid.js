/* 
 * FilterFormula Grid
 */

Ext.define('delta3.view.filter.FilterFormulaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.filterFormula',
    itemId: 'filterFormulaGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.data.Model',
        'Ext.selection.RowModel',
        'delta3.model.FieldModel',
        'delta3.store.FilterFormulaStore',
        'delta3.view.filter.SqlPopup'
    ],
    border: false,
    columnLines: true,
    hideHeaders: false,
    tbar: [{},
        {
            itemId: 'addFilterFormula',
            text: 'Add Filter Formula',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                if (thisGrid.newFieldId > 0) {
                    var r = thisGrid.fieldStore.findRecord('idModelColumn', thisGrid.newFieldId);
                    r.data.idFilter = thisGrid.filterId;
                    r.data.filteridFilter = thisGrid.filterId;
                    r.data.modelColumnidModelColumn = r.data.idModelColumn;
                    thisGrid.store.insert(0, r);
                    //thisGrid.saveRelationship(thisGrid.filterName, thisGrid.filterId, r.data.idModelColumn);  
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }
        }, {
            itemId: 'filterShowSQL',
            text: 'Show SQL',
            iconCls: 'sql-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                var sql = delta3.utils.GlobalFunc.getFilterWhereClause(thisGrid.store.data.items, thisGrid.fieldStore);
                var win = new delta3.view.filter.SqlPopup({sqlString: sql});
                win.show();                 
            }
        }, {
            itemId: 'verifyFilterSQL',
            text: 'Verify',
            iconCls: 'verify_sql-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                var sql = delta3.utils.GlobalFunc.getFilterWhereClause(thisGrid.store.data.items, thisGrid.fieldStore);
                delta3.utils.GlobalFunc.validateFilter(thisGrid.filterId, thisGrid.filterName, sql, thisGrid.verifyFilterResult);
            }
        }, {
            itemId: 'filterFormulaDelete',
            text: 'Delete',
            iconCls: 'delete-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];                 
                var sm = thisGrid.getSelectionModel();
                var selection = sm.getSelection();
                thisGrid.store.remove(selection);
                thisGrid.store.sync();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No filter formulas found',
            displayInfo: false
        }],
    fieldStore: {},
    filterId: {},
    filterName: {},
    modelId: {},
    newFieldId: 0,
    initComponent: function() {
        var me = this;
        me.columns = me.buildColumns(me);
        me.store = me.buildStore(me.filterName, me.filterId);
        me.plugins = me.buildPlugins();
        me.dockedItems[0].store = me.store;
        var dropDown = new Ext.form.ComboBox({
            store: me.fieldStore,
            itemId: 'gridFormulaComboBox',
            displayField: 'name',
            valueField: 'idModelColumn',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newFieldId = cmb.getValue();
                }
            }
        });
        me.tbar[0] = dropDown;
        me.callParent();
        me.store.load();
    },
    buildColumns: function(me) {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'Field', dataIndex: 'idModelColumn', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModelColumn'];
                    var field = me.fieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},
            {text: 'Kind', dataIndex: 'type', width: 90,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModelColumn'];
                    var field = me.fieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        var value = field.get("fieldKind");
                        switch (value) {
                            case 'Dichotomous':
                                cell.tdCls = 'dicho-icon16';
                                break;
                            case 'Continuous':
                                cell.tdCls = 'cont-icon16';
                                break;
                            case 'Enumerated':
                                cell.tdCls = 'enum-icon16';
                                break;
                            case 'Date':
                                cell.tdCls = 'date-icon16';
                                break;
                        }
                        return value;
                    }
                }},
            {text: 'Condition', dataIndex: 'formula', width: 200, editor: 'textfield'},
            {text: 'Action', dataIndex: 'type', width: 70, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.formulaTypeComboBoxStore,
                            displayField: 'type',
                            valueField: 'type',
                            emptyText: '',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                                }
                            }
                        })
            },
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    buildStore: function(filterName, filterId) {
        return new delta3.store.FilterFormulaStore({filterName: filterName, filterId: filterId});
    },
    buildPlugins: function() {
        return [Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2,
                autoCancel: false,
                itemId: 'filterFormulaGridEditor',
                listeners: {
                    edit: function(rowEditor, changes, r) {
                        var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                        thisGrid.store.sync();
                    }
                }
            })];
    },
    verifyFilterResult: function(response) {
        Ext.MessageBox.hide();
        var responseObject = JSON.parse(response.responseText);
        if (typeof responseObject !== 'undefined') {
            if (responseObject.success === false) {
                Ext.Msg.alert("DELTA3 Error", responseObject.status.message);
            } else {
                Ext.Msg.alert("DELTA3", responseObject.status.message);
            }
        } else {
            Ext.Msg.alert("DELTA3 Error", "Call to verify filter syntax failed");
        }
    },
    saveRelationship: function(filterName, idFilter, idFormula) {
        var outputJSONString = '{"filters":[{"name":"' + filterName
                + '", "idFilter":"' + idFilter
                + '"}], "formulas":[' + JSON.stringify(idFormula)
                + ']}';

        console.log('calling saveFilterFormulaRelationship: ' + filterName);
        Ext.Ajax.request
                (
                        {
                            url: '/Delta3/webresources/study/addFilterFormula',
                            method: "POST",
                            disableCaching: true,
                            params: outputJSONString,
                            success: saveRelationshipReturn,
                            failure: saveRelationshipReturn
                        }
                );

        function saveRelationshipReturn(response, options)
        {
            var returnValue = Ext.decode(response.responseText);
            if (typeof returnValue === 'undefined') {
                message = 'General server error';
            } else {
                message = returnValue.status.message;
            }
            console.log('calling saveStudyCategoryRelationship returned ' + message);
            if (response.status !== 200 || returnValue.success === false) {
                delta3.utils.GlobalVars.MsgBox.hide();
                var thisGrid = Ext.ComponentQuery.query('#filterFormulaGrid')[0];
                thisGrid.store.removeAt(0);
                thisGrid.getView().refresh();
                delta3.utils.GlobalVars.MsgBox.show
                        (
                                {
                                    title: 'DELTA3 Web Services',
                                    message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                    progressText: 'Checking...',
                                    width: 400,
                                    wait: false,
                                    closable: false,
                                    icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                    buttons: delta3.utils.GlobalVars.MsgBox.OK
                                }
                        );
            }
        }
    }
});

