/* 
 * SQL Popup Window
 */

Ext.define('delta3.view.filter.SqlPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.sql',
    layout: 'fit',
    width: 420,
    height: 210,
    itemId: 'sqlPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    sqlString: {},
    items: [{xtype: 'panel',
            frame: true,
            labelWidth: 40,
            labelAlign: 'right',
            bodyStyle: 'padding:5px 5px 0',
            width: 420,
            height: 210,
            title: 'Filter "where" clause',
            autoScroll: true,
            defaultType: 'displayfield',
            items: [{
                    fieldLabel: 'SQL:',
                    name: 'fField',
                    value: ''
                }]
        }],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#sqlPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.items[0].items[0].value = me.sqlString;
        me.callParent();
    }
});
