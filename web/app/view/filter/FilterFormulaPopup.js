/* 
 * Filter Formula Popup Window
 */

Ext.define('delta3.view.filter.FilterFormulaPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.filterFormula',
    requires: ['delta3.view.filter.FilterFormulaGrid'],
    layout: 'fit',
    width: 500,
    height: 460,
    itemId: 'filterFormulaPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3',
    fieldStore: {},
    filterId: {},
    filterName: {},
    modelId: {},
    items: [],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#filterFormulaPopup')[0];
                thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.filterId = me.selectedStudyRecord.get('idFilter');
        me.filterName = me.selectedStudyRecord.get('name');
        me.modelId = me.selectedStudyRecord.get('modelidModel');
        var title = 'Filter "' + me.filterName + '" definition';
        me.fieldStore.filter('idModel', me.modelId);
        me.items[0] = new delta3.view.filter.FilterFormulaGrid(
                {fieldStore: me.fieldStore,
                    title: title,
                    filterName: me.filterName,
                    filterId: me.filterId
                });
        me.callParent();
    }
});
