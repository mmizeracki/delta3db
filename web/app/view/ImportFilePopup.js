/* 
 * Import from file Popup Window
 */

Ext.define('delta3.view.ImportFilePopup', {
    extend: 'Ext.Window',
    requires: ['Ext.form.field.File'], 
    alias: 'widget.popup.importFile',
    layout: 'fit',
    width: 470,
    height: 440,
    itemId: 'importFilePopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3 Import',
    items: [],
    initComponent: function() {
        var me = this;
         me.items[0] = new Ext.form.Panel({
            title: 'Data Model Import',
            width: 450,
            bodyPadding: 10,
            frame: true,
            renderTo: Ext.getBody(),
            items: [{
                xtype: 'filefield',
                name: 'importFile',
                fieldLabel: 'File ',
                labelWidth: 150,
                //labelAlign: 'left',
                msgTarget: 'side',
                allowBlank: false,
                buttonOnly: true,
                //anchor: '100%',
                buttonText: 'Select file...'
            }, {
                xtype: 'checkbox',
                labelWidth: 150,
                labelAlign: 'left',                              
                fieldLabel: 'Process missing',
                inputValue: true,
                value: true,
                itemId: 'processMissingCheckbox'                           
            },{
                xtype: 'textareafield',
                width: 430,
                height: 300,            
                grow: true,
                itemId: 'importDataModelFeedback',
                value: '',
                allowBlank: true
            }],

            buttons: [{
                text: 'Upload',
                handler: function(b) {
                    var form = this.up('form').getForm();
                    if(form.isValid()){
                        form.submit({
                            url: '/Delta3/webresources/admin/importFile',
                            waitMsg: 'Uploading import file...',
                            success: function(f, a) {
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.result.status.message);
                            },
                            failure: function(f,a){
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.response.responseText);
                            }                            
                        });
                    }
                }
            }, {
                text: 'Upload and Create Flat Table',
                handler: function(b) {
                    var form = this.up('form').getForm();
                    if(form.isValid()){
                        var theUrl = '/Delta3/webresources/admin/importModelFileAndProcessAll';
                        if ( Ext.ComponentQuery.query('#processMissingCheckbox')[0].value === false ) {
                            theUrl = '/Delta3/webresources/admin/importModelFileAndProcessNoMissing';
                        }
                        form.submit({
                            url: theUrl,
                            waitMsg: 'Uploading import file...',
                            success: function(f, a) {
                                delta3.utils.GlobalFunc.showDeltaMessage('Your file "' + a.result.file + '" has been uploaded and is processing.');
                            },
                            failure: function(f,a){
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.response.responseText);
                            }                            
                        });                        
                    }
                }
            }, {
                text: 'Close',
                handler: function() {
                    this.up('#importFilePopup').destroy();
                }
            }]
        }); 
        me.callParent();
    }
});


