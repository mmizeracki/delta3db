/* 
 * User Grid
 */

Ext.define('delta3.view.RoleGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.role',
    itemId: 'roleGrid',
    autoScroll: true,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#roleGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    tbar: [{
            text: 'Add Role',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#roleGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.RoleModel.create({
                    name: 'New Role',
                    description: 'new role',
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }, {
            itemId: 'assignPermission',
            text: 'Role Permissions',
            iconCls: 'permissions-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#roleGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                if (typeof thisGrid.getSelectionModel().getSelection()[0] === 'undefined') {
                    return;
                }
                var currRolePermissions = "";          // coma delimited string of existing permissions
                var currPermissionId = new Array();    // array to hold ids of existing permissions
                var idPermission = null;               // array of new selected permission ids         
                var idRole = thisGrid.getSelectionModel().getSelection()[0].data.idRole;
                var name = thisGrid.getSelectionModel().getSelection()[0].data.name;
                delta3.utils.GlobalFunc.getRolePermissionInfo(name, idRole, function(response, options) {
                    console.log('calling getRolePermissionInfo success ' + response.responseText);
                    var responseObject = JSON.parse(response.responseText);
                    if (responseObject.success === true) {
                        for (i = 0; i < responseObject.rolePermissions.permissions.length; i++) {
                            if (i > 0) {
                                currRolePermissions += ", ";
                            }
                            currRolePermissions += responseObject.rolePermissions.permissions[i].name;
                            currPermissionId[i] = responseObject.rolePermissions.permissions[i].idPermissiontemplate;
                        }
                    }
                    var win = new Ext.Window(
                            {
                                layout: 'fit',
                                title: 'DELTA3',
                                width: 480,
                                height: 220,
                                itemId: 'permissionPopup',
                                modal: true,
                                closeAction: 'hide',
                                items: new Ext.Panel(
                                        {
                                            frame: true,
                                            labelWidth: 90,
                                            labelAlign: 'right',
                                            title: 'Select Permissions for the Role',
                                            bodyStyle: 'padding:5px 5px 0',
                                            width: 480,
                                            height: 220,
                                            autoScroll: true,
                                            itemCls: 'form_row',
                                            defaultType: 'displayfield',
                                            buttons: [
                                                {text: 'Save',
                                                    handler: function() {
                                                        //Ext.Msg.confirm('Confirm', 'Do you want to save?');
                                                        if (idRole !== null && name !== null && idPermission !== null) {
                                                            saveRolePermissionRelationship(name, idRole, idPermission, function() {
                                                                console.log('calling saveRoleUserRelationship success ' + response.responseText);
                                                                Ext.ComponentQuery.query('#permissionPopup')[0].destroy();
                                                            });
                                                        }
                                                    }
                                                },
                                                {text: 'Remove',
                                                    handler: function() {
                                                        //Ext.Msg.confirm('Confirm', 'Do you want to remove all Roles for this User?');
                                                        if (idRole !== null && name !== null) {
                                                            removeRolePermissionRelationship(name, idRole, currPermissionId, function() {
                                                                console.log('calling removeRoleUserRelationship success ' + response.responseText);
                                                                Ext.ComponentQuery.query('#permissionPopup')[0].destroy();
                                                            });
                                                        }
                                                    }
                                                },
                                                {text: 'Cancel',
                                                    handler: function() {
                                                        Ext.ComponentQuery.query('#permissionPopup')[0].destroy()
                                                    }
                                                }
                                            ],
                                            items: [{
                                                    fieldLabel: 'Role',
                                                    name: 'name',
                                                    allowBlank: false,
                                                    value: name
                                                },
                                                new Ext.form.ComboBox({
                                                    store: delta3.utils.GlobalVars.PermissionStore, // use pre-loaded
                                                    itemId: 'permissionComboBox',
                                                    fieldLabel: 'Add Permission',
                                                    displayField: 'name',
                                                    valueField: 'idPermissiontemplate',
                                                    queryMode: 'local',
                                                    multiSelect: true,
                                                    forceSelection: true,
                                                    listeners: {
                                                        'select': function(cmb, rec, idx) {
                                                            idPermission = cmb.getValue();
                                                        }
                                                    }
                                                }),
                                                {
                                                    fieldLabel: 'Permissions',
                                                    name: 'permissions',
                                                    allowBlank: true,
                                                    value: currRolePermissions
                                                }
                                            ]
                                        })
                            });
                    win.show();
                });
            }
        }, {
            itemId: 'refreshRole',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#roleGrid')[0]
                thisGrid.store.load();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No roles found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idRole'},
            {text: 'Name', dataIndex: 'name', editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', editor: 'textfield'},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield'},
            {text: 'Type', dataIndex: 'type', width: 100, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.roleTypeComboBoxStore,
                            displayField: 'type',
                            valueField: 'type',
                            emptyText: '',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#roleGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                                }
                            }
                        })
            },
            {text: 'Parent Role ID', dataIndex: 'parentId', editor: 'numberfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        console.log("role grid bilding RoleStore");
        return Ext.create('delta3.store.RoleStore');
    }

});

//------------------------------------------------------------------------------ Supporting functions
function saveRolePermissionRelationship(name, idRole, idPermission, callback)
{
    var rolePermissionJSONString = '{"roles":[{"name":"' + name
            + '", "idRole":"' + idRole
            + '"}], "permissions":' + JSON.stringify(idPermission)
            + '}';

    console.log('calling saveRolePermissionRelationship: ' + name);
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/admin/addRolePermission',
                        method: "POST",
                        disableCaching: true,
                        params: rolePermissionJSONString,
                        success: callback,
                        failure: saveRolePermissionRelationshipFailed
                    }
            );
}

function saveRolePermissionRelationshipFailed(response, options)
{
    console.log('calling saveRolePermissionRelationship failed ' + response.responseText);
}

//------------------------------------------------------------------------------ Supporting functions
function removeRolePermissionRelationship(name, idRole, idPermission, callback)
{
    var rolePermissionJSONString = '{"roles":[{"name":"' + name
            + '", "idRole":"' + idRole
            + '"}], "permissions":' + JSON.stringify(idPermission)
            + '}';

    console.log('calling removeRolePermissionRelationship: ' + name);
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/admin/removeRolePermission',
                        method: "POST",
                        disableCaching: true,
                        params: rolePermissionJSONString,
                        success: callback,
                        failure: removeRolePermissionRelationshipFailed
                    }
            );
}

function removeRolePermissionRelationshipFailed(response, options)
{
    console.log('calling removeRolePermissionRelationship failed ' + response.responseText);
}