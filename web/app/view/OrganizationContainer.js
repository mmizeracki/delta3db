/* 
 * Organization Container
 */

Ext.define('delta3.view.OrganizationContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.organizations',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        console.log("initComponent OrganizationContainer");        
        me.items = {
                xtype:  'grid.organization',
                region: 'center'
        },   
        me.callParent();
    }
});