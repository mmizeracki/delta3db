/* 
 * Permission Grid
 */

Ext.define('delta3.view.PermissionGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.permission',
    itemId: 'permissionGrid',
    autoScroll: true,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    tbar: [{
            text: 'Add Permission',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.PermissionModel.create({
                    name: 'New Permission',
                    description: 'new permission',
                    module: 1,
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No permissions found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idPermissiontemplate'},
            {text: 'Name', dataIndex: 'name', editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', editor: 'textfield'},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield'},
            {text: 'Type', dataIndex: 'type', editor: 'textfield'},
            {text: 'Tag', dataIndex: 'tag', editor: 'textfield'},
            {text: 'Sequence', dataIndex: 'sequence', type: 'int', editor: 'numberfield'},            
            {text: 'Module', dataIndex: 'module', type: 'int', editor: 'numberfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.PermissionStore');
    }

});

