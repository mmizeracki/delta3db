/* 
 * Processes controller
 */

Ext.define('delta3.controller.Processes', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Processes',
    id: 'delta3.controller.Processes',
    requires: [
        'delta3.view.ProcessGrid',
        'delta3.view.ProcessesContainer'],    
    views: [
        'ProcessGrid',
        'ProcessesContainer'
    ]  
});

