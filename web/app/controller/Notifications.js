/* 
 * Notification controller
 */

Ext.define('delta3.controller.Notifications', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Notifications',
    id: 'delta3.controller.Notifications',
    requires: [
        'delta3.view.event.NotificationGrid',
        'delta3.view.event.NotificationContainer',
        'delta3.model.NotificationModel',
        'delta3.store.NotificationStore'        
    ],    
    models: [
        'NotificationModel'
    ],
    stores: [
        'NotificationStore'
    ],    
    views: [
        'NotificationGrid',
        'NotificationContainer'
    ]  
});

