/* 
 * User controller
 */

Ext.define('delta3.controller.Users', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Users',   
    id: 'delta3.controller.Users',
    requires: [
        'delta3.view.UserGrid',
        'delta3.view.UserContainer',
        'delta3.store.UserStore',
        'delta3.store.OrganizationStore',
        'delta3.model.UserModel'],      
    models: [
        'UserModel'
    ],
    stores: [
        'UserStore',
        'OrganizationStore'
    ],
    views: [
        'UserGrid',
        'UserContainer'
    ]
});