/* 
 * Methods (statistical) controller
 */


Ext.define('delta3.controller.Methods', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Methods',       
    id: 'delta3.controller.Methods',
    requires: [
        'delta3.view.MethodGrid',
        'delta3.view.MethodContainer',
        'delta3.model.MethodModel',
        'delta3.store.MethodStore'],           
    models: [
        'MethodModel'
    ],
    stores: [
        'MethodStore'
    ],
    views: [
        'MethodGrid',
        'MethodContainer'
    ]   
});