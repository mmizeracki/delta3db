/* 
 * Eventtemplate controller
 */

Ext.define('delta3.controller.Eventtemplates', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Eventtemplates',
    id: 'delta3.controller.Eventtemplates',
    requires: [
        'delta3.view.event.EventtemplateGrid',
        'delta3.view.event.EventtemplateContainer',
        'delta3.model.EventtemplateModel',
        'delta3.store.EventtemplateStore'        
    ],    
    models: [
        'EventtemplateModel'
    ],
    stores: [
        'EventtemplateStore'
    ],    
    views: [
        'EventtemplateGrid',
        'EventtemplateContainer'
    ]  
});

