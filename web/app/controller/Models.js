/* 
 * Model(as in DELTA) controller
 */

Ext.define('delta3.controller.Models', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Models',    
    id: 'delta3.controller.Models',
    requires: [
        'delta3.view.mb.ModelGrid',
        'delta3.view.mb.ModelContainer',
        'delta3.model.ModelModel',
        'delta3.store.ModelStore'],        
    models: [
        'ModelModel'
    ],
    stores: [
        'ModelStore'
    ],
    views: [
        'ModelGrid',
        'ModelContainer'
    ]  
});
