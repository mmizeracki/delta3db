/* 
 * Configuration controller
 */


Ext.define('delta3.controller.Configurations', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Configurations',       
    id: 'delta3.controller.Configurations',
    requires: [
        'delta3.view.ConfigurationGrid',
        'delta3.view.ConfigurationContainer',
        'delta3.model.ConfigurationModel',
        'delta3.store.ConfigurationStore'],           
    models: [
        'ConfigurationModel'
    ],
    stores: [
        'ConfigurationStore'
    ],
    views: [
        'ConfigurationGrid',
        'ConfigurationContainer'
    ]   
});