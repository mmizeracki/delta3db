/* 
 * Role controller
 */


Ext.define('delta3.controller.Roles', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Roles',       
    id: 'delta3.controller.Roles',
    requires: [
        'delta3.view.RoleGrid',
        'delta3.view.RoleContainer',
        'delta3.model.RoleModel',
        'delta3.store.RoleStore'],          
    models: [
        'RoleModel'
    ],
    stores: [
        'RoleStore'
    ],
    views: [
        'RoleGrid',
        'RoleContainer'
    ]  
});