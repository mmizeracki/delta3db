/* 
 * Table Viewer controller
 */

Ext.define('delta3.controller.TableViewer', {
    extend: 'Ext.app.Controller',
    alias: 'controller.delta3.controller.TableViewer',       
    id: 'delta3.controller.TableViewer',
    models: [
        'TableViewerModel'
    ],
    stores: [
        'TableViewerStore'
    ],
    views: [
        'mb.TableViewerGrid',
        'mb.TableViewerContainer'
    ]  
});