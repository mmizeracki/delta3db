/* 
 * Relationship Store
 */

Ext.define('delta3.store.RelationshipStore',{
	extend      : 'Ext.data.Store',
	alias       : 'store.relationships',
	model		: 'delta3.model.ModelRelationshipModel',
    idModel     : {},
    pageSize    : 15,      
    autoSave    : false,
    listeners   :
        {                
            exception: function (proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Relationship WS call");
            }
        },
	proxy		: {
		type	: 'ajax',
        api     : {
                create: 'webresources/model/createModelRelationships',
                read: 'webresources/model/getModelRelationships',
                update: 'webresources/model/updateModelRelationships',
                destroy: 'webresources/model/removeModelRelationships'
        },
        extraParams: {
            model: this.idModel 
        },                
		reader	: {
            totalProperty: 'totalCount',
            successProperty : "success",    
			type	: 'json',
			rootProperty	: 'relationships'
		},
        writer  : {
            writeAllFields: true
        }
	}
});

