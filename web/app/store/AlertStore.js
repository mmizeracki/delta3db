/* 
 * Alert Store
 */

Ext.define('delta3.store.AlertStore', {
    extend: 'Ext.data.Store',
    alias: 'store.alerts',
    model: 'delta3.model.AlertModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    idModel: {},
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response.success !== 'undefined') {
                        if (response.success === false) {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getAlertStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/event/createAlerts',
            read: 'webresources/event/getAlerts',
            update: 'webresources/event/updateAlerts',
            destroy: 'webresources/event/removeAlerts'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'alerts'
        },
        writer: {
            writeAllFields: true
        }
    }
});

