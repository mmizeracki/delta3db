/* 
 * Field Store similar to Model Field Store, but with no Table Store interactions
 * and for all fields that belong to organization
 */

Ext.define('delta3.store.FieldStore', {
    extend: 'Ext.data.Store',
    requires: [
        'delta3.model.FieldModel',
        'Ext.Msg',
        'Ext.data.*'
    ],
    alias: 'store.fields',
    model: 'delta3.model.FieldModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    autoSave: false,
    listeners:
            {
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getFieldStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            //create: 'webresources/model/createModelColumns',
            read: 'webresources/model/getOrgModelColumns'
            //update: 'webresources/model/updateModelColumns',
            //destroy: 'webresources/model/removeModelColumns'                
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'modelColumns'
        },
        writer: {
            writeAllFields: true
        }
    }
});
