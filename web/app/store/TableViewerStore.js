/* 
 * TableViewer Store
 */


Ext.define('delta3.store.TableViewerStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tableViewer',
    model: 'delta3.model.TableViewerModel',
    pageSize: 25,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (response.success === true) {
                            var me = this;
                            var theGrid = Ext.ComponentQuery.query('#tableViewerGrid')[0];
                            var modelFields = JSON.parse(me.buildGridModel(response.metaData));
                            this.model.addFields(modelFields);
                            var columns = JSON.parse(me.buildGridColumns(response.metaData));
                            this.loadRawData(response); // re-load data since metadata changed
                            theGrid.reconfigure(me, columns);
                        } else {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        }
                    }
                },
                exception:
                        function(proxy, type, action, o, result, records) {
                            delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in generic table viewer WS call");
                        }
            },
    buildGridColumns: function(headers) {
        var hString = '[';
        for (var i = 0; i < headers.length; i++) {
            if (i > 0) {
                hString += ',';
            }
            //       hString += '{"header":"' + headers[i].label + ', ' + headers[i].type + '", "dataIndex":"' + headers[i].label + '"';
            hString += '{"header":"' + headers[i].label + '", "tooltip": "' + headers[i].type + '", "dataIndex":"' + headers[i].label + '"';
            if (headers[i].type.indexOf("INT") !== -1) {
                hString += ', "type":"int"';
            }
            if (headers[i].type.indexOf("LONG") !== -1) {
                hString += ', "type":"int"';
            }
            if (headers[i].type.indexOf("DOUBLE") !== -1) {
                hString += ', "type":"numberfield"';
            }
            if (headers[i].type.indexOf("FLOAT") !== -1) {
                hString += ', "type":"numberfield"';
            }
            if (headers[i].type.indexOf("DATE") !== -1) {
                hString += ', "type":"date"';
            }
            if (headers[i].type.indexOf("TIMESTAMP") !== -1) {
                hString += ', "type":"date"';
            }
            hString += "}"
        }
        return hString += ']';
    },
    buildGridModel: function(headers) {
        var hString = '[';
        for (var i = 0; i < headers.length; i++) {
            if (i > 0) {
                hString += ',';
            }
            hString += '{"name":"' + headers[i].label + '"';
            if (headers[i].type.indexOf("CHAR") !== -1) {
                //hString += ', "type":"string"';
            }
            if (headers[i].type.indexOf("INT") !== -1) {
                hString += ', "type":"int"';
            }
            if (headers[i].type.indexOf("LONG") !== -1) {
                hString += ', "type":"int"';
            }
            if (headers[i].type.indexOf("DOUBLE") !== -1) {
                hString += ', "type":"number"';
            }
            if (headers[i].type.indexOf("FLOAT") !== -1) {
                hString += ', "type":"number"';
            }
            if (headers[i].type.indexOf("DATE") !== -1) {
                hString += ', "type":"date", "dateFormat":"Y-m-d"';
            }
            if (headers[i].type.indexOf("TIMESTAMP") !== -1) {
                hString += ', "type":"date", "dateFormat":"Y-m-d H:i:s.u"';
            }
            hString += "}"
        }
        return hString += ']';
    },
    proxy: {
        type: 'ajax',
        extraParams: {modelId: '0', orderBy: '', direction: '', filterId: '0'},
        api: {
            read: 'webresources/model/getDBObject'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'content'
        },
        writer: {
            writeAllFields: true
        }
    }
});





