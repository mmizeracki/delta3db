/* 
 * Role Store
 */

Ext.define('delta3.store.RoleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.roles',
    model: 'delta3.model.RoleModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.getResponseData();
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.getResponseData();
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getRoles WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createRoles',
            read: 'webresources/admin/getRoles',
            update: 'webresources/admin/updateRoles',
            destroy: 'webresources/admin/deleteRoles'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'roles'
        },
        writer: {
            writeAllFields: true
        }
    }
});



