/* 
 * Method Store
 */

Ext.define('delta3.store.MethodStore', {
    extend: 'Ext.data.Store',
    alias: 'store.method',
    model: 'delta3.model.MethodModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response.success !== 'undefined') {
                        if (response.success === false) {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        } 
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getMethodStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createMethods',
            read: 'webresources/admin/getMethods',
            update: 'webresources/admin/updateMethods'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'methods'
        },
        writer: {
            writeAllFields: true
        }
    }
});

