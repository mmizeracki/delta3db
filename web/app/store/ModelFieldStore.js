/* 
 * Model Field Store similar to Field Store, but only for fields associated with one Model
 */

Ext.define('delta3.store.ModelFieldStore', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.Msg',
        'Ext.data.*'
    ],
    alias: 'store.modelFields',
    model: 'delta3.model.FieldModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};
                },
                load: function(store, record, options) {
                    ModelData.tableStore.loadRawData(store.proxy.reader.rawData);
                },
                /*update: function(store, operation, options) {
                    delta3.utils.GlobalFunc.doUpdateModelDropdowns(); 
                },*/
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getModelFieldStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/model/createModelColumns',
            read: 'webresources/model/getModelDetails',
            update: 'webresources/model/updateModelColumns',
            destroy: 'webresources/model/removeModelColumns'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'modelColumns'
        },
        writer: {
            writeAllFields: true
        }
    }
});
