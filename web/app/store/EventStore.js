/* 
 * Event Store
 */

Ext.define('delta3.store.EventStore', {
    extend: 'Ext.data.Store',
    alias: 'store.events',
    model: 'delta3.model.EventModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    idModel: {},
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response.success !== 'undefined') {
                        if (response.success === false) {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        } 
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getEventStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/event/createEvents',
            read: 'webresources/event/getEvents',
            update: 'webresources/event/updateEvents',
            destroy: 'webresources/event/removeEvents'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'events'
        },
        writer: {
            writeAllFields: true
        }
    }
});

