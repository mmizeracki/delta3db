/* 
 * Logistic Regression Field Store
 */


Ext.define('delta3.store.LogregrFieldStore', {
    extend: 'Ext.data.Store',
    alias: 'store.logregrfield',
    model: 'delta3.model.LogregrFieldModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    logregrName: {},
    logregrId: {},
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {logregr: '{"logregrs":[{"name":"' + this.logregrName
                                + '", "idLogregr":"' + JSON.stringify(this.logregrId) + '"}]}'};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === true) {
                                // STEP2: update all local logregrFields recods with values received from logregrDates                                
                                var logregrFieldGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];   
                                var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                                var columnSize = this.getColumnSize(dateStore);                                
                                for ( var i=0; i<response.logregrfields.length; i++ ) {
                                    logregrFieldGrid.height += 10;
                                    var z=0;
                                    for (var j=0; j<dateStore.data.length; j++, z++) {
                                        if ( z === columnSize ) {
                                            z=0; // reset column counter
                                        }                                        
                                        var rec = dateStore.getAt(j);
                                        if ( response.logregrfields[i].idLogregrField === rec.get('idLogregrField') ) {
                                            var recF = logregrFieldGrid.store.getAt(i);
                                            var newValue = rec.get('value'); 
                                            recF.set('value' + z, newValue);                                        
                                        }
                                    }
                                }
                                logregrFieldGrid.getView().refresh();    
                                var regrFieldPopup = Ext.ComponentQuery.query('#logregrFieldPopup')[0]; 
                                regrFieldPopup.doLayout();                                
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if ( response.success === false ) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);                               
                            } else { 
                                if (typeof response.logregrfields[0].logregrdates !== 'undefined') {  
                                    var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                                    for (var j = 0; j < response.logregrfields.length; j++) {
                                        for (var i = 0; i < response.logregrfields[j].logregrdates.length; i++) {
                                            if ( dateStore.findRecord("idLogregrDate", response.logregrfields[j].logregrdates[i].idLogregrDate) === null ) {
                                                var r = delta3.model.LogregrDateModel.create({
                                                    active: response.logregrfields[j].logregrdates[i].active,
                                                    name: response.logregrfields[j].logregrdates[i].name,
                                                    value: response.logregrfields[j].logregrdates[i].value,
                                                    idLogregr: response.logregrfields[j].logregrdates[i].idLogregr,
                                                    idLogregrField: response.logregrfields[j].logregrdates[i].idLogregrField,
                                                    idLogregrDate: response.logregrfields[j].logregrdates[i].idLogregrDate,                                            
                                                    modelColumnidSequencer: response.logregrfields[j].logregrdates[i].modelColumnidSequencer,
                                                    createdTS: response.logregrfields[j].logregrdates[i].createdTS,
                                                    updatedTS: response.logregrfields[j].logregrdates[i].updatedTS,
                                                    createdBy: response.logregrfields[j].logregrdates[i].createdBy,
                                                    updatedBy: response.logregrfields[j].logregrdates[i].updatedBy                                            
                                                });                                                
                                                dateStore.insert(dateStore.getCount(), r);
                                            }
                                        }   
                                    }
                                }
                            }  
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in LogregrField WS call");
                }
            },
    getColumnSize: function(dateStore) {
        // count occurance of the first LogregrField
        if ( (dateStore === null) || (dateStore.getAt(0) === null) ) return 0;
        var theId = dateStore.getAt(0).get("idLogregrField");
        var j = 0;
        for (var i=0; i<dateStore.data.length; i++) {
            if ( theId === dateStore.getAt(i).get("idLogregrField") ) {
                j++;
            }
        }
        return j;
    },      
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createLogregrFields',
            read: 'webresources/study/getLogregrFields',
            update: 'webresources/study/updateLogregrFields',
            destroy: 'webresources/study/removeLogregrFields'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'logregrfields'
        },
        writer: {
            writeAllFields: true
        }
    }
});


