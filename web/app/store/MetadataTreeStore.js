/* 
 * Metadata Tree Store
 */

Ext.define('delta3.store.MetadataTreeStore', {
    extend: 'Ext.data.TreeStore',
    requires: [
        'Ext.Msg',
        'Ext.data.*'
    ],
    alias: 'store.metadata',
    itemId: 'MetadataTreeStore',
    autoSync: false,
    autoLoad: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } else {
                                // load different icons for views and leaf tips with column "type"
                                var treeRoot = store.root;
                                treeRoot.set('iconCls', 'db-icon16');
                                var thePanel = Ext.ComponentQuery.query('#metadataTree')[0];
                                treeRoot.set('text', thePanel.dataSource);
                                for (i = 0; i < treeRoot.childNodes.length; i++) {
                                    if (treeRoot.childNodes[i].get('qtip') === 'view') {
                                        treeRoot.childNodes[i].set('iconCls', 'db_view-icon16');
                                    } else {
                                        treeRoot.childNodes[i].set('iconCls', 'db_table-icon16');
                                    }
                                    for (j = 0; j < treeRoot.childNodes[i].childNodes.length; j++) {
                                        treeRoot.childNodes[i].childNodes[j].set('qtip', treeRoot.childNodes[i].data.children[j].type);
                                    }
                                }
                            }
                        }
                    }
                    ModelData.fieldGrid.store.load();
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getMetadateTreeStore WS call");
                }
            },
    root: {text: 'Database',
        expanded: true
    },
    proxy:
            {
                type: 'ajax',
                api: {
                    create: 'webresources/model/createModels',
                    read: 'webresources/model/getMetadata',
                    update: 'webresources/model/updateModels'
                },
                reader: {
                    totalProperty: 'totalCount',
                    successProperty: "success",
                    type: 'json',
                    rootProperty: 'children'
                },
                writer: {
                    writeAllFields: true
                }
            }
});
