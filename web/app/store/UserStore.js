/* 
 * User Store
 */

Ext.define('delta3.store.UserStore', {
    extend: 'Ext.data.Store',
    alias: 'store.users',
    model: 'delta3.model.UserModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getUsers WS call");
                }
            },
    sorters: { property: 'alias', direction : 'ASC' },                     
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createUsers',
            read: 'webresources/admin/getUsers',
            update: 'webresources/admin/updateUsers',
            destroy: 'webresources/admin/deleteUsers'
        },
        reader: {
            type: 'json',
            totalProperty: 'totalCount',
            successProperty: 'success',
            rootProperty: 'users'
        },
        writer: {
            writeAllFields: true
        }
    }
});

