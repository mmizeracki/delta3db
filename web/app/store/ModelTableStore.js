/* 
 * Model Table Store
 * Coping Systems, Inc.
 */

Ext.define('delta3.store.ModelTableStore', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.Msg',
        'Ext.data.*'
    ],
    alias: 'store.modelTables',
    model: 'delta3.model.ModelTableModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    autoSave: false,
    setPrimary: function(primaryTableName) {
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            if ( record.get('idModelTable') === this.tableToIdMap[primaryTableName] ) {
                record.set('primaryTable', true);
                ModelData.relationshipTreeStore.getRootNode().getChildAt(i).set('qtip', 'primary table');
            } else {
                record.set('primaryTable', false);
                ModelData.relationshipTreeStore.getRootNode().getChildAt(i).set('qtip', '');                
            }
            record.dirty = true; // refresh all of the records
        }
        this.sync();
    },   
    tableToIdMap: {},
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};
                },
                beforesync: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};
                },
                datachanged: function(store, record, options) {
                    if ( store.data.updating === 0 ) {
                        var response = store.proxy.reader.rawData;
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } else {
                                delta3.utils.GlobalFunc.doUpdateTableTree();                         
                                var fieldGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                                fieldGrid.getView().refresh();
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getTableStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/model/createModelTables',
            read: 'webresources/model/getModelDetails',
            update: 'webresources/model/updateModelTables',
            destroy: 'webresources/model/removeModelTables'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'modelTables'
        },
        writer: {
            writeAllFields: true
        }
    }
});

 //# sourceURL=http://localhost:8080/Delta3/app/store/ModelTableStore.js