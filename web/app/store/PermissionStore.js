/* 
 * Permission Store
 */

Ext.define('delta3.store.PermissionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.permissions',
    model: 'delta3.model.PermissionModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getPermissions WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createPermissiontemplates',
            read: 'webresources/admin/getPermissiontemplates',
            update: 'webresources/admin/updatePermissiontemplates',
            destroy: 'webresources/admin/deletePermissiontemplates'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'permissiontemplates'
        },
        writer: {
            writeAllFields: true
        }
    }
});


