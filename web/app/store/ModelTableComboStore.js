/* 
 * Model Table for Combo lookup Store
 * Coping Systems, Inc.
 */

Ext.define('delta3.store.ModelTableComboStore',{
	extend      : 'Ext.data.Store',
    requires    : [
        'Ext.data.*'
    ],    
	alias       : 'store.modelTableCombo',
	model		: 'delta3.model.ModelTableModel'
});