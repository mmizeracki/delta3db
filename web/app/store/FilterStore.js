/* 
 * Filter Store
 */

Ext.define('delta3.store.FilterStore', {
    extend: 'Ext.data.Store',
    alias: 'store.filters',
    model: 'delta3.model.FilterModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    autoSave: false,
    idModel: {},
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{"models":[{"idModel":"' + JSON.stringify(this.idModel) + '"}]}'};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response.success !== 'undefined') {
                        if (response.success === false) {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        } else {
                            var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0]; 
                            if ( typeof studyGrid === 'undefined' ) {
                                return;
                            }
                            studyGrid.getView().refresh();    
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getFilterStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createModelFilters',
            read: 'webresources/study/getModelFilters',
            update: 'webresources/study/updateModelFilters',
            destroy: 'webresources/study/removeModelFilters'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'filters'
        },
        writer: {
            writeAllFields: true
        }
    }
});

