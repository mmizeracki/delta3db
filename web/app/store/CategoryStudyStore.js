/* 
 * CategoryStudy Store
 */

Ext.define('delta3.store.CategoryStudyStore', {
    extend: 'Ext.data.Store',
    alias: 'store.categoryStudy',
    itemId: 'studyCategoryStore',
    model: 'delta3.model.FieldModel',
    idStudy: {},
    studyName: {},
    pageSize: 5,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {study: '{"studies":[{"name":"' + this.studyName
                                + '", "idStudy":"' + JSON.stringify(this.idStudy) + '"}]}'};
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Relationship WS call");
                }
            },
    proxy: {
        type: 'ajax',
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: 'webresources/study/addStudyCategory',
            read: 'webresources/study/getStudyCategory',
            destroy: 'webresources/study/removeStudyCategory'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'studyCategories.categories'
        },
        writer: {
            writeAllFields: true
        }
    }
});

