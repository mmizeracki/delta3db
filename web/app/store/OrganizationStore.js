/* 
 * Organization Store
 */


Ext.define('delta3.store.OrganizationStore', {
    extend: 'Ext.data.Store',
    alias: 'store.organizations',
    model: 'delta3.model.OrganizationModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getOrganizations WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createOrganizations',
            read: 'webresources/admin/getOrganizations',
            update: 'webresources/admin/updateOrganizations',
            destroy: 'webresources/admin/deleteOrganizations'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'organizations'
        },
        writer: {
            writeAllFields: true
        }
    }
});


