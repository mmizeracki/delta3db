/* 
 * FilterFormula Store
 */

Ext.define('delta3.store.FilterFormulaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.filterFormula',
    itemId: 'filterFormulaStore',
    model: 'delta3.model.FilterFormulaModel',
    filterId: {},
    filterName: {},
    pageSize: 5,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {filter: '{"filters":[{"name":"' + this.filterName
                                + '", "idFilter":"' + JSON.stringify(this.filterId) + '"}]}'};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } else {
                                if (typeof response.filterFormulas !== 'undefined') {
                                    for (var i = 0; i < response.filterFormulas.formulas.length; i++) {
                                        // pull out ids from compound PK
                                        var record = store.getAt(i);
                                        record.set('idFilter', response.filterFormulas.formulas[i].filteridFilter);
                                        record.set('idModelColumn', response.filterFormulas.formulas[i].modelColumnidModelColumn);
                                    }
                                    store.commitChanges();
                                }
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response.success !== 'undefined') {
                        if (response.success === false) {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Filter Formula Relationship WS call");
                }
            },
    proxy: {
        type: 'ajax',
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            //create: 'webresources/study/addFilterFormula2Category',
            read: 'webresources/study/getFilterFormula',
            update: 'webresources/study/updateFilterFormula',
            destroy: 'webresources/study/removeFilterFormula'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            rootProperty: 'filterFormulas.formulas'
        },
        writer: {
            writeAllFields: true
        }
    }
});

