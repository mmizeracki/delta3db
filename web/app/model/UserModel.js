/* 
 * Users Model
 */

Ext.define('delta3.model.UserModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idUser', type: 'int'},
        'alias', 
        {name: 'active', type: 'boolean'}, 
        'type',
        'guid',
        {name: 'passExpirationTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
        {name: 'passChangedTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
        {name: 'passFailureMax', type: 'int'},
        {name: 'passFailureCount', type: 'int'},      
        'password',
        'securityHandler',
        'status',
        'locale',
        'firstName',
        'lastName',
        'emailAddress1',
        'emailAddress2',
        'uri1',
        'uri2',
        'phoneNumber1',
        'phoneNumber2',
        'alertPreference1',
        'alertPreference2',
        {name: 'createdBy', type: 'int'},
        {name: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},        
        {name: 'updatedBy', type: 'int'},
        {name: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
        {name:'groupTable', type: 'int'},
        {name:'organization', type: 'int'},
        {name:'person', type: 'int'}        
    ]
});

