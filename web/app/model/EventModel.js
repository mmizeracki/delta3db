/* 
 * Event Model
 */

Ext.define('delta3.model.EventModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idEvent', type: 'int'},
        'description',
        {name: 'idSubscriber', type: 'int'},        
        {name: 'active', type: 'boolean'},   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idOrganization', type: 'int'},
        {name: 'idEventTemplate', type: 'int'}
    ]
});



