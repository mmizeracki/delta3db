/* 
 * Alert Model
 */

Ext.define('delta3.model.AlertModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idAlert', type: 'int'},
        'name',
        'description',
        'type',        
        {name: 'retryMax', type: 'int'},        
        {name: 'timeOut', type: 'int'},
        {name: 'ackRequired', type: 'boolean'},           
        {name: 'idUser', type: 'int'},        
        {name: 'sendEmail', type: 'boolean'},           
        {name: 'active', type: 'boolean'},   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idEventTemplate', type: 'int'}
    ]
});



