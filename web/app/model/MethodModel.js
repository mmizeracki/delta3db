/* 
 * Method Model
 */

Ext.define('delta3.model.MethodModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idMethod', type: 'int'},
        'name',
        'description',
        {name: 'active', type: 'boolean'}, 
        'methodParams',
        {name: 'idOrganization', type: 'int'},
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


