/* 
 * Eventtemplate Model
 */

Ext.define('delta3.model.EventtemplateModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idEventTemplate', type: 'int'},
        'name',
        'description',
        'type',
        {name: 'active', type: 'boolean'},   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idModule', type: 'int'}
    ]
});



