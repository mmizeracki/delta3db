/* 
 * Model(as in DELTA) Model
 */

Ext.define('delta3.model.ModelModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idModel', type: 'int'},
        'name',
        'description',
        'type',
        {name: 'active', type: 'boolean'}, 
        {name: 'isAtomicFinished', type: 'boolean'}, 
        {name: 'isMissingFinished', type: 'boolean'}, 
        {name: 'isVirtualFinished', type: 'boolean'},         
        'idConfiguration',
        'connectionInfo',
        'dbUser',
        'dbPassword',
        'outputName',     
        'status',
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


