/* 
 * Logistic Regression Date/Value Model
 */

Ext.define('delta3.model.LogregrDateModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idLogregrDate', type: 'int' },
                'name',
                'value',
                {name: 'modelColumnidSequencer', type: 'int' },       
                {name: 'idLogregr', type: 'int' }, 
                {name: 'idLogregrField', type: 'int', sortType: Ext.data.SortTypes.asInt },                  
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
