/* 
 * FilterFormula Model
 */

Ext.define('delta3.model.FilterFormulaModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idFilter', type: 'int'},
        {name: 'idModelColumn', type: 'int'},
        'name',
        'formula',
        'operator',
        'type',
        'negator',
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ],
    hasMany: [
            {
                model: 'delta3.model.Filter',
                associationKey: 'filter',
                name: 'filters'
            },
            {
                model: 'delta3.model.Field',
                associationKey: 'modelColumns',
                name: 'modelColumn'
            }
        ]
});


