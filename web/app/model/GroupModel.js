/* 
 * Group Model
 */

Ext.define('delta3.model.GroupModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idGroup', type: 'int' },
                'name',
                'description',
                'type',
                {name: 'active', type: 'boolean'}, 
                {name: 'parentId', type: 'int' },
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
    //belongsTo: 'Ext.app.models.UserModel'
});




