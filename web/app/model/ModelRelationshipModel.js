/* 
 * Model(as in DELTA)Relationship Model
 */

Ext.define('delta3.model.ModelRelationshipModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idModelRelationship', type: 'int'},
        {name: 'table1', type: 'int'},       
        {name: 'key1', type: 'int'},
        {name: 'table2', type: 'int'},       
        {name: 'key2', type: 'int'},    
        {name: 'type', type: 'int'}, 
        {name: 'modelidModel', type: 'int'},                     
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});
