/* 
 * Locatioation Model
 */

Ext.define('delta3.model.LocationModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idLocation', type: 'int' },
                'guid',
                'name',
                'description',
                'address1',
                'address2',
                'country',
                'county',
                'county',
                'state',
                'zip',
                {name: 'active', type: 'boolean'}, 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ],
    belongsTo: 'Ext.app.models.PersonModel'
});