/* 
 * Permission Model
 */

Ext.define('delta3.model.PermissionModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idPermissiontemplate', type: 'int'},
        'name',
        'description',
        'type',
        'tag', 
        {name: 'sequence', type: 'int'},
        {name: 'module', type: 'int'},        
        {name: 'active', type: 'boolean'},         
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});



