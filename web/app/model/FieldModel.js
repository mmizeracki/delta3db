/* 
 * Field Model
 */

Ext.define('delta3.model.FieldModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idModelColumn', type: 'int'},
        'name',
        'description',
        'physicalName',
        'type',
        {name: 'active', type: 'boolean'}, 
        {name: 'atomic', type: 'boolean'},      
        {name: 'keyField', type: 'boolean'}, 
        {name: 'insertable', type: 'boolean'},      
        {name: 'virtual', type: 'boolean'},  
        {name: 'sub', type: 'boolean'},         
        'formula',
        'fieldClass',
        'fieldKind',
        {name: 'verified', type: 'boolean'},          
        {name: 'modelTableidModelTable', type: 'int'},     
        {name: 'idModel', type: 'int'},           
        {name: 'tableName'},             
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});



