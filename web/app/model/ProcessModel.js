/* 
 * Process Model
 */

Ext.define('delta3.model.ProcessModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idProcess', type: 'int' },
                {name: 'idOrganization', type: 'int' },
                {name: 'idModel', type: 'int' },                
                'name',
                'description',
                'data',
                'status',
                'message',
                'guid',
                {name: 'progress', type: 'int' },                
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
