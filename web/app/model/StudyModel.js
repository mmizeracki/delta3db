/* 
 * Study Model
 */

Ext.define('delta3.model.StudyModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idStudy', type: 'int' },
                'name',
                'description',
                'status',
                {name: 'idOrganization', type: 'int' },
                {name: 'idModel', type: 'int' },       
                {name: 'idOutcome', type: 'int' },
                {name: 'idTreatment', type: 'int' },
                {name: 'idMethod', type: 'int' },   
                {name: 'idKey', type: 'int' },     
                {name: 'idDate', type: 'int' },                   
                {name: 'idFilter', type: 'int' },                       
                'methodParams',
                {name: 'idAlert', type: 'int' },  
                'confidenceInterval',
                'confidenceInterval2',
                {name: 'isMissingDataUsed', type: 'boolean'},    
                {name: 'isChunking', type: 'boolean'},   
                'chunkingBy',
                {name: 'isGenericId', type: 'boolean'},                  
                {name: 'originalStudyId', type: 'int' },      
                {name: 'startTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'endTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'}, 
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
