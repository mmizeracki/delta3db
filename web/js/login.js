Ext.require([
    'Ext.form.Panel',
    'Ext.state.CookieProvider',
    'Ext.layout.container.Anchor'
    ]);

Ext.define('deltaHelp', {
    extend: 'Ext.Window',
    layout: 'fit',
    width: 460,
    height: 460,
    itemId: 'helpPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA3 Help',
    html: 'DELTA version ' + deltaVersion + ' help goes here....',
    buttons: [{
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#helpPopup')[0];
                thisWin.destroy();
            }
        }]
});

Ext.onReady(function() {
    var MsgBox = Ext.MessageBox;
    var cp = new Ext.state.CookieProvider({
        path: "/Delta3/",
        expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 30)), //30 days
        domain: "boston2a.com"
    });
    Ext.state.Manager.setProvider(cp);
    
    Ext.create('Ext.form.Panel', {
        id: 'loginPanel',
        renderTo: 'loginForm',
        xtype: 'form-login',
        standardSubmit: false, 
        title: 'DELTA3 Login',
        bodyStyle: {
            padding: '30px 30px 30px',
        },       
        cls: 'login_style',
        width: 300,
        frame: false,
        header: true,
        margin: '10 0 0 100',     
        fieldDefaults: {
            labelAlign: 'left',
            msgTarget: 'side'
        },
        defaults: {
            border: false,
            xtype: 'panel',
            flex: 1,
            layout: 'anchor'
        },
        tools: [
            {
                type:'help',
                tooltip: 'Get Help',
                callback: function(panel, tool, event) {
                    var win = new deltaHelp();
                    win.show();                    
                    console.log("show some help here");
                }
            }
        ],
        layout: 'anchor',
        items: [{
            xtype:'textfield',
            name: 'UID',
            id: 'UID',
            fieldLabel: 'User Name',
            anchor: '100%'
        }, {
            xtype:'textfield',
            name: 'PWD',
            id: 'PWD',
            fieldLabel: 'Password',
            inputType: 'password',
            anchor: '100%'
        }],
        buttons: [{
            text: 'Login',    
            id: 'loginButton',              
            handler: function() {
                var userAlias = Ext.getCmp('UID');
                var password = Ext.getCmp('PWD');
                Ext.Ajax.request
                        (
                                {
                                    url: '/Delta3/webresources/admin/login',
                                    method: "GET",
                                    disableCaching: true,
                                    params: {Username: userAlias.getValue(), Password: password.getValue()},
                                    success: loginSuccess,
                                    failure: loginFailed
                                }
                        );
            }
        }, {
            text: 'Close',
            handler: function() {
                Ext.getCmp('loginPanel').close();
                window.location = 'http://www.boston2a.com';
            }
        }]
    });
    
    Ext.getCmp('UID').focus('', 10);  

    var map = new Ext.util.KeyMap('loginPanel',{                
        key: [10, 13], 
        fn: function(){ Ext.getCmp('loginButton').handler() }
    });
    
    function loginSuccess(response, options)
    {
        var resp = JSON.parse(response.responseText);
        if(typeof resp.success !== 'undefined') {     
            if ( resp.success === "false" ) {
                MsgBox.hide();
                MsgBox.show
                    (
                        {
                            title: 'DELTA3',
                            msg: '<b style="color:#cc0000;">' + resp.status.message 
                                + '</b><br/><br/><div style="padding-left:48px;"> Please enter the credentials and try again<br/><br/>',
                            progressText: 'Checking...',
                            width: 400,
                            wait: false,
                            closable: false,
                            icon: MsgBox.ERROR,
                            buttons: MsgBox.OK
                        }
                    );
            } else {
                setTimeout(gotoMain, 100);               
            }                  
        }         
    }

    function gotoMain()
    {
        MsgBox.hide();
        window.location = './mainLanding.html';
    }

    function loginFailed(response, options)
    {
        MsgBox.hide();
        MsgBox.show
            (
                {
                    title: 'DELTA3',
                    msg: '<b style="color:#cc0000;">Access denied!</b> Please enter the credentials and try again<br/><br/>',
                    progressText: 'Checking...',
                    width: 400,
                    wait: false,
                    closable: false,
                    icon: MsgBox.ERROR,
                    buttons: MsgBox.OK
                }
            );
    }    
});  
